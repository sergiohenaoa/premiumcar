/**
 * Metodos.java:
 * 
 * 	Esta clase contiene diferentes métodos que serán utilizadas en 
 * 	la aplicación.
 *
 * @author Sergio Henao, 06/03/2014
 * @Modifier Sergio Henao, 06/03/2014
 * @version 1.0
 * 
 */
package com.sh.pvs.utils;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

public class Metodos {

	/**
	 * Método para registrar acciones en el log info
	 * @param nomProceso
	 * @param nomClase
	 * @param nomMetodo
	 * @param numLinea
	 * @param accion
	 * @param mensaje
	 */
	public static void infoLog(Thread hilo, String mensaje){
		
		Logger log = Logger.getLogger(Constantes.INFOLOG);	
		String fullClassName = Thread.currentThread().getStackTrace()[2].getClassName();            
        String methodName = Thread.currentThread().getStackTrace()[2].getMethodName();
        int lineNumber = Thread.currentThread().getStackTrace()[2].getLineNumber();
	    log.info(Metodos.formatLogMessage(fullClassName, methodName, lineNumber+"", mensaje));
	    
	}
	
	/**
	 * Método para registrar acciones en el log warn
	 * @param nomProceso
	 * @param nomClase
	 * @param nomMetodo
	 * @param numLinea
	 * @param accion
	 * @param mensaje
	 */
	public static void warnLog(Thread hilo, String mensaje){
		
		Logger log = Logger.getLogger(Constantes.WARNLOG);	
		String fullClassName = Thread.currentThread().getStackTrace()[2].getClassName();            
        String methodName = Thread.currentThread().getStackTrace()[2].getMethodName();
        int lineNumber = Thread.currentThread().getStackTrace()[2].getLineNumber();
	    log.warn(Metodos.formatLogMessage(fullClassName, methodName, lineNumber+"", mensaje));
	    
	}
	
	/**
	 * Método para registrar acciones en el log error
	 * @param nomProceso
	 * @param nomClase
	 * @param nomMetodo
	 * @param numLinea
	 * @param accion
	 * @param mensaje
	 */
	public static void errorLog(Thread hilo, String mensaje){
		
		Logger log = Logger.getLogger(Constantes.ERRORLOG);		
		String fullClassName = Thread.currentThread().getStackTrace()[2].getClassName();            
        String methodName = Thread.currentThread().getStackTrace()[2].getMethodName();
        int lineNumber = Thread.currentThread().getStackTrace()[2].getLineNumber();
	    log.error(Metodos.formatLogMessage(fullClassName, methodName, lineNumber+"", mensaje));
	    
	}
	
	/**
	 * Método para formatear el mensaje que se publicará en el log
	 * 
	 * @param nomProceso
	 * @param nomClase
	 * @param nomMetodo
	 * @param numLinea
	 * @param accion
	 * @param mensaje
	 * @return
	 */
	public static String formatLogMessage(String nomClase, String nomMetodo, String numLinea, String mensaje){
		
		String DATE_FORMAT_NOW = "yyyy-MM-dd HH:mm:ss";
		Calendar cal = Calendar.getInstance();
	    SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
		
	    String msg = "(CBAS / PRI)["+sdf.format(cal.getTime())+"] | "
	    	+ "<Clase		: " + nomClase + "> | "
	    	+ "<Método		: " + nomMetodo + "> | "
			+ "<Número Línea: " + numLinea + "> | "
			+ "<Mensaje		: " + mensaje + ">";
	    
	    return msg;
	}
	
	/**
	 * Método para codificar archivos 
	 * @throws IOException 
	 */
	public static String encodeFile(InputStream is, long length) throws IOException{
		byte[] bytes = loadFile(is,length);
		byte[] encoded = Base64.encodeBase64(bytes);
		return new String(encoded);
	}
	
	public static byte[] decodeFile(String fileString) throws IOException{
		return Base64.decodeBase64(fileString);
	}
	
	private static byte[] loadFile(InputStream is, long length) throws IOException {
	    if (length > Integer.MAX_VALUE) {
	        // File is too large
	    }
	    
	    byte[] bytes = new byte[(int)length];
	    
	    int offset = 0;
	    int numRead = 0;
	    while (offset < bytes.length
	           && (numRead=is.read(bytes, offset, bytes.length-offset)) >= 0) {
	        offset += numRead;
	    }
 
	    if (offset < bytes.length) {
	        throw new IOException("Could not completely read file ");
	    }
 
	    is.close();
	    return bytes;
	}

}