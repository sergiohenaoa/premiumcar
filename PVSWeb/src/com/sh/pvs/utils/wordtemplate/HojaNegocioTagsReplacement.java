package com.sh.pvs.utils.wordtemplate;

import com.sh.pvs.core.utils.Constantes;
import com.sh.pvs.model.dto.configuracion.Cliente;
import com.sh.pvs.model.dto.configuracion.Concesionario;
import com.sh.pvs.model.dto.configuracion.Vehiculo;
import com.sh.pvs.model.dto.hojanegocio.GastoxHojaNegocio;
import com.sh.pvs.model.dto.hojanegocio.HojaNegocio;
import com.sh.pvs.model.dto.hojanegocio.PagoxHojaNegocio;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.usermodel.Range;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


public class HojaNegocioTagsReplacement {

	private HWPFDocument word;
	private HojaNegocio hojaNegocio;
	private SimpleDateFormat dateFormat;
	private NumberFormat numberFormat;
	private Boolean esComprador;
	
	public HojaNegocioTagsReplacement(HWPFDocument word, HojaNegocio hojaNegocio, Boolean esComprador) {
		super();
		this.word = word;
		this.hojaNegocio = hojaNegocio;
		this.dateFormat = new SimpleDateFormat("dd/MM/yyyy");		
		this.numberFormat = NumberFormat.getNumberInstance();
		this.esComprador = esComprador;

	}
	
	public void replaceTags(){
		
		Range range = word.getRange();
		
		try{
			Vehiculo vehiculo = new Vehiculo();
			Integer kilometraje = 0;
			Date fchVenRT = null;
			Date fchVenSoat = null;
			
			if(hojaNegocio.getFromConsignacion()){
				vehiculo = hojaNegocio.getConsignacion().getVehiculo();
				kilometraje = hojaNegocio.getConsignacion().getKilometraje();
				if(hojaNegocio.getConsignacion().getFchVenceRevtm() != null)
					fchVenRT = hojaNegocio.getConsignacion().getFchVenceRevtm();
				if(hojaNegocio.getConsignacion().getFchVenceSoat() != null)
					fchVenSoat = hojaNegocio.getConsignacion().getFchVenceSoat();
			}else if(hojaNegocio.getFromRetoma()){
				vehiculo = hojaNegocio.getRetoma().getVehiculo();
				kilometraje = hojaNegocio.getRetoma().getKilometraje();
				if(hojaNegocio.getRetoma().getFchVenceRevtm() != null)
					fchVenRT = hojaNegocio.getRetoma().getFchVenceRevtm();
				if(hojaNegocio.getRetoma().getFchVenceSoat() != null)
					fchVenSoat = hojaNegocio.getRetoma().getFchVenceSoat();
			}
			
			//Datos del vehículo
			range.replaceText("<<#Marca>>", vehiculo.getMarca().getNombre()); 
			range.replaceText("<<#Linea>>", vehiculo.getLinea()); 
			range.replaceText("<<#Modelo>>", vehiculo.getModelo()); 
			range.replaceText("<<#Tipo>>", vehiculo.getTipoVehiculo().getNombre()); 
			range.replaceText("<<#Placa>>", vehiculo.getPlaca()); 
			range.replaceText("<<#Color>>", vehiculo.getColor().getNombre()); 
			range.replaceText("<<#NroMotor>>", vehiculo.getMotor()); 
			range.replaceText("<<#Serie>>", vehiculo.getSerie()); 
			range.replaceText("<<#NroChasis>>", vehiculo.getSerie()); 
			range.replaceText("<<#Kilometraje>>", numberFormat.format(kilometraje));
			
			if(fchVenRT != null){
				range.replaceText("<<#FchVenceRevtm>>", dateFormat.format(fchVenRT));
			}else{
				range.replaceText("<<#FchVenceRevtm>>", "");
			}
			if(fchVenSoat != null){
				range.replaceText("<<#FchVenceSoat>>", dateFormat.format(fchVenSoat));
			}else{
				range.replaceText("<<#FchVenceSoat>>", "");
			}
			
			if(!esComprador){
				
				range.replaceText("<<#GeneradoPara>>", "VENDEDOR");
				
				Cliente cliente = new Cliente();
				Concesionario concesionario = new Concesionario();
				
				if(hojaNegocio.getFromConsignacion()){
					cliente = hojaNegocio.getConsignacion().getCliente();
				}else if(hojaNegocio.getFromRetoma()){
					cliente = hojaNegocio.getRetoma().getCliente();
					concesionario = hojaNegocio.getRetoma().getConcesionario();
				}
				
				//Datos del cliente
				if(cliente != null){
					range.replaceText("<<#NombreCliente>>", cliente.getNombres() + " " + cliente.getApellidos());
					range.replaceText("<<#NOMBRECLIENTE>>", cliente.getNombres().toUpperCase() + " " + cliente.getApellidos().toUpperCase());
					range.replaceText("<<#CedulaCliente>>", cliente.getCedula());
					range.replaceText("<<#TelefonoCliente>>", cliente.getTelefono1()); 
					range.replaceText("<<#CelularCliente>>", cliente.getCelular1()); 
					range.replaceText("<<#DireccionCliente>>", cliente.getDireccion()); 
					range.replaceText("<<#EmailCliente>>", cliente.getCorreoElectronico()); 
				}
				
				//Datos del concesionario
				if(concesionario != null){
					range.replaceText("<<#NombreConcesionario>>", concesionario.getNombre());
					range.replaceText("<<#NitConcesionario>>", concesionario.getNit());
					range.replaceText("<<#ContactoConcesionario>>", concesionario.getContacto()); 
					range.replaceText("<<#TelefonoConcesionario>>", concesionario.getTelefono()); 
				}
			}else{
				
				range.replaceText("<<#GeneradoPara>>", "COMPRADOR");
				
				//Si es vendedor
				Cliente cliente = new Cliente();
				
				if(hojaNegocio.getHasVenta()){
					cliente = hojaNegocio.getVenta().getCliente();
				}
				
				//Datos del cliente
				if(cliente != null){
					range.replaceText("<<#NombreCliente>>", cliente.getNombres() + " " + cliente.getApellidos());
					range.replaceText("<<#NOMBRECLIENTE>>", cliente.getNombres().toUpperCase() + " " + cliente.getApellidos().toUpperCase());
					range.replaceText("<<#CedulaCliente>>", cliente.getCedula());
					range.replaceText("<<#TelefonoCliente>>", cliente.getTelefono1()); 
					range.replaceText("<<#CelularCliente>>", cliente.getCelular1()); 
					range.replaceText("<<#DireccionCliente>>", cliente.getDireccion()); 
					range.replaceText("<<#EmailCliente>>", cliente.getCorreoElectronico()); 
				}
			}
			
			String codAsignadoA = Constantes.CODVENDEDOR;
			if(esComprador){
				codAsignadoA = Constantes.CODCOMPRADOR;
			}
			
			//Información del estado del negocio
			range.replaceText("<<#saldovehiculo>>", "$"+numberFormat.format(hojaNegocio.getSaldoVehiculo(codAsignadoA)));
			range.replaceText("<<#totalabonos>>", "$"+numberFormat.format(hojaNegocio.getTotalAbonos(codAsignadoA)));
			range.replaceText("<<#totalgastostraspaso>>", "$"+numberFormat.format(hojaNegocio.getTotalGastosTraspaso(codAsignadoA)));
			range.replaceText("<<#totalgastosadicionales>>", "$"+numberFormat.format(hojaNegocio.getTotalGastosAdicionales(codAsignadoA)));
			range.replaceText("<<#saldototal>>", "$"+numberFormat.format(hojaNegocio.getSaldoTotal(codAsignadoA)));
			range.replaceText("<<#vlrvehiculocomprador>>", "$"+numberFormat.format(hojaNegocio.getVlrVehComprador()));
			range.replaceText("<<#vlrvehiculovendedor>>", "$"+numberFormat.format(hojaNegocio.getVlrVehVendedor()));

			//Información de gastos de traspaso	
			StringBuffer gastosTraspasoStr = new StringBuffer();
			for (Iterator<GastoxHojaNegocio> iterator = hojaNegocio.gastosTraspasoList(codAsignadoA).iterator(); iterator.hasNext();) {
				GastoxHojaNegocio gasto = (GastoxHojaNegocio) iterator.next();
				gastosTraspasoStr.append("\r");
				gastosTraspasoStr.append(gasto.getGasto().getNombre() + "\t$ " + numberFormat.format(gasto.getValor()));
			}
			range.replaceText("<<#GastosTraspaso>>", gastosTraspasoStr.toString());
			
			//Información de gastos adicionales	
			StringBuffer gastosAdicionalesStr = new StringBuffer();
			for (Iterator<GastoxHojaNegocio> iterator = hojaNegocio.gastosAdicionalesList(codAsignadoA).iterator(); iterator.hasNext();) {
				GastoxHojaNegocio gasto = (GastoxHojaNegocio) iterator.next();
				gastosAdicionalesStr.append("\r");
				gastosAdicionalesStr.append(gasto.getGasto().getNombre() + "\t$ " + numberFormat.format(gasto.getValor()));
			}
			range.replaceText("<<#GastosAdicionales>>", gastosAdicionalesStr.toString());
			
			//Información de abonos
			StringBuffer abonosStr = new StringBuffer();
			for (Iterator<PagoxHojaNegocio> iterator = hojaNegocio.abonosList(codAsignadoA).iterator(); iterator.hasNext();) {
				PagoxHojaNegocio pago = (PagoxHojaNegocio) iterator.next();
				abonosStr.append("\r");
				abonosStr.append(pago.getConcepto() + "\t$ " + numberFormat.format(pago.getValor()));
			}
			range.replaceText("<<#Abonos>>", abonosStr.toString());
			
			if(!esComprador){
				//Información de abonos
				StringBuffer abonosPagoStr = new StringBuffer();
				
				List<PagoxHojaNegocio> pagosAlVendedor = new ArrayList<>(); 
				for (Iterator<PagoxHojaNegocio> itPagos = hojaNegocio.getPagos().iterator(); itPagos.hasNext();) {
					PagoxHojaNegocio pagoxHojaNegocio = (PagoxHojaNegocio) itPagos.next();
					if(pagoxHojaNegocio.getPagadoA().equals(com.sh.pvs.core.utils.Constantes.CODVENDEDOR)){
						pagosAlVendedor.add(pagoxHojaNegocio);
					}
				}
				
				for (Iterator<PagoxHojaNegocio> iterator = pagosAlVendedor.iterator(); iterator.hasNext();) {
					PagoxHojaNegocio pago = iterator.next();
					abonosStr.append("\r");
					abonosStr.append(pago.getConcepto() + "\t$ " + numberFormat.format(pago.getValor()));
				}
				range.replaceText("<<#AbonosAlVendedor>>", abonosStr.toString());
			}
			
		}catch(Exception e){
			
		}
		
	}
	
}
