package com.sh.pvs.utils.wordtemplate;

import com.sh.pvs.model.dto.consignacion.Consignacion;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.usermodel.Range;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;

public class ConsignacionTagsReplacement {

	private HWPFDocument word;
	private Consignacion consignacion;
	private SimpleDateFormat dateFormat;
	private NumberFormat numberFormat;
	
	public ConsignacionTagsReplacement(HWPFDocument word,
			Consignacion consignacion) {
		super();
		this.word = word;
		this.consignacion = consignacion;
		dateFormat = new SimpleDateFormat("dd/MM/yyyy");		
		numberFormat = NumberFormat.getNumberInstance();

	}
	
	public void replaceTags(){
		
		Range range = word.getRange();
		
		//Datos de la consignación
		range.replaceText("<<#Codigo>>", consignacion.getCodigo());
		range.replaceText("<<#FchConsignacion>>", dateFormat.format(consignacion.getFchConsignacion()));
		range.replaceText("<<#VlrVehiculo>>", numberFormat.format(consignacion.getValorVehiculo()));
		if(consignacion.getPrecioMinimo() != null)
		{
			range.replaceText("<<#PrecioMinimo>>", numberFormat.format(consignacion.getPrecioMinimo()));
		}
		range.replaceText("<<#Estado>>", consignacion.getEstadoLbl()); 
		
		//Datos del cliente
		if(consignacion.getCliente() != null){
			range.replaceText("<<#NombreCliente>>", consignacion.getCliente().getNombres() + " " + consignacion.getCliente().getApellidos());
			range.replaceText("<<#NOMBRECLIENTE>>", consignacion.getCliente().getNombres().toUpperCase() + " " + consignacion.getCliente().getApellidos().toUpperCase());
			range.replaceText("<<#CedulaCliente>>", consignacion.getCliente().getCedula());
			range.replaceText("<<#TelefonoCliente>>", consignacion.getCliente().getTelefono1()); 
			range.replaceText("<<#CelularCliente>>", consignacion.getCliente().getCelular1()); 
			range.replaceText("<<#DireccionCliente>>", consignacion.getCliente().getDireccion()); 
			range.replaceText("<<#EmailCliente>>", consignacion.getCliente().getCorreoElectronico()); 
		}
		
		//Datos del vehículo
		range.replaceText("<<#Marca>>", consignacion.getVehiculo().getMarca().getNombre()); 
		range.replaceText("<<#Linea>>", consignacion.getVehiculo().getLinea()); 
		range.replaceText("<<#Modelo>>", consignacion.getVehiculo().getModelo()); 
		range.replaceText("<<#Tipo>>", consignacion.getVehiculo().getTipoVehiculo().getNombre()); 
		range.replaceText("<<#Placa>>", consignacion.getVehiculo().getPlaca()); 
		range.replaceText("<<#Color>>", consignacion.getVehiculo().getColor().getNombre()); 
		range.replaceText("<<#NroMotor>>", consignacion.getVehiculo().getMotor());
		range.replaceText("<<#Servicio>>", "Particular");
		if(StringUtils.isNotEmpty(consignacion.getVehiculo().getServicio())){
			if(consignacion.getVehiculo().getServicio().equals("Pu"))
			{
				range.replaceText("<<#Servicio>>", "Público");
			}
		}
		
		range.replaceText("<<#Serie>>", consignacion.getVehiculo().getSerie()); 
		range.replaceText("<<#NroChasis>>", consignacion.getVehiculo().getChasis()); 
		range.replaceText("<<#Carroceria>>", consignacion.getVehiculo().getCarroceria().getNombre()); 
		range.replaceText("<<#Kilometraje>>", numberFormat.format(consignacion.getKilometraje()));
		range.replaceText("<<#tieneMatricula>>", this.formatBoolean(consignacion.getTieneMatricula()));
		range.replaceText("<<#tieneSoat>>", this.formatBoolean(consignacion.getTieneSoat()));
		range.replaceText("<<#tieneTecnoMecanica>>", this.formatBoolean(consignacion.getTieneTecnoMecanica()));
		range.replaceText("<<#tieneCopiaLlave>>", this.formatBoolean(consignacion.getTieneCopiaLlave()));
		range.replaceText("<<#copiaLlaveEntregada>>", this.formatBoolean(consignacion.getCopiaLlaveEntregada()));
		
		if(consignacion.getFchVenceRevtm() != null)
			range.replaceText("<<#FchVenceRevtm>>", dateFormat.format(consignacion.getFchVenceRevtm()));
		if(consignacion.getFchVenceSoat() != null)	
			range.replaceText("<<#FchVenceSoat>>", dateFormat.format(consignacion.getFchVenceSoat()));
		
	}
	
	private String formatBoolean(Boolean valor){
		if(valor == null || !valor){
			return "No";
		}
		return "Si";
	}
	
}
