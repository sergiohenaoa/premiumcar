package com.sh.pvs.utils.wordtemplate;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;

import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.usermodel.Range;

import com.sh.pvs.model.dto.retoma.Retoma;

public class RetomaTagsReplacement {

	private HWPFDocument word;
	private Retoma retoma;
	private SimpleDateFormat dateFormat;
	private NumberFormat numberFormat;
	
	public RetomaTagsReplacement(HWPFDocument word,
			Retoma retoma) {
		super();
		this.word = word;
		this.retoma = retoma;
		dateFormat = new SimpleDateFormat("dd/MM/yyyy");		
		numberFormat = NumberFormat.getNumberInstance();

	}
	
	public void replaceTags(){
		
		Range range = word.getRange();
		
		try{
			//Datos de la consignación
			range.replaceText("<<#Codigo>>", retoma.getCodigo());
			range.replaceText("<<#FchRetoma>>", dateFormat.format(retoma.getFchRetoma()));
			range.replaceText("<<#VlrVehiculo>>", numberFormat.format(retoma.getValorVehiculo()));
			range.replaceText("<<#Estado>>", retoma.getEstadoLbl()); 
			
			//Datos del cliente
			if(retoma.getCliente() != null){
				range.replaceText("<<#NombreCliente>>", retoma.getCliente().getNombres() + " " + retoma.getCliente().getApellidos());
				range.replaceText("<<#NOMBRECLIENTE>>", retoma.getCliente().getNombres().toUpperCase() + " " + retoma.getCliente().getApellidos().toUpperCase());
				range.replaceText("<<#CedulaCliente>>", retoma.getCliente().getCedula());
				range.replaceText("<<#TelefonoCliente>>", retoma.getCliente().getTelefono1()); 
				range.replaceText("<<#CelularCliente>>", retoma.getCliente().getCelular1()); 
				range.replaceText("<<#DireccionCliente>>", retoma.getCliente().getDireccion()); 
				range.replaceText("<<#EmailCliente>>", retoma.getCliente().getCorreoElectronico()); 
			}
			
			//Datos del concesionario
			if(retoma.getConcesionario() != null){
				range.replaceText("<<#NombreConcesionario>>", retoma.getConcesionario().getNombre());
				range.replaceText("<<#NitConcesionario>>", retoma.getConcesionario().getNit());
				range.replaceText("<<#ContactoConcesionario>>", retoma.getConcesionario().getContacto()); 
				range.replaceText("<<#TelefonoConcesionario>>", retoma.getConcesionario().getTelefono()); 
			}
			
			//Datos del vehículo
			range.replaceText("<<#Marca>>", retoma.getVehiculo().getMarca().getNombre()); 
			range.replaceText("<<#Linea>>", retoma.getVehiculo().getLinea()); 
			range.replaceText("<<#Modelo>>", retoma.getVehiculo().getModelo()); 
			range.replaceText("<<#Tipo>>", retoma.getVehiculo().getTipoVehiculo().getNombre()); 
			range.replaceText("<<#Placa>>", retoma.getVehiculo().getPlaca()); 
			range.replaceText("<<#Color>>", retoma.getVehiculo().getColor().getNombre()); 
			range.replaceText("<<#NroMotor>>", retoma.getVehiculo().getMotor()); 
			range.replaceText("<<#Serie>>", retoma.getVehiculo().getSerie()); 
			range.replaceText("<<#NroChasis>>", retoma.getVehiculo().getChasis()); 
			range.replaceText("<<#NroChasis>>", retoma.getVehiculo().getSerie()); 
			range.replaceText("<<#Kilometraje>>", numberFormat.format(retoma.getKilometraje()));
			
			if(retoma.getFchVenceRevtm() != null)
				range.replaceText("<<#FchVenceRevtm>>", dateFormat.format(retoma.getFchVenceRevtm()));
			if(retoma.getFchVenceSoat() != null)
				range.replaceText("<<#FchVenceSoat>>", dateFormat.format(retoma.getFchVenceSoat()));
			
		}catch(Exception e){
			
		}
		
	}
	
}
