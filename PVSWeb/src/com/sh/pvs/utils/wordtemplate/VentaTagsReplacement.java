package com.sh.pvs.utils.wordtemplate;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;

import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.usermodel.Range;

import com.sh.pvs.model.dto.venta.Venta;

public class VentaTagsReplacement {

	private HWPFDocument word;
	private Venta venta;
	private SimpleDateFormat dateFormat;
	private NumberFormat numberFormat;
	
	public VentaTagsReplacement(HWPFDocument word,
			Venta venta) {
		super();
		this.word = word;
		this.venta = venta;
		dateFormat = new SimpleDateFormat("dd/MM/yyyy");		
		numberFormat = NumberFormat.getNumberInstance();

	}
	
	public void replaceTags(){
		
		Range range = word.getRange();
		
		//Datos de la venta
		range.replaceText("<<#Codigo>>", venta.getCodigo());
		range.replaceText("<<#FchVenta>>", dateFormat.format(venta.getFchVenta()));
		range.replaceText("<<#VlrVehiculo>>", numberFormat.format(venta.getValorVehiculo()));
		range.replaceText("<<#Estado>>", venta.getEstadoLbl()); 
		
		//Datos del cliente
		if(venta.getCliente() != null){
			range.replaceText("<<#NombreCliente>>", venta.getCliente().getNombres() + " " + venta.getCliente().getApellidos());
			range.replaceText("<<#NOMBRECLIENTE>>", venta.getCliente().getNombres().toUpperCase() + " " + venta	.getCliente().getApellidos().toUpperCase());
			range.replaceText("<<#CedulaCliente>>", venta.getCliente().getCedula());
			range.replaceText("<<#TelefonoCliente>>", venta.getCliente().getTelefono1()); 
			range.replaceText("<<#CelularCliente>>", venta.getCliente().getCelular1()); 
			range.replaceText("<<#DireccionCliente>>", venta.getCliente().getDireccion()); 
			range.replaceText("<<#EmailCliente>>", venta.getCliente().getCorreoElectronico()); 
		}
		
		//Datos del vehículo
		range.replaceText("<<#Marca>>", venta.getVehiculo().getMarca().getNombre()); 
		range.replaceText("<<#Linea>>", venta.getVehiculo().getLinea()); 
		range.replaceText("<<#Modelo>>", venta.getVehiculo().getModelo()); 
		range.replaceText("<<#Tipo>>", venta.getVehiculo().getTipoVehiculo().getNombre()); 
		range.replaceText("<<#Placa>>", venta.getVehiculo().getPlaca()); 
		range.replaceText("<<#Color>>", venta.getVehiculo().getColor().getNombre()); 
		range.replaceText("<<#NroMotor>>", venta.getVehiculo().getMotor()); 
		range.replaceText("<<#Serie>>", venta.getVehiculo().getSerie()); 
		range.replaceText("<<#NroChasis>>", venta.getVehiculo().getChasis()); 
		range.replaceText("<<#NroChasis>>", venta.getVehiculo().getSerie()); 

	}
	
}
