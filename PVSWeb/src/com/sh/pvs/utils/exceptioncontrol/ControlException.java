/**
 * ControlException.java:
 * 
 * 	Esta clase es utilizada para controlar las excepciones que pueda presentar el sistema
 *
 * @author Sergio Henao, 06/03/2014
 * @Modifier Sergio Henao, 06/03/2014
 * @version 1.0
 * 
 */
package com.sh.pvs.utils.exceptioncontrol;


public class ControlException {
	
	/*METHODS*/
	/**
	 * 
	 * @param tipoExcepcion
	 * @param claseGenExcepcion
	 * @param entidadGenExcepcion
	 * @param metodoExcepcion
	 * @param ex
	 * @return
	 */
	public static Exception formatException(String tipoExcepcion, String claseGenExcepcion, String entidadGenExcepcion,
			String metodoExcepcion, Exception ex){
		String separator = ":";
		
		return new Exception(tipoExcepcion + separator +
								claseGenExcepcion + separator + 
								entidadGenExcepcion + separator + 
								metodoExcepcion + separator + 
								ex.getClass().getName() + separator + 
								ex.getMessage());
	}
	
}
