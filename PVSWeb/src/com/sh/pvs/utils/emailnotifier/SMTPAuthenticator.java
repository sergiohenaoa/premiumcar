package com.sh.pvs.utils.emailnotifier;

import javax.mail.PasswordAuthentication;

/**
 *  Autenticador para envio de correo
 *  
 *   @author E-DEAS LTDA
 */
public class SMTPAuthenticator extends javax.mail.Authenticator {

	//------------------FIELDS------------------
	private String fUser;
	private String fPassword;

	//------------------CONSTRUCTORS------------------
	/**
	 * Constructor
	 */
	public SMTPAuthenticator(String user, String password) {
		fUser = user;
		fPassword = password;
	}

	@Override
	public PasswordAuthentication getPasswordAuthentication() {
		return new PasswordAuthentication(fUser, fPassword);
	}
	
}
