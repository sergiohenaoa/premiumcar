package com.sh.pvs.utils.emailnotifier;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import com.sh.pvs.utils.Metodos;
import com.sh.pvs.utils.exceptioncontrol.ControlException;


/**
 *  CONTROLADOR Correo: encargado de envio de correos
 *  
 */
public class Notifier {

	//------------------FIELDS------------------
	private String subject;
	private String body;
	private ArrayList<String> toSendMany = new ArrayList<String>(0);
	private String toSend;
	
	//------------------CONSTRUCTORS------------------
	/**
	 * @param subject
	 * @param body
	 * @param toSendMany
	 */
	public Notifier(String subject, String body, ArrayList<String> toSendMany) {
		super();
		this.subject = subject;
		this.body = body;
		this.toSendMany = toSendMany;
	}
	
	/**
	 * @param subject
	 * @param body
	 * @param toSend
	 */
	public Notifier(String subject, String body, String toSend) {
		super();
		this.subject = subject;
		this.body = body;
		this.toSend= toSend;
	}

	/**
	 * Constructor
	 */
	public Notifier() {
		super();
	}

	//------------------METHODS------------------
	/**
	 * Método para crear el mensaje a enviar
	 */
	public void createSendMsg() throws Exception{
		try
	    {
			
	        // Propiedades de la conexión
	        Properties props = new Properties();
	        InputStream is = getClass().getResourceAsStream("/mail.properties");
	        props.load(is);	     

	        // Preparamos la sesion
	        SMTPAuthenticator auth = new SMTPAuthenticator((String)props.get("mail.user"),(String)props.get("mail.password"));
	        Session mailSession = Session.getInstance(props,auth);

	        // Construimos el mensaje
	        MimeMessage message = new MimeMessage(mailSession);
	        //from
	        message.setFrom(new InternetAddress((String)props.get("mail.user")));

	        //Titulo del mensaje
	        message.setSubject(this.subject);
	        message.setContent(this.body,"text/html");

	        if(this.toSend!=null && !this.toSend.equals(""))	  this.toSendMany.add(this.toSend);      
	        for (Iterator<String> iter = this.toSendMany.iterator(); iter.hasNext();) {
	        	String correo = iter.next();
	        	 message.addRecipient(
	     	            Message.RecipientType.TO,
	     	            new InternetAddress(correo));
			}
	        
	        // Lo enviamos.
	        if(this.toSendMany.size()>0){
	        	Transport.send(message, message.getAllRecipients());
	        }


	    }
	    catch (Exception e){
	    	throw ControlException.formatException("TECNICA", Notifier.class.getName(), "", "createSendMsg", e);
	    }

	}
	
	/**
	 * Método para crear el mensaje con adjunto a enviar
	 */
	public void createSendMsgMultipart(String urladjunto, String nombreadjunto) throws Exception{
		try
	    {
			//Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
			
	        // Propiedades de la conexión
	        Properties props = new Properties();
	        InputStream is = getClass().getResourceAsStream("/mail.properties");
	        props.load(is);	     

	        // Preparamos la sesion
	        SMTPAuthenticator auth = new SMTPAuthenticator((String)props.get("mail.user"),(String)props.get("mail.password"));
	        Session mailSession = Session.getInstance(props,auth);

	        //Crear el archivo como adjunto para el correo
			BodyPart adjunto = new MimeBodyPart();
			adjunto.setDataHandler(new DataHandler(new FileDataSource(urladjunto)));
			adjunto.setFileName(nombreadjunto);
			
			//Se crea el body para el correo
			BodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setContent(body, "text/html");
			
			//Se crea el mensaje multiparte
			MimeMultipart multiParte = new MimeMultipart();
			multiParte.addBodyPart(messageBodyPart);
			multiParte.addBodyPart(adjunto);
	        	        
	        // Construimos el mensaje
	        MimeMessage message = new MimeMessage(mailSession);
	        //from
	        message.setFrom(new InternetAddress((String)props.get("mail.user")));

	        //Titulo del mensaje
	        message.setSubject(this.subject);
	        message.setContent(multiParte);

	        if(this.toSend!=null && !this.toSend.equals(""))	  this.toSendMany.add(this.toSend);      
	        for (Iterator<String> iter = this.toSendMany.iterator(); iter.hasNext();) {
	        	String correo = iter.next();
	        	 message.addRecipient(
	     	            Message.RecipientType.TO,
	     	            new InternetAddress(correo));
			}
	        
	        // Lo enviamos.
	        if(this.toSendMany.size()>0){
	        	Transport.send(message, message.getAllRecipients());
	        }

	    }
	    catch (Exception e){
	    	throw ControlException.formatException("TECNICA", Notifier.class.getName(), "", "createSendMsgMultipart", e);
	    }

	}
	
	/**
	 * Método para crear el mensaje con adjunto a enviar
	 * archivos[0] nombre del adjunto
	 * archivos[1] adjunto (longblob)
	 * archivos[2] tipo
	 */
	public void createSendMsgMultipart(ArrayList<String[]> archivos) throws Exception{
		try
	    {
			//Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
			
	        // Propiedades de la conexión
	        Properties props = new Properties();
	        InputStream is = getClass().getResourceAsStream("/mail.properties");
	        props.load(is);	     

	        // Preparamos la sesion
	        SMTPAuthenticator auth = new SMTPAuthenticator((String)props.get("mail.user"),(String)props.get("mail.password"));
	        Session mailSession = Session.getInstance(props,auth);

			//Se crea el body para el correo
			BodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setContent(body, "text/html");
			
			//Se crea el mensaje multiparte
			MimeMultipart multiParte = new MimeMultipart();
			multiParte.addBodyPart(messageBodyPart);
			
			//Se iteran los archivos y se adjuntan
			for (Iterator<String[]> itArchivos = archivos.iterator(); itArchivos.hasNext();) {
				//[0]==> nombre, [1]==>blob, [2]==>tipo
				String[] archivo = (String[]) itArchivos.next();
				//Crear el archivo como adjunto para el correo
				BodyPart adjunto = new MimeBodyPart();
				
				byte[] decodedFile = Metodos.decodeFile(archivo[1]);
				
	            DataSource dataSource = new ByteArrayDataSource(decodedFile, archivo[2]);

				adjunto.setDataHandler(new DataHandler(dataSource));
				adjunto.setFileName(archivo[0]);
				multiParte.addBodyPart(adjunto);
			}
			        
	        // Construimos el mensaje
	        MimeMessage message = new MimeMessage(mailSession);
	        //from
	        message.setFrom(new InternetAddress((String)props.get("mail.user")));

	        //Titulo del mensaje
	        message.setSubject(this.subject);
	        message.setContent(multiParte);

	        if(this.toSend!=null && !this.toSend.equals(""))	  this.toSendMany.add(this.toSend);      
	        for (Iterator<String> iter = this.toSendMany.iterator(); iter.hasNext();) {
	        	String correo = iter.next();
	        	 message.addRecipient(
	     	            Message.RecipientType.TO,
	     	            new InternetAddress(correo));
			}
	        
	        // Lo enviamos.
	        if(this.toSendMany.size()>0){
	        	Transport.send(message, message.getAllRecipients());
	        }

	    }
	    catch (Exception e){
	    	throw ControlException.formatException("TECNICA", Notifier.class.getName(), "", "createSendMsgMultipart", e);
	    }

	}

	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * @param subject the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * @return the body
	 */
	public String getBody() {
		return body;
	}

	/**
	 * @param body the body to set
	 */
	public void setBody(String body) {
		this.body = body;
	}

	/**
	 * @return the toSendMany
	 */
	public ArrayList<String> getToSendMany() {
		return toSendMany;
	}

	/**
	 * @param toSendMany the toSendMany to set
	 */
	public void setToSendMany(ArrayList<String> toSendMany) {
		this.toSendMany = toSendMany;
	}

	/**
	 * @return the toSend
	 */
	public String getToSend() {
		return toSend;
	}

	/**
	 * @param toSend the toSend to set
	 */
	public void setToSend(String toSend) {
		this.toSend = toSend;
	}

}
