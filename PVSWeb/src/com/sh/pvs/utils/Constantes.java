/**
 * Constantes.java:
 * 
 * 	Esta clase contiene diferentes constantes que serán utilizadas en 
 * 	la aplicación.
 *
 * @author Sergio Henao, 06/03/2014
 * @Modifier Sergio Henao, 06/03/2014
 * @version 1.0
 * 
 */

package com.sh.pvs.utils;

import java.text.SimpleDateFormat;

public class Constantes {

	/*LOG*/
	public static final String INFOLOG = "infoLog";
	public static final String WARNLOG = "warnLog";
	public static final String ERRORLOG = "errorLog";
	
	/*MENSAJES DE NOTIFICACION*/
	public static final String RECORDAR_CLAVE = "Recordar_clave";
	public static final String NUEVA_CLAVE = "Nueva_clave";
	public static final String RESTITUCION_CLAVE = "Restitucion_clave";
	
	/*ACCIONES*/
	public static final String CREAR = "CREAR";
	public static final String LEER = "LEER";
	public static final String EDITAR = "EDITAR";
	public static final String BORRAR = "BORRAR";
	public static final String RESTITURCLAVE = "Restituir clave";
	
	/*FORMATOS*/
	public static final SimpleDateFormat FORMAT_DD_MM_YYYY = new SimpleDateFormat("dd/MM/yyyy");
	public static final SimpleDateFormat FORMAT_DD_MM_YYYY_HH_MM_A = new SimpleDateFormat("dd/MM/yyyy hh:mm");
	
}

