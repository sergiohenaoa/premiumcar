/**
 * 
 */
package com.sh.pvs.core.services.venta;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import com.sh.pvs.core.interfaces.venta.IVentaTerceroServices;
import com.sh.pvs.core.services.general.HomeServices;
import com.sh.pvs.core.utils.Constantes;
import com.sh.pvs.core.utils.Metodos;
import com.sh.pvs.model.dao.venta.VentaTerceroDAO;
import com.sh.pvs.model.dto.venta.VentaTercero;
import com.sh.pvs.model.utils.Condition;
import com.sh.pvs.model.utils.ControlQuery;


public class VentaTerceroServices extends HomeServices<VentaTercero> implements IVentaTerceroServices {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * Minimal constructor
	 */
	public VentaTerceroServices() {
		super(VentaTercero.class);
	}
	
	/**
	 * Método para crear una entidad utilizando una unidad de trabajo transaccional
	 * @param entity
	 */
	@Override
	public VentaTercero createVentaTerceroTX(VentaTercero entity) throws Exception{
		VentaTercero result = null;
		try {
			
			beginTransaction();	
			
			//Se asigna el código
			entity.setCodigo(createCodigo(entity));
			result = save(entity);
			
			endTransaction();
		} catch (Exception e) {
			throw e;
		} finally {
			try {
				closeSession();
			} catch (Exception e1) {
				throw e1;
			}
		}
		return result;
	}
	
	/**
	 * Método para construir el código de la venta
	 */
	private String createCodigo(VentaTercero entity) throws Exception{
		
		String codigo = "";
		//Se asigna el código
		String dateStr = new SimpleDateFormat("ddMMyyyy").format(new Date());
		
		ControlQuery cq = new ControlQuery();
		Condition condCia = new Condition(VentaTerceroDAO.IDCOMPANIA, Condition.IGUAL, entity.getCompania().getId()); 
		Condition condFecha = new Condition("DATE("+VentaTerceroDAO.FCHVENTA+")", Condition.IGUAL, Metodos.removeTime(new Date()));		
		cq.add(condCia);
		cq.addAND(condFecha);
		
		int mayorConsecutivo = 0;
		List<VentaTercero> ventaTerceroes = findAll(cq);
		for (Iterator<VentaTercero> itRes = ventaTerceroes.iterator(); itRes.hasNext();) {
			VentaTercero ventaTercero = (VentaTercero) itRes.next();
			
			if(ventaTercero.getCodigo().contains("-")){
				String[] codigoSplitted = ventaTercero.getCodigo().split("-");
				if(codigoSplitted.length == 3){
					int consCodigo = Integer.parseInt(codigoSplitted[2]);
					if(consCodigo > mayorConsecutivo)
						mayorConsecutivo = consCodigo;
				}
			}
		}
		mayorConsecutivo++;
		codigo = Constantes.CODVENTATERCERO+ "-" + dateStr + "-" + mayorConsecutivo;
		
		return codigo;
	}
	
}
