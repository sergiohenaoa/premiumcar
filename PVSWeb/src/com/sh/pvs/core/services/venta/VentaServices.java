/**
 * 
 */
package com.sh.pvs.core.services.venta;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import com.sh.pvs.core.interfaces.venta.IVentaServices;
import com.sh.pvs.core.services.general.HomeServices;
import com.sh.pvs.core.utils.Constantes;
import com.sh.pvs.core.utils.Metodos;
import com.sh.pvs.model.dao.hojanegocio.HojaNegocioDAO;
import com.sh.pvs.model.dao.venta.VentaDAO;
import com.sh.pvs.model.dto.configuracion.Vehiculo;
import com.sh.pvs.model.dto.hojanegocio.HojaNegocio;
import com.sh.pvs.model.dto.venta.Venta;
import com.sh.pvs.model.utils.Condition;
import com.sh.pvs.model.utils.ControlQuery;


public class VentaServices extends HomeServices<Venta> implements IVentaServices {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * Minimal constructor
	 */
	public VentaServices() {
		super(Venta.class);
	}
	
	/**
	 * Método para crear una entidad utilizando una unidad de trabajo transaccional
	 * @param entity
	 */
	@Override
	public Venta createVentaTX(Venta entity) throws Exception{
		Venta result = null;
		try {
			
			beginTransaction();	
			entity.setCodigo(createCodigo(entity));
			if(entity.getRetoma() != null && entity.getRetoma().getId() == null)
				entity.setRetoma(null);
			if(entity.getConsignacion() != null && entity.getConsignacion().getId() == null)
				entity.setConsignacion(null);
			result = save(entity);
			
			//Se activa el vehículo en el inventario
			HomeServices<Vehiculo> serviceVehiculo = new HomeServices<Vehiculo>(Vehiculo.class);
			Vehiculo vehiculo = serviceVehiculo.findById(entity.getVehiculo().getId());
			vehiculo.setEsActivoInventario(false);
			serviceVehiculo.merge(vehiculo);
			serviceVehiculo.getSession().flush();
			
			//Se actualiza la hoja de negocio respectiva
			HomeServices<HojaNegocio> serviceHojaNegocio = new HomeServices<HojaNegocio>(HojaNegocio.class);
			
			if(result.getConsignacion() != null  && result.getConsignacion().getId() != null){
				List<HojaNegocio> hojasNegocioxConsignacion = serviceHojaNegocio.findByProperty(HojaNegocioDAO.IDCONSIGNACION, result.getConsignacion().getId());
				for (Iterator<HojaNegocio> itHN = hojasNegocioxConsignacion.iterator(); itHN
						.hasNext();) {
					HojaNegocio hojaNegocio = (HojaNegocio) itHN.next();
					hojaNegocio.setVenta(result);
					hojaNegocio.setVlrVehComprador(result.getValorVehiculo());
					hojaNegocio.setVlrVehVendedor(result.getValorVehiculo());
					serviceHojaNegocio.merge(hojaNegocio);
					serviceHojaNegocio.getSession().flush();
					
				}
				result = merge(result);
			}else{
				if(result.getRetoma() != null  && result.getRetoma().getId() != null){
					List<HojaNegocio> hojasNegocioxRetoma = serviceHojaNegocio.findByProperty(HojaNegocioDAO.IDRETOMA, result.getRetoma().getId());
					for (Iterator<HojaNegocio> itHN = hojasNegocioxRetoma.iterator(); itHN.hasNext();) {
						HojaNegocio hojaNegocio = (HojaNegocio) itHN.next();
						hojaNegocio.setVenta(result);
						hojaNegocio.setVlrVehComprador(result.getValorVehiculo());
						hojaNegocio.setVlrVehVendedor(result.getValorVehiculo());
						serviceHojaNegocio.merge(hojaNegocio);
						serviceHojaNegocio.getSession().flush();
						
					}
					result = merge(result);
				}
			}
			
			endTransaction();
		} catch (Exception e) {
			throw e;
		} finally {
			try {
				closeSession();
			} catch (Exception e1) {
				throw e1;
			}
		}
		return result;
	}
	
	/**
	 * Método para construir el código de la venta
	 */
	private String createCodigo(Venta entity) throws Exception{
		
		String codigo = "";
		//Se asigna el código
		String dateStr = new SimpleDateFormat("ddMMyyyy").format(new Date());
		
		ControlQuery cq = new ControlQuery();
		Condition condCia = new Condition(VentaDAO.IDCOMPANIA, Condition.IGUAL, entity.getCompania().getId()); 
		Condition condFecha = new Condition("DATE("+VentaDAO.FCHVENTA+")", Condition.IGUAL, Metodos.removeTime(new Date()));		
		cq.add(condCia);
		cq.addAND(condFecha);
		
		int mayorConsecutivo = 0;
		List<Venta> ventaes = findAll(cq);
		for (Iterator<Venta> itRes = ventaes.iterator(); itRes.hasNext();) {
			Venta venta = (Venta) itRes.next();
			
			if(venta.getCodigo().contains("-")){
				String[] codigoSplitted = venta.getCodigo().split("-");
				if(codigoSplitted.length == 3){
					int consCodigo = Integer.parseInt(codigoSplitted[2]);
					if(consCodigo > mayorConsecutivo)
						mayorConsecutivo = consCodigo;
				}
			}
		}
		mayorConsecutivo++;
		codigo = Constantes.CODVENTA + "-" + dateStr + "-" + mayorConsecutivo;
		
		return codigo;
	}
	
	/**
	 * Método para cambiar el estado de una venta a declinado
	 * @param entity
	 */
	@Override
	public Venta declinarVentaTX(Venta entity) throws Exception{
		Venta result = null;
		try {
			
			beginTransaction();	
			
			result = findById(entity.getId());
			
			result.setEstado(com.sh.pvs.model.utils.Constantes.VENTA_ESTADO_DECLINADA_COD);
			
			result = merge(result);
			
			getSession().flush();
			
			//Se activa el vehículo en el inventario
			HomeServices<Vehiculo> serviceVehiculo = new HomeServices<Vehiculo>(Vehiculo.class);
			Vehiculo vehiculo = serviceVehiculo.findById(entity.getVehiculo().getId());
			vehiculo.setEsActivoInventario(true);
			serviceVehiculo.merge(vehiculo);
			serviceVehiculo.getSession().flush();
			
			endTransaction();
		} catch (Exception e) {
			throw e;
		} finally {
			try {
				closeSession();
			} catch (Exception e1) {
				throw e1;
			}
		}
		return result;
	}
	
}
