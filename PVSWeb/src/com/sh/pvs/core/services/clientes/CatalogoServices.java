/**
 * 
 */
package com.sh.pvs.core.services.clientes;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;

import com.sh.pvs.core.interfaces.clientes.ICatalogoServices;
import com.sh.pvs.core.interfaces.consignacion.IConsignacionServices;
import com.sh.pvs.core.interfaces.retoma.IRetomaServices;
import com.sh.pvs.core.services.consignacion.ConsignacionServices;
import com.sh.pvs.core.services.general.HomeServices;
import com.sh.pvs.core.services.retoma.RetomaServices;
import com.sh.pvs.model.dao.configuracion.VehiculoDAO;
import com.sh.pvs.model.dao.consignacion.ConsignacionDAO;
import com.sh.pvs.model.dao.retoma.RetomaDAO;
import com.sh.pvs.model.dto.configuracion.Vehiculo;
import com.sh.pvs.model.dto.consignacion.Consignacion;
import com.sh.pvs.model.dto.retoma.Retoma;
import com.sh.pvs.model.utils.Condition;
import com.sh.pvs.model.utils.ControlQuery;


public class CatalogoServices implements ICatalogoServices {
	
	/*CONSTRUCTORS*/
	
	/**
	 * Minimal constructor
	 */
	public CatalogoServices() {
	}
	
	/*METHODS*/
	/**
	 * Método para buscar todos los vehículos activos del inventario según compañía
	 * @param Cia
	 */
	public ArrayList<Vehiculo> findAllVehiculosToCatalogo(Integer idCia) throws Exception{
		
		ControlQuery queryVehiculos = new ControlQuery();
		queryVehiculos.add(new Condition(VehiculoDAO.IDCOMPANIA, Condition.IGUAL, idCia));
		queryVehiculos.addAND(new Condition(VehiculoDAO.ES_ACTIVO_INVENTARIO, Condition.IGUAL, true));
		
		HomeServices<Vehiculo> serviceVehiculo = new HomeServices<Vehiculo>(Vehiculo.class);
		List<Vehiculo> vehiculosActivos = serviceVehiculo.findAll(queryVehiculos);
		
		ArrayList<Vehiculo> vehiculos = new ArrayList<Vehiculo>();
		for (Iterator<Vehiculo> iterator = vehiculosActivos.iterator(); iterator.hasNext();) {
			Vehiculo vehiculo = (Vehiculo) iterator.next();
			
			IConsignacionServices servicioConsignacion = new ConsignacionServices();
			List<Consignacion> consignaciones = servicioConsignacion.findByProperty(ConsignacionDAO.IDVEHICULO, vehiculo.getId());
			if(consignaciones != null && consignaciones.size() > 0){
				Consignacion ultimaConsignacion = new Consignacion();
				for (Iterator<Consignacion> itConsignaciones = consignaciones.iterator(); itConsignaciones.hasNext();) {
					Consignacion consignacion = (Consignacion) itConsignaciones.next();
					if((ultimaConsignacion.getId() == null
							|| ultimaConsignacion.getId() <= consignacion.getId())
							&& consignacion.getEstado().equals(com.sh.pvs.model.utils.Constantes.CONS_ESTADO_INICIAL_COD))
						ultimaConsignacion = consignacion;
				}
				vehiculo.setKilometrosTmp(ultimaConsignacion.getKilometraje());
				vehiculo.setPrecioTmp(ultimaConsignacion.getValorVehiculo());
			}else{
				IRetomaServices servicioRetoma = new RetomaServices();
				List<Retoma> retomas = servicioRetoma.findByProperty(RetomaDAO.IDVEHICULO, vehiculo.getId());
				if(retomas != null && retomas.size() > 0){
					Retoma ultimaRetoma = new Retoma();
					for (Iterator<Retoma> itRetomas = retomas.iterator(); itRetomas.hasNext();) {
						Retoma retoma = (Retoma) itRetomas.next();
						if((ultimaRetoma.getId() == null
								|| ultimaRetoma.getId() <= retoma.getId())
								&& retoma.getEstado().equals(com.sh.pvs.model.utils.Constantes.RETOMA_ESTADO_INICIAL_COD))
							ultimaRetoma = retoma;
					}
					vehiculo.setKilometrosTmp(ultimaRetoma.getKilometraje());
					vehiculo.setPrecioTmp(ultimaRetoma.getValorVehiculo());
				}
			}
			
			vehiculos.add(vehiculo);
		}
		
		return new ArrayList<Vehiculo>(new TreeSet<Vehiculo>(vehiculos));
	}

}
