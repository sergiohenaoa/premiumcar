package com.sh.pvs.core.services.seguridad;

import java.util.ArrayList;
import java.util.List;

import com.sh.pvs.core.interfaces.seguridad.IFormularioServices;
import com.sh.pvs.core.services.general.HomeServices;
import com.sh.pvs.model.dao.seguridad.FormularioDAO;
import com.sh.pvs.model.dto.seguridad.Formulario;
import com.sh.pvs.model.utils.Condition;
import com.sh.pvs.model.utils.ControlQuery;

public class FormularioServices extends HomeServices<Formulario> implements IFormularioServices {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * Minimal constructor
	 */
	public FormularioServices() {
		super(Formulario.class);
	}

	/*METHODS*/	
	@Override
	public ArrayList<Formulario> findEncabezadosMenu(Integer idCia) throws Exception {
		
		ControlQuery cq = new ControlQuery();
		
		Condition condForm = new Condition(FormularioDAO.FORMULARIO, Condition.IGUAL, false);
		Condition condEstado = new Condition(FormularioDAO.ESTADO, Condition.IGUAL, true);
		Condition condCia = new Condition(FormularioDAO.IDCOMPANIA, Condition.IGUAL, idCia);
		
		cq.add(condForm);
		cq.addAND(condEstado);
		cq.addAND(condCia);
		
		String[] orderbyOrden = new String[]{FormularioDAO.ORDEN, Condition.ASC};
		String[] orderbyNombre = new String[]{FormularioDAO.NOMBREDESCRIPTIVO, Condition.ASC};
		cq.getOrderby().add(orderbyOrden);
		cq.getOrderby().add(orderbyNombre);
		
		List<Formulario> formularios = findAll(cq);
		
		return new ArrayList<Formulario>(formularios);
	}
	
	@Override
	public ArrayList<Formulario> findAllActivos(Integer idCia) throws Exception {
		
		ControlQuery cq = new ControlQuery();		
		Condition condEstado = new Condition(FormularioDAO.ESTADO, Condition.IGUAL, true);	
		Condition condCia = new Condition(FormularioDAO.IDCOMPANIA, Condition.IGUAL, idCia);
		cq.addAND(condCia);
		cq.add(condEstado);
		
		String[] orderbyNombre = new String[]{FormularioDAO.NOMBREDESCRIPTIVO, Condition.ASC};
		
		cq.getOrderby().add(orderbyNombre);
		
		List<Formulario> formularios = findAll(cq);
		
		return new ArrayList<Formulario>(formularios);
	}
	
	@Override
	public ArrayList<Formulario> findAll(Integer idCia) throws Exception {
		
		ControlQuery cq = new ControlQuery();		
		Condition condCia = new Condition(FormularioDAO.IDCOMPANIA, Condition.IGUAL, idCia);
		cq.addAND(condCia);
		
		String[] orderbyNombre = new String[]{FormularioDAO.NOMBREDESCRIPTIVO, Condition.ASC};
		
		cq.getOrderby().add(orderbyNombre);
		
		List<Formulario> formularios = findAll(cq);
		
		return new ArrayList<Formulario>(formularios);
	}
	
	@Override
	public Formulario findById(Integer idElement, Integer idCia) throws Exception {
		
		ControlQuery cq = new ControlQuery();
		Condition condId = new Condition(FormularioDAO.ID, Condition.IGUAL, idElement);
		cq.addAND(condId);
		Condition condCia = new Condition(FormularioDAO.IDCOMPANIA, Condition.IGUAL, idCia);
		cq.addAND(condCia);
		
		List<Formulario> formularios = findAll(cq);
		
		if(formularios == null || formularios.size() == 0)
			return null;
		
		return formularios.get(0);
	}
	
	@Override
	public ArrayList<Formulario> findAllActivosAndNoFormularioAndNoIdentidad(Integer idFormulario, Integer idCia) throws Exception {
		
		ControlQuery cq = new ControlQuery();
		
		Condition condEstado = new Condition(FormularioDAO.ESTADO, Condition.IGUAL, true);				
		cq.add(condEstado);
		
		Condition condCia = new Condition(FormularioDAO.IDCOMPANIA, Condition.IGUAL, idCia);
		cq.addAND(condCia);
		
		Condition condNoFormulario = new Condition(FormularioDAO.FORMULARIO, Condition.IGUAL, false);				
		cq.add(condNoFormulario);
		
		if (idFormulario != null) {
			Condition condNoIdentidad = new Condition(FormularioDAO.ID, Condition.DIFERENTE, idFormulario);				
			cq.add(condNoIdentidad);
		}
		
		String[] orderbyNombre = new String[]{FormularioDAO.NOMBREDESCRIPTIVO, Condition.ASC};
		
		cq.getOrderby().add(orderbyNombre);
		
		List<Formulario> formularios = findAll(cq);
		
		return new ArrayList<Formulario>(formularios);
	}

	@Override
	public Formulario crearFormularioTX(Formulario ent) throws Exception {
		boolean exception = false;
		try {
			
			if(ent.getFormularioPadre() == null 
					|| ent.getFormularioPadre().getId() == null 
					|| ent.getFormularioPadre().getId().equals(new Integer(0))){
				ent.setFormularioPadre(null);
			}else{
				Formulario padre = new Formulario();
				padre.setId(ent.getFormularioPadre().getId());
				ent.setFormularioPadre(padre);
			}
			
			ent.setEsFormulario(false);
			
			ent.setId(null);
			
			beginTransaction();
			save(ent);						
			endTransaction();
		} catch (Exception e) {
			 exception = true;
			throw e;
		} finally {
			try {
				closeSession();
			} catch (Exception e1) {
				if(!exception)
					throw e1;
			}
		}
		return ent;
	}

	@Override
	public Formulario actualizarFormularioTX(Formulario ent) throws Exception {
		try {
			
			if(ent.getFormularioPadre() != null 
					&& (ent.getFormularioPadre().getId() == null || ent.getFormularioPadre().getId().equals(new Integer(0)))){
				ent.setFormularioPadre(null);
			}
			
			beginTransaction();			
			merge(ent);						
			endTransaction();
		} catch (Exception e) {
			throw e;
		} finally {
			try {
				closeSession();
			} catch (Exception e1) {
				throw e1;
			}
		}
		return ent;
	}

	@Override
	public void borrarFormularioTX(Formulario ent) throws Exception {
		try {
			Formulario entPers = findById(ent.getId());
			if(entPers != null){
				beginTransaction();			
				delete(entPers);	
				endTransaction();
			}
		} catch (Exception e) {
			throw e;
		} finally {
			try {
				closeSession();
			} catch (Exception e1) {
				throw e1;
			}
		}
	}

}