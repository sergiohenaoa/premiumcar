/**
 * 
 */
package com.sh.pvs.core.services.seguridad;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.primefaces.model.LazyDataModel;

import com.sh.pvs.core.interfaces.seguridad.IAuditoriaServices;
import com.sh.pvs.core.services.general.HomeServices;
import com.sh.pvs.core.utils.LazyAuditoriaDataModel;
import com.sh.pvs.model.dao.seguridad.AuditoriaDAO;
import com.sh.pvs.model.dto.seguridad.Auditoria;
import com.sh.pvs.model.dto.seguridad.Usuario;
import com.sh.pvs.model.utils.Condition;
import com.sh.pvs.model.utils.ControlQuery;

/**
 * @author andress
 *
 */
public class AuditoriaServices extends HomeServices<Auditoria> implements IAuditoriaServices{
	
	private static final long serialVersionUID = 1L;

	/**
	 * Minimal constructor
	 */
	public AuditoriaServices() {
		super(Auditoria.class);
	}

	@Override
	public void registrarAuditoria(Usuario user, String accion, String formulario, String registro, String observaciones) throws Exception {
		try {
			
			Auditoria auditoria = new Auditoria();			
			auditoria.setFecha(new Timestamp((new Date()).getTime()));
			auditoria.setUsuario(user.getUsuario());
			auditoria.setAccion(accion);
			auditoria.setFormulario(formulario);	
			auditoria.setRegistro(registro);
			auditoria.setObservaciones(observaciones);
			auditoria.setCompania(user.getCompania().getNombre());
			auditoria.setCompaniaId(user.getCompania().getId());
			
			beginTransaction();
			save(auditoria);						
			endTransaction();
		} catch (Exception e) {
			throw e;
		} finally {
			try {
				closeSession();
			} catch (Exception e1) {
				throw e1;
			}
		}
		
	}
	
	@Override
	public LazyDataModel<Auditoria> findAllLazyAuditoriaByFilters(Date fechaIni, Date fechaFin, Auditoria entity, Integer idCia){
		int cont=0;
		ControlQuery query = new ControlQuery();
		
		query.setAlias("Auditoria");
		
		Condition condCia = new Condition();
		condCia.setPropiedad(AuditoriaDAO.COMPANIAID);
		condCia.setOperador(Condition.IGUAL);
		condCia.setValue(idCia);
		query.add(condCia);
		
		if (fechaIni != null && fechaFin != null) {
			Date fchHoraCero = new Timestamp(fechaIni.getTime());
			Condition a = new Condition();
			a.setPropiedad(AuditoriaDAO.FECHA);
			a.setOperador(Condition.MAYOROIGUAL);
			a.setValue(fchHoraCero);
			query.add(a);

			Calendar calendario = Calendar.getInstance();
			calendario.setTimeInMillis(fechaFin.getTime());
			calendario.add(Calendar.DATE, 1);
			Date fchHoraFin = new Timestamp(calendario.getTime().getTime());

			Condition b = new Condition();
			b.setPropiedad(AuditoriaDAO.FECHA);
			b.setOperador(Condition.MENOR);
			b.setValue(fchHoraFin);
			query.add(b);

			cont++;
		}

		if (entity.getAccion() != null & !entity.getAccion().equals("")) {
			Condition a = new Condition();
			a.setPropiedad(AuditoriaDAO.ACCION);
			a.setOperador(Condition.LIKE);
			a.setValue("%" + entity.getAccion() + "%");
			query.add(a);
			cont++;
		}
		
		if (entity.getFormulario() != null
				& !entity.getFormulario().equals("")) {
			Condition a = new Condition();
			a.setPropiedad(AuditoriaDAO.FORMULARIO);
			a.setOperador(Condition.LIKE);
			a.setValue("%" + entity.getFormulario() + "%");
			query.add(a);
			cont++;
		}

		if (entity.getRegistro() != null & !entity.getRegistro().equals("")) {
			Condition a = new Condition();
			a.setPropiedad(AuditoriaDAO.REGISTRO);
			a.setOperador(Condition.LIKE);
			a.setValue("%" + entity.getRegistro() + "%");
			query.add(a);
			cont++;
		}

		if (entity.getUsuario() != null
				& !entity.getUsuario().equals("")) {
			Condition a = new Condition();
			a.setPropiedad(AuditoriaDAO.USUARIO);
			a.setOperador(Condition.LIKE);
			a.setValue("%" + entity.getUsuario() + "%");
			query.add(a);
			cont++;
		}

		String[] orderby = new String[]{AuditoriaDAO.FECHA, Condition.DESC};
		query.getOrderby().add(orderby);
		
		if (cont > 0){
			return new LazyAuditoriaDataModel(query);
		}else{
			return null;
		}
	}
	
	@Override
	public List<Auditoria> findAllAuditoriaByFilters(Date fechaIni, Date fechaFin, Auditoria entity, Integer idCia) throws Exception{
		@SuppressWarnings("unused")
		int cont=0;
		ControlQuery query = new ControlQuery();
		
		query.setAlias("Auditoria");
		
		//Filtro Compania
		Condition condCia = new Condition();
		condCia.setPropiedad(AuditoriaDAO.COMPANIAID);
		condCia.setOperador(Condition.IGUAL);
		condCia.setValue(idCia);
		query.add(condCia);
		
		//Filtro Fecha
		if (fechaIni != null && fechaFin != null) {
			Date fchHoraCero = new Timestamp(fechaIni.getTime());
			Condition a = new Condition();
			a.setPropiedad(AuditoriaDAO.FECHA);
			a.setOperador(Condition.MAYOROIGUAL);
			a.setValue(fchHoraCero);
			query.add(a);

			Calendar calendario = Calendar.getInstance();
			calendario.setTimeInMillis(fechaFin.getTime());
			calendario.add(Calendar.DATE, 1);
			Date fchHoraFin = new Timestamp(calendario.getTime().getTime());

			Condition b = new Condition();
			b.setPropiedad(AuditoriaDAO.FECHA);
			b.setOperador(Condition.MENOR);
			b.setValue(fchHoraFin);
			query.add(b);

			cont++;
		}

		//Filtro Accion
		if (entity.getAccion() != null & !entity.getAccion().equals("")) {
			Condition a = new Condition();
			a.setPropiedad(AuditoriaDAO.ACCION);
			a.setOperador(Condition.LIKE);
			a.setValue("%" + entity.getAccion() + "%");
			query.add(a);
			cont++;
		}
		
		//Filtro Formulario
		if (entity.getFormulario() != null
				& !entity.getFormulario().equals("")) {
			Condition a = new Condition();
			a.setPropiedad(AuditoriaDAO.FORMULARIO);
			a.setOperador(Condition.LIKE);
			a.setValue("%" + entity.getFormulario() + "%");
			query.add(a);
			cont++;
		}

		//Filtro Registro
		if (entity.getRegistro() != null & !entity.getRegistro().equals("")) {
			Condition a = new Condition();
			a.setPropiedad(AuditoriaDAO.REGISTRO);
			a.setOperador(Condition.LIKE);
			a.setValue("%" + entity.getRegistro() + "%");
			query.add(a);
			cont++;
		}

		//Filtro Usuario
		if (entity.getUsuario() != null
				& !entity.getUsuario().equals("")) {
			Condition a = new Condition();
			a.setPropiedad(AuditoriaDAO.USUARIO);
			a.setOperador(Condition.LIKE);
			a.setValue("%" + entity.getUsuario() + "%");
			query.add(a);
			cont++;
		}

		String[] orderby = new String[]{AuditoriaDAO.FECHA, Condition.DESC};
		query.getOrderby().add(orderby);
		
		return findAll(query);
	}

}
