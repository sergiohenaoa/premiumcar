package com.sh.pvs.core.services.seguridad;

import com.sh.pvs.core.interfaces.seguridad.IAccionServices;
import com.sh.pvs.core.services.general.HomeServices;
import com.sh.pvs.model.dto.seguridad.Accion;

public class AccionServices extends HomeServices<Accion> implements IAccionServices {
	
	private static final long serialVersionUID = 1L;

	/**
	 * Minimal constructor
	 */
	public AccionServices() {
		super(Accion.class);
	}
	

}
