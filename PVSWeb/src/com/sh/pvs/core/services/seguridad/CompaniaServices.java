package com.sh.pvs.core.services.seguridad;

import java.util.ArrayList;
import java.util.List;

import com.sh.pvs.core.interfaces.seguridad.ICompaniaServices;
import com.sh.pvs.core.services.general.HomeServices;
import com.sh.pvs.model.dao.seguridad.CompaniaDAO;
import com.sh.pvs.model.dto.seguridad.Compania;
import com.sh.pvs.model.utils.Condition;
import com.sh.pvs.model.utils.ControlQuery;

public class CompaniaServices extends HomeServices<Compania> implements ICompaniaServices {
	
	private static final long serialVersionUID = 1L;

	/**
	 * Minimal constructor
	 */
	public CompaniaServices() {
		super(Compania.class);
	}

	/**
	 * Método que lista todas las compañías activas ordenadas por nombre
	 * @return
	 * @throws Exception
	 */
	@Override
	public ArrayList<Compania> findAllActivasOrdenados() throws Exception {
		
		ControlQuery cq = new ControlQuery();		
		Condition condEstado = new Condition(CompaniaDAO.ESTADO, Condition.IGUAL, true);				
		cq.add(condEstado);
		
		String[] orderbyNombre = new String[]{CompaniaDAO.NOMBRE, Condition.ASC};
		
		cq.getOrderby().add(orderbyNombre);
		
		List<Compania> companias = findAll(cq);
		
		return new ArrayList<Compania>(companias);
	}

}
