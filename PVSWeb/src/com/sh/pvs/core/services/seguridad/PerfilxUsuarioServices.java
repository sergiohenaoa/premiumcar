package com.sh.pvs.core.services.seguridad;

import com.sh.pvs.core.interfaces.seguridad.IPerfilxUsuarioServices;
import com.sh.pvs.core.services.general.HomeServices;
import com.sh.pvs.model.dto.seguridad.PerfilxUsuario;

public class PerfilxUsuarioServices extends HomeServices<PerfilxUsuario> implements IPerfilxUsuarioServices {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * Minimal constructor
	 */
	public PerfilxUsuarioServices() {
		super(PerfilxUsuario.class);
	}

	
}