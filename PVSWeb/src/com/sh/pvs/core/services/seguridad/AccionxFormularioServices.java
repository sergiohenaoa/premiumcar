package com.sh.pvs.core.services.seguridad;

import java.util.ArrayList;
import java.util.List;

import com.sh.pvs.core.interfaces.seguridad.IAccionxFormularioServices;
import com.sh.pvs.core.services.general.HomeServices;
import com.sh.pvs.model.dao.seguridad.AccionxFormularioDAO;
import com.sh.pvs.model.dto.seguridad.AccionxFormulario;
import com.sh.pvs.model.dto.seguridad.Formulario;
import com.sh.pvs.model.utils.Condition;
import com.sh.pvs.model.utils.ControlQuery;

public class AccionxFormularioServices extends HomeServices<AccionxFormulario> implements IAccionxFormularioServices {
	
	private static final long serialVersionUID = 1L;

	/**
	 * Minimal constructor
	 */
	public AccionxFormularioServices() {
		super(AccionxFormulario.class);
	}

	@Override
	public ArrayList<AccionxFormulario> findAllByFormulario(Formulario form) throws Exception {
		
		ControlQuery cq = new ControlQuery();		
		Condition cond= new Condition(AccionxFormularioDAO.IDFORMULARIO, Condition.IGUAL, form.getId());				
		cq.add(cond);
		
		String[] orderby = new String[]{AccionxFormularioDAO.IDACCION, Condition.ASC};
		
		cq.getOrderby().add(orderby);
		
		List<AccionxFormulario> accionesxform = findAll(cq);
		
		return new ArrayList<AccionxFormulario>(accionesxform);
	}
	
	@Override
	public List<AccionxFormulario> findByAccionAndFormulario(int idAccion, int idFormulario) throws Exception{
		ControlQuery cq = new ControlQuery();
		
		Condition condAccion = new Condition(AccionxFormularioDAO.IDACCION, Condition.IGUAL, idAccion);
		cq.add(condAccion);
		
		Condition condForm = new Condition(AccionxFormularioDAO.IDFORMULARIO, Condition.IGUAL, idFormulario);
		cq.add(condForm);
		
		return findAll(cq);
	}

}
