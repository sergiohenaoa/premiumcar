package com.sh.pvs.core.services.seguridad;

import java.util.Iterator;
import java.util.List;

import com.sh.pvs.core.interfaces.seguridad.IAccionxFormularioServices;
import com.sh.pvs.core.interfaces.seguridad.IPermisoServices;
import com.sh.pvs.core.services.general.HomeServices;
import com.sh.pvs.model.dao.seguridad.AccionDAO;
import com.sh.pvs.model.dao.seguridad.FormularioDAO;
import com.sh.pvs.model.dao.seguridad.PermisoDAO;
import com.sh.pvs.model.dto.seguridad.Accion;
import com.sh.pvs.model.dto.seguridad.AccionxFormulario;
import com.sh.pvs.model.dto.seguridad.Formulario;
import com.sh.pvs.model.dto.seguridad.Perfil;
import com.sh.pvs.model.dto.seguridad.Permiso;
import com.sh.pvs.model.utils.Condition;
import com.sh.pvs.model.utils.ControlQuery;
import com.sh.pvs.utils.exceptioncontrol.ControlException;

public class PermisoServices extends HomeServices<Permiso> implements IPermisoServices {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * Minimal constructor
	 */
	public PermisoServices() {
		super(Permiso.class);
	}

	@Override
	public Permiso crearPermisoTX(Integer idperfil, Integer idform, String[] acciones) throws Exception {
		
		try{
			beginTransaction();
			PerfilServices perfilSrv = new PerfilServices();
			Perfil perfil = perfilSrv.findById(idperfil);
			
			
			AccionServices accionSrv = new AccionServices();
			for (int i = 0; i < acciones.length; i++) {
				String accionTmp = acciones[i];
				List<Accion> accionesList = accionSrv.findByProperty(AccionDAO.NOMBRE, accionTmp);
				
				for (Iterator<Accion> it = accionesList.iterator(); it.hasNext();) {
					Accion accion = it.next();
					
					IAccionxFormularioServices accionFormSrv = new AccionxFormularioServices();
					
					
					AccionxFormulario accxform = accionFormSrv.findByAccionAndFormulario(accion.getId(), idform).get(0);
					
					Permiso permiso = new Permiso();
					permiso.setPerfil(perfil);
					permiso.setAccionxFormulario(accxform);
					
					this.save(permiso);
					getSession().flush();
					
				}
			}
			endTransaction();
		} catch (Exception e) {
			throw ControlException.formatException("TECNICA", PermisoServices.class.getName(), "", "crearPermisoTX", e);
		} finally {
			try {
				closeSession();
			} catch (Exception e1) {
				throw e1;
			}
		}
		return null;
	}

	@Override
	public void borrarPermisosByFormTX(Integer idperfil, String nombreform, Integer idCia) throws Exception {
		try{
			
			FormularioServices formSrv = new FormularioServices();
			
			ControlQuery cqFormularios = new ControlQuery();
			Condition condNombreDescriptivo = new Condition(FormularioDAO.NOMBREDESCRIPTIVO, Condition.IGUAL, nombreform);
			Condition condCia = new Condition(FormularioDAO.IDCOMPANIA, Condition.IGUAL, idCia);
			cqFormularios.add(condNombreDescriptivo);
			cqFormularios.addAND(condCia);
			
			List<Formulario> formularios = formSrv.findAll(cqFormularios);
			
			List<Permiso> permisosList = findByProperty(PermisoDAO.IDPERFIL, idperfil);
			beginTransaction();	
			for (Iterator<Permiso> itPermisos = permisosList.iterator(); itPermisos.hasNext();) {
				Permiso permiso = itPermisos.next();
				if(permiso.getAccionxFormulario().getFormulario().getId().equals(formularios.get(0).getId()))
					delete(permiso);
			}
			endTransaction();
		} catch (Exception e) {
			rollBackTransaction();
			throw e;
		} finally {
			try {
				closeSession();
			} catch (Exception e1) {
				throw e1;
			}
		}
	}

}
