package com.sh.pvs.core.services.seguridad;

import java.security.Key;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import com.sh.pvs.core.interfaces.configuracion.IMensajeNotificacionServices;
import com.sh.pvs.core.interfaces.seguridad.IUsuarioServices;
import com.sh.pvs.core.services.configuracion.MensajeNotificacionServices;
import com.sh.pvs.core.services.general.HomeServices;
import com.sh.pvs.core.utils.Metodos;
import com.sh.pvs.core.utils.SendMailThreadGenerico;
import com.sh.pvs.model.dao.seguridad.UsuarioDAO;
import com.sh.pvs.model.dto.configuracion.MensajeNotificacion;
import com.sh.pvs.model.dto.seguridad.Usuario;
import com.sh.pvs.model.utils.Condition;
import com.sh.pvs.model.utils.ControlQuery;
import com.sh.pvs.utils.Constantes;
import com.sh.pvs.utils.exceptioncontrol.ControlException;

public class UsuarioServices extends HomeServices<Usuario> implements IUsuarioServices {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * Minimal constructor
	 */
	public UsuarioServices() {
		super(Usuario.class);
	}

	/**
     * Valida el usuario que se intenta autenticar.
     * 
     * @return null si el usuario no existe o la clave no es correcta, true si existe y está activo, false si existe y está inactivo
     * @throws Exception Excepción para mejorar presentación de excepciones no controladas
     */
	@Override
	public Boolean validarLogin(Usuario usr) throws Exception {
		
		ControlQuery queryUsuarioCia = new ControlQuery();
		Condition condicionUsuario = new Condition(UsuarioDAO.USUARIO, Condition.IGUAL, usr.getUsuario());
		Condition condicionCia = new Condition(UsuarioDAO.IDCOMPANIA, Condition.IGUAL, usr.getCompania().getId());
		Condition condicionCiaEstado = new Condition(UsuarioDAO.ESTADOCOMPANIA, Condition.IGUAL, true);
		queryUsuarioCia.add(condicionUsuario);
		queryUsuarioCia.addAND(condicionCia);
		queryUsuarioCia.addAND(condicionCiaEstado);
		
		List<Usuario> usuarios = findAll(queryUsuarioCia);		
		
		for (Iterator<Usuario> itUsuarios = usuarios.iterator(); itUsuarios.hasNext();) {
			Usuario usuario = itUsuarios.next();
			
			Key key =  Metodos.generateKey();	
			String decryptedPsw = Metodos.decrypt(usuario.getClave(), key);
			if(decryptedPsw.equals(usr.getClave()) && usr.getCompania().getId().equals(usuario.getCompania().getId())){
				if(usuario.getEsActivo() == true){
					return true;
				}else{
					return false;
				}
			}
			
		}
		
		return null;
	}
	
	/**
     * Reenvía la contraseña al correo del usuario en cuestión
     * 
     * @return true si existe y se pudo enviar, false en caso contrario
     * @throws Exception Excepción para mejorar presentación de excepciones no controladas
     */
	@Override
	public Boolean reenviarPw(Usuario usr) throws Exception {
		
		ControlQuery queryUsuarioCia = new ControlQuery();
		Condition condicionUsuario = new Condition(UsuarioDAO.USUARIO, Condition.IGUAL, usr.getUsuario());
		Condition condicionCia = new Condition(UsuarioDAO.IDCOMPANIA, Condition.IGUAL, usr.getCompania().getId());
		Condition condicionCiaEstado = new Condition(UsuarioDAO.ESTADOCOMPANIA, Condition.IGUAL, true);
		queryUsuarioCia.add(condicionUsuario);
		queryUsuarioCia.addAND(condicionCia);
		queryUsuarioCia.addAND(condicionCiaEstado);
		
		List<Usuario> usuarios = findAll(queryUsuarioCia);	
		
		for (Iterator<Usuario> itUsuarios = usuarios.iterator(); itUsuarios.hasNext();) {
			Usuario usuario = itUsuarios.next();
			
			Key key =  Metodos.generateKey();	
			String decryptedPsw = Metodos.decrypt(usuario.getClave(), key);
						
			if(usuario.getEmail() != null && !usuario.getEmail().equals("")){
				
				try {
					IMensajeNotificacionServices msjSrv = new MensajeNotificacionServices();
					MensajeNotificacion mensaje = (MensajeNotificacion) msjSrv.findByCodigoAndCia(usr.getCompania().getId(), Constantes.RECORDAR_CLAVE).get(0);
					
					String mensajeStr = mensaje.getMensaje();
					mensajeStr += "<br/><br/> Usuario: " + usuario.getUsuario();
					mensajeStr += "<br/> Clave: " + decryptedPsw;
					
					SendMailThreadGenerico hilo = new SendMailThreadGenerico(mensaje.getAsunto(), mensajeStr, usuario.getEmail());
					Thread t1 = new Thread(hilo);
					t1.start();
					
				} catch (Exception e) {
					throw e;
				}
				
				
				return true;
			}
			
		}
		
		return false;
	}

	/**
     * Cambiar password
     * 
     * @return false si las contraseñas no coinciden, true si el cambio se realiza
     * @throws Exception Excepción para mejorar presentación de excepciones no controladas
     */
	@Override
	public Boolean cambiarPw(Usuario usr, String pwanterior, String pwnueva, String pwconfirm) throws Exception {
		
		//se valida que la nueva contraseña se haya confirmado bien
		if(!pwnueva.equals(pwconfirm) || pwnueva.trim().equals("")){
			return false;
		}
		
		//se valida que la contraseña anterior sea correcta
		Key key =  Metodos.generateKey();	
		String decryptedPsw = Metodos.decrypt(usr.getClave(), key);
		if(!decryptedPsw.equals(pwanterior)){
			return false;
		}
		
		//se actualiza la contraseña
		Usuario newUsr = findById(usr.getId());	
		newUsr.setClave(Metodos.encrypt(pwnueva, key));
		this.updateTX(newUsr);		
		
		return true;
	}

	/**
	 * Método transaccional para crear un usuario (Calcula y asigna la clave inicial -Envia correo electrónico-)
     * 
     * @return usuario creado
     * @throws Exception Excepción para mejorar presentación de excepciones no controladas 
	 */
	@Override
	public Usuario crearUsuarioTX(Usuario ent) throws Exception {
		try {
			
			String [] passwords = generarPasswordDefecto();
			
			ent.setClave(passwords[0]);
					
			beginTransaction();			
			save(ent);
			getSession().flush();
			
			//Agrega Compania
			IMensajeNotificacionServices msjSrv = new MensajeNotificacionServices();
			MensajeNotificacion mensaje = (MensajeNotificacion) msjSrv.findByCodigoAndCia(ent.getCompania().getId(), Constantes.NUEVA_CLAVE).get(0);
			
			String mensajeStr = mensaje.getMensaje();
			mensajeStr += "<br/><br/> Usuario: " + ent.getUsuario();
			mensajeStr += "<br/> Clave: " + passwords [1];
			
			SendMailThreadGenerico hilo = new SendMailThreadGenerico(mensaje.getAsunto(), mensajeStr, ent.getEmail());
			Thread t1 = new Thread(hilo);
			t1.start();
			
			endTransaction();
		} catch (Exception e) {
			throw ControlException.formatException("TECNICA", UsuarioServices.class.getName(), "", "crearUsuarioTX", e);
		} finally {
			try {
				closeSession();
			} catch (Exception e1) {
				throw e1;
			}
		}
		return ent;
	}

	@Override
	public Boolean restituriPw(Usuario ent) throws Exception {
		try {

			String [] passwords = generarPasswordDefecto();
			
			Usuario entPers = findByProperty(UsuarioDAO.USUARIO, ent.getUsuario()).get(0);		
			IMensajeNotificacionServices msjSrv = new MensajeNotificacionServices();
			MensajeNotificacion mensaje = (MensajeNotificacion) msjSrv.findByCodigoAndCia(ent.getCompania().getId(), Constantes.RESTITUCION_CLAVE).get(0);
			
			entPers.setClave(passwords[0]);
			beginTransaction();			
			merge(entPers);			
									
			String mensajeStr = mensaje.getMensaje();
			mensajeStr += "<br/><br/> Usuario: " + ent.getUsuario();
			mensajeStr += "<br/> Clave: " + passwords[1];
			
			
			SendMailThreadGenerico hilo = new SendMailThreadGenerico(mensaje.getAsunto(), mensajeStr, ent.getEmail());
			Thread t1 = new Thread(hilo);
			t1.start();
			
			endTransaction();
		} catch (Exception e) {
			throw e;
		} finally {
			try {
				closeSession();
			} catch (Exception e1) {
				throw e1;
			}
		}
		return true;
	}
	
	@Override
	public List<Usuario> UsuariosActivosByCompania(int idCompania) throws Exception{
		ControlQuery cq = new ControlQuery();
		
		Condition condicionEstado = new Condition(UsuarioDAO.ESTADO, Condition.IGUAL, true);
		cq.add(condicionEstado);
		
		Condition condicionCompania = new Condition(UsuarioDAO.IDCOMPANIA, Condition.IGUAL, idCompania);
		cq.addAND(condicionCompania);
 		
		return findAll(cq);
	}
	
	/**
	 * Método para generar password por defecto que retorona el valor del password y su valor encriptado
	 * @return [0]: encryptedPsw ; [1] psw
	 * @throws Exception
	 */
	private String[] generarPasswordDefecto() throws Exception{
		//Por defecto 0000
		String encryptedPsw = "vknYC3Intzg=";
		
		//Generación automatica del password				
		String psw = "0000";
	    Random randomGenerator = new Random();
	    int randomInt = randomGenerator.nextInt(999999);
	    psw = String.valueOf(randomInt);
	    
		//Encripción del password 0000 -> /5xOcBaZjL4=											
		Key key =  Metodos.generateKey();
		encryptedPsw = Metodos.encrypt(psw, key);
		
		String [] returnValue = {encryptedPsw, psw};
		return returnValue;	
	}

}