package com.sh.pvs.core.services.seguridad;

import java.util.List;

import com.sh.pvs.core.interfaces.seguridad.IPerfilServices;
import com.sh.pvs.core.services.general.HomeServices;
import com.sh.pvs.model.dao.seguridad.CompaniaDAO;
import com.sh.pvs.model.dao.seguridad.PerfilDAO;
import com.sh.pvs.model.dto.seguridad.Perfil;
import com.sh.pvs.model.utils.Condition;
import com.sh.pvs.model.utils.ControlQuery;

public class PerfilServices extends HomeServices<Perfil> implements IPerfilServices {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * Minimal constructor
	 */
	public PerfilServices() {
		super(Perfil.class);
	}

	/*METHODS*/		
	
	@Override
	public List<Perfil> PerfilesActivosByCompania(int idCompania) throws Exception{
		ControlQuery cq = new ControlQuery();
		
		Condition condicionEstado = new Condition(PerfilDAO.ESTADO, Condition.IGUAL, true);
		cq.add(condicionEstado);
		
		Condition condicionCompania = new Condition(PerfilDAO.COMPANIA, Condition.IGUAL, idCompania);
		cq.addAND(condicionCompania);
 		
		return findAll(cq);
	}
	
	@Override
	public List<Perfil> perfilByNombreAndCompania(int idCompania, String nombre) throws Exception{
		ControlQuery cq = new ControlQuery();
		
		Condition condicionNombre = new Condition(PerfilDAO.NOMBRE, Condition.IGUAL, nombre);
		cq.add(condicionNombre);
	
		Condition condicionCompania = new Condition(PerfilDAO.COMPANIA + "." + CompaniaDAO.ID, Condition.IGUAL, idCompania);
		cq.addAND(condicionCompania);
 		
		return findAll(cq);
	}
	
}