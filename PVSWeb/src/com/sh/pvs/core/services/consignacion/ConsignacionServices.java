/**
 * 
 */
package com.sh.pvs.core.services.consignacion;

import com.sh.pvs.core.interfaces.consignacion.IConsignacionServices;
import com.sh.pvs.core.services.general.HomeServices;
import com.sh.pvs.core.utils.Constantes;
import com.sh.pvs.core.utils.Metodos;
import com.sh.pvs.model.dao.configuracion.GastoDAO;
import com.sh.pvs.model.dao.consignacion.ConsignacionDAO;
import com.sh.pvs.model.dto.configuracion.Gasto;
import com.sh.pvs.model.dto.configuracion.Vehiculo;
import com.sh.pvs.model.dto.consignacion.Consignacion;
import com.sh.pvs.model.dto.hojanegocio.GastoxHojaNegocio;
import com.sh.pvs.model.dto.hojanegocio.HojaNegocio;
import com.sh.pvs.model.utils.Condition;
import com.sh.pvs.model.utils.ControlQuery;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


public class ConsignacionServices extends HomeServices<Consignacion> implements IConsignacionServices {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * Minimal constructor
	 */
	public ConsignacionServices() {
		super(Consignacion.class);
	}
	
	/*METHODS*/
	/**
	 * Método para crear una entidad utilizando una unidad de trabajo transaccional
	 * @param entity
	 */
	@Override
	public Consignacion createConsignacionTX(Consignacion entity) throws Exception{
		Consignacion result = null;
		try {
			
			beginTransaction();	
			
			//Se asigna el código
			entity.setCodigo(createCodigo(entity));
			result = save(entity);
			
			//Se activa el vehículo en el inventario
			HomeServices<Vehiculo> serviceVehiculo = new HomeServices<Vehiculo>(Vehiculo.class);
			Vehiculo vehiculo = serviceVehiculo.findById(entity.getVehiculo().getId());
			vehiculo.setEsActivoInventario(true);
			serviceVehiculo.merge(vehiculo);
			serviceVehiculo.getSession().flush();
			
			//Se crea la hoja de negocio
			HomeServices<HojaNegocio> serviceHojaNegocio = new HomeServices<HojaNegocio>(HojaNegocio.class);
			HojaNegocio hojaNegocio = new HojaNegocio();
			hojaNegocio.setConsignacion(result);
			hojaNegocio.setVlrVehVendedor(result.getValorVehiculo());
			serviceHojaNegocio.save(hojaNegocio);
			result = merge(result);
			
			//Se asocian todos los gastos por defecto
			HomeServices<Gasto> serviceGasto = new HomeServices<Gasto>(Gasto.class);
			HomeServices<GastoxHojaNegocio> serviceGastoxHN = new HomeServices<GastoxHojaNegocio>(null);
			ControlQuery cq = new ControlQuery();
			Condition cond = new Condition(GastoDAO.ES_DEFECTO_CONSIGNACION, Condition.IGUAL, true);
			cq.add(cond);
			List<Gasto> gastosDefecto = serviceGasto.findAll(cq);
			for (Iterator<Gasto> itGastosDefecto = gastosDefecto.iterator(); itGastosDefecto.hasNext();) {
				Gasto gastoTmp = (Gasto) itGastosDefecto.next();
				GastoxHojaNegocio gastoxhn = new GastoxHojaNegocio();
				
				gastoxhn.setAsignadoA(Constantes.CODVENDEDOR);
				gastoxhn.setDescripcion(gastoTmp.getDescripcion());
				gastoxhn.setGasto(gastoTmp);
				gastoxhn.setHojaNegocio(hojaNegocio);
				gastoxhn.setTipoGasto(Constantes.CODTIPOGASTOADICIONAL);
				gastoxhn.setValor(gastoTmp.getValor());
				gastoxhn.setCuenta(null);
				
				serviceGastoxHN.save(gastoxhn);
			}
			
			result = merge(result);
			
			endTransaction();
		} catch (Exception e) {
			throw e;
		} finally {
			try {
				closeSession();
			} catch (Exception e1) {
				throw e1;
			}
		}
		return result;
	}
	
	/**
	 * Método para construir el código de la consignación
	 */
	private String createCodigo(Consignacion entity) throws Exception{
		
		String codigo = "";
		//Se asigna el código
		String dateStr = new SimpleDateFormat("ddMMyyyy").format(new Date());
		
		ControlQuery cq = new ControlQuery();
		Condition condCia = new Condition(ConsignacionDAO.IDCOMPANIA, Condition.IGUAL, entity.getCompania().getId()); 
		Condition condFecha = new Condition("DATE("+ConsignacionDAO.FCHCONSIGNACION+")", Condition.IGUAL, Metodos.removeTime(new Date()));		
		cq.add(condCia);
		cq.addAND(condFecha);
		
		int mayorConsecutivo = 0;
		List<Consignacion> consignaciones = findAll(cq);
		for (Iterator<Consignacion> itRes = consignaciones.iterator(); itRes.hasNext();) {
			Consignacion consignacion = (Consignacion) itRes.next();
			
			if(consignacion.getCodigo().contains("-")){
				String[] codigoSplitted = consignacion.getCodigo().split("-");
				if(codigoSplitted.length == 3){
					int consCodigo = Integer.parseInt(codigoSplitted[2]);
					if(consCodigo > mayorConsecutivo)
						mayorConsecutivo = consCodigo;
				}
			}
		}
		mayorConsecutivo++;
		codigo = Constantes.CODCONSIGNACION + "-" + dateStr + "-" + mayorConsecutivo;
		
		return codigo;
	}
	
	/**
	 * Método para cambiar el estado de una consignación a declinado
	 * @param entity
	 */
	@Override
	public Consignacion declinarConsignacionTX(Consignacion entity) throws Exception{
		Consignacion result = null;
		try {
			
			beginTransaction();	
			
			result = findById(entity.getId());
			
			result.setEstado(com.sh.pvs.model.utils.Constantes.CONS_ESTADO_DECLINADA_COD);
			
			result = merge(result);
			
			getSession().flush();
			
			//Se activa el vehículo en el inventario
			HomeServices<Vehiculo> serviceVehiculo = new HomeServices<Vehiculo>(Vehiculo.class);
			Vehiculo vehiculo = serviceVehiculo.findById(entity.getVehiculo().getId());
			vehiculo.setEsActivoInventario(false);
			serviceVehiculo.merge(vehiculo);
			serviceVehiculo.getSession().flush();
			
			endTransaction();
		} catch (Exception e) {
			throw e;
		} finally {
			try {
				closeSession();
			} catch (Exception e1) {
				throw e1;
			}
		}
		return result;
	}
	
}
