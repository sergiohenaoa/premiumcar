/**
 * 
 */
package com.sh.pvs.core.services.hojanegocio;

import com.sh.pvs.core.interfaces.hojanegocio.IPagoxHojaNegocioServices;
import com.sh.pvs.core.services.general.HomeServices;
import com.sh.pvs.model.dto.hojanegocio.PagoxHojaNegocio;


public class PagoxHojaNegocioServices extends HomeServices<PagoxHojaNegocio> implements IPagoxHojaNegocioServices {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * Minimal constructor
	 */
	public PagoxHojaNegocioServices() {
		super(PagoxHojaNegocio.class);
	}
	
	/*METHODS*/
}
