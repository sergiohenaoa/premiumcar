/**
 * 
 */
package com.sh.pvs.core.services.hojanegocio;

import com.sh.pvs.core.interfaces.hojanegocio.IHojaNegocioServices;
import com.sh.pvs.core.services.configuracion.CuentaServices;
import com.sh.pvs.core.services.contabilidad.MovimientoContableServices;
import com.sh.pvs.core.services.general.HomeServices;
import com.sh.pvs.core.utils.Constantes;
import com.sh.pvs.model.dao.hojanegocio.HojaNegocioDAO;
import com.sh.pvs.model.dto.configuracion.OficinaTransito;
import com.sh.pvs.model.dto.configuracion.TramitexOficinaTransito;
import com.sh.pvs.model.dto.contabilidad.MovimientoContable;
import com.sh.pvs.model.dto.hojanegocio.GastoxHojaNegocio;
import com.sh.pvs.model.dto.hojanegocio.HojaNegocio;
import com.sh.pvs.model.dto.hojanegocio.PagoxHojaNegocio;
import com.sh.pvs.model.dto.seguridad.Usuario;
import com.sh.pvs.model.utils.Condition;
import com.sh.pvs.model.utils.ControlQuery;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


public class HojaNegocioServices extends HomeServices<HojaNegocio> implements IHojaNegocioServices {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * Minimal constructor
	 */
	public HojaNegocioServices() {
		super(HojaNegocio.class);
	}
	
	/*METHODS*/
	/**
	 * Método para agregar un pago a una hoja de negocio
	 * @param pago
	 */
	public void addPago(PagoxHojaNegocio pago, Usuario loggedUser) throws Exception{
		
		HomeServices<PagoxHojaNegocio> service = new HomeServices<PagoxHojaNegocio>(null);
		HojaNegocio entity = findById(pago.getHojaNegocio().getId());
		
		int mayorConsecutivo = 0;
		
		for (Iterator<PagoxHojaNegocio> itPagos = entity.getPagos().iterator(); itPagos.hasNext();) {
			PagoxHojaNegocio pagoxHojaNegocio = itPagos.next();
			String consecutivo = pagoxHojaNegocio.getConsecutivo().split("-")[1];
			if(Integer.parseInt(consecutivo) > mayorConsecutivo){
				mayorConsecutivo = Integer.parseInt(consecutivo);
			}
		}
		
		String comprobante = pago.getHojaNegocio().getId() + "-" + (mayorConsecutivo+1);
		pago.setConsecutivo(comprobante);
		
		if(pago.getCuentaEntrada() != null 
				&& (pago.getCuentaEntrada().getId() == null || pago.getCuentaEntrada().getId().equals(new Integer(0)))){
			pago.setCuentaEntrada(null);
		}else{
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			MovimientoContableServices movimientoContableServices = new MovimientoContableServices();
			MovimientoContable movimientoContable = new MovimientoContable();
			movimientoContable.setCodigo(dateFormat.format(new Date()));
			movimientoContable.setCompania(loggedUser.getCompania());
			movimientoContable.setCuenta(pago.getCuentaEntrada());
			movimientoContable.setDescripcion("Hoja de negocio: " + pago.getHojaNegocio().getVehiculo().getPlaca() + ": " + pago.getConcepto());
			movimientoContable.setEsIngreso(true);
			movimientoContable.setFecha(new Date());
			movimientoContable.setUsuario(loggedUser);
			movimientoContable.setValor(pago.getValor());
			movimientoContable.setEsPendiente(false);
			movimientoContableServices.createTX(movimientoContable);
		}
		
		if(pago.getCuentaSalida() != null 
				&& (pago.getCuentaSalida().getId() == null || pago.getCuentaSalida().getId().equals(new Integer(0)))){
			pago.setCuentaSalida(null);
		}else{
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			MovimientoContableServices movimientoContableServices = new MovimientoContableServices();
			MovimientoContable movimientoContable = new MovimientoContable();
			movimientoContable.setCodigo(dateFormat.format(new Date()));
			movimientoContable.setCompania(loggedUser.getCompania());
			movimientoContable.setCuenta(pago.getCuentaSalida());
			movimientoContable.setDescripcion("Hoja de negocio: " + pago.getHojaNegocio().getVehiculo().getPlaca() + ": " + pago.getConcepto());
			movimientoContable.setEsIngreso(false);
			movimientoContable.setFecha(new Date());
			movimientoContable.setUsuario(loggedUser);
			movimientoContable.setValor(pago.getValor());
			movimientoContable.setEsPendiente(false);
			movimientoContableServices.createTX(movimientoContable);
		}
		service.createTX(pago);
	}

	/**
	 * Método para actualizar un pago de una hoja de negocio
	 * @param pago
	 */
	public void updatePago(PagoxHojaNegocio pago, Usuario loggedUser) throws Exception{
		HomeServices<PagoxHojaNegocio> service = new HomeServices<>(PagoxHojaNegocio.class);
		PagoxHojaNegocio oldPago = service.findById(pago.getId());
		
		if(!oldPago.getValor().equals(pago.getValor())
				&& oldPago.getCuentaEntrada() != null
				&& oldPago.getCuentaEntrada().getId() != null
				&& pago.getCuentaEntrada() != null
				&& pago.getCuentaEntrada().getId() != null){
			
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			MovimientoContableServices movimientoContableServices = new MovimientoContableServices();
			
			MovimientoContable movimientoContable = new MovimientoContable();
			movimientoContable.setCuenta(oldPago.getCuentaEntrada());
			
			movimientoContable.setCodigo(dateFormat.format(new Date()));
			movimientoContable.setCompania(loggedUser.getCompania());
			movimientoContable.setDescripcion("Hoja de negocio: " + oldPago.getHojaNegocio().getVehiculo().getPlaca() + " - Gasto actualizado - " + oldPago.getConcepto());
			movimientoContable.setEsIngreso(false);
			movimientoContable.setFecha(new Date());
			movimientoContable.setUsuario(loggedUser);
			movimientoContable.setValor(oldPago.getValor());
			movimientoContable.setEsPendiente(false);
			movimientoContableServices.createTX(movimientoContable);
			
			movimientoContableServices = new MovimientoContableServices();
			MovimientoContable movimientoContableNuevo = new MovimientoContable();
			movimientoContableNuevo.setCodigo(dateFormat.format(new Date()));
			movimientoContableNuevo.setCompania(loggedUser.getCompania());
			movimientoContableNuevo.setCuenta(pago.getCuentaEntrada());
			movimientoContableNuevo.setEsPendiente(false);
			
			movimientoContableNuevo.setDescripcion("Hoja de negocio: " + pago.getHojaNegocio().getVehiculo().getPlaca() + " - Gasto actualizado - " + pago.getConcepto());
			movimientoContableNuevo.setEsIngreso(true);
			movimientoContableNuevo.setFecha(new Date());
			movimientoContableNuevo.setUsuario(loggedUser);
			movimientoContableNuevo.setValor(pago.getValor());
			movimientoContableServices.createTX(movimientoContableNuevo);
			
		}
		
		if(!oldPago.getValor().equals(pago.getValor())
				&& oldPago.getCuentaSalida() != null
				&& oldPago.getCuentaSalida().getId() != null
				&& pago.getCuentaSalida() != null
				&& pago.getCuentaSalida().getId() != null){
			
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			MovimientoContableServices movimientoContableServices = new MovimientoContableServices();
			
			MovimientoContable movimientoContable = new MovimientoContable();
			movimientoContable.setCuenta(oldPago.getCuentaSalida());			
			movimientoContable.setCodigo(dateFormat.format(new Date()));
			movimientoContable.setCompania(loggedUser.getCompania());
			movimientoContable.setDescripcion("Hoja de negocio: " + oldPago.getHojaNegocio().getVehiculo().getPlaca() + " - Gasto actualizado - " + oldPago.getConcepto());
			movimientoContable.setEsIngreso(true);
			movimientoContable.setFecha(new Date());
			movimientoContable.setUsuario(loggedUser);
			movimientoContable.setValor(oldPago.getValor());
			movimientoContable.setEsPendiente(false);
			movimientoContableServices.createTX(movimientoContable);
			
			movimientoContableServices = new MovimientoContableServices();
			MovimientoContable movimientoContableNuevo = new MovimientoContable();
			movimientoContableNuevo.setCodigo(dateFormat.format(new Date()));
			movimientoContableNuevo.setCompania(loggedUser.getCompania());
			movimientoContableNuevo.setCuenta(pago.getCuentaSalida());			
			movimientoContableNuevo.setDescripcion("Hoja de negocio: " + pago.getHojaNegocio().getVehiculo().getPlaca() + " - Gasto actualizado - " + pago.getConcepto());
			movimientoContableNuevo.setEsIngreso(false);
			movimientoContableNuevo.setFecha(new Date());
			movimientoContableNuevo.setUsuario(loggedUser);
			movimientoContableNuevo.setValor(pago.getValor());
			movimientoContableNuevo.setEsPendiente(false);
			movimientoContableServices.createTX(movimientoContableNuevo);
			
		}
		
		service.updateTX(pago);
	}
	
	/**
	 * Método para eliminar un pago de una hoja de negocio
	 * @param pago
	 */
	public void deletePago(PagoxHojaNegocio pago, Usuario loggedUser) throws Exception{
		HomeServices<PagoxHojaNegocio> service = new HomeServices<PagoxHojaNegocio>(PagoxHojaNegocio.class);
		pago = service.findById(pago.getId());
		
		if(pago.getPagadoA().equals(com.sh.pvs.core.utils.Constantes.CODCIA))
		{
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			MovimientoContableServices movimientoContableServices = new MovimientoContableServices();
			MovimientoContable movimientoContable = new MovimientoContable();
			movimientoContable.setCodigo(dateFormat.format(new Date()));
			movimientoContable.setCompania(loggedUser.getCompania());
			movimientoContable.setEsPendiente(false);
			if (pago.getCuentaSalida() != null && pago.getCuentaSalida().getId() != null)
			{
				movimientoContable.setCuenta(pago.getCuentaSalida());
				movimientoContable.setEsIngreso(true);
			}
			
			if (pago.getCuentaEntrada() != null && pago.getCuentaEntrada().getId() != null)
			{
				movimientoContable.setCuenta(pago.getCuentaEntrada());
				movimientoContable.setEsIngreso(false);
			}
			movimientoContable.setDescripcion(
					"Hoja de negocio: " + pago.getHojaNegocio().getVehiculo().getPlaca() + " - Gasto eliminado - " + pago.getConcepto());
			movimientoContable.setFecha(new Date());
			movimientoContable.setUsuario(loggedUser);
			movimientoContable.setValor(pago.getValor());
			movimientoContableServices.createTX(movimientoContable);
		}
		
		if(pago != null){
			service.deleteTX(pago);
		}
	}
	
	/**
	 * Método para asignar una oficina de tránsito a una hoja de negocio y calcular los costos de los trámites
	 * @param hojaNegocio
	 * @param hojaNegocio
	 */
	public void assignOficinaTransito(HojaNegocio hojaNegocio) throws Exception{
		
		HomeServices<OficinaTransito> oficinaTransitoSrv = new HomeServices<OficinaTransito>(OficinaTransito.class);
		HomeServices<GastoxHojaNegocio> gastoTraspasoxHNSrv = new HomeServices<GastoxHojaNegocio>(GastoxHojaNegocio.class);
		OficinaTransito oficinaTransito = oficinaTransitoSrv.findById(hojaNegocio.getOficinaTransito().getId());
		
		for (Iterator<TramitexOficinaTransito> itTramites = oficinaTransito.getTramites().iterator(); itTramites.hasNext();) {
			TramitexOficinaTransito tramiteConfigurado = (TramitexOficinaTransito) itTramites.next();
			
			if(tramiteConfigurado.getAsignadoA().equals("CO") || tramiteConfigurado.getAsignadoA().equals("VE")){
				GastoxHojaNegocio gastoTraspasoxHojaNegocio = new GastoxHojaNegocio();
				gastoTraspasoxHojaNegocio.setAsignadoA(tramiteConfigurado.getAsignadoA());
				gastoTraspasoxHojaNegocio.setHojaNegocio(hojaNegocio);
				gastoTraspasoxHojaNegocio.setValor(tramiteConfigurado.getValor());
				gastoTraspasoxHojaNegocio.setGasto(tramiteConfigurado.getGasto());
				gastoTraspasoxHojaNegocio.setTipoGasto(Constantes.CODTIPOGASTOTRASPASO);
				
				gastoTraspasoxHNSrv.createTX(gastoTraspasoxHojaNegocio);
				
				hojaNegocio.getGastos().add(gastoTraspasoxHojaNegocio);
				
			}else{
				GastoxHojaNegocio gastoTraspasoxHojaNegocioV = new GastoxHojaNegocio();
				gastoTraspasoxHojaNegocioV.setAsignadoA("VE");
				gastoTraspasoxHojaNegocioV.setHojaNegocio(hojaNegocio);
				gastoTraspasoxHojaNegocioV.setValor(tramiteConfigurado.getValor() / 2);
				gastoTraspasoxHojaNegocioV.setGasto(tramiteConfigurado.getGasto());
				gastoTraspasoxHojaNegocioV.setTipoGasto(Constantes.CODTIPOGASTOTRASPASO);
				
				gastoTraspasoxHNSrv.createTX(gastoTraspasoxHojaNegocioV);
				hojaNegocio.getGastos().add(gastoTraspasoxHojaNegocioV);
				
				GastoxHojaNegocio gastoTraspasoxHojaNegocioC = new GastoxHojaNegocio();
				gastoTraspasoxHojaNegocioC.setAsignadoA("CO");
				gastoTraspasoxHojaNegocioC.setHojaNegocio(hojaNegocio);
				gastoTraspasoxHojaNegocioC.setValor(tramiteConfigurado.getValor() / 2);
				gastoTraspasoxHojaNegocioC.setGasto(tramiteConfigurado.getGasto());
				gastoTraspasoxHojaNegocioC.setTipoGasto(Constantes.CODTIPOGASTOTRASPASO);
				
				gastoTraspasoxHNSrv.createTX(gastoTraspasoxHojaNegocioC);
				hojaNegocio.getGastos().add(gastoTraspasoxHojaNegocioC);
				
			}
			
		}
		hojaNegocio = updateTX(hojaNegocio);
	}
	
	/**
	 * Método para agregar un gasto a una hoja de negocio
	 * @param gasto
	 */
	public void addGasto(GastoxHojaNegocio gasto, Usuario loggedUser) throws Exception{
		HomeServices<GastoxHojaNegocio> service = new HomeServices<GastoxHojaNegocio>(null);
		gasto = service.createTX(gasto);
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		MovimientoContableServices movimientoContableServices = new MovimientoContableServices();
		MovimientoContable movimientoContable = new MovimientoContable();
		movimientoContable.setCodigo(dateFormat.format(new Date()));
		movimientoContable.setCompania(loggedUser.getCompania());
		movimientoContable.setCuenta(gasto.getCuenta());
		movimientoContable.setDescripcion("Hoja de negocio: " + gasto.getHojaNegocio().getVehiculo().getPlaca() + ": " + gasto.getDescripcion());
		movimientoContable.setEsIngreso(false);
		movimientoContable.setFecha(new Date());
		movimientoContable.setUsuario(loggedUser);
		movimientoContable.setValor(gasto.getValor());
		movimientoContable.setEsPendiente(false);
		movimientoContableServices.createTX(movimientoContable);
	}

	/**
	 * Método para actualizar un gasto de una hoja de negocio
	 * @param gasto
	 */
	public void updateGasto(GastoxHojaNegocio gasto, Usuario loggedUser) throws Exception{
		HomeServices<GastoxHojaNegocio> service = new HomeServices<>(GastoxHojaNegocio.class);
		GastoxHojaNegocio oldGasto = service.findById(gasto.getId());
		
		if(!oldGasto.getValor().equals(gasto.getValor()) 
				&& oldGasto.getCuenta() != null 
				&& oldGasto.getCuenta().getId() != null
				&& gasto.getCuenta() != null
				&& gasto.getCuenta().getId() != null){
			
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			MovimientoContableServices movimientoContableServices = new MovimientoContableServices();
			
			MovimientoContable movimientoContable = new MovimientoContable();
			movimientoContable.setCuenta(oldGasto.getCuenta());
			
			movimientoContable.setCodigo(dateFormat.format(new Date()));
			movimientoContable.setCompania(loggedUser.getCompania());			
			movimientoContable.setDescripcion("Hoja de negocio: " + oldGasto.getHojaNegocio().getVehiculo().getPlaca() + " - Gasto actualizado - " + oldGasto.getDescripcion());
			movimientoContable.setEsIngreso(false);
			movimientoContable.setFecha(new Date());
			movimientoContable.setUsuario(loggedUser);
			movimientoContable.setValor(oldGasto.getValor());
			movimientoContable.setEsPendiente(false);
			movimientoContableServices.createTX(movimientoContable);
						
			movimientoContableServices = new MovimientoContableServices();
			MovimientoContable movimientoContableNuevo = new MovimientoContable();
			movimientoContableNuevo.setCodigo(dateFormat.format(new Date()));
			movimientoContableNuevo.setCompania(loggedUser.getCompania());
			movimientoContableNuevo.setCuenta(gasto.getCuenta());
			
			movimientoContableNuevo.setDescripcion("Hoja de negocio: " + gasto.getHojaNegocio().getVehiculo().getPlaca() + " - Gasto actualizado - " + gasto.getDescripcion());
			movimientoContableNuevo.setEsIngreso(true);
			movimientoContableNuevo.setFecha(new Date());
			movimientoContableNuevo.setUsuario(loggedUser);
			movimientoContableNuevo.setValor(gasto.getValor());
			movimientoContableNuevo.setEsPendiente(false);
			movimientoContableServices.createTX(movimientoContableNuevo);
						
		}
		
		if(gasto.getCuenta() == null || gasto.getCuenta().getId() == null){
			gasto.setCuenta(null);
		}else{
			CuentaServices cuentaServices = new CuentaServices();			
			gasto.setCuenta(cuentaServices.findById(gasto.getCuenta().getId()));
		}
		
		service = new HomeServices<>(GastoxHojaNegocio.class);
		service.updateTX(gasto);
		
	}
	
	/**
	 * Método para eliminar un gasto de una hoja de negocio
	 * @param gasto
	 */
	public void deleteGasto(GastoxHojaNegocio gasto, Usuario loggedUser) throws Exception{
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		MovimientoContableServices movimientoContableServices = new MovimientoContableServices();
		MovimientoContable movimientoContable = new MovimientoContable();
		movimientoContable.setCodigo(dateFormat.format(new Date()));
		movimientoContable.setCompania(loggedUser.getCompania());
		movimientoContable.setCuenta(gasto.getCuenta());
		movimientoContable.setDescripcion("Hoja de negocio: " + gasto.getHojaNegocio().getVehiculo().getPlaca() + " - Gasto eliminado - " + gasto.getDescripcion());
		movimientoContable.setEsIngreso(true);
		movimientoContable.setFecha(new Date());
		movimientoContable.setUsuario(loggedUser);
		movimientoContable.setValor(gasto.getValor());
		movimientoContable.setEsPendiente(false);
		movimientoContableServices.createTX(movimientoContable);
		
		HomeServices<GastoxHojaNegocio> service = new HomeServices<GastoxHojaNegocio>(GastoxHojaNegocio.class);
		gasto = service.findById(gasto.getId());
		if(gasto != null){
			service.deleteTX(gasto);
		}
	}
	
	@Override
	public ArrayList<HojaNegocio> findAll(Integer idCia) throws Exception {
		
		ControlQuery cq = new ControlQuery();
		cq.getOrderby().add(new String[]{HojaNegocioDAO.ID, Condition.DESC});
		List<HojaNegocio> entidades = findAll(cq);
		
		ArrayList<HojaNegocio> filteredEnts = new ArrayList<HojaNegocio>();
		for (Iterator<HojaNegocio> itEnts = entidades.iterator(); itEnts.hasNext();) {
			HojaNegocio hojaNegocio = (HojaNegocio) itEnts.next();
			if(hojaNegocio.getFromConsignacion()){
				if(hojaNegocio.getConsignacion().getCompania().getId().equals(idCia)){
					filteredEnts.add(hojaNegocio);
				}
			}else{
				if(hojaNegocio.getFromRetoma()){
					if(hojaNegocio.getRetoma().getCompania().getId().equals(idCia)){
						filteredEnts.add(hojaNegocio);
					}
				}
			}
		}
		
		return new ArrayList<HojaNegocio>(filteredEnts);
	}

	/**
	 * Método para finalizar una hoja de negocio
	 * @param entity
	 */
	public HojaNegocio finalizarHojaNegocioTX(HojaNegocio entity) throws Exception{
		HojaNegocio result = null;
		try {
			
			beginTransaction();	
			
			result = findById(entity.getId());
			
			result.setEstado(com.sh.pvs.model.utils.Constantes.HN_EST_FINALIZADA);
			
			result = merge(result);
			
			getSession().flush();
			
			endTransaction();
		} catch (Exception e) {
			throw e;
		} finally {
			try {
				closeSession();
			} catch (Exception e1) {
				throw e1;
			}
		}
		return result;
	}
	
	@Override
	public List<HojaNegocio> findAllHojasNegocioByFiltro(Date fchIni, Date fchFin, Integer idCia) throws Exception
	{
		
		List<HojaNegocio> hojaNegocios = new ArrayList<>();
		
		if (idCia != null)
		{
			
			ControlQuery cq = new ControlQuery();
			
			if (fchIni != null)
			{
				Condition condFI = new Condition(HojaNegocioDAO.FCHCONSIGNACION, Condition.MAYOROIGUAL, fchIni);
				cq.add(condFI);
			}
			
			if (fchFin != null)
			{
				Condition condFF = new Condition(HojaNegocioDAO.FCHCONSIGNACION, Condition.MENOROIGUAL, fchFin);
				cq.add(condFF);
			}
			
			Condition condFCia = new Condition(HojaNegocioDAO.IDCIACONSIGNACION, Condition.IGUAL, idCia);
			cq.add(condFCia);
			
			Condition condEstado = new Condition(HojaNegocioDAO.ESTADO, Condition.IGUAL, "FI");
			cq.add(condEstado);
			
			try
			{
				hojaNegocios = this.findAll(cq);
			}
			catch (Exception e)
			{
				hojaNegocios = new ArrayList<>();
			}
			
		}
		
		return hojaNegocios;
	}
}
