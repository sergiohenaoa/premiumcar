/**
 * 
 */
package com.sh.pvs.core.services.hojanegocio;

import com.sh.pvs.core.interfaces.hojanegocio.IGastoxHojaNegocioServices;
import com.sh.pvs.core.services.general.HomeServices;
import com.sh.pvs.model.dto.hojanegocio.GastoxHojaNegocio;


public class GastoxHojaNegocioServices extends HomeServices<GastoxHojaNegocio> implements IGastoxHojaNegocioServices {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * Minimal constructor
	 */
	public GastoxHojaNegocioServices() {
		super(GastoxHojaNegocio.class);
	}
	
	/*METHODS*/
}
