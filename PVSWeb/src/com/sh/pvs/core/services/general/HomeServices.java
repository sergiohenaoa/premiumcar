package com.sh.pvs.core.services.general;

import java.io.Serializable;

import com.sh.pvs.core.interfaces.general.IHomeServices;
import com.sh.pvs.model.dao.general.HomeDAO;


public class HomeServices<T> extends HomeDAO<T> implements IHomeServices<T>, Serializable {

	private static final long serialVersionUID = 1L;

	public HomeServices(Class<T> entityClass) {
		super(entityClass);
	}

	/**
	 * Método para crear una entidad utilizando una unidad de trabajo transaccional
	 * @param entity
	 */
	@Override
	public T createTX(T entity) throws Exception{
		T result = null;
		try {
			
			beginTransaction();			
			result = save(entity);						
			endTransaction();
		} catch (Exception e) {
			throw e;
		} finally {
			try {
				closeSession();
			} catch (Exception e1) {
				throw e1;
			}
		}
		return result;
	}

	/**
	 * Método para actualizar una entidad utilizando una unidad de trabajo transaccional
	 * @param entity
	 */
	@Override
	public T updateTX(T entity) throws Exception{
		T result = null;
		try {
			
			beginTransaction();
			result = merge(entity);
			endTransaction();
		} catch (Exception e) {
			throw e;
		} finally {
			try {
				closeSession();
			} catch (Exception e1) {
				throw e1;
			}
		}
		return result;
	}

	/**
	 * Método para eliminar una entidad utilizando una unidad de trabajo transaccional
	 * @param entity
	 */
	@Override
	public void deleteTX(T entity) throws Exception{
		try {
			
			beginTransaction();	
			delete(entity);						
			endTransaction();
		} catch (Exception e) {
			throw e;
		} finally {
			try {
				closeSession();
			} catch (Exception e1) {
				throw e1;
			}
		}		
	}
	
	/**
	 * Método para eliminar una entidad utilizando una unidad de trabajo transaccional y una clave
	 * @param <E>
	 * @param entity
	 */
	@Override
	public <E extends Serializable> void deleteTX(T entity, E key) throws Exception{
		try {
			
			beginTransaction();	
			entity = extracted(entity, key);
			delete(entity);
			endTransaction();
		} catch (Exception e) {
			throw e;
		} finally {
			try {
				closeSession();
			} catch (Exception e1) {
				throw e1;
			}
		}		
	}
	
	/*
	 * Extraccion
	 */
	@SuppressWarnings("unchecked")
	private <E extends Serializable> T extracted(T entity, E pKeyComp) throws Exception {
		T load = (T)getSession().load(entity.getClass(), pKeyComp);
		return load;
	}

}
