/**
 * 
 */
package com.sh.pvs.core.services.retoma;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import com.sh.pvs.core.interfaces.retoma.IRetomaServices;
import com.sh.pvs.core.services.general.HomeServices;
import com.sh.pvs.core.utils.Constantes;
import com.sh.pvs.core.utils.Metodos;
import com.sh.pvs.model.dao.retoma.RetomaDAO;
import com.sh.pvs.model.dto.configuracion.Vehiculo;
import com.sh.pvs.model.dto.hojanegocio.HojaNegocio;
import com.sh.pvs.model.dto.retoma.Retoma;
import com.sh.pvs.model.utils.Condition;
import com.sh.pvs.model.utils.ControlQuery;


public class RetomaServices extends HomeServices<Retoma> implements IRetomaServices {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * Minimal constructor
	 */
	public RetomaServices() {
		super(Retoma.class);
	}
	
	/**
	 * Método para crear una entidad utilizando una unidad de trabajo transaccional
	 * @param entity
	 */
	@Override
	public Retoma createRetomaTX(Retoma entity) throws Exception{
		Retoma result = null;
		try {
			
			if(entity.getCliente() != null && entity.getCliente().getId() == null)
				entity.setCliente(null);
			if(entity.getConcesionario() != null && entity.getConcesionario().getId() == null)
				entity.setConcesionario(null);
			
			beginTransaction();	
			//Se asigna el código
			entity.setCodigo(createCodigo(entity));
			result = save(entity);
			
			//Se activa el vehículo en el inventario
			HomeServices<Vehiculo> serviceVehiculo = new HomeServices<Vehiculo>(Vehiculo.class);
			Vehiculo vehiculo = serviceVehiculo.findById(entity.getVehiculo().getId());
			vehiculo.setEsActivoInventario(true);
			serviceVehiculo.merge(vehiculo);
			serviceVehiculo.getSession().flush();
			
			//Se crea la hoja de negocio
			HomeServices<HojaNegocio> serviceHojaNegocio = new HomeServices<HojaNegocio>(HojaNegocio.class);
			HojaNegocio hojaNegocio = new HojaNegocio();
			hojaNegocio.setRetoma(result);
			hojaNegocio.setVlrVehVendedor(result.getValorVehiculo());
			serviceHojaNegocio.save(hojaNegocio);
			result = merge(result);
			
			endTransaction();
		} catch (Exception e) {
			throw e;
		} finally {
			try {
				closeSession();
			} catch (Exception e1) {
				throw e1;
			}
		}
		return result;
	}
	
	/**
	 * Método para construir el código de la retoma
	 */
	private String createCodigo(Retoma entity) throws Exception{
		
		String codigo = "";
		//Se asigna el código
		String dateStr = new SimpleDateFormat("ddMMyyyy").format(new Date());
		
		ControlQuery cq = new ControlQuery();
		Condition condCia = new Condition(RetomaDAO.IDCOMPANIA, Condition.IGUAL, entity.getCompania().getId()); 
		Condition condFecha = new Condition("DATE("+RetomaDAO.FCHRETOMA+")", Condition.IGUAL, Metodos.removeTime(new Date()));		
		cq.add(condCia);
		cq.addAND(condFecha);
		
		int mayorConsecutivo = 0;
		List<Retoma> retomaes = findAll(cq);
		for (Iterator<Retoma> itRes = retomaes.iterator(); itRes.hasNext();) {
			Retoma retoma = (Retoma) itRes.next();
			
			if(retoma.getCodigo().contains("-")){
				String[] codigoSplitted = retoma.getCodigo().split("-");
				if(codigoSplitted.length == 3){
					int consCodigo = Integer.parseInt(codigoSplitted[2]);
					if(consCodigo > mayorConsecutivo)
						mayorConsecutivo = consCodigo;
				}
			}
		}
		mayorConsecutivo++;
		codigo = Constantes.CODRETOMA + "-" + dateStr + "-" + mayorConsecutivo;
		
		return codigo;
	}
	
	/**
	 * Método para cambiar el estado de una retoma a declinado
	 * @param entity
	 */
	@Override
	public Retoma declinarRetomaTX(Retoma entity) throws Exception{
		Retoma result = null;
		try {
			
			beginTransaction();	
			
			result = findById(entity.getId());
			
			result.setEstado(com.sh.pvs.model.utils.Constantes.RETOMA_ESTADO_DECLINADA_COD);
			
			result = merge(result);
			
			getSession().flush();
			
			//Se activa el vehículo en el inventario
			HomeServices<Vehiculo> serviceVehiculo = new HomeServices<Vehiculo>(Vehiculo.class);
			Vehiculo vehiculo = serviceVehiculo.findById(entity.getVehiculo().getId());
			vehiculo.setEsActivoInventario(false);
			serviceVehiculo.merge(vehiculo);
			serviceVehiculo.getSession().flush();
			
			endTransaction();
		} catch (Exception e) {
			throw e;
		} finally {
			try {
				closeSession();
			} catch (Exception e1) {
				throw e1;
			}
		}
		return result;
	}
	
}
