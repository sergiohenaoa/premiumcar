/**
 * 
 */
package com.sh.pvs.core.services.contabilidad;

import com.sh.pvs.core.interfaces.contabilidad.ICompraVentaAccesoriosServices;
import com.sh.pvs.core.interfaces.contabilidad.IMovimientoContableServices;
import com.sh.pvs.core.interfaces.contabilidad.IReporteContableServices;
import com.sh.pvs.core.interfaces.hojanegocio.IPagoxHojaNegocioServices;
import com.sh.pvs.core.services.general.HomeServices;
import com.sh.pvs.core.services.hojanegocio.PagoxHojaNegocioServices;
import com.sh.pvs.model.dao.contabilidad.CompraVentaAccesoriosDAO;
import com.sh.pvs.model.dao.contabilidad.MovimientoContableDAO;
import com.sh.pvs.model.dao.hojanegocio.PagoxHojaNegocioDAO;
import com.sh.pvs.model.dto.configuracion.Cuenta;
import com.sh.pvs.model.dto.contabilidad.CompraVentaAccesorios;
import com.sh.pvs.model.dto.contabilidad.MovimientoContable;
import com.sh.pvs.model.dto.contabilidad.ReporteContableRow;
import com.sh.pvs.model.dto.hojanegocio.PagoxHojaNegocio;
import com.sh.pvs.model.utils.Condition;
import com.sh.pvs.model.utils.ControlQuery;
import com.sh.pvs.view.utils.Constantes;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;


public class ReporteContableServices extends HomeServices<ReporteContableRow> implements IReporteContableServices{
	
	private static final long serialVersionUID = 1L;

	public ReporteContableServices(Class<ReporteContableRow> entityClass) {
		super(entityClass);
	}

	/**
	 * Método para consultar todos los movimientos contables según los filtros
	 */
	@Override
	public ArrayList<ReporteContableRow> findAllMovtos(Date fchIni,
			Date fchFin, Cuenta cuenta, Integer idCia) throws Exception {
		
		ArrayList<ReporteContableRow> rows = new ArrayList<ReporteContableRow>();
		
		if(idCia != null){
			
			//Se agregan los movimientos contables
			ArrayList<MovimientoContable> movtos = this.getMovtosContables(fchIni, fchFin, cuenta, idCia);
			for (Iterator<MovimientoContable> iterator = movtos.iterator(); iterator.hasNext();) {
				MovimientoContable movimientoContable = iterator.next();
				ReporteContableRow row = new ReporteContableRow();
				row.setCuenta(movimientoContable.getCuenta());
				row.setDescripcion(movimientoContable.getDescripcion());
				row.setFecha(movimientoContable.getFecha());
				
				if(movimientoContable.getEsIngreso()){
					row.setIngreso(movimientoContable.getValor());
				}else{
					row.setEgreso(movimientoContable.getValor());
				}
				
				row.setOrigen(Constantes.ORGMOV_MOVTOCONT);
				
				rows.add(row);
			}
			
			//Se agregan las compraventas de accesorios
			/*ArrayList<CompraVentaAccesorios> compraventas = this.getCompraVentasAccesorios(fchIni, fchFin, cuenta, idCia);
			for (Iterator<CompraVentaAccesorios> iterator = compraventas.iterator(); iterator.hasNext();) {
				CompraVentaAccesorios compraventa = (CompraVentaAccesorios) iterator.next();
				
				ReporteContableRow row = new ReporteContableRow();
				row.setCuenta(compraventa.getCuenta());
				row.setDescripcion(compraventa.getObservaciones());
				row.setFecha(compraventa.getFecha());
				
				if(compraventa.getEsCompra()){
					row.setEgreso(compraventa.getTotal());
				}else{
					row.setIngreso(compraventa.getTotal());
				}
				
				row.setOrigen(Constantes.ORGMOV_COMPRAVENTA);
				
				rows.add(row);
			}*/
			
			//Se agregan las pagos de la hoja de negocio
			/*ArrayList<PagoxHojaNegocio> pagosHn = this.getPagosHN(fchIni, fchFin, cuenta, idCia);
			for (Iterator<PagoxHojaNegocio> iterator = pagosHn.iterator(); iterator.hasNext();) {
				PagoxHojaNegocio pagos = (PagoxHojaNegocio) iterator.next();
				
				ReporteContableRow row = new ReporteContableRow();
				
				row.setDescripcion(pagos.getConcepto());
				row.setFecha(pagos.getFchPago());
				
				if(pagos.getPagadoPor().equals(com.sh.pvs.core.utils.Constantes.CODCIA)){
					row.setEgreso(pagos.getValor());
					row.setCuenta(pagos.getCuentaSalida());
				}else{
					row.setIngreso(pagos.getValor());
					row.setCuenta(pagos.getCuentaEntrada());
				}
				
				row.setOrigen(Constantes.ORGMOV_MOVTOHN);
				
				rows.add(row);
			}*/
		}
		return rows;
	}
	
	/**
	 * Método para cargar todos los movimientos contables*/
	private ArrayList<MovimientoContable> getMovtosContables(Date fchIni, Date fchFin, Cuenta cuenta, Integer idCia){
		
		ArrayList<MovimientoContable> movtos = new ArrayList<MovimientoContable>();
		
		ControlQuery cq = new ControlQuery();
		
		if(fchIni != null){
			Condition condFI = new Condition(MovimientoContableDAO.FECHA, Condition.MAYOROIGUAL, fchIni);
			cq.add(condFI);
		}
		
		if(fchFin != null){
			Condition condFF = new Condition(MovimientoContableDAO.FECHA, Condition.MENOROIGUAL, fchFin);
			cq.add(condFF);
		}
		
		Condition condFCia = new Condition(MovimientoContableDAO.IDCOMPANIA, Condition.IGUAL, idCia);
		cq.add(condFCia);
		
		if(cuenta != null && cuenta.getId() != null && !cuenta.getId().equals(new Integer(0))){
			Condition condFCuenta = new Condition(MovimientoContableDAO.IDCUENTA, Condition.IGUAL, cuenta.getId());
			cq.add(condFCuenta);
		}
		
		try {
			IMovimientoContableServices movtoService = new MovimientoContableServices();
			movtos = (ArrayList<MovimientoContable>)movtoService.findAll(cq);
		} catch (Exception e) {
			movtos = new ArrayList<MovimientoContable>();
		}
		
		return movtos;
	}
	
	/**
	 * Método para cargar todos las compraventas de accesorios*/
	private ArrayList<CompraVentaAccesorios> getCompraVentasAccesorios(Date fchIni, Date fchFin, Cuenta cuenta, Integer idCia){
		
		ArrayList<CompraVentaAccesorios> movtos = new ArrayList<CompraVentaAccesorios>();
		
		ControlQuery cq = new ControlQuery();
		
		if(fchIni != null){
			Condition condFI = new Condition(CompraVentaAccesoriosDAO.FECHA, Condition.MAYOROIGUAL, fchIni);
			cq.add(condFI);
		}
		
		if(fchFin != null){
			Condition condFF = new Condition(CompraVentaAccesoriosDAO.FECHA, Condition.MENOROIGUAL, fchFin);
			cq.add(condFF);
		}
		
		Condition condFCia = new Condition(CompraVentaAccesoriosDAO.IDCIA, Condition.IGUAL, idCia);
		cq.add(condFCia);
		
		if(cuenta != null && cuenta.getId() != null && !cuenta.getId().equals(new Integer(0))){
			Condition condFCuenta = new Condition(CompraVentaAccesoriosDAO.IDCUENTA, Condition.IGUAL, cuenta.getId());
			cq.add(condFCuenta);
		}
		
		try {
			ICompraVentaAccesoriosServices movtoService = new CompraVentaAccesoriosServices();
			movtos = (ArrayList<CompraVentaAccesorios>)movtoService.findAll(cq);
		} catch (Exception e) {
			movtos = new ArrayList<CompraVentaAccesorios>();
		}
		
		return movtos;
	}
	
	/**
	 * Método para cargar todos los pagos de las hojas de negocio*/
	private ArrayList<PagoxHojaNegocio> getPagosHN(Date fchIni, Date fchFin, Cuenta cuenta, Integer idCia){
		
		ArrayList<PagoxHojaNegocio> movtos = new ArrayList<PagoxHojaNegocio>();
		
		ControlQuery cq = new ControlQuery();
		if(fchIni != null){
			Condition condFI = new Condition(PagoxHojaNegocioDAO.FCHPAGO, Condition.MAYOROIGUAL, fchIni);
			cq.add(condFI);
		}
		
		if(fchFin != null){
			Condition condFF = new Condition(PagoxHojaNegocioDAO.FCHPAGO, Condition.MENOROIGUAL, fchFin);
			cq.add(condFF);
		}
		
		if(cuenta != null && cuenta.getId() != null && !cuenta.getId().equals(new Integer(0))){
			ControlQuery cqCuentas = new ControlQuery();
			Condition condFCuentaS = new Condition(PagoxHojaNegocioDAO.IDCUENTASALIDA, Condition.IGUAL, cuenta.getId());
			Condition condFCuentaE = new Condition(PagoxHojaNegocioDAO.IDCUENTAENTRADA, Condition.IGUAL, cuenta.getId());
			cqCuentas.add(condFCuentaS);
			cqCuentas.addOR(condFCuentaE);
			cq.add(cqCuentas);
		}
		
		ControlQuery cqCiaMovto = new ControlQuery();
		Condition condPagPor = new Condition(PagoxHojaNegocioDAO.PAGADOA, Condition.IGUAL, com.sh.pvs.core.utils.Constantes.CODCIA);
		Condition condPagA = new Condition(PagoxHojaNegocioDAO.PAGADOPOR, Condition.IGUAL, com.sh.pvs.core.utils.Constantes.CODCIA);
		cqCiaMovto.add(condPagPor);
		cqCiaMovto.addOR(condPagA);
		cq.add(cqCiaMovto);
		
		try {
			IPagoxHojaNegocioServices movtoService = new PagoxHojaNegocioServices();
			ArrayList<PagoxHojaNegocio> movtosTmp = (ArrayList<PagoxHojaNegocio>)movtoService.findAll(cq);
			for (Iterator<PagoxHojaNegocio> iterator = movtosTmp.iterator(); iterator.hasNext();) {
				PagoxHojaNegocio pagoxHojaNegocio = (PagoxHojaNegocio) iterator.next();
				if(pagoxHojaNegocio.getHojaNegocio().getIdCia().equals(idCia)){
					movtos.add(pagoxHojaNegocio);
				}
				
			}
		} catch (Exception e) {
			movtos = new ArrayList<PagoxHojaNegocio>();
		}
		
		return movtos;
	}
	
}
