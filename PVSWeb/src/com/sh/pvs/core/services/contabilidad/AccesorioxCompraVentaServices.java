/**
 * 
 */
package com.sh.pvs.core.services.contabilidad;

import com.sh.pvs.core.interfaces.contabilidad.IAccesorioxCompraVentaServices;
import com.sh.pvs.core.services.general.HomeServices;
import com.sh.pvs.model.dto.contabilidad.AccesorioxCompraVenta;


public class AccesorioxCompraVentaServices extends HomeServices<AccesorioxCompraVenta> implements IAccesorioxCompraVentaServices {
	
	private static final long serialVersionUID = 1L;

	/**
	 * Minimal constructor
	 */
	public AccesorioxCompraVentaServices() {
		super(AccesorioxCompraVenta.class);
	}
	
	/*METHODS*/
}
