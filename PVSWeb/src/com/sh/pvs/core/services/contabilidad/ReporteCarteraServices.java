/**
 * 
 */
package com.sh.pvs.core.services.contabilidad;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;

import com.sh.pvs.core.interfaces.contabilidad.ICompraVentaAccesoriosServices;
import com.sh.pvs.core.interfaces.contabilidad.IReporteCarteraServices;
import com.sh.pvs.core.interfaces.hojanegocio.IHojaNegocioServices;
import com.sh.pvs.core.services.general.HomeServices;
import com.sh.pvs.core.services.hojanegocio.HojaNegocioServices;
import com.sh.pvs.model.dao.contabilidad.CompraVentaAccesoriosDAO;
import com.sh.pvs.model.dao.hojanegocio.HojaNegocioDAO;
import com.sh.pvs.model.dto.contabilidad.CompraVentaAccesorios;
import com.sh.pvs.model.dto.contabilidad.ReporteCarteraRow;
import com.sh.pvs.model.dto.hojanegocio.HojaNegocio;
import com.sh.pvs.model.utils.Condition;
import com.sh.pvs.model.utils.ControlQuery;
import com.sh.pvs.view.utils.Constantes;


public class ReporteCarteraServices extends HomeServices<ReporteCarteraRow> implements IReporteCarteraServices{
	
	private static final long serialVersionUID = 1L;
	
	public ReporteCarteraServices(Class<ReporteCarteraRow> entityClass) {
		super(entityClass);
	}

	/**
	 * Método para consultar todos los movimientos Carteras según los filtros
	 */
	@Override
	public ArrayList<ReporteCarteraRow> findAllMovtos(Boolean compraPPago, Boolean ventaPCobro, 
			Boolean pagosPVendedor, Boolean cobrosPVendedor, Boolean pagosPComprador, Boolean cobrosPComprador, Integer idCia) throws Exception {
		
		ArrayList<ReporteCarteraRow> rows = new ArrayList<ReporteCarteraRow>();
		
		if(compraPPago){
			//Se agregan los movimientos Carteras
			ArrayList<CompraVentaAccesorios> movtos = this.getMovtosCompraVenta(idCia, true);
			for (Iterator<CompraVentaAccesorios> iterator = movtos.iterator(); iterator.hasNext();) {
				CompraVentaAccesorios movtoCompraVenta = (CompraVentaAccesorios) iterator.next();
				ReporteCarteraRow row = new ReporteCarteraRow();
				
				String tipoMovto = "Compra";
				if(!movtoCompraVenta.getEsCompra())
					tipoMovto = "Venta";
				row.setDescripcion("-Tipo-: " + tipoMovto + " -Proveedor-: " + movtoCompraVenta.getProveedor() + " -Nro Factura-: " + movtoCompraVenta.getNroFactura());
				row.setFecha(movtoCompraVenta.getFecha());
				row.setPlazo(movtoCompraVenta.getPlazo());
				row.setValor(movtoCompraVenta.getTotal());
				
				Calendar venc = Calendar.getInstance();
				venc.setTime(movtoCompraVenta.getFecha());
				venc.add(Calendar.DAY_OF_YEAR, movtoCompraVenta.getPlazo());
				
				row.setFechaV(venc.getTime());
				
				row.setOrigen(Constantes.REPORTECARTERA_COMPRAPPAGO);
				
				rows.add(row);
			}
		}
		
		if(ventaPCobro){
			//Se agregan los movimientos Carteras
			ArrayList<CompraVentaAccesorios> movtos = this.getMovtosCompraVenta(idCia, false);
			for (Iterator<CompraVentaAccesorios> iterator = movtos.iterator(); iterator.hasNext();) {
				CompraVentaAccesorios movtoCompraVenta = (CompraVentaAccesorios) iterator.next();
				ReporteCarteraRow row = new ReporteCarteraRow();
				
				String tipoMovto = "Compra";
				if(!movtoCompraVenta.getEsCompra())
					tipoMovto = "Venta";
				row.setDescripcion("-Tipo-: " + tipoMovto + " -Proveedor-: " + movtoCompraVenta.getProveedor() + " -Nro Factura-: " + movtoCompraVenta.getNroFactura());
				row.setFecha(movtoCompraVenta.getFecha());
				row.setPlazo(movtoCompraVenta.getPlazo());
				row.setValor(movtoCompraVenta.getTotal());
				
				Calendar venc = Calendar.getInstance();
				venc.setTime(movtoCompraVenta.getFecha());
				venc.add(Calendar.DAY_OF_YEAR, movtoCompraVenta.getPlazo());
				
				row.setFechaV(venc.getTime());
				
				row.setOrigen(Constantes.REPORTECARTERA_VENTAPCOBRO);
				
				rows.add(row);
			}
		}
		
		if(pagosPVendedor || cobrosPComprador || cobrosPVendedor || pagosPComprador){
			ArrayList<HojaNegocio> movtos = this.getHojasNegocioPendientes();
			for (Iterator<HojaNegocio> iterator = movtos.iterator(); iterator.hasNext();) {
				HojaNegocio hojaN = (HojaNegocio) iterator.next();
				if(hojaN.getIdCia().equals(idCia)){
					if(pagosPVendedor){
						if(hojaN.getSaldoTotal(com.sh.pvs.core.utils.Constantes.CODVENDEDOR) != 0){
							Double valor = hojaN.getSaldoTotal(com.sh.pvs.core.utils.Constantes.CODVENDEDOR);
							if(valor > 0){
								ReporteCarteraRow row = new ReporteCarteraRow();
								row.setDescripcion("Vehículo: " + hojaN.getVehiculo().getPlaca() + " Vendedor: " + hojaN.getNombreVendedor());
								row.setFecha(new Date());
								row.setPlazo(null);
								row.setValor(valor);
								row.setOrigen(Constantes.REPORTECARTERA_PAGOPVENDEDOR);
								rows.add(row);
							}
						}
					}
					if(cobrosPVendedor){
						if(hojaN.getSaldoTotal(com.sh.pvs.core.utils.Constantes.CODVENDEDOR) != 0){
							Double valor = hojaN.getSaldoTotal(com.sh.pvs.core.utils.Constantes.CODVENDEDOR);
							if(valor < 0){
								ReporteCarteraRow row = new ReporteCarteraRow();
								row.setDescripcion("Vehículo: " + hojaN.getVehiculo().getPlaca() + " Vendedor: " + hojaN.getNombreVendedor());
								row.setFecha(new Date());
								row.setPlazo(null);
								row.setValor(valor);
								row.setOrigen(Constantes.REPORTECARTERA_COBROPVENDEDOR);
								rows.add(row);
							}
						}
					}
					if(pagosPComprador){
						if(hojaN.getSaldoTotal(com.sh.pvs.core.utils.Constantes.CODCOMPRADOR) != 0){
							Double valor = hojaN.getSaldoTotal(com.sh.pvs.core.utils.Constantes.CODCOMPRADOR);
							if(valor > 0){
								ReporteCarteraRow row = new ReporteCarteraRow();
								String descripcion = "Vehículo: " + hojaN.getVehiculo().getPlaca();
								if(hojaN.getVenta() != null){
									descripcion += " Comprador: " + hojaN.getVenta().getCliente().getCedula() + " - " + hojaN.getVenta().getCliente().getNombres();
								}
								row.setDescripcion(descripcion);
								row.setFecha(new Date());
								row.setPlazo(null);
								row.setValor(valor);
								row.setOrigen(Constantes.REPORTECARTERA_PAGOPCOMPRADOR);
								rows.add(row);
							}
						}
					}
					if(cobrosPComprador){
						if(hojaN.getSaldoTotal(com.sh.pvs.core.utils.Constantes.CODCOMPRADOR) != 0){
							Double valor = hojaN.getSaldoTotal(com.sh.pvs.core.utils.Constantes.CODCOMPRADOR);
							if(valor < 0){
								ReporteCarteraRow row = new ReporteCarteraRow();
								String descripcion = "Vehículo: " + hojaN.getVehiculo().getPlaca();
								if(hojaN.getVenta() != null){
									descripcion += " Comprador: " + hojaN.getVenta().getCliente().getCedula() + " - " + hojaN.getVenta().getCliente().getNombres();
								}
								row.setDescripcion(descripcion);
								row.setFecha(new Date());
								row.setPlazo(null);
								row.setValor(valor);
								row.setOrigen(Constantes.REPORTECARTERA_COBROPCOMPRADOR);
								rows.add(row);
							}
						}
					}
				}
			}
		}
			
		return rows;
	}
	
	/**
	 * Método para cargar todos los movimientos Carteras*/
	private ArrayList<CompraVentaAccesorios> getMovtosCompraVenta(Integer idCia, Boolean esCompra){
		
		ArrayList<CompraVentaAccesorios> movtos = new ArrayList<CompraVentaAccesorios>();
		
		ControlQuery cq = new ControlQuery();
		Condition condFCia = new Condition(CompraVentaAccesoriosDAO.IDCIA, Condition.IGUAL, idCia);
		Condition condFPendiente = new Condition(CompraVentaAccesoriosDAO.ESPAGADO, Condition.IGUAL, false);
		
		if(esCompra){
			Condition condCompra = new Condition(CompraVentaAccesoriosDAO.ESCOMPRA, Condition.IGUAL, true);
			cq.add(condCompra);
		}else{
			Condition condVenta = new Condition(CompraVentaAccesoriosDAO.ESCOMPRA, Condition.IGUAL, false);
			cq.add(condVenta);
		}
		
		cq.add(condFCia);
		cq.add(condFPendiente);
		
		try {
			ICompraVentaAccesoriosServices movtoService = new CompraVentaAccesoriosServices();
			movtos = (ArrayList<CompraVentaAccesorios>)movtoService.findAll(cq);
		} catch (Exception e) {
			movtos = new ArrayList<CompraVentaAccesorios>();
		}
		
		return movtos;
	}
	
	/**
	 * Método para cargar todas las hojas de negocio pendientes*/
	private ArrayList<HojaNegocio> getHojasNegocioPendientes(){
		
		ArrayList<HojaNegocio> movtos = new ArrayList<HojaNegocio>();
		
		ControlQuery cq = new ControlQuery();
		Condition condEstado = new Condition(HojaNegocioDAO.ESTADO, Condition.DIFERENTE, com.sh.pvs.model.utils.Constantes.HN_EST_FINALIZADA);
		
		cq.add(condEstado);
		
		try {
			IHojaNegocioServices movtoService = new HojaNegocioServices();
			movtos = (ArrayList<HojaNegocio>)movtoService.findAll(cq);
		} catch (Exception e) {
			movtos = new ArrayList<HojaNegocio>();
		}
		
		return movtos;
	}
	
}
