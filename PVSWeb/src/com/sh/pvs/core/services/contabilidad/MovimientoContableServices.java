/**
 * 
 */
package com.sh.pvs.core.services.contabilidad;

import com.sh.pvs.core.interfaces.contabilidad.IMovimientoContableServices;
import com.sh.pvs.core.services.general.HomeServices;
import com.sh.pvs.model.dto.contabilidad.MovimientoContable;


public class MovimientoContableServices extends HomeServices<MovimientoContable> implements IMovimientoContableServices {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * Minimal constructor
	 */
	public MovimientoContableServices() {
		super(MovimientoContable.class);
	}
	
	/*METHODS*/
}
