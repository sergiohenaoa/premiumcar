/**
 * 
 */
package com.sh.pvs.core.services.contabilidad;

import com.sh.pvs.core.interfaces.contabilidad.ICompraVentaAccesoriosServices;
import com.sh.pvs.core.interfaces.contabilidad.IInventarioAccesoriosServices;
import com.sh.pvs.core.services.general.HomeServices;
import com.sh.pvs.model.dao.contabilidad.CompraVentaAccesoriosDAO;
import com.sh.pvs.model.dao.contabilidad.InventarioAccesoriosDAO;
import com.sh.pvs.model.dto.contabilidad.AccesorioxCompraVenta;
import com.sh.pvs.model.dto.contabilidad.CompraVentaAccesorios;
import com.sh.pvs.model.dto.contabilidad.InventarioAccesorios;
import com.sh.pvs.model.utils.Condition;
import com.sh.pvs.model.utils.ControlQuery;

import java.util.ArrayList;
import java.util.List;


public class CompraVentaAccesoriosServices extends HomeServices<CompraVentaAccesorios> implements ICompraVentaAccesoriosServices {
	
	private static final long serialVersionUID = 1L;

	/**
	 * Minimal constructor
	 */
	public CompraVentaAccesoriosServices() {
		super(CompraVentaAccesorios.class);
	}
	
	/*METHODS*/
	/**
	 * Método para agregar un accesorio a una compraventa
	 */
	public void addAccesorio(AccesorioxCompraVenta accesorioxcompraventa) throws Exception{
		
		HomeServices<AccesorioxCompraVenta> service = new HomeServices<>(null);
		
		Integer accesorioId = accesorioxcompraventa.getAccesorio().getId();
		service.createTX(accesorioxcompraventa);
				
		//Actualización del inventario
		IInventarioAccesoriosServices serviceInventario = new InventarioAccesoriosServices();
		List<InventarioAccesorios> inventario;
		try {
			ControlQuery cq = new ControlQuery();
			Condition cond = new Condition(InventarioAccesoriosDAO.IDACCESORIO, Condition.IGUAL, accesorioId);
			cq.add(cond);
			inventario = serviceInventario.findAll(cq);
		} catch (Exception e) {
			inventario = new ArrayList<>();
		}
		
		if(accesorioxcompraventa.getCompraVentaAccesorios().getEsCompra()){
			if(inventario == null || inventario.size()==0){
				InventarioAccesorios inv = new InventarioAccesorios();
				inv.setAccesorio(accesorioxcompraventa.getAccesorio());
				inv.setCantidadDisponible(accesorioxcompraventa.getCantidad());
				inv.setCompania(accesorioxcompraventa.getCompraVentaAccesorios().getCompania());
				inv.setDescripcion(accesorioxcompraventa.getCompraVentaAccesorios().getNroFactura());
				inv.setEsActivo(true);
				serviceInventario.createTX(inv);
			}else{
				InventarioAccesorios inv = inventario.get(0);
				inv.setCantidadDisponible(inv.getCantidadDisponible() + accesorioxcompraventa.getCantidad());
				inv.setDescripcion(inv.getDescripcion() + " - " + accesorioxcompraventa.getCompraVentaAccesorios().getNroFactura());
				inv.setEsActivo(true);
				serviceInventario.updateTX(inv);
			}
		}else{
			if(inventario != null && inventario.size()>0){
				InventarioAccesorios inv = inventario.get(0);
				inv.setCantidadDisponible(inv.getCantidadDisponible() - accesorioxcompraventa.getCantidad());
				inv.setDescripcion(inv.getDescripcion() + " - " + accesorioxcompraventa.getCompraVentaAccesorios().getNroFactura());
				inv.setEsActivo(true);
				serviceInventario.updateTX(inv);
			}
		}
		
		
	}
	
	/**
	 * Método para actualizar un accesorio de una compraventa
	 */
	public void updateAccesorio(AccesorioxCompraVenta accesorioxcompraventa) throws Exception{
		
		HomeServices<AccesorioxCompraVenta> service = new HomeServices<>(AccesorioxCompraVenta.class);
		AccesorioxCompraVenta accesorioxcompraventaOld = service.findById(accesorioxcompraventa.getId());
		
		service.updateTX(accesorioxcompraventa);
				
		//Actualización del inventario
		Integer accesorioId = accesorioxcompraventa.getAccesorio().getId();
		IInventarioAccesoriosServices serviceInventario = new InventarioAccesoriosServices();
		List<InventarioAccesorios> inventario;
		try {
			ControlQuery cq = new ControlQuery();
			Condition cond = new Condition(InventarioAccesoriosDAO.IDACCESORIO, Condition.IGUAL, accesorioId);
			cq.add(cond);
			inventario = serviceInventario.findAll(cq);
		} catch (Exception e) {
			inventario = new ArrayList<>();
		}
		
		if(accesorioxcompraventa.getCompraVentaAccesorios().getEsCompra()){
			if(inventario != null && inventario.size()>0){
				
				InventarioAccesorios inv = inventario.get(0);
				inv.setCantidadDisponible(inv.getCantidadDisponible() - accesorioxcompraventaOld.getCantidad() + accesorioxcompraventa.getCantidad());
				inv.setDescripcion(inv.getDescripcion() + " - " + accesorioxcompraventa.getCompraVentaAccesorios().getNroFactura());
				inv.setEsActivo(true);
				serviceInventario.updateTX(inv);
			}
		}else{
			if(inventario != null && inventario.size()>0){
				InventarioAccesorios inv = inventario.get(0);
				inv.setCantidadDisponible(inv.getCantidadDisponible() + accesorioxcompraventaOld.getCantidad() - accesorioxcompraventa.getCantidad());
				inv.setDescripcion(inv.getDescripcion() + " - " + accesorioxcompraventa.getCompraVentaAccesorios().getNroFactura());
				inv.setEsActivo(true);
				serviceInventario.updateTX(inv);
			}
		}
	}
	
	/**
	 * Método para eliminar un accesorio de una compraventa
	 */
	public void deleteAccesorio(AccesorioxCompraVenta accesorioxcompraventa) throws Exception{
		//Actualización del inventario
		Integer accesorioId = accesorioxcompraventa.getAccesorio().getId();
		IInventarioAccesoriosServices serviceInventario = new InventarioAccesoriosServices();
		List<InventarioAccesorios> inventario = new ArrayList<>();
		try {
			ControlQuery cq = new ControlQuery();
			Condition cond = new Condition(InventarioAccesoriosDAO.IDACCESORIO, Condition.IGUAL, accesorioId);
			cq.add(cond);
			inventario = serviceInventario.findAll(cq);
		} catch (Exception e) {
			inventario = new ArrayList<InventarioAccesorios>();
		}
		
		if(accesorioxcompraventa.getCompraVentaAccesorios().getEsCompra()){
			if(inventario != null && inventario.size()>0){
				
				InventarioAccesorios inv = inventario.get(0);
				inv.setCantidadDisponible(inv.getCantidadDisponible() - accesorioxcompraventa.getCantidad());
				inv.setDescripcion(inv.getDescripcion() + " - " + accesorioxcompraventa.getCompraVentaAccesorios().getNroFactura());
				inv.setEsActivo(true);
				serviceInventario.updateTX(inv);
			}
		}else{
			if(inventario != null && inventario.size()>0){
				InventarioAccesorios inv = inventario.get(0);
				inv.setCantidadDisponible(inv.getCantidadDisponible() + accesorioxcompraventa.getCantidad());
				inv.setDescripcion(inv.getDescripcion() + " - " + accesorioxcompraventa.getCompraVentaAccesorios().getNroFactura());
				inv.setEsActivo(true);
				serviceInventario.updateTX(inv);
			}
		}
		
		HomeServices<AccesorioxCompraVenta> service = new HomeServices<>(AccesorioxCompraVenta.class);
		accesorioxcompraventa = service.findById(accesorioxcompraventa.getId());
		if(accesorioxcompraventa != null){
			service.deleteTX(accesorioxcompraventa);
		}
		
	}
	
	@Override
	public ArrayList<CompraVentaAccesorios> findAll(Integer idCia) throws Exception {
		
		ControlQuery cq = new ControlQuery();
		cq.getOrderby().add(new String[]{CompraVentaAccesoriosDAO.ID, Condition.DESC});
		List<CompraVentaAccesorios> entidades = findAll(cq);
		
		return new ArrayList<>(entidades);
	}

}
