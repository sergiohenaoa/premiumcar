/**
 * 
 */
package com.sh.pvs.core.services.contabilidad;

import java.util.ArrayList;
import java.util.List;

import com.sh.pvs.core.interfaces.contabilidad.IInventarioAccesoriosServices;
import com.sh.pvs.core.services.general.HomeServices;
import com.sh.pvs.model.dao.contabilidad.InventarioAccesoriosDAO;
import com.sh.pvs.model.dto.contabilidad.InventarioAccesorios;
import com.sh.pvs.model.utils.Condition;
import com.sh.pvs.model.utils.ControlQuery;


public class InventarioAccesoriosServices extends HomeServices<InventarioAccesorios> implements IInventarioAccesoriosServices {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * Minimal constructor
	 */
	public InventarioAccesoriosServices() {
		super(InventarioAccesorios.class);
	}
	
	/*METHODS*/
	@Override
	public ArrayList<InventarioAccesorios> findAllActivosByCia(Integer idCia) throws Exception {
		
		ControlQuery cq = new ControlQuery();		
		Condition condCia = new Condition(InventarioAccesoriosDAO.IDCOMPANIA, Condition.IGUAL, idCia);
		cq.addAND(condCia);
		Condition condEstado = new Condition(InventarioAccesoriosDAO.ES_ACTIVO, Condition.IGUAL, true);
		cq.addAND(condEstado);
		
		String[] orderbyNombre = new String[]{InventarioAccesoriosDAO.NOMBRE, Condition.ASC};
		
		cq.getOrderby().add(orderbyNombre);
		
		List<InventarioAccesorios> entidades = findAll(cq);
		
		return new ArrayList<InventarioAccesorios>(entidades);
	}
	
}
