/**
 * 
 */
package com.sh.pvs.core.services.configuracion;

import java.util.List;

import com.sh.pvs.core.interfaces.configuracion.IConstanteServices;
import com.sh.pvs.core.services.general.HomeServices;
import com.sh.pvs.model.dao.configuracion.ConstanteDAO;
import com.sh.pvs.model.dto.configuracion.Constante;
import com.sh.pvs.model.utils.Condition;
import com.sh.pvs.model.utils.ControlQuery;


public class ConstanteServices extends HomeServices<Constante> implements IConstanteServices {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * Minimal constructor
	 */
	public ConstanteServices() {
		super(Constante.class);
	}
	
	/*METHODS*/
	@Override
	public List<Constante> constantesByCompaniaAndCodigo(int idCompania, String codigo) throws Exception{
		ControlQuery cq = new ControlQuery();
		
		Condition condicionCodigo = new Condition(ConstanteDAO.CODIGO, Condition.IGUAL, codigo);
		cq.add(condicionCodigo);
	
		Condition condicionCompania = new Condition(ConstanteDAO.IDCOMPANIA, Condition.IGUAL, idCompania);
		cq.addAND(condicionCompania);
 		
		return findAll(cq);
	}
	
}
