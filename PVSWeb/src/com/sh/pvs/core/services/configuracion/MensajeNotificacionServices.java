/**
 * 
 */
package com.sh.pvs.core.services.configuracion;

import java.util.ArrayList;
import java.util.List;

import com.sh.pvs.core.interfaces.configuracion.IMensajeNotificacionServices;
import com.sh.pvs.core.services.general.HomeServices;
import com.sh.pvs.model.dao.configuracion.MensajeNotificacionDAO;
import com.sh.pvs.model.dto.configuracion.MensajeNotificacion;
import com.sh.pvs.model.utils.Condition;
import com.sh.pvs.model.utils.ControlQuery;


public class MensajeNotificacionServices extends HomeServices<MensajeNotificacion> implements IMensajeNotificacionServices {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * Minimal constructor
	 */
	public MensajeNotificacionServices() {
		super(MensajeNotificacion.class);
	}
	
	@Override
	public ArrayList<MensajeNotificacion> findByCodigoAndCia(Integer idCia, String codigo) throws Exception {
		
		ControlQuery cq = new ControlQuery();		
		Condition condCia = new Condition(MensajeNotificacionDAO.IDCOMPANIA, Condition.IGUAL, idCia);
		cq.addAND(condCia);
		Condition condCod = new Condition(MensajeNotificacionDAO.CODIGO, Condition.IGUAL, codigo);
		cq.addAND(condCod);
		
		List<MensajeNotificacion> entidades = findAll(cq);
		
		return new ArrayList<MensajeNotificacion>(entidades);
	}
}
