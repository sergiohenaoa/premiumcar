/**
 * 
 */
package com.sh.pvs.core.services.configuracion;

import java.util.ArrayList;
import java.util.List;

import com.sh.pvs.core.interfaces.configuracion.IAccesorioServices;
import com.sh.pvs.core.services.general.HomeServices;
import com.sh.pvs.model.dao.configuracion.AccesorioDAO;
import com.sh.pvs.model.dto.configuracion.Accesorio;
import com.sh.pvs.model.utils.Condition;
import com.sh.pvs.model.utils.ControlQuery;


public class AccesorioServices extends HomeServices<Accesorio> implements IAccesorioServices {
	
	private static final long serialVersionUID = 1L;

	/**
	 * Minimal constructor
	 */
	public AccesorioServices() {
		super(Accesorio.class);
	}
	
	/*METHODS*/
	@Override
	public ArrayList<Accesorio> findAllActivos(Integer idCia) throws Exception {
		
		ControlQuery cq = new ControlQuery();		
		Condition condCia = new Condition(AccesorioDAO.IDCOMPANIA, Condition.IGUAL, idCia);
		cq.addAND(condCia);
		Condition condEstado = new Condition(AccesorioDAO.ESTADO, Condition.IGUAL, true);
		cq.addAND(condEstado);
		
		String[] orderbyNombre = new String[]{AccesorioDAO.NOMBRE, Condition.ASC};
		
		cq.getOrderby().add(orderbyNombre);
		
		List<Accesorio> entidades = findAll(cq);
		
		return new ArrayList<Accesorio>(entidades);
	}
	
}
