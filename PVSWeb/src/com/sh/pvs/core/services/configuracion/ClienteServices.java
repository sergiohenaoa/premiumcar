/**
 * 
 */
package com.sh.pvs.core.services.configuracion;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.sh.pvs.core.interfaces.configuracion.IClienteServices;
import com.sh.pvs.core.services.general.HomeServices;
import com.sh.pvs.model.dao.configuracion.ClienteDAO;
import com.sh.pvs.model.dto.configuracion.Cliente;
import com.sh.pvs.model.utils.Condition;
import com.sh.pvs.model.utils.ControlQuery;


public class ClienteServices extends HomeServices<Cliente> implements IClienteServices {
	
	private static final long serialVersionUID = 1L;

	/**
	 * Minimal constructor
	 */
	public ClienteServices() {
		super(Cliente.class);
	}
	
	/*METHODS*/
	/**
	 * Método para consultar un cliente por cédula y compañía
	 * @param Cédula, Compañía
	 * @return Cliente
	 */
	public Cliente findClienteByCedulaCia(String cedula, Integer idCia) throws Exception{
		
		List<Cliente> clientes = this.findByProperty(ClienteDAO.CEDULA, cedula);
		
		for (Iterator<Cliente> itClientes = clientes.iterator(); itClientes.hasNext();) {
			Cliente cliente = (Cliente) itClientes.next();
			if(cliente.getCompania().getId().equals(idCia))
				return cliente;
		}
		
		return null;
		
	}
	
	@Override
	public ArrayList<Cliente> findAll(Integer idCia) throws Exception {
		
		ControlQuery cq = new ControlQuery();		
		Condition condCia = new Condition(ClienteDAO.IDCOMPANIA, Condition.IGUAL, idCia);
		cq.addAND(condCia);
		
		String[] orderbyNombre = new String[]{ClienteDAO.NOMBRE, Condition.ASC};
		
		cq.getOrderby().add(orderbyNombre);
		
		List<Cliente> entidades = findAll(cq);
		
		return new ArrayList<Cliente>(entidades);
	}
	
}
