/**
 * 
 */
package com.sh.pvs.core.services.configuracion;

import java.util.ArrayList;
import java.util.List;

import com.sh.pvs.core.interfaces.configuracion.IOficinaTransitoServices;
import com.sh.pvs.core.services.general.HomeServices;
import com.sh.pvs.model.dao.configuracion.OficinaTransitoDAO;
import com.sh.pvs.model.dto.configuracion.OficinaTransito;
import com.sh.pvs.model.dto.configuracion.TramitexOficinaTransito;
import com.sh.pvs.model.utils.Condition;
import com.sh.pvs.model.utils.ControlQuery;


public class OficinaTransitoServices extends HomeServices<OficinaTransito> implements IOficinaTransitoServices {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * Minimal constructor
	 */
	public OficinaTransitoServices() {
		super(OficinaTransito.class);
	}
	
	/*METHODS*/
	/**
	 * Método para agregar un trámite a una oficina de tránsito
	 * @param oficinaTransito
	 * @param tramite
	 */
	public void addTramite(OficinaTransito oficinaTransito, TramitexOficinaTransito tramite) throws Exception{
		
		HomeServices<TramitexOficinaTransito> tramiteSrv = new HomeServices<TramitexOficinaTransito>(null);
		tramite.setOficinaTransito(oficinaTransito);
		tramiteSrv.createTX(tramite);
		
	}
	
	/**
	 * Método para eliminar un trámite a una oficina de tránsito
	 * @param oficinaTransito
	 * @param tramite
	 */
	public void deleteTramite(TramitexOficinaTransito tramite) throws Exception{
		
		HomeServices<TramitexOficinaTransito> tramiteSrv = new HomeServices<TramitexOficinaTransito>(TramitexOficinaTransito.class);
		tramite = tramiteSrv.findById(tramite.getId());
		if(tramite != null){
			tramiteSrv.deleteTX(tramite);
		}
		
	}
	
	@Override
	public ArrayList<OficinaTransito> findAllActivos(Integer idCia) throws Exception {
		
		ControlQuery cq = new ControlQuery();		
		Condition condCia = new Condition(OficinaTransitoDAO.IDCOMPANIA, Condition.IGUAL, idCia);
		cq.addAND(condCia);
		Condition condEstado = new Condition(OficinaTransitoDAO.ESTADO, Condition.IGUAL, true);
		cq.addAND(condEstado);
		
		String[] orderbyNombre = new String[]{OficinaTransitoDAO.NOMBRE, Condition.ASC};
		
		cq.getOrderby().add(orderbyNombre);
		
		List<OficinaTransito> entidades = findAll(cq);
		
		return new ArrayList<OficinaTransito>(entidades);
	}
	
}
