/**
 * 
 */
package com.sh.pvs.core.services.configuracion;

import com.sh.pvs.core.interfaces.configuracion.ICarroceriaServices;
import com.sh.pvs.core.services.general.HomeServices;
import com.sh.pvs.model.dto.configuracion.Carroceria;


public class CarroceriaServices extends HomeServices<Carroceria> implements ICarroceriaServices {
	
	private static final long serialVersionUID = 1L;

	/**
	 * Minimal constructor
	 */
	public CarroceriaServices() {
		super(Carroceria.class);
	}
	
	/*METHODS*/
}
