/**
 * 
 */
package com.sh.pvs.core.services.configuracion;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.sh.pvs.core.interfaces.configuracion.IConcesionarioServices;
import com.sh.pvs.core.services.general.HomeServices;
import com.sh.pvs.model.dao.configuracion.ConcesionarioDAO;
import com.sh.pvs.model.dto.configuracion.Concesionario;
import com.sh.pvs.model.utils.Condition;
import com.sh.pvs.model.utils.ControlQuery;


public class ConcesionarioServices extends HomeServices<Concesionario> implements IConcesionarioServices {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * Minimal constructor
	 */
	public ConcesionarioServices() {
		super(Concesionario.class);
	}
	
	/*METHODS*/
	/**
	 * Método para consultar un Concesionario por nit y compañía
	 * @param Nit, Compañía
	 * @return Concesionario
	 */
	public Concesionario findConcesionarioByNitCia(String nit, Integer idCia) throws Exception{
		
		List<Concesionario> concesionarios = this.findByProperty(ConcesionarioDAO.NIT, nit);
		
		for (Iterator<Concesionario> itConcesionarios = concesionarios.iterator(); itConcesionarios.hasNext();) {
			Concesionario concesionario = (Concesionario) itConcesionarios.next();
			if(concesionario.getCompania().getId().equals(idCia))
				return concesionario;
		}
		
		return null;
		
	}

	@Override
	public ArrayList<Concesionario> findAll(Integer idCia) throws Exception {
		
		ControlQuery cq = new ControlQuery();		
		Condition condCia = new Condition(ConcesionarioDAO.IDCOMPANIA, Condition.IGUAL, idCia);
		cq.addAND(condCia);
		
		String[] orderbyNombre = new String[]{ConcesionarioDAO.NOMBRE, Condition.ASC};
		
		cq.getOrderby().add(orderbyNombre);
		
		List<Concesionario> entidades = findAll(cq);
		
		return new ArrayList<Concesionario>(entidades);
	}
	
}
