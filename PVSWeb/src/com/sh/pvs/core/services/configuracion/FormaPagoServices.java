/**
 * 
 */
package com.sh.pvs.core.services.configuracion;

import java.util.ArrayList;
import java.util.List;

import com.sh.pvs.core.interfaces.configuracion.IFormaPagoServices;
import com.sh.pvs.core.services.general.HomeServices;
import com.sh.pvs.model.dao.configuracion.FormaPagoDAO;
import com.sh.pvs.model.dto.configuracion.FormaPago;
import com.sh.pvs.model.utils.Condition;
import com.sh.pvs.model.utils.ControlQuery;


public class FormaPagoServices extends HomeServices<FormaPago> implements IFormaPagoServices {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * Minimal constructor
	 */
	public FormaPagoServices() {
		super(FormaPago.class);
	}
	
	/*METHODS*/
	@Override
	public ArrayList<FormaPago> findAllActivos(Integer idCia) throws Exception {
		
		ControlQuery cq = new ControlQuery();		
		Condition condCia = new Condition(FormaPagoDAO.IDCOMPANIA, Condition.IGUAL, idCia);
		cq.addAND(condCia);
		Condition condEstado = new Condition(FormaPagoDAO.ESTADO, Condition.IGUAL, true);
		cq.addAND(condEstado);
		
		String[] orderbyNombre = new String[]{FormaPagoDAO.NOMBRE, Condition.ASC};
		
		cq.getOrderby().add(orderbyNombre);
		
		List<FormaPago> entidades = findAll(cq);
		
		return new ArrayList<FormaPago>(entidades);
	}
	
}
