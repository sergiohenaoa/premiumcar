/**
 * 
 */
package com.sh.pvs.core.services.configuracion;

import java.util.ArrayList;
import java.util.List;

import com.sh.pvs.core.interfaces.configuracion.ITipoVehiculoServices;
import com.sh.pvs.core.services.general.HomeServices;
import com.sh.pvs.model.dao.configuracion.TipoVehiculoDAO;
import com.sh.pvs.model.dto.configuracion.TipoVehiculo;
import com.sh.pvs.model.utils.Condition;
import com.sh.pvs.model.utils.ControlQuery;


public class TipoVehiculoServices extends HomeServices<TipoVehiculo> implements ITipoVehiculoServices {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * Minimal constructor
	 */
	public TipoVehiculoServices() {
		super(TipoVehiculo.class);
	}
	
	@Override
	public ArrayList<TipoVehiculo> findAllActivos(Integer idCia) throws Exception {
		
		ControlQuery cq = new ControlQuery();		
		Condition condCia = new Condition(TipoVehiculoDAO.IDCOMPANIA, Condition.IGUAL, idCia);
		cq.addAND(condCia);
		Condition condEstado = new Condition(TipoVehiculoDAO.ESTADO, Condition.IGUAL, true);
		cq.addAND(condEstado);
		
		String[] orderbyNombre = new String[]{TipoVehiculoDAO.NOMBRE, Condition.ASC};
		
		cq.getOrderby().add(orderbyNombre);
		
		List<TipoVehiculo> entidades = findAll(cq);
		
		return new ArrayList<TipoVehiculo>(entidades);
	}
	
}
