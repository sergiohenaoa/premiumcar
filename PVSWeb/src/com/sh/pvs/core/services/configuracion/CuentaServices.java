/**
 * 
 */
package com.sh.pvs.core.services.configuracion;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.sh.pvs.core.interfaces.configuracion.ICuentaServices;
import com.sh.pvs.core.services.contabilidad.ReporteContableServices;
import com.sh.pvs.core.services.general.HomeServices;
import com.sh.pvs.model.dao.configuracion.CuentaDAO;
import com.sh.pvs.model.dto.configuracion.Cuenta;
import com.sh.pvs.model.dto.contabilidad.ReporteContableRow;
import com.sh.pvs.model.utils.Condition;
import com.sh.pvs.model.utils.ControlQuery;


public class CuentaServices extends HomeServices<Cuenta> implements ICuentaServices {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * Minimal constructor
	 */
	public CuentaServices() {
		super(Cuenta.class);
	}
	
	/*METHODS*/
	@Override
	public ArrayList<Cuenta> findAllActivos(Integer idCia) throws Exception {
		
		ControlQuery cq = new ControlQuery();		
		Condition condCia = new Condition(CuentaDAO.IDCOMPANIA, Condition.IGUAL, idCia);
		cq.addAND(condCia);
		Condition condEstado = new Condition(CuentaDAO.ESTADO, Condition.IGUAL, true);
		cq.addAND(condEstado);
		
		String[] orderbyNombre = new String[]{CuentaDAO.NOMBRE, Condition.ASC};
		
		cq.getOrderby().add(orderbyNombre);
		
		List<Cuenta> entidades = findAll(cq);
		
		return new ArrayList<Cuenta>(entidades);
	}
	
	@Override
	public ArrayList<Cuenta> findAllActivosConSaldo(Integer idCia) throws Exception {
		
		ReporteContableServices reporteService = new ReporteContableServices(null);
		List<ReporteContableRow> rowList = reporteService.findAllMovtos(null, null, null, idCia);
		
		
		ControlQuery cq = new ControlQuery();		
		Condition condCia = new Condition(CuentaDAO.IDCOMPANIA, Condition.IGUAL, idCia);
		cq.addAND(condCia);
		Condition condEstado = new Condition(CuentaDAO.ESTADO, Condition.IGUAL, true);
		cq.addAND(condEstado);
		
		String[] orderbyNombre = new String[]{CuentaDAO.NOMBRE, Condition.ASC};
		
		cq.getOrderby().add(orderbyNombre);
		
		List<Cuenta> entidades = findAll(cq);
		
		for (Iterator<Cuenta> iterator = entidades.iterator(); iterator.hasNext();) {
			Cuenta cuenta = (Cuenta) iterator.next();
			Double saldoReal = 0.0;
			for (Iterator<ReporteContableRow> rows = rowList.iterator(); rows.hasNext();) {
				ReporteContableRow row = (ReporteContableRow) rows.next();
				if(row.getCuenta() != null){
					if(row.getCuenta().getId().equals(cuenta.getId())){
						if(row.getIngreso() != null)
							saldoReal += row.getIngreso();
						if(row.getEgreso() != null)
							saldoReal -= row.getEgreso();
					}
				}
			}
			cuenta.setSaldoReal(saldoReal);
		}
		
		return new ArrayList<Cuenta>(entidades);
	}
	
}
