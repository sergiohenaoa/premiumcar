/**
 * 
 */
package com.sh.pvs.core.services.configuracion;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.sh.pvs.core.interfaces.configuracion.IVehiculoServices;
import com.sh.pvs.core.services.general.HomeServices;
import com.sh.pvs.model.dao.configuracion.VehiculoDAO;
import com.sh.pvs.model.dao.general.HomeDAO;
import com.sh.pvs.model.dto.configuracion.FotoxVehiculo;
import com.sh.pvs.model.dto.configuracion.Vehiculo;
import com.sh.pvs.model.utils.Condition;
import com.sh.pvs.model.utils.ControlQuery;


public class VehiculoServices extends HomeServices<Vehiculo> implements IVehiculoServices {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * Minimal constructor
	 */
	public VehiculoServices() {
		super(Vehiculo.class);
	}
	
	public Vehiculo actualizarFotos(Vehiculo entity) throws Exception{
		
		Vehiculo persitentVehicle = findById(entity.getId());
		HomeDAO<FotoxVehiculo> fotosDao = new HomeDAO<FotoxVehiculo>(FotoxVehiculo.class);
		
		try {
			
			beginTransaction();			
			
			for (Iterator<FotoxVehiculo> itFotos = persitentVehicle.getFotos().iterator(); itFotos.hasNext();) {
				FotoxVehiculo foto = (FotoxVehiculo) itFotos.next();
				fotosDao.delete(foto);
				getSession().flush();
			}
			
			for (Iterator<FotoxVehiculo> itFotos = entity.getFotos().iterator(); itFotos.hasNext();) {
				FotoxVehiculo foto = (FotoxVehiculo) itFotos.next();
				fotosDao.save(foto);
				getSession().flush();
			}
			
			entity = findById(entity.getId());
			
			endTransaction();
		} catch (Exception e) {
			throw e;
		} finally {
			try {
				closeSession();
			} catch (Exception e1) {
				throw e1;
			}
		}
		return entity;
	}
	
	/**
	 * Método para consultar un vehículo por placa y compañía
	 * @param Placa, Compañía
	 * @return Vehículo
	 */
	public Vehiculo findVehiculoByPlacaCia(String placa, Integer idCia) throws Exception{
		
		List<Vehiculo> vehiculos = this.findByProperty(VehiculoDAO.PLACA, placa);
		
		for (Iterator<Vehiculo> itVehiculos = vehiculos.iterator(); itVehiculos.hasNext();) {
			Vehiculo vehiculo = (Vehiculo) itVehiculos.next();
			if(vehiculo.getCompania().getId().equals(idCia))
				return vehiculo;
		}
		
		return null;
		
	}

	@Override
	public ArrayList<Vehiculo> findAllNoActivosInventario(Integer idCia) throws Exception {
		
		ControlQuery cq = new ControlQuery();		
		Condition condCia = new Condition(VehiculoDAO.IDCOMPANIA, Condition.IGUAL, idCia);
		cq.addAND(condCia);
		
		Condition condEstado = new Condition(VehiculoDAO.ES_ACTIVO_INVENTARIO, Condition.IGUAL, false);
		cq.addAND(condEstado);
		
		String[] orderbyNombre = new String[]{VehiculoDAO.PLACA, Condition.ASC};
		
		cq.getOrderby().add(orderbyNombre);
		
		List<Vehiculo> entidades = findAll(cq);
		
		return new ArrayList<Vehiculo>(entidades);
	}
	
	@Override
	public ArrayList<Vehiculo> findAllActivosInventario(Integer idCia) throws Exception {
		
		ControlQuery cq = new ControlQuery();		
		Condition condCia = new Condition(VehiculoDAO.IDCOMPANIA, Condition.IGUAL, idCia);
		cq.addAND(condCia);
		
		Condition condEstado = new Condition(VehiculoDAO.ES_ACTIVO_INVENTARIO, Condition.IGUAL, true);
		cq.addAND(condEstado);
		
		String[] orderbyNombre = new String[]{VehiculoDAO.PLACA, Condition.ASC};
		
		cq.getOrderby().add(orderbyNombre);
		
		List<Vehiculo> entidades = findAll(cq);
		
		return new ArrayList<Vehiculo>(entidades);
	}
	
}
