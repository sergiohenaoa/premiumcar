/**
 * 
 */
package com.sh.pvs.core.services.configuracion;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.sh.pvs.core.interfaces.configuracion.IMarcaServices;
import com.sh.pvs.core.services.general.HomeServices;
import com.sh.pvs.model.dao.configuracion.MarcaDAO;
import com.sh.pvs.model.dao.configuracion.TipoVehiculoxMarcaDAO;
import com.sh.pvs.model.dao.general.HomeDAO;
import com.sh.pvs.model.dto.configuracion.Marca;
import com.sh.pvs.model.dto.configuracion.TipoVehiculoxMarca;
import com.sh.pvs.model.utils.Condition;
import com.sh.pvs.model.utils.ControlQuery;


public class MarcaServices extends HomeServices<Marca> implements IMarcaServices {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * Minimal constructor
	 */
	public MarcaServices() {
		super(Marca.class);
	}
	
	/*METHODS*/
	/**
	 * Método para agregar un tipo de vehículo a una marca
	 * @param marca
	 * @param tipoVehiculo
	 */
	public void addTipoVehiculo(Marca marca, TipoVehiculoxMarca tipoVehiculoxMarca) throws Exception{
		
		HomeServices<TipoVehiculoxMarca> tiposVSrv = new HomeServices<TipoVehiculoxMarca>(null);
		tipoVehiculoxMarca.setMarca(marca);
		tiposVSrv.createTX(tipoVehiculoxMarca);
		
	}
	
	/**
	 * Método para eliminar un tipo de vehículo de una marca
	 * @param tipoVehiculo
	 */
	public void deleteTipoVehiculo(TipoVehiculoxMarca tipoVehiculoxMarca) throws Exception{
		
		HomeServices<TipoVehiculoxMarca> tiposVSrv = new HomeServices<TipoVehiculoxMarca>(TipoVehiculoxMarca.class);
		tipoVehiculoxMarca = tiposVSrv.findById(tipoVehiculoxMarca.getId());
		if(tipoVehiculoxMarca != null){
			tiposVSrv.deleteTX(tipoVehiculoxMarca);
		}
		
	}
	
	@Override
	public ArrayList<Marca> findAllActivos(Integer idCia) throws Exception {
		
		ControlQuery cq = new ControlQuery();		
		Condition condCia = new Condition(MarcaDAO.IDCOMPANIA, Condition.IGUAL, idCia);
		cq.addAND(condCia);
		Condition condEstado = new Condition(MarcaDAO.ESTADO, Condition.IGUAL, true);
		cq.addAND(condEstado);
		
		String[] orderbyNombre = new String[]{MarcaDAO.NOMBRE, Condition.ASC};
		
		cq.getOrderby().add(orderbyNombre);
		
		List<Marca> entidades = findAll(cq);
		
		return new ArrayList<Marca>(entidades);
	}
	
	@Override
	public ArrayList<Marca> findAllActivosByTipoVehiculo(Integer idCia, Integer idTipoV) throws Exception {
		
		//Buscar todas las marcas por tipo de vehículo
		HomeDAO<TipoVehiculoxMarca> tipoVxMarcadao = new HomeDAO<TipoVehiculoxMarca>(TipoVehiculoxMarca.class);
		ControlQuery cq = new ControlQuery();		
		Condition condCia = new Condition(TipoVehiculoxMarcaDAO.IDCIAMARCA, Condition.IGUAL, idCia);
		cq.addAND(condCia);
		Condition condEstado = new Condition(TipoVehiculoxMarcaDAO.ESTADOMARCA, Condition.IGUAL, true);
		cq.addAND(condEstado);
		Condition condTipoV = new Condition(TipoVehiculoxMarcaDAO.IDTIPOVEHICULO, Condition.IGUAL, idTipoV);
		cq.addAND(condTipoV);
		
		String[] orderbyNombre = new String[]{TipoVehiculoxMarcaDAO.NOMBREMARCA, Condition.ASC};
		
		cq.getOrderby().add(orderbyNombre);
		
		List<TipoVehiculoxMarca> entidades = tipoVxMarcadao.findAll(cq);
		
		ArrayList<Marca> marcas = new ArrayList<Marca>();
		for (Iterator<TipoVehiculoxMarca> iterator = entidades.iterator(); iterator.hasNext();) {
			TipoVehiculoxMarca tipoVehiculoxMarca = (TipoVehiculoxMarca) iterator.next();
			marcas.add(tipoVehiculoxMarca.getMarca());
		}
		
		return marcas;
	}
}
