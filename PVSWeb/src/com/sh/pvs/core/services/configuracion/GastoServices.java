/**
 * 
 */
package com.sh.pvs.core.services.configuracion;

import java.util.ArrayList;
import java.util.List;

import com.sh.pvs.core.interfaces.configuracion.IGastoServices;
import com.sh.pvs.core.services.general.HomeServices;
import com.sh.pvs.core.utils.Constantes;
import com.sh.pvs.model.dao.configuracion.GastoDAO;
import com.sh.pvs.model.dto.configuracion.Gasto;
import com.sh.pvs.model.utils.Condition;
import com.sh.pvs.model.utils.ControlQuery;


public class GastoServices extends HomeServices<Gasto> implements IGastoServices {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * Minimal constructor
	 */
	public GastoServices() {
		super(Gasto.class);
	}
	
	/*METHODS*/
	@Override
	public ArrayList<Gasto> findAllActivosByCiaAndTraspaso(Integer idCia) throws Exception {
		
		ControlQuery cq = new ControlQuery();		
		Condition condCia = new Condition(GastoDAO.IDCOMPANIA, Condition.IGUAL, idCia);
		cq.addAND(condCia);
		Condition condEstado = new Condition(GastoDAO.ES_ACTIVO, Condition.IGUAL, true);
		cq.addAND(condEstado);
		Condition condTipoGasto = new Condition(GastoDAO.TIPOGASTO, Condition.IGUAL, Constantes.CODTIPOGASTOTRASPASO);
		cq.addAND(condTipoGasto);
		
		String[] orderbyNombre = new String[]{GastoDAO.NOMBRE, Condition.ASC};
		
		cq.getOrderby().add(orderbyNombre);
		
		List<Gasto> entidades = findAll(cq);
		
		return new ArrayList<Gasto>(entidades);
	}
	
	@Override
	public ArrayList<Gasto> findAllActivosByCiaAndAdicional(Integer idCia) throws Exception {
		
		ControlQuery cq = new ControlQuery();		
		Condition condCia = new Condition(GastoDAO.IDCOMPANIA, Condition.IGUAL, idCia);
		cq.addAND(condCia);
		Condition condEstado = new Condition(GastoDAO.ES_ACTIVO, Condition.IGUAL, true);
		cq.addAND(condEstado);
		Condition condTipoGasto = new Condition(GastoDAO.TIPOGASTO, Condition.IGUAL, Constantes.CODTIPOGASTOADICIONAL);
		cq.addAND(condTipoGasto);
		
		String[] orderbyNombre = new String[]{GastoDAO.NOMBRE, Condition.ASC};
		
		cq.getOrderby().add(orderbyNombre);
		
		List<Gasto> entidades = findAll(cq);
		
		return new ArrayList<Gasto>(entidades);
	}
	
	@Override
	public ArrayList<Gasto> findAllActivosByCia(Integer idCia) throws Exception {
		
		ControlQuery cq = new ControlQuery();		
		Condition condCia = new Condition(GastoDAO.IDCOMPANIA, Condition.IGUAL, idCia);
		cq.addAND(condCia);
		Condition condEstado = new Condition(GastoDAO.ES_ACTIVO, Condition.IGUAL, true);
		cq.addAND(condEstado);
		
		String[] orderbyNombre = new String[]{GastoDAO.NOMBRE, Condition.ASC};
		
		cq.getOrderby().add(orderbyNombre);
		
		List<Gasto> entidades = findAll(cq);
		
		return new ArrayList<Gasto>(entidades);
	}
	
}
