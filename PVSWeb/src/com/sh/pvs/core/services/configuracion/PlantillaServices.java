/**
 * 
 */
package com.sh.pvs.core.services.configuracion;

import java.util.ArrayList;
import java.util.List;

import com.sh.pvs.core.interfaces.configuracion.IPlantillaServices;
import com.sh.pvs.core.services.general.HomeServices;
import com.sh.pvs.model.dao.configuracion.PlantillaDAO;
import com.sh.pvs.model.dto.configuracion.Plantilla;
import com.sh.pvs.model.utils.Condition;
import com.sh.pvs.model.utils.ControlQuery;


public class PlantillaServices extends HomeServices<Plantilla> implements IPlantillaServices {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * Minimal constructor
	 */
	public PlantillaServices() {
		super(Plantilla.class);
	}
	
	/*METHODS*/
	public ArrayList<Plantilla> findAllActivosByProceso(Integer idCia, String proceso) throws Exception {
		
		ControlQuery cq = new ControlQuery();		
		Condition condCia = new Condition(PlantillaDAO.IDCOMPANIA, Condition.IGUAL, idCia);
		cq.addAND(condCia);
		Condition condEstado = new Condition(PlantillaDAO.ESTADO, Condition.IGUAL, true);
		cq.addAND(condEstado);
		Condition condProceso = new Condition(PlantillaDAO.PROCESO, Condition.IGUAL, proceso);
		cq.addAND(condProceso);
		
		String[] orderbyNombre = new String[]{PlantillaDAO.NOMBRE, Condition.ASC};
		
		cq.getOrderby().add(orderbyNombre);
		
		List<Plantilla> entidades = findAll(cq);
		
		return new ArrayList<Plantilla>(entidades);
	}
}
