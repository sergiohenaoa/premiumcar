package com.sh.pvs.core.utils;

import com.sh.pvs.utils.emailnotifier.Notifier;

public class SendMailThreadGenerico extends Thread {
	
	private String asunto;
	private String mensajeStr;
	private String correo;
	
	
	public SendMailThreadGenerico(String asunto, String mensajeStr,
			String correo) {
		super();
		this.asunto = asunto;
		this.mensajeStr = mensajeStr;
		this.correo = correo;
	}

	@Override
	public void run(){
		try {
			Notifier notificador = new Notifier(asunto, mensajeStr, correo);
			notificador.createSendMsg();
		} catch (Exception e) {
		}
	}
	
	
	public String getMensajeStr() {
		return mensajeStr;
	}

	public void setMensajeStr(String mensajeStr) {
		this.mensajeStr = mensajeStr;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getAsunto() {
		return asunto;
	}

	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}

}
