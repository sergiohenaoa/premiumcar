package com.sh.pvs.core.utils;

public class Constantes {

	/*CÓDIGO DE ENTIDADES*/
	public static final String CODCONSIGNACION = "C";
	public static final String CODRETOMA = "R";
	public static final String CODVENTA = "V";
	public static final String CODHOJANEGOCIO = "H";
	public static final String CODVENTATERCERO = "VT";
	public static final String CODMOVTOTALLER = "MT";
	public static final String CODCOMPRAVENTAACCESORIOS = "CVA";
	
	/*CÓDIGOS PARA ASIGNADO A*/
	public static final String CODCOMPRADOR = "CO";
	public static final String CODVENDEDOR = "VE";
	public static final String CODCIA = "CI";
	
	/*CÓDIGOS PARA TIPO GASTO*/
	public static final String CODTIPOGASTOTRASPASO = "TR";
	public static final String CODTIPOGASTOADICIONAL = "AD";
	
}
