/**
 * Metodos.java:
 * 
 * 	Esta clase contiene diferentes métodos que serán utilizadas en 
 * 	el paquete de lógica de la aplicación. (Core Utils)
 *
 * @author Sergio Henao, 06/03/2014
 * @Modifier Sergio Henao, 06/03/2014
 * @version 1.0
 * 
 */

package com.sh.pvs.core.utils;

import java.security.Key;
import java.util.Calendar;
import java.util.Date;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import com.sh.pvs.utils.exceptioncontrol.ControlException;

public class Metodos {
	
	// GENERALES
	private static final String CIPHER_TYPE = "DES/ECB/PKCS5Padding";
	
	/**
	 * Generar una clave base para ser usado en método cipher
     * 
     *  @return
     *  @exception
     */
	public static Key generateKey() throws Exception{
		
		try {
			
			String base = "-shpvs*2";
			byte raw[] = base.getBytes();
			SecretKey key = new SecretKeySpec( raw, "DES" );
			return key;
			
		} catch (Exception e) {
			throw ControlException.formatException("TECNICA", Metodos.class.getName(), "", "generateKey", e);
		}
		
	}
	
	/**
	 * Proceso de desencripción por medio de método cipher
   	 * 
   	 * @param password
   	 * @param key
   	 * @return
   	 */
	
	public static String decrypt(String password, Key key) throws Exception {
		try {
			
			BASE64Decoder decoder = new BASE64Decoder();
			byte encrypted[] = decoder.decodeBuffer(password);
			Cipher cipher = Cipher.getInstance(CIPHER_TYPE);
			cipher.init(Cipher.DECRYPT_MODE, key);
			byte[] outputBytes = cipher.doFinal( encrypted );
			String ret = new String( outputBytes );
			
			return ret;
		}catch (Exception e) {
			throw ControlException.formatException("TECNICA", Metodos.class.getName(), "", "decrypt", e);
		}
	}
	
	/**
	 * Encrypt by cipher
	 * 
	 * @param password
	 * @param key
	 * @return
	 */
	public static String encrypt(String password, Key key) throws Exception {
		try {
			Cipher cipher = Cipher.getInstance(CIPHER_TYPE);
			cipher.init(Cipher.ENCRYPT_MODE, key);

			byte[] outputBytes = cipher.doFinal(password.getBytes());

			BASE64Encoder encoder = new BASE64Encoder();
			String base64 = encoder.encode(outputBytes);

			return base64;
		} catch (Exception e) {
			throw ControlException.formatException("TECNICA", Metodos.class.getName(), "", "encrypt", e);
		}
	}
	
	public static Date removeTime(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }
	
}
