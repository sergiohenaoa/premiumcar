/**
 * 
 * 	Esta clase corresponde a la clase de un comparador generico
 *
 * @author Jhoel Acosta - jhoel.acosta@e-deas.com.co - 17/03/2015
 * @Modifier Jhoel Acosta - jhoel.acosta@e-deas.com.co - 17/03/2015
 * @version 1.0
 * 
 */
package com.sh.pvs.core.utils;

import java.lang.reflect.Field;
import java.util.Comparator;

import org.primefaces.model.SortOrder;

public class LazySorter<T> implements Comparator<T> {
	
	private String 		sortField;
    private SortOrder 	sortOrder;
    private Class<T>    clase;
     
    public LazySorter(String sortField, SortOrder sortOrder, Class<T> classe) {
        this.sortField = sortField;
        this.sortOrder = sortOrder;
        this.clase=classe;
    }

	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public int compare(T o1, T o2) {
		int value=0;
		try {
			Field field1= clase.getSuperclass().getDeclaredField(this.sortField);
			field1.setAccessible(true);
            Object value1 = field1.get(o1);
            Object value2 = field1.get(o2);
 
            value = ((Comparable)value1).compareTo(value2);
             
            return SortOrder.ASCENDING.equals(sortOrder) ? value : -1 * value;
        }
        catch(Exception e) {
            return 1* value;
        }
	}

}
