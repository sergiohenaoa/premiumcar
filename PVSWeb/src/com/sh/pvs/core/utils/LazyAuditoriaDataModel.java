/**
 * 	AuditoriaMBean.java:
 * 
 * 	Esta clase corresponde al servicio para construir el filtro de la consulta
 * 	de auditoria usando la clase LazyDataModel de primefaces
 *
 * @author Jhoel Acosta - jhoel.acosta@e-deas.com.co - 17/03/2015
 * @Modifier Jhoel Acosta - jhoel.acosta@e-deas.com.co - 17/03/2015
 * @version 1.0
 * 
 */
package com.sh.pvs.core.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.SerializationUtils;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.sh.pvs.core.interfaces.seguridad.IAuditoriaServices;
import com.sh.pvs.core.services.seguridad.AuditoriaServices;
import com.sh.pvs.model.dto.seguridad.Auditoria;
import com.sh.pvs.model.utils.Condition;
import com.sh.pvs.model.utils.ControlQuery;

public class LazyAuditoriaDataModel extends LazyDataModel<Auditoria> {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8448252796900540676L;
	private ControlQuery cq;

    
    public LazyAuditoriaDataModel(ControlQuery pControlQuery) {
    	this.cq=pControlQuery;
    }

    @Override
	public List<Auditoria> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters) {
		ControlQuery cq = SerializationUtils.clone(this.cq);
        List<Auditoria> data = new ArrayList<Auditoria>();
        IAuditoriaServices entityService;
        try {
        	entityService = new AuditoriaServices();
        	
        	if (filters != null && !filters.isEmpty()) {
				for (String value : filters.keySet()) {
					Condition filtro = new Condition();
					filtro.setPropiedad(value);
					filtro.setOperador(Condition.LIKE);
					filtro.setValue("%" + filters.get(value) + "%");
					cq.add(filtro);
				}
			}
        	
        	//sort
        	if(sortField != null){
				String[] order=new String[2];
				order[0]=sortField;
				if(sortOrder.name().equals("ASCENDING")){
					order[1]=Condition.ASC;
				}else{
					order[1]=Condition.DESC;
				}
				cq.getOrderby().add(order);
			}
        	
        	data = entityService.findAll(cq, first, pageSize);
			
        	
        	//rowCount
            this.setRowCount(data.size());
            
            
        	
        	
			
		} catch (Exception e) {
			e.printStackTrace();
		}
                
        return data;
    }

}
