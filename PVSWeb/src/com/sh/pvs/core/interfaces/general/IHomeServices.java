package com.sh.pvs.core.interfaces.general;

import java.io.Serializable;
import java.util.List;

import com.sh.pvs.model.utils.ControlQuery;

public interface IHomeServices<T>{

	/* Querys */
	public List<T> findAll() throws Exception;
	
	public List<T> findAll(int start,int countData) throws Exception;
	
	public List<T> findAll(ControlQuery cq) throws Exception;
	
	public List<T> findAll(ControlQuery cq,int start,int countData) throws Exception;
	
	public List<Object[]> findAllByNativeQuery(String query)  throws Exception;
	
	public List<Object[]> findAllByNativeQuery(String query, int maxresults)  throws Exception;
	
	public T findById(Integer id) throws Exception;
	
	public T findById(String id) throws Exception;
	
	public List<T> findByProperty(String propertyName, Object value) throws Exception;

	/* Transactional */
	public T createTX(T entity)throws Exception;
	
	public T updateTX(T entity)throws Exception;
	
	public void deleteTX(T entity)throws Exception;
	
	public <E extends Serializable> void deleteTX(T entity, E key) throws Exception;

	
}
