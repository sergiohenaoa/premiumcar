/**
 * 
 */
package com.sh.pvs.core.interfaces.consignacion;

import com.sh.pvs.core.interfaces.general.IHomeServices;
import com.sh.pvs.model.dto.consignacion.Consignacion;

public interface IConsignacionServices extends IHomeServices<Consignacion> {

	/**
	 * Método para guardar una consignación y asignar el código
	 * @param consignacion
	 */
	public Consignacion createConsignacionTX(Consignacion consignacion) throws Exception;
	
	/**
	 * Método para declinar una consignación
	 * @param consignacion
	 */
	public Consignacion declinarConsignacionTX(Consignacion consignacion) throws Exception;
	
}
