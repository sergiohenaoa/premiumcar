/**
 * IFormularioServices.java:
 * 
 * 	Esta interfaz contiene los métodos que se exponen para le entidad formulario
 * 	hereda de la interfaz IHomeServices que expone los métodos básicos de todas las entidades
 *
 * @author Sergio Henao, 06/03/2014
 * @Modifier Sergio Henao, 06/03/2014
 * @version 1.0
 * 
 */
package com.sh.pvs.core.interfaces.seguridad;

import java.util.ArrayList;

import com.sh.pvs.core.interfaces.general.IHomeServices;
import com.sh.pvs.model.dto.seguridad.Formulario;

public interface IFormularioServices extends IHomeServices<Formulario> {

	/* EXPOSED METHODS */
	public ArrayList<Formulario> findEncabezadosMenu(Integer idCia) throws Exception;

	public ArrayList<Formulario> findAllActivos(Integer idCia) throws Exception;
	
	public ArrayList<Formulario> findAll(Integer idCia) throws Exception;

	public Formulario crearFormularioTX(Formulario ent) throws Exception;

	public Formulario actualizarFormularioTX(Formulario ent) throws Exception;

	public void borrarFormularioTX(Formulario ent) throws Exception;

	public ArrayList<Formulario> findAllActivosAndNoFormularioAndNoIdentidad(Integer idFormulario, Integer idCia) throws Exception;
	
	public Formulario findById(Integer idElement, Integer idCia) throws Exception;

}
