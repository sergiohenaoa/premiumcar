/**
 * IPerfilServices.java:
 * 
 * 	Esta interfaz contiene los métodos que se exponen para le entidad perfil
 * 	hereda de la interfaz IHomeServices que expone los métodos básicos de todas las entidades
 *
 * @author Sergio Henao, 06/03/2014
 * @Modifier Sergio Henao, 06/03/2014
 * @version 1.0
 * 
 */
package com.sh.pvs.core.interfaces.seguridad;

import java.util.List;

import com.sh.pvs.core.interfaces.general.IHomeServices;
import com.sh.pvs.model.dto.seguridad.Perfil;

public interface IPerfilServices extends IHomeServices<Perfil> {

	/* EXPOSED METHODS */

	List<Perfil> PerfilesActivosByCompania(int idCompania) throws Exception;

	List<Perfil> perfilByNombreAndCompania(int idCompania, String nombre)
			throws Exception;

}
