/**
 * 
 */
package com.sh.pvs.core.interfaces.seguridad;

import java.util.Date;
import java.util.List;

import org.primefaces.model.LazyDataModel;

import com.sh.pvs.core.interfaces.general.IHomeServices;
import com.sh.pvs.model.dto.seguridad.Auditoria;
import com.sh.pvs.model.dto.seguridad.Usuario;

/**
 * @author andress
 *
 */
public interface IAuditoriaServices extends IHomeServices<Auditoria> {

	/* EXPOSED METHODS */
	public void registrarAuditoria(Usuario user, String accion, String formulario, String registro, String observaciones) throws Exception;

	public List<Auditoria> findAllAuditoriaByFilters(Date fechaIni, Date fechaFin, Auditoria entity, Integer idCia) throws Exception;

	public LazyDataModel<Auditoria> findAllLazyAuditoriaByFilters(Date fechaIni, Date fechaFin, Auditoria entity, Integer idCia);

}
