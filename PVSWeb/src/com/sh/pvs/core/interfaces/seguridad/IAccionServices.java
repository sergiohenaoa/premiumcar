package com.sh.pvs.core.interfaces.seguridad;

import com.sh.pvs.core.interfaces.general.IHomeServices;
import com.sh.pvs.model.dto.seguridad.Accion;


public interface IAccionServices extends IHomeServices<Accion>{

	/*EXPOSED METHODS*/
}
