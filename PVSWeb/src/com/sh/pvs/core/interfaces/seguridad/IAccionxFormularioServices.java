package com.sh.pvs.core.interfaces.seguridad;

import java.util.ArrayList;
import java.util.List;

import com.sh.pvs.core.interfaces.general.IHomeServices;
import com.sh.pvs.model.dto.seguridad.AccionxFormulario;
import com.sh.pvs.model.dto.seguridad.Formulario;


public interface IAccionxFormularioServices extends IHomeServices<AccionxFormulario>{

	/*EXPOSED METHODS*/
	public ArrayList<AccionxFormulario> findAllByFormulario(Formulario form) throws Exception;

	List<AccionxFormulario> findByAccionAndFormulario(int idAccion, int idFormulario) throws Exception;
}
