package com.sh.pvs.core.interfaces.seguridad;

import com.sh.pvs.core.interfaces.general.IHomeServices;
import com.sh.pvs.model.dto.seguridad.Permiso;


public interface IPermisoServices extends IHomeServices<Permiso>{

	/*EXPOSED METHODS*/
	public Permiso crearPermisoTX(Integer idperfil, Integer idform, String[] acciones) throws Exception;
	public void borrarPermisosByFormTX(Integer idperfil, String nombreform, Integer idCia) throws Exception;
}
