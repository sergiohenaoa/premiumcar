package com.sh.pvs.core.interfaces.seguridad;

import java.util.ArrayList;

import com.sh.pvs.core.interfaces.general.IHomeServices;
import com.sh.pvs.model.dto.seguridad.Compania;


public interface ICompaniaServices extends IHomeServices<Compania>{

	/*EXPOSED METHODS*/
	/**
	 * Método que lista todas las compañías activas ordenadas por nombre
	 * @return
	 * @throws Exception
	 */
	public ArrayList<Compania> findAllActivasOrdenados() throws Exception;
	
}
