/**
 * IPerfilServices.java:
 * 
 * 	Esta interfaz contiene los métodos que se exponen para le entidad perfil
 * 	hereda de la interfaz IHomeServices que expone los métodos básicos de todas las entidades
 *
 * @author Sergio Henao, 06/03/2014
 * @Modifier Sergio Henao, 06/03/2014
 * @version 1.0
 * 
 */
package com.sh.pvs.core.interfaces.seguridad;

import com.sh.pvs.core.interfaces.general.IHomeServices;
import com.sh.pvs.model.dto.seguridad.PerfilxUsuario;

public interface IPerfilxUsuarioServices extends IHomeServices<PerfilxUsuario> {

	/* EXPOSED METHODS */

}
