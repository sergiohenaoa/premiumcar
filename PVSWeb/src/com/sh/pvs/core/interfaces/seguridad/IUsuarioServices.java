/**
 * IUsuarioServices.java:
 * 
 * 	Esta interfaz contiene los métodos que se exponen para le entidad usuario
 * 	hereda de la interfaz IHomeServices que expone los métodos básicos de todas las entidades
 *
 * @author Sergio Henao, 06/03/2014
 * @Modifier Sergio Henao, 06/03/2014
 * @version 1.0
 * 
 */
package com.sh.pvs.core.interfaces.seguridad;

import java.util.List;

import com.sh.pvs.core.interfaces.general.IHomeServices;
import com.sh.pvs.model.dto.seguridad.Usuario;

public interface IUsuarioServices extends IHomeServices<Usuario> {

	/* EXPOSED METHODS */
	public Boolean validarLogin(Usuario entity) throws Exception;

	public Boolean reenviarPw(Usuario entity) throws Exception;

	public Boolean cambiarPw(Usuario usr, String pwanterior, String pwnueva,
			String pwconfirm) throws Exception;

	public Usuario crearUsuarioTX(Usuario ent) throws Exception;

	public Boolean restituriPw(Usuario ent) throws Exception;

	List<Usuario> UsuariosActivosByCompania(int idCompania) throws Exception;

}
