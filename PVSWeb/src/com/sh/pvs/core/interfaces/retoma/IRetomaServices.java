/**
 * 
 */
package com.sh.pvs.core.interfaces.retoma;

import com.sh.pvs.core.interfaces.general.IHomeServices;
import com.sh.pvs.model.dto.retoma.Retoma;

public interface IRetomaServices extends IHomeServices<Retoma> {

	/**
	 * Método para guardar una retoma y asignar el código
	 * @param retoma
	 */
	public Retoma createRetomaTX(Retoma retoma) throws Exception;
	
	/**
	 * Método para declinar una retoma
	 * @param retoma
	 */
	public Retoma declinarRetomaTX(Retoma retoma) throws Exception;
}
