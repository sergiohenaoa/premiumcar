/**
 * 
 */
package com.sh.pvs.core.interfaces.configuracion;

import java.util.ArrayList;

import com.sh.pvs.core.interfaces.general.IHomeServices;
import com.sh.pvs.model.dto.configuracion.Cuenta;

public interface ICuentaServices extends IHomeServices<Cuenta> {

	public ArrayList<Cuenta> findAllActivos(Integer idCia) throws Exception;
	
	public ArrayList<Cuenta> findAllActivosConSaldo(Integer idCia) throws Exception;
	
}
