/**
 * 
 */
package com.sh.pvs.core.interfaces.configuracion;

import java.util.ArrayList;

import com.sh.pvs.core.interfaces.general.IHomeServices;
import com.sh.pvs.model.dto.configuracion.Cliente;

public interface IClienteServices extends IHomeServices<Cliente> {

	/**
	 * Método para consultar un cliente por cédula y compañía
	 * @param Cédula, Compañía
	 * @return Cliente
	 */
	public Cliente findClienteByCedulaCia(String cedula, Integer idCia) throws Exception;
	
	public ArrayList<Cliente> findAll(Integer idCia) throws Exception;
}
