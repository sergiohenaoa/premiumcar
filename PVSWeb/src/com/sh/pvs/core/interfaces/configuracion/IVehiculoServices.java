/**
 * 
 */
package com.sh.pvs.core.interfaces.configuracion;

import java.util.ArrayList;

import com.sh.pvs.core.interfaces.general.IHomeServices;
import com.sh.pvs.model.dto.configuracion.Vehiculo;

public interface IVehiculoServices extends IHomeServices<Vehiculo> {

	/* EXPOSED METHODS */
	public Vehiculo actualizarFotos(Vehiculo entity) throws Exception;
	
	/**
	 * Método para consultar un vehículo por placa y compañía
	 * @param Placa, Compañía
	 * @return Vehículo
	 */
	public Vehiculo findVehiculoByPlacaCia(String placa, Integer idCia) throws Exception;
	
	public ArrayList<Vehiculo> findAllNoActivosInventario(Integer idCia) throws Exception;
	
	public ArrayList<Vehiculo> findAllActivosInventario(Integer idCia) throws Exception;
	
}
