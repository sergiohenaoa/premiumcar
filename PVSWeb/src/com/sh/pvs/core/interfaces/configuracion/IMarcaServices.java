/**
 * 
 */
package com.sh.pvs.core.interfaces.configuracion;

import java.util.ArrayList;

import com.sh.pvs.core.interfaces.general.IHomeServices;
import com.sh.pvs.model.dto.configuracion.Marca;
import com.sh.pvs.model.dto.configuracion.TipoVehiculoxMarca;

public interface IMarcaServices extends IHomeServices<Marca> {

	/**
	 * Método para agregar un tipo de vehículo a una marca
	 * @param marca
	 * @param tipoVehiculo
	 */
	public void addTipoVehiculo(Marca marca, TipoVehiculoxMarca tipoVehiculoxMarca) throws Exception;
	
	/**
	 * Método para eliminar un tipo de vehículo de una marca
	 * @param tipoVehiculo
	 */
	public void deleteTipoVehiculo(TipoVehiculoxMarca tipoVehiculoxMarca) throws Exception;
	
	public ArrayList<Marca> findAllActivos(Integer idCia) throws Exception;
	
	public ArrayList<Marca> findAllActivosByTipoVehiculo(Integer idCia, Integer idTipoV) throws Exception;
}
