/**
 * 
 */
package com.sh.pvs.core.interfaces.configuracion;

import java.util.ArrayList;

import com.sh.pvs.core.interfaces.general.IHomeServices;
import com.sh.pvs.model.dto.configuracion.Accesorio;

public interface IAccesorioServices extends IHomeServices<Accesorio> {

	public ArrayList<Accesorio> findAllActivos(Integer idCia) throws Exception;
	
}
