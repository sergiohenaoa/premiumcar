/**
 * 
 */
package com.sh.pvs.core.interfaces.configuracion;

import java.util.ArrayList;

import com.sh.pvs.core.interfaces.general.IHomeServices;
import com.sh.pvs.model.dto.configuracion.OficinaTransito;
import com.sh.pvs.model.dto.configuracion.TramitexOficinaTransito;

public interface IOficinaTransitoServices extends IHomeServices<OficinaTransito> {

	/**
	 * Método para agregar un trámite a una oficina de tránsito
	 * @param oficinaTransito
	 * @param tramite
	 */
	public void addTramite(OficinaTransito oficinaTransito, TramitexOficinaTransito tramite) throws Exception;
	
	/**
	 * Método para eliminar un trámite a una oficina de tránsito
	 * @param oficinaTransito
	 * @param tramite
	 */
	public void deleteTramite(TramitexOficinaTransito tramite) throws Exception;
	
	public ArrayList<OficinaTransito> findAllActivos(Integer idCia) throws Exception;
	
}
