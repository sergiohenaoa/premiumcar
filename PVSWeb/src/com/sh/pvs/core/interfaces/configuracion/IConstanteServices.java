/**
 * 
 */
package com.sh.pvs.core.interfaces.configuracion;

import java.util.List;

import com.sh.pvs.core.interfaces.general.IHomeServices;
import com.sh.pvs.model.dto.configuracion.Constante;

public interface IConstanteServices extends IHomeServices<Constante> {

	List<Constante> constantesByCompaniaAndCodigo(int idCompania, String codigo)
			throws Exception;

}
