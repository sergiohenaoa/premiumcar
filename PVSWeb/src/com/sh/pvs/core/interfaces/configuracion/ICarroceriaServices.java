/**
 * 
 */
package com.sh.pvs.core.interfaces.configuracion;

import com.sh.pvs.core.interfaces.general.IHomeServices;
import com.sh.pvs.model.dto.configuracion.Carroceria;

public interface ICarroceriaServices extends IHomeServices<Carroceria> {

}
