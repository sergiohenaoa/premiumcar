/**
 * 
 */
package com.sh.pvs.core.interfaces.configuracion;

import java.util.ArrayList;

import com.sh.pvs.core.interfaces.general.IHomeServices;
import com.sh.pvs.model.dto.configuracion.FormaPago;

public interface IFormaPagoServices extends IHomeServices<FormaPago> {

	public ArrayList<FormaPago> findAllActivos(Integer idCia) throws Exception;
	
}
