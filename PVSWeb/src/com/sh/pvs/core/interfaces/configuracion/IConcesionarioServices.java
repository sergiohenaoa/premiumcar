/**
 * 
 */
package com.sh.pvs.core.interfaces.configuracion;

import java.util.ArrayList;

import com.sh.pvs.core.interfaces.general.IHomeServices;
import com.sh.pvs.model.dto.configuracion.Concesionario;

public interface IConcesionarioServices extends IHomeServices<Concesionario> {
	
	/**
	 * Método para consultar un concesionario por nit y compañía
	 * @param Nit, Compañía
	 * @return Concesionario
	 */
	public Concesionario findConcesionarioByNitCia(String nit, Integer idCia) throws Exception;

	public ArrayList<Concesionario> findAll(Integer idCia) throws Exception;
	
}
