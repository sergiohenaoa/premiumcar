/**
 * 
 */
package com.sh.pvs.core.interfaces.configuracion;

import java.util.ArrayList;

import com.sh.pvs.core.interfaces.general.IHomeServices;
import com.sh.pvs.model.dto.configuracion.Gasto;

public interface IGastoServices extends IHomeServices<Gasto> {

	public ArrayList<Gasto> findAllActivosByCiaAndTraspaso(Integer idCia) throws Exception;
	
	public ArrayList<Gasto> findAllActivosByCiaAndAdicional(Integer idCia) throws Exception;
	
	public ArrayList<Gasto> findAllActivosByCia(Integer idCia) throws Exception;
	
}
