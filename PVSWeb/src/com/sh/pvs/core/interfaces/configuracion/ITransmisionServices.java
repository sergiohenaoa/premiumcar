/**
 * 
 */
package com.sh.pvs.core.interfaces.configuracion;

import java.util.ArrayList;

import com.sh.pvs.core.interfaces.general.IHomeServices;
import com.sh.pvs.model.dto.configuracion.Transmision;

public interface ITransmisionServices extends IHomeServices<Transmision> {

	public ArrayList<Transmision> findAllActivos(Integer idCia) throws Exception;
	
}
