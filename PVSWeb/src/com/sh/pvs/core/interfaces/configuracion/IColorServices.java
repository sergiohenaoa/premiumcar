/**
 * 
 */
package com.sh.pvs.core.interfaces.configuracion;

import java.util.ArrayList;

import com.sh.pvs.core.interfaces.general.IHomeServices;
import com.sh.pvs.model.dto.configuracion.Color;

public interface IColorServices extends IHomeServices<Color> {

	public ArrayList<Color> findAllActivos(Integer idCia) throws Exception;
	
}
