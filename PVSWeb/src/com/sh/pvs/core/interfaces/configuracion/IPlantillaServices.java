/**
 * 
 */
package com.sh.pvs.core.interfaces.configuracion;

import java.util.ArrayList;

import com.sh.pvs.core.interfaces.general.IHomeServices;
import com.sh.pvs.model.dto.configuracion.Plantilla;

public interface IPlantillaServices extends IHomeServices<Plantilla> {

	public ArrayList<Plantilla> findAllActivosByProceso(Integer idCia, String proceso) throws Exception;
	
}
