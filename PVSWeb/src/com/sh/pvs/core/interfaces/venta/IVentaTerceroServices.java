/**
 * 
 */
package com.sh.pvs.core.interfaces.venta;

import com.sh.pvs.core.interfaces.general.IHomeServices;
import com.sh.pvs.model.dto.venta.VentaTercero;

public interface IVentaTerceroServices extends IHomeServices<VentaTercero> {

	/**
	 * Método para guardar una venta y asignar el código
	 * @param ventaTercero
	 */
	public VentaTercero createVentaTerceroTX(VentaTercero ventaTercero) throws Exception;
	
}
