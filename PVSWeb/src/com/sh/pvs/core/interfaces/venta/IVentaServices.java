/**
 * 
 */
package com.sh.pvs.core.interfaces.venta;

import com.sh.pvs.core.interfaces.general.IHomeServices;
import com.sh.pvs.model.dto.venta.Venta;

public interface IVentaServices extends IHomeServices<Venta> {

	/**
	 * Método para guardar una consignación y asignar el código
	 * @param venta
	 */
	public Venta createVentaTX(Venta venta) throws Exception;
	
	/**
	 * Método para declinar una venta
	 * @param venta
	 */
	public Venta declinarVentaTX(Venta venta) throws Exception;

}
