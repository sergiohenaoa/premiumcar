/**
 * 
 */
package com.sh.pvs.core.interfaces.contabilidad;

import java.util.ArrayList;

import com.sh.pvs.core.interfaces.general.IHomeServices;
import com.sh.pvs.model.dto.contabilidad.ReporteCarteraRow;

public interface IReporteCarteraServices extends IHomeServices<ReporteCarteraRow> {

	/**
	 * Método para consultar todos los movimientos Carteras según los filtros
	 */
	public ArrayList<ReporteCarteraRow> findAllMovtos(Boolean compraPPago, Boolean ventaPCobro, 
			Boolean pagosPVendedor, Boolean cobrosPVendedor, Boolean pagosPComprador, Boolean cobrosPComprador, Integer idCia) throws Exception;

}
