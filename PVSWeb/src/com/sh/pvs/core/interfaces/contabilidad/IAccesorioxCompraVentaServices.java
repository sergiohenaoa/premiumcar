/**
 * 
 */
package com.sh.pvs.core.interfaces.contabilidad;

import com.sh.pvs.core.interfaces.general.IHomeServices;
import com.sh.pvs.model.dto.contabilidad.AccesorioxCompraVenta;

public interface IAccesorioxCompraVentaServices extends IHomeServices<AccesorioxCompraVenta> {

	
}
