/**
 * 
 */
package com.sh.pvs.core.interfaces.contabilidad;

import com.sh.pvs.core.interfaces.general.IHomeServices;
import com.sh.pvs.model.dto.contabilidad.MovimientoContable;

public interface IMovimientoContableServices extends IHomeServices<MovimientoContable> {

}
