/**
 * 
 */
package com.sh.pvs.core.interfaces.contabilidad;

import java.util.ArrayList;

import com.sh.pvs.core.interfaces.general.IHomeServices;
import com.sh.pvs.model.dto.contabilidad.InventarioAccesorios;

public interface IInventarioAccesoriosServices extends IHomeServices<InventarioAccesorios> {

	public ArrayList<InventarioAccesorios> findAllActivosByCia(Integer idCia) throws Exception;
	
}
