/**
 * 
 */
package com.sh.pvs.core.interfaces.contabilidad;

import java.util.ArrayList;
import java.util.Date;

import com.sh.pvs.core.interfaces.general.IHomeServices;
import com.sh.pvs.model.dto.configuracion.Cuenta;
import com.sh.pvs.model.dto.contabilidad.ReporteContableRow;

public interface IReporteContableServices extends IHomeServices<ReporteContableRow> {

	/**
	 * Método para consultar todos los movimientos contables según los filtros
	 */
	public ArrayList<ReporteContableRow> findAllMovtos(Date fchIni, Date fchFin, Cuenta cuenta, Integer idCia) throws Exception;

}
