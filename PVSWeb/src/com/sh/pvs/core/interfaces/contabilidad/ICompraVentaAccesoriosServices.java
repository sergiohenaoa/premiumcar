/**
 * 
 */
package com.sh.pvs.core.interfaces.contabilidad;

import java.util.ArrayList;

import com.sh.pvs.core.interfaces.general.IHomeServices;
import com.sh.pvs.model.dto.contabilidad.AccesorioxCompraVenta;
import com.sh.pvs.model.dto.contabilidad.CompraVentaAccesorios;

public interface ICompraVentaAccesoriosServices extends IHomeServices<CompraVentaAccesorios> {

	/**
	 * Método para agregar un accesorio a una compraventa
	 */
	public void addAccesorio(AccesorioxCompraVenta accesorioxcompraventa) throws Exception;

	/**
	 * Método para actualizar un accesorio de una compraventa
	 */
	public void updateAccesorio(AccesorioxCompraVenta accesorioxcompraventa) throws Exception;
	
	/**
	 * Método para eliminar un accesorio de una compraventa
	 */
	public void deleteAccesorio(AccesorioxCompraVenta accesorioxcompraventa) throws Exception;
	
	public ArrayList<CompraVentaAccesorios> findAll(Integer idCia) throws Exception;
	
}
