/**
 * 
 */
package com.sh.pvs.core.interfaces.clientes;

import java.util.ArrayList;

import com.sh.pvs.model.dto.configuracion.Vehiculo;


public interface ICatalogoServices {

	/**
	 * Método para buscar todos los vehículos activos del inventario según compañía
	 * @param Cia
	 */
	public ArrayList<Vehiculo> findAllVehiculosToCatalogo(Integer idCia) throws Exception;
	
}
