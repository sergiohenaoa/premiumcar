/**
 * 
 */
package com.sh.pvs.core.interfaces.hojanegocio;

import com.sh.pvs.core.interfaces.general.IHomeServices;
import com.sh.pvs.model.dto.hojanegocio.PagoxHojaNegocio;

public interface IPagoxHojaNegocioServices extends IHomeServices<PagoxHojaNegocio> {

}
