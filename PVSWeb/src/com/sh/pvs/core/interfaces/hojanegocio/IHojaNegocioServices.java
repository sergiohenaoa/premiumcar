/**
 * 
 */
package com.sh.pvs.core.interfaces.hojanegocio;

import com.sh.pvs.core.interfaces.general.IHomeServices;
import com.sh.pvs.model.dto.hojanegocio.GastoxHojaNegocio;
import com.sh.pvs.model.dto.hojanegocio.HojaNegocio;
import com.sh.pvs.model.dto.hojanegocio.PagoxHojaNegocio;
import com.sh.pvs.model.dto.seguridad.Usuario;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public interface IHojaNegocioServices extends IHomeServices<HojaNegocio> {

	/**
	 * Método para agregar un pago a una hoja de negocio
	 * @param pago
	 */
	void addPago(PagoxHojaNegocio pago, Usuario loggedUser) throws Exception;

	/**
	 * Método para actualizar un pago de una hoja de negocio
	 * @param pago
	 */
	void updatePago(PagoxHojaNegocio pago, Usuario loggedUser) throws Exception;
	
	/**
	 * Método para eliminar un pago de una hoja de negocio
	 * @param pago
	 */
	void deletePago(PagoxHojaNegocio pago, Usuario loggedUser) throws Exception;
	
	/**
	 * Método para agregar un pago a una hoja de negocio
	 * @param hojaNegocio
	 */
	void assignOficinaTransito(HojaNegocio hojaNegocio) throws Exception;
	
	/**
	 * Método para agregar un gasto a una hoja de negocio
	 * @param gasto
	 */
	void addGasto(GastoxHojaNegocio gasto, Usuario loggedUser) throws Exception;
	
	/**
	 * Método para actualizar un gasto de una hoja de negocio
	 * @param gasto
	 */
	void updateGasto(GastoxHojaNegocio gasto, Usuario loggedUser) throws Exception;
	
	/**
	 * Método para eliminar un gasto de una hoja de negocio
	 * @param gasto
	 */
	void deleteGasto(GastoxHojaNegocio gasto, Usuario loggedUser) throws Exception;
	
	ArrayList<HojaNegocio> findAll(Integer idCia) throws Exception;
	
	/**
	 * Método para finalizar una hoja de negocio
	 * @param hojanegocio
	 */
	HojaNegocio finalizarHojaNegocioTX(HojaNegocio hojanegocio) throws Exception;
	
	/**
	 * Método para consultar todas las hojas de negocio según los filtros
	 */
	List<HojaNegocio> findAllHojasNegocioByFiltro(Date fchIni, Date fchFin, Integer idCia) throws Exception;
	
}
