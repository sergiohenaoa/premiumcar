package com.sh.pvs.model.hibernate;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.sh.pvs.model.dao.general.HomeDAO;
import com.sh.pvs.utils.exceptioncontrol.ControlException;

/**
 * Data access object (DAO) for domain model
 * @author MyEclipse Persistence Tools
 */
public class BaseHibernateDAO implements IBaseHibernateDAO {
	
	private Transaction tx;
	
	/**
	 * Obtención de la sesión
	 * @throws Exception
	 */
	@Override
	public Session getSession() throws Exception {
		return HibernateSessionFactory.getSession();
	}
	
	/**
	 * Inicio de la transaccion
	 * @throws Exception
	 */
	@Override
	public void beginTransaction() throws Exception {
		tx = getSession().beginTransaction();
		getSession().clear();
	}
	
	/**
	 * Fin de la transaccion
	 * @throws Exception
	 */
	@Override
	public void endTransaction() throws Exception {	
		if (tx != null && (!tx.wasCommitted() || tx.wasRolledBack())) {
			try {
				tx.commit();
			} catch (HibernateException he) {
				if (tx != null)
					tx.rollback();
				throw ControlException.formatException("TECNICA", BaseHibernateDAO.class.getName(), "", "endTransaction", he);
			} catch (RuntimeException re) {
				throw ControlException.formatException("TECNICA", HomeDAO.class.getName(), "", "endTransaction", re);
			}  catch (Exception e) {
				throw ControlException.formatException("TECNICA", HomeDAO.class.getName(), "", "endTransaction", e);
			}
		}
	}
	
	/**
	 * Rollback de las acciones
	 * @throws Exception
	 */
	@Override
	public void rollBackTransaction() throws Exception{
		if (tx != null && (!tx.wasCommitted())) {
			try {
				tx.rollback();
			} catch (HibernateException he) {
				throw ControlException.formatException("TECNICA", BaseHibernateDAO.class.getName(), "", "rollBackTransaction", he);
			}
		}
	}
	
	/**
	 * Cierre de la sesion
	 * @throws Exception
	 */
	@Override
	public void closeSession() throws Exception {
		try {
			if (getSession() != null){
				getSession().close();
			}
		} catch (HibernateException he) {
			throw ControlException.formatException("TECNICA", BaseHibernateDAO.class.getName(), "", "closeSession", he);
		}
	}
	
}
