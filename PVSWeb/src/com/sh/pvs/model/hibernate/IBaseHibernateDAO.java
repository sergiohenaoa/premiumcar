package com.sh.pvs.model.hibernate;

import org.hibernate.Session;

public interface IBaseHibernateDAO {
	
	public Session getSession() throws Exception;
	
	public void beginTransaction() throws Exception;
	
	public void endTransaction() throws Exception;
	
	public void closeSession() throws Exception;
	
	public void rollBackTransaction() throws Exception;
	
}
