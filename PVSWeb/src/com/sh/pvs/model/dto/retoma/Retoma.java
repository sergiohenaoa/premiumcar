package com.sh.pvs.model.dto.retoma;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.sh.pvs.model.dto.configuracion.Cliente;
import com.sh.pvs.model.dto.configuracion.Vehiculo;
import com.sh.pvs.model.utils.Constantes;


/**
 * Retoma entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="tb_retomas"
    ,catalog="db_pvs"
)
public class Retoma extends AbstractRetoma implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	// Constructors

    /** default constructor */
    public Retoma() {
    }

	/** minimal constructor */
    public Retoma(Cliente tbClientes, Vehiculo vehiculo, String codigo, Double valorVehiculo, Timestamp fchRetoma, Boolean requiereDatosMatricula, String estado) {
        super(tbClientes, vehiculo, codigo, valorVehiculo, fchRetoma, requiereDatosMatricula, estado);        
    }
    
    /** full constructor */
    public Retoma(Cliente tbClientes, Vehiculo vehiculo, String codigo, Integer kilometraje, Timestamp fchVenceSoat, Timestamp fchVenceRevtm, Double valorVehiculo, Timestamp fchRetoma, Boolean requiereDatosMatricula, String estado) {
        super(tbClientes, vehiculo, codigo, kilometraje, fchVenceSoat, fchVenceRevtm, valorVehiculo, fchRetoma, requiereDatosMatricula, estado);        
    }
  
    @Transient
    public String getEstadoLbl(){
    	if(this.getEstado().equals(Constantes.RETOMA_ESTADO_INICIAL_COD))
    		return Constantes.RETOMA_ESTADO_INICIAL_LBL;
    	if(this.getEstado().equals(Constantes.RETOMA_ESTADO_ENPROCESO_COD))
    		return Constantes.RETOMA_ESTADO_ENPROCESO_LBL;
    	if(this.getEstado().equals(Constantes.RETOMA_ESTADO_FINALIZADA_COD))
    		return Constantes.RETOMA_ESTADO_FINALIZADA_LBL;
    	if(this.getEstado().equals(Constantes.RETOMA_ESTADO_DECLINADA_COD))
    		return Constantes.RETOMA_ESTADO_DECLINADA_LBL;
    	return "";
    }

    @Transient
    public Boolean getIsRetomaCliente(){
    	if(this.getConcesionario() != null && this.getConcesionario().getId() != null
				&& !this.getConcesionario().getId().equals(new Integer(0)))
			return false;
		else
			return true;
    }
}
