package com.sh.pvs.model.dto.retoma;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import com.sh.pvs.model.dto.configuracion.Cliente;
import com.sh.pvs.model.dto.configuracion.Concesionario;
import com.sh.pvs.model.dto.configuracion.Vehiculo;
import com.sh.pvs.model.dto.seguridad.Compania;
import com.sh.pvs.model.utils.Constantes;


/**
 * AbstractRetoma entity provides the base persistence definition of the Retoma entity. @author MyEclipse Persistence Tools
 */
@MappedSuperclass

public abstract class AbstractRetoma  implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	// Fields    

     private Integer id;
     private Cliente cliente;
     private Concesionario concesionario;
     private Vehiculo vehiculo;
     private Compania compania;
     private String codigo;
     private Integer kilometraje;
     private Date fchVenceSoat;
     private Date fchVenceRevtm;
     private Double valorVehiculo;
     private Date fchRetoma = new Date();
     private Boolean requiereDatosMatricula;
     private Boolean aplicaRevisionTM;
     private Boolean negociable;
     private String identificacionMatricula;
     private String nombreMatricula;
     private String estado = Constantes.RETOMA_ESTADO_INICIAL_COD;

    // Constructors

    /** default constructor */
    public AbstractRetoma() {
    }

	/** minimal constructor */
    public AbstractRetoma(Cliente cliente, Vehiculo vehiculo, String codigo, Double valorVehiculo, Date fchRetoma, Boolean requiereDatosMatricula, String estado) {
        this.cliente = cliente;
        this.vehiculo = vehiculo;
        this.codigo = codigo;
        this.valorVehiculo = valorVehiculo;
        this.fchRetoma = fchRetoma;
        this.requiereDatosMatricula = requiereDatosMatricula;
        this.estado = estado;
    }
    
    /** full constructor */
    public AbstractRetoma(Cliente cliente, Vehiculo vehiculo, String codigo, Integer kilometraje, Date fchVenceSoat, Date fchVenceRevtm, Double valorVehiculo, Date fchRetoma, Boolean requiereDatosMatricula, String estado) {
        this.cliente = cliente;
        this.vehiculo = vehiculo;
        this.codigo = codigo;
        this.kilometraje = kilometraje;
        this.fchVenceSoat = fchVenceSoat;
        this.fchVenceRevtm = fchVenceRevtm;
        this.valorVehiculo = valorVehiculo;
        this.fchRetoma = fchRetoma;
        this.requiereDatosMatricula = requiereDatosMatricula;
        this.estado = estado;
    }

   
    // Property accessors
    @Id @GeneratedValue
    @Column(name="id", unique=true, nullable=false)
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="cliente_id")
    public Cliente getCliente() {
        return this.cliente;
    }
    
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
    
    @ManyToOne(fetch=FetchType.EAGER)
	    @JoinColumn(name="concesionario_id")
	public Concesionario getConcesionario() {
	    return this.concesionario;
	}
	
	public void setConcesionario(Concesionario concesionario) {
	    this.concesionario = concesionario;
	}
    
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="vehiculo_id", nullable=false)
    public Vehiculo getVehiculo() {
        return this.vehiculo;
    }
    
    public void setVehiculo(Vehiculo vehiculo) {
        this.vehiculo = vehiculo;
    }
    
    @ManyToOne(fetch=FetchType.EAGER)
	    @JoinColumn(name="compania_id", nullable=false)
	public Compania getCompania() {
	    return this.compania;
	}
	
	public void setCompania(Compania companias) {
	    this.compania = companias;
	}

    @Column(name="codigo", nullable=false, length=45)
    public String getCodigo() {
        return this.codigo;
    }
    
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
    
    @Column(name="kilometraje")
    public Integer getKilometraje() {
        return this.kilometraje;
    }
    
    public void setKilometraje(Integer kilometraje) {
        this.kilometraje = kilometraje;
    }
    
    @Column(name="fch_vence_soat", length=19)
    public Date getFchVenceSoat() {
        return this.fchVenceSoat;
    }
    
    public void setFchVenceSoat(Date fchVenceSoat) {
        this.fchVenceSoat = fchVenceSoat;
    }
    
    @Column(name="fch_vence_revtm", length=19)
    public Date getFchVenceRevtm() {
        return this.fchVenceRevtm;
    }
    
    public void setFchVenceRevtm(Date fchVenceRevtm) {
        this.fchVenceRevtm = fchVenceRevtm;
    }
    
    @Column(name="valor_vehiculo", nullable=false, precision=17, scale=4)
    public Double getValorVehiculo() {
        return this.valorVehiculo;
    }
    
    public void setValorVehiculo(Double valorVehiculo) {
        this.valorVehiculo = valorVehiculo;
    }

    @Column(name="fch_retoma", nullable=false, length=19)
    public Date getFchRetoma() {
        return this.fchRetoma;
    }
    
    public void setFchRetoma(Date fchRetoma) {
        this.fchRetoma = fchRetoma;
    }
    
    @Column(name="requiere_datos_matricula", nullable=false)
    public Boolean getRequiereDatosMatricula() {
        return this.requiereDatosMatricula;
    }
    
    public void setRequiereDatosMatricula(Boolean requiereDatosMatricula) {
        this.requiereDatosMatricula = requiereDatosMatricula;
    }
    
    @Column(name="aplica_revision_tm", nullable=false)
    public Boolean getAplicaRevisionTM() {
		return aplicaRevisionTM;
	}

	public void setAplicaRevisionTM(Boolean aplicaRevisionTM) {
		this.aplicaRevisionTM = aplicaRevisionTM;
	}
    
	@Column(name="negociable", nullable=false)
    public Boolean getNegociable() {
        return this.negociable;
    }
    
    public void setNegociable(Boolean negociable) {
        this.negociable = negociable;
    }
    
    @Column(name="estado", nullable=false, length=2)
    public String getEstado() {
        return this.estado;
    }
    
    public void setEstado(String estado) {
        this.estado = estado;
    }

	
    @Column(name="identificacion_matricula", length=50)
    public String getIdentificacionMatricula() {
		return identificacionMatricula;
	}

	public void setIdentificacionMatricula(String identificacionMatricula) {
		this.identificacionMatricula = identificacionMatricula;
	}
	
	@Column(name="nombre_matricula", length=200)
	public String getNombreMatricula() {
		return nombreMatricula;
	}

	public void setNombreMatricula(String nombreMatricula) {
		this.nombreMatricula = nombreMatricula;
	}
    
}