package com.sh.pvs.model.dto.hojanegocio;


import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.sh.pvs.model.dto.configuracion.Cuenta;
import com.sh.pvs.model.dto.configuracion.FormaPago;


/**
 * PagoxHojaNegocio entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="tb_pagosxhojanegocio"
    ,catalog="db_pvs"
)
public class PagoxHojaNegocio extends AbstractPagoxHojaNegocio implements java.io.Serializable, Comparable<PagoxHojaNegocio> {

	private static final long serialVersionUID = 1L;

	// Constructors

    /** default constructor */
    public PagoxHojaNegocio() {
    }

    /** full constructor */
	public PagoxHojaNegocio(String consecutivo, Date fchPago, String pagadoPor,
			String pagadoA, String concepto, Double valor,
			HojaNegocio hojaNegocio, Cuenta cuentaSalida, Cuenta cuentaEntrada,
			FormaPago formaPago) {
		super(consecutivo, fchPago, pagadoPor, pagadoA, concepto, valor, hojaNegocio,
				cuentaSalida, cuentaEntrada, formaPago);
	}

	@Override
    public int compareTo(final PagoxHojaNegocio o) {
		if(this.getId() != null && o.getId() != null)
			return Integer.compare(this.getId(), o.getId());
		return 0;
    }
}
