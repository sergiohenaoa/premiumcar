package com.sh.pvs.model.dto.hojanegocio;

import com.sh.pvs.model.dto.configuracion.OficinaTransito;
import com.sh.pvs.model.dto.consignacion.Consignacion;
import com.sh.pvs.model.dto.retoma.Retoma;
import com.sh.pvs.model.dto.venta.Venta;
import com.sh.pvs.model.utils.Constantes;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;


/**
 * AbstractHojaNegocio entity provides the base persistence definition of the HojaNegocio entity. @author MyEclipse Persistence Tools
 */
@MappedSuperclass

public abstract class AbstractHojaNegocio  implements java.io.Serializable {


    // Fields    
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private String estado = Constantes.HN_EST_ENPROCESO;
	private Double vlrVehComprador = new Double(0);
	private Double vlrVehVendedor = new Double(0);
	private Double porcentajeComision = new Double(0);
     private Consignacion consignacion;
     private Retoma retoma;
     private Venta venta;
     private OficinaTransito oficinaTransito;
     private Set<PagoxHojaNegocio> pagos = new HashSet<PagoxHojaNegocio>(0);
     private Set<GastoxHojaNegocio> gastos = new HashSet<GastoxHojaNegocio>(0);
     
    // Constructors

    /** default constructor */
    public AbstractHojaNegocio() {
    }

    
    /** full constructor */
    public AbstractHojaNegocio(Consignacion consignacion, Retoma retoma, Venta venta) {
        this.consignacion = consignacion;
        this.retoma = retoma;
        this.venta = venta;
    }

   
    // Property accessors
    @Id @GeneratedValue
    
    @Column(name="id", unique=true, nullable=false)
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="consignacion_id")
    public Consignacion getConsignacion() {
        return this.consignacion;
    }
    
    public void setConsignacion(Consignacion consignacion) {
        this.consignacion = consignacion;
    }
    
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="retoma_id")
	public Retoma getRetoma() {
	    return this.retoma;
	}
	
	public void setRetoma(Retoma retoma) {
	    this.retoma = retoma;
	}
    
	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="venta_id")
	public Venta getVenta() {
	    return this.venta;
	}
	
	public void setVenta(Venta venta) {
	    this.venta = venta;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="oficinatransito_id")
	public OficinaTransito getOficinaTransito() {
		return oficinaTransito;
	}

	public void setOficinaTransito(OficinaTransito oficinaTransito) {
		this.oficinaTransito = oficinaTransito;
	}
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "hojaNegocio")
    public Set<PagoxHojaNegocio> getPagos() {
		return pagos;
	}
    
	public void setPagos(Set<PagoxHojaNegocio> pagos) {
		this.pagos = pagos;
	}
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "hojaNegocio")
	public Set<GastoxHojaNegocio> getGastos() {
		return gastos;
	}

	public void setGastos(Set<GastoxHojaNegocio> gastos) {
		this.gastos = gastos;
	}

	 @Column(name="estado", nullable=false, length=2)
	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	@Column(name="vlr_veh_comprador", nullable=false, precision=17, scale=4)
	public Double getVlrVehComprador() {
		return vlrVehComprador;
	}
	
	public void setVlrVehComprador(Double vlrVehComprador) {
		this.vlrVehComprador = vlrVehComprador;
	}

	@Column(name="vlr_veh_vendedor", nullable=false, precision=17, scale=4)
	public Double getVlrVehVendedor() {
		return vlrVehVendedor;
	}

	public void setVlrVehVendedor(Double vlrVehVendedor) {
		this.vlrVehVendedor = vlrVehVendedor;
	}
	
	@Column(name="porcentajeComision", precision=17, scale=4)
	public Double getPorcentajeComision()
	{
		return porcentajeComision;
	}
	
	public void setPorcentajeComision(Double porcentajeComision)
	{
		this.porcentajeComision = porcentajeComision;
	}
}
