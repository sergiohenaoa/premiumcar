package com.sh.pvs.model.dto.hojanegocio;

import com.sh.pvs.model.dto.configuracion.Cuenta;
import com.sh.pvs.model.dto.configuracion.Gasto;

import javax.persistence.*;


/**
 * AbstractGastoxHojaNegocio entity provides the base persistence definition of the HojaNegocio entity. @author MyEclipse Persistence Tools
 */
@MappedSuperclass

public abstract class AbstractGastoxHojaNegocio  implements java.io.Serializable {


    // Fields    
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private HojaNegocio hojaNegocio;
	private Gasto gasto;
	private Double valor;
	private String descripcion;
	private String asignadoA;
	private String tipoGasto;
	private Cuenta cuenta = new Cuenta();
	
    // Constructors

    /** default constructor */
    public AbstractGastoxHojaNegocio() {
    }

    
    /** full constructor */
    public AbstractGastoxHojaNegocio(HojaNegocio hojaNegocio, Double valor,
			String descripcion, String asignadoA) {
		super();
		this.hojaNegocio = hojaNegocio;
		this.valor = valor;
		this.descripcion = descripcion;
		this.asignadoA = asignadoA;
	}
   
    // Property accessors
    @Id @GeneratedValue
    @Column(name="id", unique=true, nullable=false)
    public Integer getId() {
        return this.id;
    }

	public void setId(Integer id) {
        this.id = id;
    }
	
	@Column(name="asignado_a", nullable=false, length=2)
	public String getAsignadoA() {
		return asignadoA;
	}

	public void setAsignadoA(String asignadoA) {
		this.asignadoA = asignadoA;
	}
	
	@Column(name="tipo_gasto", nullable=false, length=2)
	public String getTipoGasto() {
		return tipoGasto;
	}

	public void setTipoGasto(String tipoGasto) {
		this.tipoGasto = tipoGasto;
	}
	
	@Column(name="descripcion")
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	@Column(name="valor", nullable=false, precision=17, scale=4)
	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="hojanegocio_id", nullable=false)
	public HojaNegocio getHojaNegocio() {
		return hojaNegocio;
	}

	public void setHojaNegocio(HojaNegocio hojaNegocio) {
		this.hojaNegocio = hojaNegocio;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="gasto_id", nullable=false)
	public Gasto getGasto() {
		return gasto;
	}
	
	public void setGasto(Gasto gasto) {
		this.gasto = gasto;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="cuenta_id")
	public Cuenta getCuenta() {
		return cuenta;
	}
	
	public void setCuenta(Cuenta cuenta) {
		this.cuenta = cuenta;
	}
}
