package com.sh.pvs.model.dto.hojanegocio;


import javax.persistence.Entity;
import javax.persistence.Table;


/**
 * GastoTraspasoxHojaNegocio entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="tb_gastosxhojanegocio"
    ,catalog="db_pvs"
)
public class GastoxHojaNegocio extends AbstractGastoxHojaNegocio implements java.io.Serializable, Comparable<GastoxHojaNegocio> {

	private static final long serialVersionUID = 1L;

	// Constructors

    /** default constructor */
    public GastoxHojaNegocio() {
    }

    /** full constructor */
    public GastoxHojaNegocio(HojaNegocio hojaNegocio, Double valor,
			String descripcion, String asignadoA) {
		super(hojaNegocio, valor, descripcion, asignadoA);
	}

	@Override
    public int compareTo(final GastoxHojaNegocio o) {
		if(this.getId() != null && o.getId() != null)
			return Integer.compare(this.getId(), o.getId());
		return 0;
    }

}