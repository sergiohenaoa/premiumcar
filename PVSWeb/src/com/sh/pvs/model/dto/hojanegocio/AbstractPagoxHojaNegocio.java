package com.sh.pvs.model.dto.hojanegocio;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import com.sh.pvs.model.dto.configuracion.Cuenta;
import com.sh.pvs.model.dto.configuracion.FormaPago;


/**
 * AbstractPagoxHojaNegocio entity provides the base persistence definition of the HojaNegocio entity. @author MyEclipse Persistence Tools
 */
@MappedSuperclass

public abstract class AbstractPagoxHojaNegocio  implements java.io.Serializable {


    // Fields    
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private String consecutivo = ".";
	private Date fchPago;
	private String pagadoPor;
	private String pagadoA;
	private String concepto;
	private Double valor;
	private HojaNegocio hojaNegocio;
	private Cuenta cuentaSalida = new Cuenta();
	private Cuenta cuentaEntrada = new Cuenta();
	private FormaPago formaPago = new FormaPago();
     
    // Constructors

    /** default constructor */
    public AbstractPagoxHojaNegocio() {
    }

    
    /** full constructor */
    public AbstractPagoxHojaNegocio(String consecutivo, Date fchPago,
			String pagadoPor, String pagadoA, String concepto, Double valor,
			HojaNegocio hojaNegocio, Cuenta cuentaSalida, Cuenta cuentaEntrada,
			FormaPago formaPago) {
		super();
		this.consecutivo = consecutivo;
		this.fchPago = fchPago;
		this.pagadoPor = pagadoPor;
		this.pagadoA = pagadoA;
		this.concepto = concepto;
		this.valor = valor;
		this.hojaNegocio = hojaNegocio;
		this.cuentaSalida = cuentaSalida;
		this.cuentaEntrada = cuentaEntrada;
		this.formaPago = formaPago;
	}

   
    // Property accessors
    @Id @GeneratedValue
    @Column(name="id", unique=true, nullable=false)
    public Integer getId() {
        return this.id;
    }

	public void setId(Integer id) {
        this.id = id;
    }
	
	@Column(name="consecutivo", nullable=false, length=45)
	public String getConsecutivo() {
		return consecutivo;
	}

	public void setConsecutivo(String consecutivo) {
		this.consecutivo = consecutivo;
	}
	
	@Column(name="fch_pago", nullable=false, length=19)
	public Date getFchPago() {
		return fchPago;
	}

	public void setFchPago(Date fchPago) {
		this.fchPago = fchPago;
	}
	
	@Column(name="pagado_por", nullable=false, length=2)
	public String getPagadoPor() {
		return pagadoPor;
	}

	public void setPagadoPor(String pagadoPor) {
		this.pagadoPor = pagadoPor;
	}

	@Column(name="pagado_a", nullable=false, length=2)
	public String getPagadoA() {
		return pagadoA;
	}

	public void setPagadoA(String pagadoA) {
		this.pagadoA = pagadoA;
	}
	
	@Column(name="concepto", nullable=false, length=1000)
	public String getConcepto() {
		return concepto;
	}

	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}
	
	@Column(name="valor", nullable=false, precision=17, scale=4)
	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="hojanegocio_id", nullable=false)
	public HojaNegocio getHojaNegocio() {
		return hojaNegocio;
	}

	public void setHojaNegocio(HojaNegocio hojaNegocio) {
		this.hojaNegocio = hojaNegocio;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="cuentasalida_id")
	public Cuenta getCuentaSalida() {
		return cuentaSalida;
	}

	public void setCuentaSalida(Cuenta cuentaSalida) {
		this.cuentaSalida = cuentaSalida;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="cuentaentrada_id")
	public Cuenta getCuentaEntrada() {
		return cuentaEntrada;
	}

	public void setCuentaEntrada(Cuenta cuentaEntrada) {
		this.cuentaEntrada = cuentaEntrada;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="formapago_id", nullable=false)
	public FormaPago getFormaPago() {
		return formaPago;
	}

	public void setFormaPago(FormaPago formaPago) {
		this.formaPago = formaPago;
	}
	
}