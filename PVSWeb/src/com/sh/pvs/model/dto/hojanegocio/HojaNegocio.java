package com.sh.pvs.model.dto.hojanegocio;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.TreeSet;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.sh.pvs.model.dto.configuracion.Vehiculo;
import com.sh.pvs.model.dto.consignacion.Consignacion;
import com.sh.pvs.model.dto.retoma.Retoma;
import com.sh.pvs.model.dto.venta.Venta;
import com.sh.pvs.model.utils.Constantes;


/**
 * HojaNegocio entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="tb_hojasnegocio"
    ,catalog="db_pvs"
)
public class HojaNegocio extends AbstractHojaNegocio implements java.io.Serializable {

	private static final long serialVersionUID = 1L;


	// Constructors

    /** default constructor */
    public HojaNegocio() {
    }

    /** full constructor */
	public HojaNegocio(Consignacion consignacion, Retoma retoma, Venta venta) {
		super(consignacion, retoma, venta);
	}
	
	@Transient
	public Boolean getFromConsignacion(){
		if(getConsignacion() != null 
				&& getConsignacion().getCliente() != null
				&& getConsignacion().getCliente().getId() != null
				&& !getConsignacion().getCliente().getId().equals(new Integer(0)))
			return true;
		
		return false;
	}
	
	@Transient
	public Boolean getFromRetoma(){
		if(getRetoma() != null 
				&& getRetoma().getId() != null
				&& !getRetoma().getId().equals(new Integer(0)))
			return true;
		
		return false;
	}
	
	@Transient
	public Boolean getFromRetomaCliente(){
		if(getRetoma() != null 
				&& getRetoma().getId() != null
				&& !getRetoma().getId().equals(new Integer(0))){
			if(getRetoma().getCliente() != null
					&& getRetoma().getCliente().getId() != null
					&& !getRetoma().getCliente().getId().equals(new Integer(0)))
			return true;
		}
		
		return false;
	}

	@Transient
	public Vehiculo getVehiculo(){
		if(getFromConsignacion())
			return getConsignacion().getVehiculo();
		
		if(getFromRetoma())
			return getRetoma().getVehiculo();
		
		return null;
	}
	
	@Transient
	public Integer getIdCia(){
		if(getFromConsignacion())
			return getConsignacion().getCompania().getId();
		
		if(getFromRetoma())
			return getRetoma().getCompania().getId();
		
		return null;
	}
	
	@Transient
	public String getNombreVendedor(){
		if(getFromConsignacion())
			return getConsignacion().getCliente().getCedula() + " - " + getConsignacion().getCliente().getNombres() + " " + getConsignacion().getCliente().getApellidos();
		
		if(getFromRetoma()){
			if(getRetoma().getConcesionario() != null && getRetoma().getConcesionario().getId() != null)
				return getRetoma().getConcesionario().getNit() + " - " + getRetoma().getConcesionario().getNombre();
			if(getRetoma().getCliente() != null && getRetoma().getCliente().getId() != null)
				return getRetoma().getCliente().getCedula() + " - " + getRetoma().getCliente().getNombres() + " " + getRetoma().getCliente().getApellidos();
		}
		
		return "";
	}
	
	@Transient
	public Boolean getHasVenta(){
		if(getVenta() != null 
				&& getVenta().getCliente() != null
				&& getVenta().getCliente().getId() != null
				&& !getVenta().getCliente().getId().equals(new Integer(0)))
			return true;
		
		return false;
	}
	
	@Transient
	public Boolean getHasOficinaTransito(){
		if(getOficinaTransito() != null 
				&& getOficinaTransito().getId() != null
				&& !getOficinaTransito().getId().equals(new Integer(0)))
			return true;
		
		return false;
	}
	
	@Transient
	public double getTotalGastosTraspaso(String asignadoA){
		
		Double total = 0.0;
		for (Iterator<GastoxHojaNegocio> itGastosxHojaNegocio = this.getGastos().iterator(); itGastosxHojaNegocio.hasNext();) {
			GastoxHojaNegocio gastoxHojaNegocio = (GastoxHojaNegocio) itGastosxHojaNegocio.next();
			if(gastoxHojaNegocio.getAsignadoA().equals(asignadoA)){
				if(gastoxHojaNegocio.getTipoGasto().equals(com.sh.pvs.core.utils.Constantes.CODTIPOGASTOTRASPASO)){
					total += gastoxHojaNegocio.getValor();
				}
			}
		}
		return total*(-1);
		
	}
	
	@Transient
	public double getTotalGastosAdicionales(String asignadoA){
		
		Double total = 0.0;
		for (Iterator<GastoxHojaNegocio> itGastosxHojaNegocio = this.getGastos().iterator(); itGastosxHojaNegocio.hasNext();) {
			GastoxHojaNegocio gastoxHojaNegocio = (GastoxHojaNegocio) itGastosxHojaNegocio.next();
			if(gastoxHojaNegocio.getAsignadoA().equals(asignadoA)){
				if(gastoxHojaNegocio.getTipoGasto().equals(com.sh.pvs.core.utils.Constantes.CODTIPOGASTOADICIONAL)){
					total += gastoxHojaNegocio.getValor();
				}
			}
		}
		return total*(-1);
		
	}
	
	@Transient
	public double getTotalGastosCia(){
		
		Double total = 0.0;
		for (Iterator<PagoxHojaNegocio> itPagosxHojaNegocio = this.getPagos().iterator(); itPagosxHojaNegocio.hasNext();) {
			PagoxHojaNegocio pagoxHojaNegocio = (PagoxHojaNegocio) itPagosxHojaNegocio.next();
			if(pagoxHojaNegocio.getPagadoPor().equals(com.sh.pvs.core.utils.Constantes.CODCIA)){
				total += pagoxHojaNegocio.getValor();
			}
		}
		return total*(-1);
		
	}
	
	@Transient
	public double getTotalAbonos(String pagadoPor){
		
		Double total = 0.0;
		for (Iterator<PagoxHojaNegocio> itPagosxHojaNegocio = this.getPagos().iterator(); itPagosxHojaNegocio.hasNext();) {
			PagoxHojaNegocio pagoxHojaNegocio = (PagoxHojaNegocio) itPagosxHojaNegocio.next();
			if(pagoxHojaNegocio.getPagadoPor().equals(pagadoPor)){
				total += pagoxHojaNegocio.getValor();
			}
		}
		return total;
		
	}
	
	@Transient
	public double getSaldoTotal(String asignadoA){
		Double total = 0.0;
		if(asignadoA.equals(com.sh.pvs.core.utils.Constantes.CODVENDEDOR)){
			total = getSaldoVehiculo(asignadoA) + getTotalAbonos(asignadoA) + getTotalGastosTraspaso(asignadoA) + getTotalGastosAdicionales(asignadoA);
		}
		if(asignadoA.equals(com.sh.pvs.core.utils.Constantes.CODCOMPRADOR)){
			total = getTotalAbonos(asignadoA) + getVlrVehCompradorTmp() + getTotalGastosTraspaso(asignadoA) + getTotalGastosAdicionales(asignadoA);
		}
		if(asignadoA.equals(com.sh.pvs.core.utils.Constantes.CODCIA)){
			total = getTotalPagosACia() + getTotalGastosCia();
		}
		return total;
	}
	
	@Transient
	public double getUtilidadTotal(String asignadoA){
		Double total = 0.0;
		if(this.getPorcentajeComision() != null){
			total += this.getVlrVehVendedor() * this.getPorcentajeComision() / 100;
		}
		if(this.getVlrVehComprador() != null && this.getVlrVehVendedor() != null){
			total += this.getVlrVehComprador() - this.getVlrVehVendedor();
		}
		total = total + this.getTotalGastosAdicionales(asignadoA);
		return total;
	}
	
	@Transient
	public double getSaldoVehiculo(String asignadoA){
		Double total = 0.0;
		if(asignadoA.equals(com.sh.pvs.core.utils.Constantes.CODVENDEDOR)){
			total = this.getVlrVehVendedor() - this.getTotalAbonosxValorVehiculoAlVendedor();
		}
		if(asignadoA.equals(com.sh.pvs.core.utils.Constantes.CODCOMPRADOR)){
			total = (this.getVlrVehComprador() - this.getTotalAbonosxValorVehiculoAlComprador())*(-1);
		}
		return total;
		
	}
	
	@Transient
	public ArrayList<GastoxHojaNegocio> gastosTraspasoList(String asignadoA){
		ArrayList<GastoxHojaNegocio> gastos = new ArrayList<GastoxHojaNegocio>();
		for (Iterator<GastoxHojaNegocio> itGastosxHojaNegocio = this.getGastos().iterator(); itGastosxHojaNegocio.hasNext();) {
			GastoxHojaNegocio gastoxHojaNegocio = (GastoxHojaNegocio) itGastosxHojaNegocio.next();
			if(gastoxHojaNegocio.getAsignadoA().equals(asignadoA)){
				if(gastoxHojaNegocio.getTipoGasto().equals(com.sh.pvs.core.utils.Constantes.CODTIPOGASTOTRASPASO)){
					gastos.add(gastoxHojaNegocio);
				}
			}
		}
		return new ArrayList<GastoxHojaNegocio>(new TreeSet<GastoxHojaNegocio>(gastos));
	}
	
	@Transient
	public ArrayList<GastoxHojaNegocio> gastosAdicionalesList(String asignadoA){
		ArrayList<GastoxHojaNegocio> gastos = new ArrayList<GastoxHojaNegocio>();
		for (Iterator<GastoxHojaNegocio> itGastosxHojaNegocio = this.getGastos().iterator(); itGastosxHojaNegocio.hasNext();) {
			GastoxHojaNegocio gastoxHojaNegocio = (GastoxHojaNegocio) itGastosxHojaNegocio.next();
			if(gastoxHojaNegocio.getAsignadoA().equals(asignadoA)){
				if(gastoxHojaNegocio.getTipoGasto().equals(com.sh.pvs.core.utils.Constantes.CODTIPOGASTOADICIONAL)){
					gastos.add(gastoxHojaNegocio);
				}
			}
		}
		return new ArrayList<GastoxHojaNegocio>(new TreeSet<GastoxHojaNegocio>(gastos));
	}

	@Transient
	public ArrayList<PagoxHojaNegocio> abonosList(String pagadoPor) {
		ArrayList<PagoxHojaNegocio> pagos = new ArrayList<PagoxHojaNegocio>();
		for (Iterator<PagoxHojaNegocio> itPagos = this.getPagos().iterator(); itPagos.hasNext();) {
			PagoxHojaNegocio pagoxHojaNegocio = (PagoxHojaNegocio) itPagos.next();
			if(pagoxHojaNegocio.getPagadoPor().equals(pagadoPor)){
				pagos.add(pagoxHojaNegocio);
			}
		}
		return new ArrayList<PagoxHojaNegocio>(new TreeSet<PagoxHojaNegocio>(pagos));
	}
	
	@Transient
	public Double getTotalAbonosxValorVehiculoAlVendedor(){
		Double totalAbonos = 0.0;
		for (Iterator<PagoxHojaNegocio> itPagos = this.getPagos().iterator(); itPagos.hasNext();) {
			PagoxHojaNegocio pagoxHojaNegocio = (PagoxHojaNegocio) itPagos.next();
			if(pagoxHojaNegocio.getPagadoA().equals(com.sh.pvs.core.utils.Constantes.CODVENDEDOR)){
				totalAbonos += pagoxHojaNegocio.getValor();
			}
		}
		return totalAbonos;
	}
	
	@Transient
	public Double getTotalAbonosxValorVehiculoAlComprador(){
		Double totalAbonos = 0.0;
		for (Iterator<PagoxHojaNegocio> itPagos = this.getPagos().iterator(); itPagos.hasNext();) {
			PagoxHojaNegocio pagoxHojaNegocio = (PagoxHojaNegocio) itPagos.next();
			if(pagoxHojaNegocio.getPagadoPor().equals(com.sh.pvs.core.utils.Constantes.CODCOMPRADOR)){
				totalAbonos += pagoxHojaNegocio.getValor();
			}
		}
		return totalAbonos;
	}
	
	@Transient
	public Double getVlrVehCompradorTmp() {
		return this.getVlrVehComprador()*(-1);
	}
	
	@Transient
	public ArrayList<PagoxHojaNegocio> getPagosAlVendedor() {
		ArrayList<PagoxHojaNegocio> pagosxgasto = new ArrayList<PagoxHojaNegocio>();
		for (Iterator<PagoxHojaNegocio> itPagos = this.getPagos().iterator(); itPagos.hasNext();) {
			PagoxHojaNegocio pagoxHojaNegocio = (PagoxHojaNegocio) itPagos.next();
			if(pagoxHojaNegocio.getPagadoA().equals(com.sh.pvs.core.utils.Constantes.CODVENDEDOR)){
				pagosxgasto.add(pagoxHojaNegocio);
			}
		}
		return new ArrayList<PagoxHojaNegocio>(new TreeSet<PagoxHojaNegocio>(pagosxgasto));
	}
	
	@Transient
	public ArrayList<PagoxHojaNegocio> getPagosACia() {
		ArrayList<PagoxHojaNegocio> pagosxgasto = new ArrayList<PagoxHojaNegocio>();
		for (Iterator<PagoxHojaNegocio> itPagos = this.getPagos().iterator(); itPagos.hasNext();) {
			PagoxHojaNegocio pagoxHojaNegocio = (PagoxHojaNegocio) itPagos.next();
			if(pagoxHojaNegocio.getPagadoA().equals(com.sh.pvs.core.utils.Constantes.CODCIA)){
				pagosxgasto.add(pagoxHojaNegocio);
			}
		}
		return new ArrayList<PagoxHojaNegocio>(new TreeSet<PagoxHojaNegocio>(pagosxgasto));
	}
	
	@Transient
	public Double getTotalPagosACia() {
		Double total = 0.0;
		for (Iterator<PagoxHojaNegocio> itPagos = this.getPagos().iterator(); itPagos.hasNext();) {
			PagoxHojaNegocio pagoxHojaNegocio = (PagoxHojaNegocio) itPagos.next();
			if(pagoxHojaNegocio.getPagadoA().equals(com.sh.pvs.core.utils.Constantes.CODCIA)){
				total += pagoxHojaNegocio.getValor();
			}
		}
		return total;
	}
	
	@Transient
    public String getEstadoLbl(){
    	if(this.getEstado().equals(Constantes.HN_EST_ENPROCESO))
    		return Constantes.HN_EST_ENPROCESO_LBL;
    	if(this.getEstado().equals(Constantes.HN_EST_FINALIZADA))
    		return Constantes.HN_EST_FINALIZADA_LBL;
    	return "";
    }
	
}
