package com.sh.pvs.model.dto.seguridad;

import java.sql.Timestamp;
import javax.persistence.Entity;
import javax.persistence.Table;


/**
 * Auditoria entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="tb_auditorias"
    ,catalog="db_pvs"
)
public class Auditoria extends AbstractAuditoria implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	// Constructors
    /** default constructor */
    public Auditoria() {
    }

	/** minimal constructor */
    public Auditoria(String compania) {
        super(compania);        
    }
    
    /** full constructor */
    public Auditoria(String formulario, String usuario, String registro, String accion, Timestamp fecha, String observaciones, String compania) {
        super(formulario, usuario, registro, accion, fecha, observaciones, compania);        
    }
   
}
