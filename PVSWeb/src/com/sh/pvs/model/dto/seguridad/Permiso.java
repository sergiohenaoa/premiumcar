package com.sh.pvs.model.dto.seguridad;

import javax.persistence.Entity;
import javax.persistence.Table;


/**
 * Permiso entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="tb_permisos"
    ,catalog="db_pvs"
)
public class Permiso extends AbstractPermiso implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	// Constructors

    /** default constructor */
    public Permiso() {
    }

    
    /** full constructor */
    public Permiso(AccionxFormulario accionxFormulario, Perfil perfil) {
        super(accionxFormulario, perfil);        
    }
   
}
