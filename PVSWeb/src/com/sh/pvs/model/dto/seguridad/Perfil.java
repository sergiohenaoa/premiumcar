package com.sh.pvs.model.dto.seguridad;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * Perfil entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "tb_perfiles", catalog = "db_pvs")
public class Perfil extends AbstractPerfil implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	// Constructors

	/** default constructor */
	public Perfil() {
	}

	/** minimal constructor */
	public Perfil(Compania tbCompanias, String nombre, String descripcion,
			Boolean estado) {
		super(tbCompanias, nombre, descripcion, estado);
	}

	/** full constructor */
	public Perfil(Compania tbCompanias, String nombre, String descripcion,
			Boolean estado, Set<Permiso> permisos) {
		super(tbCompanias, nombre, descripcion, estado, permisos);
	}
	
	  /**
     * Método que retorna la lista de permisos formateada para presentar en el formulario de perfiles
     */
    @Transient
    public ArrayList<String[]> getPermisosToPerfil(){
    	ArrayList<String[]> permisosFormateados = new ArrayList<String[]>();
    	ArrayList<String> formulariosAgregados = new ArrayList<String>();
    	
    	for (Iterator<Permiso> itPermisos = this.getPermisos().iterator(); itPermisos.hasNext();) {
    		Permiso permiso = itPermisos.next();
    		if(formulariosAgregados.contains(permiso.getAccionxFormulario().getFormulario().getNombreDescriptivo())){
    			for (Iterator<String[]> itPerm = permisosFormateados.iterator(); itPerm.hasNext();) {
					String[] permisoForm = itPerm.next();
					if(permisoForm[0].equals(permiso.getAccionxFormulario().getFormulario().getNombreDescriptivo())){
						permisoForm[1] += ", " + permiso.getAccionxFormulario().getAccion().getNombre();
					}					
				}
    		}else{
    			String[] permisoFormateado = {permiso.getAccionxFormulario().getFormulario().getNombreDescriptivo(),permiso.getAccionxFormulario().getAccion().getNombre()};
    			permisosFormateados.add(permisoFormateado);
    			formulariosAgregados.add(permiso.getAccionxFormulario().getFormulario().getNombreDescriptivo());
    		}
		}
    	
    	return permisosFormateados;
    }	

}
