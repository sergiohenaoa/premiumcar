package com.sh.pvs.model.dto.seguridad;

import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Formulario entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "tb_formularios", catalog = "db_pvs")
public class Formulario extends AbstractFormulario implements
		java.io.Serializable {

	private static final long serialVersionUID = 1L;

	// Constructors

	/** default constructor */
	public Formulario() {
	}

	/** minimal constructor */
	public Formulario(String nombreDescriptivo, Boolean estado,
			Boolean esformulario, Integer orden) {
		super(nombreDescriptivo, estado, esformulario, orden);
	}

	/** full constructor */
	public Formulario(Formulario formulario, String nombreDescriptivo,
			String nombreTabla, String nombreClase, String url, Boolean estado,
			Boolean esformulario, Integer orden,
			Set<AccionxFormulario> accionxFormularios,
			Set<Formulario> formularios) {
		super(formulario, nombreDescriptivo, nombreTabla, nombreClase, url,
				estado, esformulario, orden, accionxFormularios, formularios);
	}

}
