package com.sh.pvs.model.dto.seguridad;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;


/**
 * AbstractPermiso entity provides the base persistence definition of the Permiso entity. @author MyEclipse Persistence Tools
 */
@MappedSuperclass
public abstract class AbstractPermiso  implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	// Fields    
     private Integer id;
     private AccionxFormulario accionxFormulario;
     private Perfil perfil;


    // Constructors

    /** default constructor */
    public AbstractPermiso() {
    }

    
    /** full constructor */
    public AbstractPermiso(AccionxFormulario accionxFormulario, Perfil perfil) {
        this.accionxFormulario = accionxFormulario;
        this.perfil = perfil;
    }

   
    // Property accessors
    @Id @GeneratedValue    
    @Column(name="id", unique=true, nullable=false)
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="accionxformulario_id", nullable=false)
    public AccionxFormulario getAccionxFormulario() {
        return this.accionxFormulario;
    }
    
    public void setAccionxFormulario(AccionxFormulario accionxFormulario) {
        this.accionxFormulario = accionxFormulario;
    }
    
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="perfil_id", nullable=false)
    public Perfil getPerfil() {
        return this.perfil;
    }
    
    public void setPerfil(Perfil perfil) {
        this.perfil = perfil;
    }

}