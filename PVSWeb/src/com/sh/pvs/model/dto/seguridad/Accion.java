package com.sh.pvs.model.dto.seguridad;

import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.Table;


/**
 * Accion entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="tb_acciones"
    ,catalog="db_pvs"
)
public class Accion extends AbstractAccion implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	// Constructors

    /** default constructor */
    public Accion() {
    }

	/** minimal constructor */
    public Accion(String nombre) {
        super(nombre);        
    }
    
    /** full constructor */
    public Accion(String nombre, Set<AccionxFormulario> accionxFormularios) {
        super(nombre, accionxFormularios);        
    }
   
}
