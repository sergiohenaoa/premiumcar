package com.sh.pvs.model.dto.seguridad;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;


/**
 * AbstractAccionxFormulario entity provides the base persistence definition of the AccionxFormulario entity. @author MyEclipse Persistence Tools
 */
@MappedSuperclass
public abstract class AbstractAccionxFormulario  implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	// Fields    
     private Integer id;
     private Accion accion;
     private Formulario formulario;
     private Set<Permiso> permisos = new HashSet<Permiso>(0);


    // Constructors

    /** default constructor */
    public AbstractAccionxFormulario() {
    }

	/** minimal constructor */
    public AbstractAccionxFormulario(Accion accion, Formulario formulario) {
        this.accion = accion;
        this.formulario = formulario;
    }
    
    /** full constructor */
    public AbstractAccionxFormulario(Accion accion, Formulario formulario, Set<Permiso> permisos) {
        this.accion = accion;
        this.formulario = formulario;
        this.permisos = permisos;
    }

   
    // Property accessors
    @Id @GeneratedValue    
    @Column(name="id", unique=true, nullable=false)
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="accion_id", nullable=false)
    public Accion getAccion() {
        return this.accion;
    }
    
    public void setAccion(Accion accion) {
        this.accion = accion;
    }
    
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="formulario_id", nullable=false)
    public Formulario getFormulario() {
        return this.formulario;
    }
    
    public void setFormulario(Formulario formulario) {
        this.formulario = formulario;
    }
    
    @OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="accionxFormulario")
    public Set<Permiso> getPermisos() {
        return this.permisos;
    }
    
    public void setPermisos(Set<Permiso> permisos) {
        this.permisos = permisos;
    }
   
}