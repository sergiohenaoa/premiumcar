package com.sh.pvs.model.dto.seguridad;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;


/**
 * AbstractAccion entity provides the base persistence definition of the Accion entity. @author MyEclipse Persistence Tools
 */
@MappedSuperclass

public abstract class AbstractAccion  implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	// Fields    

     private Integer id;
     private String nombre;
     private Set<AccionxFormulario> accionxFormularios = new HashSet<AccionxFormulario>(0);


    // Constructors

    /** default constructor */
    public AbstractAccion() {
    }

	/** minimal constructor */
    public AbstractAccion(String nombre) {
        this.nombre = nombre;
    }
    
    /** full constructor */
    public AbstractAccion(String nombre, Set<AccionxFormulario> accionxFormularios) {
        this.nombre = nombre;
        this.accionxFormularios = accionxFormularios;
    }

    // Property accessors
    @Id @GeneratedValue    
    @Column(name="id", unique=true, nullable=false)
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    
    @Column(name="nombre", nullable=false, length=50)
    public String getNombre() {
        return this.nombre;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    @OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="accion")
    public Set<AccionxFormulario> getAccionxFormularios() {
        return this.accionxFormularios;
    }
    
    public void setAccionxFormularios(Set<AccionxFormulario> accionxFormularios) {
        this.accionxFormularios = accionxFormularios;
    }
   
}