package com.sh.pvs.model.dto.seguridad;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;

/**
 * AbstractFormulario entity provides the base persistence definition of the Formulario entity. @author MyEclipse Persistence Tools
 */
@MappedSuperclass
public abstract class AbstractFormulario  implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	// Fields
	private Integer id;
     private Formulario formularioPadre;
     private Compania compania;
     private String nombreDescriptivo;
     private String nombreCarpeta;
     private String nombreClase;
     private String url;
     private Boolean esActivo;
     private Boolean esFormulario;
     private Integer orden;
     private Set<AccionxFormulario> accionxFormularios = new HashSet<AccionxFormulario>(0);
     private Set<Formulario> formularios = new HashSet<Formulario>(0);


    // Constructors

    /** default constructor */
    public AbstractFormulario() {
    }

	/** minimal constructor */
    public AbstractFormulario(String nombreDescriptivo, Boolean esActivo, Boolean esFormulario, Integer orden) {
        this.nombreDescriptivo = nombreDescriptivo;
        this.esActivo = esActivo;
        this.esFormulario = esFormulario;
        this.orden = orden;
    }
    
    /** full constructor */
    public AbstractFormulario(Formulario formularioPadre, String nombreDescriptivo, String nombreCarpeta, String nombreClase, String url, Boolean esActivo, Boolean esFormulario, Integer orden, Set<AccionxFormulario> accionxFormularios, Set<Formulario> formularios) {
        this.formularioPadre = formularioPadre;
        this.nombreDescriptivo = nombreDescriptivo;
        this.nombreCarpeta = nombreCarpeta;
        this.nombreClase = nombreClase;
        this.url = url;
        this.esActivo = esActivo;
        this.esFormulario = esFormulario;
        this.orden = orden;
        this.accionxFormularios = accionxFormularios;
        this.formularios = formularios;
    }

    // Property accessors
    @Id @GeneratedValue    
    @Column(name="id", unique=true, nullable=false)
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    
	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="formulario_padre_id")
    public Formulario getFormularioPadre() {
        return this.formularioPadre;
    }
    
    public void setFormularioPadre(Formulario formularioPadre) {
        this.formularioPadre = formularioPadre;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "compania_id", nullable = false)
	public Compania getCompania() {
		return this.compania;
	}

	public void setCompania(Compania compania) {
		this.compania = compania;
	}
    
    @Column(name="nombre_descriptivo", nullable=false, length=20)
    public String getNombreDescriptivo() {
        return this.nombreDescriptivo;
    }
    
    public void setNombreDescriptivo(String nombreDescriptivo) {
        this.nombreDescriptivo = nombreDescriptivo;
    }
    
    @Column(name="nombre_carpeta", length=100)
    public String getNombreCarpeta() {
        return this.nombreCarpeta;
    }
    
    public void setNombreCarpeta(String nombreCarpeta) {
        this.nombreCarpeta = nombreCarpeta;
    }
    
    @Column(name="nombre_clase", length=50)
    public String getNombreClase() {
        return this.nombreClase;
    }
    
    public void setNombreClase(String nombreClase) {
        this.nombreClase = nombreClase;
    }
    
    @Column(name="url", length=200)
    public String getUrl() {
        return this.url;
    }
    
    public void setUrl(String url) {
        this.url = url;
    }
    
    @Column(name="es_activo", nullable=false)
    public Boolean getEsActivo() {
        return this.esActivo;
    }
    
    public void setEsActivo(Boolean esActivo) {
        this.esActivo = esActivo;
    }
    
    @Column(name="es_formulario", nullable=false)
    public Boolean getEsFormulario() {
        return this.esFormulario;
    }
    
    public void setEsFormulario(Boolean esFormulario) {
        this.esFormulario = esFormulario;
    }
    
    @Column(name="orden", nullable=false)
    public Integer getOrden() {
        return this.orden;
    }
    
    public void setOrden(Integer orden) {
        this.orden = orden;
    }
    
    @OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="formulario")
    public Set<AccionxFormulario> getAccionxFormularios() {
        return this.accionxFormularios;
    }
    
    public void setAccionxFormularios(Set<AccionxFormulario> accionxFormularios) {
        this.accionxFormularios = accionxFormularios;
    }
    
    @OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="formularioPadre")
    public Set<Formulario> getFormularios() {
        return this.formularios;
    }
    
    public void setFormularios(Set<Formulario> formularios) {
        this.formularios = formularios;
    }

}