package com.sh.pvs.model.dto.seguridad;

import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.Table;


/**
 * AccionxFormulario entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="tb_accionesxformulario"
    ,catalog="db_pvs"
)
public class AccionxFormulario extends AbstractAccionxFormulario implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	// Constructors

    /** default constructor */
    public AccionxFormulario() {
    }

	/** minimal constructor */
    public AccionxFormulario(Accion accion, Formulario formulario) {
        super(accion, formulario);        
    }
    
    /** full constructor */
    public AccionxFormulario(Accion accion, Formulario formulario, Set<Permiso> permisos) {
        super(accion, formulario, permisos);        
    }
   
}
