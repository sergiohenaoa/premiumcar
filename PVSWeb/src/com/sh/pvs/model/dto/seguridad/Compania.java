package com.sh.pvs.model.dto.seguridad;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.sh.pvs.model.dto.configuracion.Constante;
import com.sh.pvs.model.dto.configuracion.MensajeNotificacion;


/**
 * Compania entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="tb_companias"
    ,catalog="db_pvs"
)
public class Compania extends AbstractCompania implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	// Constructors

    /** default constructor */
    public Compania() {
    }

	/** minimal constructor */
    public Compania(String codigo, String nombre, Boolean estado) {
        super(codigo, nombre, estado);        
    }
    
    /** full constructor */
    public Compania(String codigo, String nombre, Boolean estado, Set<Usuario> usuarios, Set<Perfil> perfils, Set<Constante> constantes, Set<MensajeNotificacion> mensajeNotificacions) {
        super(codigo, nombre, estado, usuarios, perfils, constantes, mensajeNotificacions);        
    }
   
}
