 package com.sh.pvs.model.dto.seguridad;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;

/**
 * AbstractUsuario entity provides the base persistence definition of the
 * Usuario entity. @author MyEclipse Persistence Tools
 */
@MappedSuperclass
public abstract class AbstractUsuario implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	// Fields
	private Integer id;
	private Compania compania;
	private String nombre;
	private String email;
	private String usuario;
	private String clave;
	private Boolean esActivo;
	private String direccion;
	private String telefono;
	private String celular;
	private Set<PerfilxUsuario> perfilesxusuario = new HashSet<PerfilxUsuario>(0);

	// Constructors

	/** default constructor */
	public AbstractUsuario() {
	}

	/** full constructor */
	public AbstractUsuario(Compania compania, String nombre,
			String email, String usuario, String clave, Boolean esActivo) {
		this.compania = compania;
		this.nombre = nombre;
		this.email = email;
		this.usuario = usuario;
		this.clave = clave;
		this.esActivo = esActivo;
	}

	// Property accessors
	@Id
	@GeneratedValue
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "compania_id", nullable = false)
	public Compania getCompania() {
		return this.compania;
	}

	public void setCompania(Compania compania) {
		this.compania = compania;
	}

	@Column(name = "nombre", nullable = false, length = 300)
	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Column(name = "email", nullable = false, length = 200)
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "usuario", nullable = false, length = 45)
	public String getUsuario() {
		return this.usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	@Column(name = "clave", nullable = false, length = 200)
	public String getClave() {
		return this.clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	@Column(name = "es_activo", nullable = false)
	public Boolean getEsActivo() {
		return this.esActivo;
	}

	public void setEsActivo(Boolean esActivo) {
		this.esActivo = esActivo;
	}

	@Column(name = "direccion", length = 1000)
	public String getDireccion() {
		return this.direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	
	@Column(name = "telefono", length = 45)
	public String getTelefono() {
		return this.telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	
	@Column(name = "celular", length = 45)
	public String getCelular() {
		return this.celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "usuario")
	public Set<PerfilxUsuario> getPerfilesxusuario() {
		return perfilesxusuario;
	}

	
	public void setPerfilesxusuario(Set<PerfilxUsuario> perfilesxusuario) {
		this.perfilesxusuario = perfilesxusuario;
	}
	
}