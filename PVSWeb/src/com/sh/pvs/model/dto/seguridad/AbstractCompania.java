package com.sh.pvs.model.dto.seguridad;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;

import com.sh.pvs.model.dto.configuracion.Constante;
import com.sh.pvs.model.dto.configuracion.MensajeNotificacion;


/**
 * AbstractCompania entity provides the base persistence definition of the Compania entity. @author MyEclipse Persistence Tools
 */
@MappedSuperclass
public abstract class AbstractCompania  implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	// Fields    
     private Integer id;
     private String codigo;
     private String nombre;
     private Boolean esActiva;
     private String cssFolder;
     private Set<Usuario> usuarios = new HashSet<Usuario>(0);
     private Set<Perfil> perfils = new HashSet<Perfil>(0);
     private Set<Constante> constantes = new HashSet<Constante>(0);
     private Set<MensajeNotificacion> mensajeNotificacions = new HashSet<MensajeNotificacion>(0);


    // Constructors

    /** default constructor */
    public AbstractCompania() {
    }

	/** minimal constructor */
    public AbstractCompania(String codigo, String nombre, Boolean esActiva) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.esActiva = esActiva;
    }
    
    /** full constructor */
    public AbstractCompania(String codigo, String nombre, Boolean esActiva, Set<Usuario> usuarios, Set<Perfil> perfils, Set<Constante> constantes, Set<MensajeNotificacion> mensajeNotificacions) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.esActiva = esActiva;
        this.usuarios = usuarios;
        this.perfils = perfils;
        this.constantes = constantes;
        this.mensajeNotificacions = mensajeNotificacions;
    }

    // Property accessors
    @Id @GeneratedValue    
    @Column(name="id", unique=true, nullable=false)
    public Integer getId() {
        return this.id;
    }
    
    
    public void setId(Integer id) {
        this.id = id;
    }
    
    
    @Column(name="codigo", unique=true, nullable=false, length=20)
    public String getCodigo() {
        return this.codigo;
    }
    
    
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
    
    @Column(name="nombre", unique=true, nullable=false, length=200)
    public String getNombre() {
        return this.nombre;
    }
    
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    
    @Column(name="es_activa", nullable=false)
    public Boolean getEsActiva() {
        return this.esActiva;
    }
    
    
    public void setEsActiva(Boolean esActiva) {
        this.esActiva = esActiva;
    }
    
    @Column(name="css_folder", unique=true, nullable=false, length=45)
    public String getCssFolder() {
        return this.cssFolder;
    }
    
    
    public void setCssFolder(String cssFolder) {
        this.cssFolder = cssFolder;
    }
    
    
    @OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="compania")
    public Set<Usuario> getUsuarios() {
        return this.usuarios;
    }
    
    
    public void setUsuarios(Set<Usuario> usuarios) {
        this.usuarios = usuarios;
    }
    
    
    @OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="compania")
    public Set<Perfil> getPerfils() {
        return this.perfils;
    }
    
    
    public void setPerfils(Set<Perfil> perfils) {
        this.perfils = perfils;
    }
    
    
    @OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="compania")
    public Set<Constante> getConstantes() {
        return this.constantes;
    }
    
    
    public void setConstantes(Set<Constante> constantes) {
        this.constantes = constantes;
    }
    
    
    @OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="compania")
    public Set<MensajeNotificacion> getMensajeNotificacions() {
        return this.mensajeNotificacions;
    }
    
    
    public void setMensajeNotificacions(Set<MensajeNotificacion> mensajeNotificacions) {
        this.mensajeNotificacions = mensajeNotificacions;
    }

    
   
}