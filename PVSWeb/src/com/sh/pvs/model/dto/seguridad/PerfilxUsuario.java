package com.sh.pvs.model.dto.seguridad;

import javax.persistence.Entity;
import javax.persistence.Table;


/**
 * PerfilxUsuario entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="tb_perfilesxusuario"
    ,catalog="db_pvs"
)
public class PerfilxUsuario extends AbstractPerfilxUsuario implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	// Constructors

    /** default constructor */
    public PerfilxUsuario() {
    }

	/** minimal constructor */
    public PerfilxUsuario(Perfil perfil, Usuario usuario) {
        super(perfil, usuario);        
    }
    
}
