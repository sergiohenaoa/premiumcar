package com.sh.pvs.model.dto.seguridad;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Usuario entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "tb_usuarios",  catalog = "db_pvs")
public class Usuario extends AbstractUsuario implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	// Constructors

	/** default constructor */
	public Usuario() {
	}

	
	/** full constructor */
	public Usuario(Compania compania, String nombre, String email,
			String usuario, String clave, Boolean esActivo) {
		super(compania, nombre, email, usuario, clave, esActivo);
	}

}
