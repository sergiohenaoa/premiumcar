package com.sh.pvs.model.dto.seguridad;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;


/**
 * AbstractPerfilxUsuario entity provides the base persistence definition of the PerfilxUsuario entity. @author MyEclipse Persistence Tools
 */
@MappedSuperclass
public abstract class AbstractPerfilxUsuario  implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	// Fields    
     private Integer id;
     private Perfil perfil;
     private Usuario usuario;

    // Constructors

    /** default constructor */
    public AbstractPerfilxUsuario() {
    }

	/** full constructor */
    public AbstractPerfilxUsuario(Perfil perfil, Usuario usuario) {
        this.perfil = perfil;
        this.usuario = usuario;
    }
    
    // Property accessors
    @Id @GeneratedValue    
    @Column(name="id", unique=true, nullable=false)
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="perfil_id", nullable=false)
    public Perfil getPerfil() {
        return this.perfil;
    }
    
    public void setPerfil(Perfil perfil) {
        this.perfil = perfil;
    }
    
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="usuario_id", nullable=false)
    public Usuario getUsuario() {
        return this.usuario;
    }
    
    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
   
}