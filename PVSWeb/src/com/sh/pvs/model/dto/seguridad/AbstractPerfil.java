package com.sh.pvs.model.dto.seguridad;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;

/**
 * AbstractPerfil entity provides the base persistence definition of the Perfil
 * entity. @author MyEclipse Persistence Tools
 */
@MappedSuperclass
public abstract class AbstractPerfil implements java.io.Serializable {

	// Fields
	private static final long serialVersionUID = 1L;
	private Integer id;
	private Compania compania;
	private String nombre;
	private String descripcion;
	private Boolean esActivo;
	private Set<Permiso> permisos = new HashSet<Permiso>(0);

	// Constructors

	/** default constructor */
	public AbstractPerfil() {
	}

	/** minimal constructor */
	public AbstractPerfil(Compania compania, String nombre,
			String descripcion, Boolean esActivo) {
		this.compania = compania;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.esActivo = esActivo;
	}

	/** full constructor */
	public AbstractPerfil(Compania compania, String nombre,
			String descripcion, Boolean esActivo, Set<Permiso> permisos) {
		this.compania = compania;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.esActivo = esActivo;
		this.permisos = permisos;
	}

	// Property accessors
	@Id
	@GeneratedValue
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "compania_id", nullable = false)
	public Compania getCompania() {
		return this.compania;
	}

	public void setCompania(Compania compania) {
		this.compania = compania;
	}

	@Column(name = "nombre", nullable = false, length = 20)
	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Column(name = "descripcion", length = 1000)
	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Column(name = "es_activo", nullable = false)
	public Boolean getEsActivo() {
		return this.esActivo;
	}

	public void setEsActivo(Boolean esActivo) {
		this.esActivo = esActivo;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "perfil")
	public Set<Permiso> getPermisos() {
		return this.permisos;
	}

	public void setPermisos(Set<Permiso> permisos) {
		this.permisos = permisos;
	}

}