package com.sh.pvs.model.dto.seguridad;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;


/**
 * AbstractAuditoria entity provides the base persistence definition of the Auditoria entity. @author MyEclipse Persistence Tools
 */
@MappedSuperclass

public abstract class AbstractAuditoria  implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	// Fields    
     private Integer id;
     private String formulario;
     private String usuario;
     private String registro;
     private String accion;
     private Timestamp fecha;
     private String observaciones;
     private String compania;
     private Integer companiaId;

    // Constructors

    /** default constructor */
    public AbstractAuditoria() {
    }

	/** minimal constructor */
    public AbstractAuditoria(String compania) {
        this.compania = compania;
    }
    
    /** full constructor */
    public AbstractAuditoria(String formulario, String usuario, String registro, String accion, Timestamp fecha, String observaciones, String compania) {
        this.formulario = formulario;
        this.usuario = usuario;
        this.registro = registro;
        this.accion = accion;
        this.fecha = fecha;
        this.observaciones = observaciones;
        this.compania = compania;
    }

    // Property accessors
    @Id @GeneratedValue
    @Column(name="id", unique=true, nullable=false)
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    
    @Column(name="formulario", length=100)
    public String getFormulario() {
        return this.formulario;
    }
    
    public void setFormulario(String formulario) {
        this.formulario = formulario;
    }
    
    @Column(name="usuario", length=100)
    public String getUsuario() {
        return this.usuario;
    }
    
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
    
    @Column(name="registro", length=100)
    public String getRegistro() {
        return this.registro;
    }
    
    public void setRegistro(String registro) {
        this.registro = registro;
    }
    
    @Column(name="accion", length=100)
    public String getAccion() {
        return this.accion;
    }
    
    public void setAccion(String accion) {
        this.accion = accion;
    }
    
    @Column(name="fecha", length=16)
    public Timestamp getFecha() {
        return this.fecha;
    }
    
    public void setFecha(Timestamp fecha) {
        this.fecha = fecha;
    }
    
    @Column(name="observaciones")
    public String getObservaciones() {
        return this.observaciones;
    }
    
    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }
    
    @Column(name="compania", nullable=false, length=100)
    public String getCompania() {
        return this.compania;
    }
    
    public void setCompania(String compania) {
        this.compania = compania;
    }

	
    @Column(name="compania_id")
    public Integer getCompaniaId() {
		return companiaId;
	}
    

	public void setCompaniaId(Integer companiaId) {
		this.companiaId = companiaId;
	}

}