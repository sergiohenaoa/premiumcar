package com.sh.pvs.model.dto.venta;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import com.sh.pvs.model.dto.configuracion.Cliente;
import com.sh.pvs.model.dto.configuracion.Vehiculo;
import com.sh.pvs.model.dto.consignacion.Consignacion;
import com.sh.pvs.model.dto.retoma.Retoma;
import com.sh.pvs.model.dto.seguridad.Compania;
import com.sh.pvs.model.utils.Constantes;


/**
 * AbstractVenta entity provides the base persistence definition of the Venta entity. @author MyEclipse Persistence Tools
 */
@MappedSuperclass

public abstract class AbstractVenta  implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	// Fields    

     private Integer id;
     private Cliente cliente;
     private Vehiculo vehiculo;
     private Compania compania;
     private Consignacion consignacion;
     private Retoma retoma;
     private String codigo;
     private Double valorVehiculo;
     private Date fchVenta = new Date();
     private String identificacionMatricula;
     private String nombreMatricula;
     private Boolean requiereDatosMatricula;
     private String estado = Constantes.VENTA_ESTADO_INICIAL_COD;
     
    // Constructors

    /** default constructor */
    public AbstractVenta() {
    }

	/** minimal constructor */
    public AbstractVenta(Cliente cliente, Vehiculo vehiculo, String codigo, Double valorVehiculo, Date fchVenta, Boolean requiereDatosMatricula, String estado) {
        this.cliente = cliente;
        this.vehiculo = vehiculo;
        this.codigo = codigo;
        this.valorVehiculo = valorVehiculo;
        this.fchVenta = fchVenta;
        this.requiereDatosMatricula = requiereDatosMatricula;
        this.estado = estado;
    }

   
    // Property accessors
    @Id @GeneratedValue
    @Column(name="id", unique=true, nullable=false)
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="cliente_id", nullable=false)
    public Cliente getCliente() {
        return this.cliente;
    }
    
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
    
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="vehiculo_id", nullable=false)
    public Vehiculo getVehiculo() {
        return this.vehiculo;
    }
    
    public void setVehiculo(Vehiculo vehiculo) {
        this.vehiculo = vehiculo;
    }
    
    @ManyToOne(fetch=FetchType.EAGER)
	    @JoinColumn(name="compania_id", nullable=false)
	public Compania getCompania() {
	    return this.compania;
	}
	
	public void setCompania(Compania companias) {
	    this.compania = companias;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="consignacion_id")
	public Consignacion getConsignacion() {
	    return this.consignacion;
	}
	
	public void setConsignacion(Consignacion consignacion) {
	    this.consignacion = consignacion;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="retoma_id")
	public Retoma getRetoma() {
	    return this.retoma;
	}
	
	public void setRetoma(Retoma retoma) {
	    this.retoma = retoma;
	}

    @Column(name="codigo", nullable=false, length=45)
    public String getCodigo() {
        return this.codigo;
    }
    
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
    
    @Column(name="valor_vehiculo", nullable=false, precision=17, scale=4)
    public Double getValorVehiculo() {
        return this.valorVehiculo;
    }
    
    public void setValorVehiculo(Double valorVehiculo) {
        this.valorVehiculo = valorVehiculo;
    }
    
    @Column(name="fch_venta", nullable=false, length=19)
    public Date getFchVenta() {
        return this.fchVenta;
    }
    
    public void setFchVenta(Date fchVenta) {
        this.fchVenta = fchVenta;
    }
    
    @Column(name="requiere_datos_matricula", nullable=false)
    public Boolean getRequiereDatosMatricula() {
        return this.requiereDatosMatricula;
    }
    
    public void setRequiereDatosMatricula(Boolean requiereDatosMatricula) {
        this.requiereDatosMatricula = requiereDatosMatricula;
    }
    
    @Column(name="estado", nullable=false, length=2)
    public String getEstado() {
        return this.estado;
    }
    
    public void setEstado(String estado) {
        this.estado = estado;
    }
    
    @Column(name="identificacion_matricula", length=50)
    public String getIdentificacionMatricula() {
		return identificacionMatricula;
	}

	public void setIdentificacionMatricula(String identificacionMatricula) {
		this.identificacionMatricula = identificacionMatricula;
	}
	
	@Column(name="nombre_matricula", length=200)
	public String getNombreMatricula() {
		return nombreMatricula;
	}

	public void setNombreMatricula(String nombreMatricula) {
		this.nombreMatricula = nombreMatricula;
	}
}