package com.sh.pvs.model.dto.venta;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import com.sh.pvs.model.dto.configuracion.Cliente;
import com.sh.pvs.model.dto.configuracion.Cuenta;
import com.sh.pvs.model.dto.configuracion.FormaPago;
import com.sh.pvs.model.dto.configuracion.Vehiculo;
import com.sh.pvs.model.dto.seguridad.Compania;


/**
 * AbstractVentaTercero entity provides the base persistence definition of the VentaTercero entity. @author MyEclipse Persistence Tools
 */
@MappedSuperclass

public abstract class AbstractVentaTercero  implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	// Fields    

     private Integer id;
     private Cliente cliente;
     private Vehiculo vehiculo;
     private Compania compania;
     private String codigo;
     private Double comision;
     private Date fchVenta = new Date();
     private Double vlrVehiculo;
     private FormaPago formaPago = new FormaPago();
     private Cuenta cuentaEntrada = new Cuenta();

    // Constructors

    /** default constructor */
    public AbstractVentaTercero() {
    }

	/** minimal constructor */
    public AbstractVentaTercero(Cliente cliente, Vehiculo vehiculo, String codigo, Double comision, Date fchVenta) {
        this.cliente = cliente;
        this.vehiculo = vehiculo;
        this.codigo = codigo;
        this.comision = comision;
        this.fchVenta = fchVenta;
    }
    
    // Property accessors
    @Id @GeneratedValue
    @Column(name="id", unique=true, nullable=false)
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="cliente_id", nullable=false)
    public Cliente getCliente() {
        return this.cliente;
    }
    
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
    
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="vehiculo_id", nullable=false)
    public Vehiculo getVehiculo() {
        return this.vehiculo;
    }
    
    public void setVehiculo(Vehiculo vehiculo) {
        this.vehiculo = vehiculo;
    }
    
    @ManyToOne(fetch=FetchType.EAGER)
	    @JoinColumn(name="compania_id", nullable=false)
	public Compania getCompania() {
	    return this.compania;
	}
	
	public void setCompania(Compania companias) {
	    this.compania = companias;
	}

    @Column(name="codigo", nullable=false, length=45)
    public String getCodigo() {
        return this.codigo;
    }
    
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    @Column(name="comision", nullable=false, precision=17, scale=4)
    public Double getComision() {
        return this.comision;
    }
    
    public void setComision(Double comision) {
        this.comision = comision;
    }

    @Column(name="fch_venta", nullable=false, length=19)
    public Date getFchVenta() {
        return this.fchVenta;
    }
    
    public void setFchVenta(Date fchVenta) {
        this.fchVenta = fchVenta;
    }

    @Column(name="vlr_vehiculo", nullable=false, precision=17, scale=4)
    public Double getVlrVehiculo() {
        return this.vlrVehiculo;
    }
    
    public void setVlrVehiculo(Double vlrVehiculo) {
        this.vlrVehiculo = vlrVehiculo;
    }
    
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="formapagocomision_id", nullable=false)
	public FormaPago getFormaPago() {
		return formaPago;
	}

	public void setFormaPago(FormaPago formaPago) {
		this.formaPago = formaPago;
	}

	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="cuentaentradacomision_id", nullable=false)
	public Cuenta getCuentaEntrada() {
		return cuentaEntrada;
	}

	public void setCuentaEntrada(Cuenta cuentaEntrada) {
		this.cuentaEntrada = cuentaEntrada;
	}
	
	
}