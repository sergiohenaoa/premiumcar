package com.sh.pvs.model.dto.venta;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.sh.pvs.model.dto.configuracion.Cliente;
import com.sh.pvs.model.dto.configuracion.Vehiculo;
import com.sh.pvs.model.utils.Constantes;


/**
 * Venta entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="tb_ventas"
    ,catalog="db_pvs"
)
public class Venta extends AbstractVenta implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	// Constructors

    /** default constructor */
    public Venta() {
    }

	/** minimal constructor */
    public Venta(Cliente tbClientes, Vehiculo vehiculo, String codigo, Double valorVehiculo, Timestamp fchVenta, Boolean requiereDatosMatricula, String estado) {
        super(tbClientes, vehiculo, codigo, valorVehiculo, fchVenta, requiereDatosMatricula, estado);        
    }
    
    @Transient
    public String getEstadoLbl(){
    	if(this.getEstado().equals(Constantes.VENTA_ESTADO_INICIAL_COD))
    		return Constantes.VENTA_ESTADO_INICIAL_LBL;
    	if(this.getEstado().equals(Constantes.VENTA_ESTADO_ENPROCESO_COD))
    		return Constantes.VENTA_ESTADO_ENPROCESO_LBL;
    	if(this.getEstado().equals(Constantes.VENTA_ESTADO_FINALIZADA_COD))
    		return Constantes.VENTA_ESTADO_FINALIZADA_LBL;
    	if(this.getEstado().equals(Constantes.VENTA_ESTADO_DECLINADA_COD))
    		return Constantes.VENTA_ESTADO_DECLINADA_LBL;
    	return "";
    }

}
