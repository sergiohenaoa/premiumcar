package com.sh.pvs.model.dto.venta;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.sh.pvs.model.dto.configuracion.Cliente;
import com.sh.pvs.model.dto.configuracion.Vehiculo;


/**
 * VentaTercero entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="tb_ventastercero"
    ,catalog="db_pvs"
)
public class VentaTercero extends AbstractVentaTercero implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	// Constructors

    /** default constructor */
    public VentaTercero() {
    }

	/** minimal constructor */
    public VentaTercero(Cliente tbClientes, Vehiculo vehiculo, String codigo, Double valorVehiculo, Timestamp fchVentaTercero) {
        super(tbClientes, vehiculo, codigo, valorVehiculo, fchVentaTercero);        
    }
    
}
