package com.sh.pvs.model.dto.configuracion;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Carroceria entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "tb_carrocerias", catalog = "db_pvs")
public class Carroceria extends AbstractCarroceria implements
		java.io.Serializable {

	private static final long serialVersionUID = 1L;

	// Constructors

	/** default constructor */
	public Carroceria() {
	}

	/** minimal constructor */
	public Carroceria(String nombre, String descripcion, Boolean esActivo) {
		super(nombre, descripcion, esActivo);
	}

}
