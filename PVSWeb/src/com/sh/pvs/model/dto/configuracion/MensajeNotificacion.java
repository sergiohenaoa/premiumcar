package com.sh.pvs.model.dto.configuracion;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.sh.pvs.model.dto.seguridad.Compania;

/**
 * MensajeNotificacion entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "tb_mensajesnotificacion", catalog = "db_pvs")
public class MensajeNotificacion extends AbstractMensajeNotificacion implements
		java.io.Serializable {

	private static final long serialVersionUID = 1L;

	// Constructors

	/** default constructor */
	public MensajeNotificacion() {
	}

	/** full constructor */
	public MensajeNotificacion(Compania companias, String codigo,
			String asunto, String mensaje) {
		super(companias, codigo, asunto, mensaje);
	}

}
