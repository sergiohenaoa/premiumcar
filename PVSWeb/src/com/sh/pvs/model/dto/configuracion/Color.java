package com.sh.pvs.model.dto.configuracion;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Color entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "tb_colores", catalog = "db_pvs")
public class Color extends AbstractColor implements
		java.io.Serializable {

	private static final long serialVersionUID = 1L;

	// Constructors

	/** default constructor */
	public Color() {
	}

	/** minimal constructor */
	public Color(String nombre, String descripcion, Boolean esActivo) {
		super(nombre, descripcion, esActivo);
	}

}
