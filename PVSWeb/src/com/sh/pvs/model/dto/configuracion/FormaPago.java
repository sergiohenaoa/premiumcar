package com.sh.pvs.model.dto.configuracion;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * FormaPago entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "tb_formaspago", catalog = "db_pvs")
public class FormaPago extends AbstractFormaPago implements
		java.io.Serializable {

	private static final long serialVersionUID = 1L;

	// Constructors

	/** default constructor */
	public FormaPago() {
	}

	/** minimal constructor */
	public FormaPago(String nombre, String descripcion, Boolean esActivo) {
		super(nombre, descripcion, esActivo);
	}

}
