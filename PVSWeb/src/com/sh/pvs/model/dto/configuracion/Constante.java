package com.sh.pvs.model.dto.configuracion;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.sh.pvs.model.dto.seguridad.Compania;

/**
 * Constante entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "tb_constantes", catalog = "db_pvs")
public class Constante extends AbstractConstante implements
		java.io.Serializable {

	private static final long serialVersionUID = 1L;

	// Constructors

	/** default constructor */
	public Constante() {
	}

	/** minimal constructor */
	public Constante(Compania companias, String codigo, String valor) {
		super(companias, codigo, valor);
	}

	/** full constructor */
	public Constante(Compania companias, String codigo, String valor,
			String descripcion) {
		super(companias, codigo, valor, descripcion);
	}

}
