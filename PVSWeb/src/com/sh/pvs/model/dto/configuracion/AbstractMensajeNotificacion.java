package com.sh.pvs.model.dto.configuracion;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import com.sh.pvs.model.dto.seguridad.Compania;


/**
 * AbstractMensajeNotificacion entity provides the base persistence definition of the MensajeNotificacion entity. @author MyEclipse Persistence Tools
 */
@MappedSuperclass

public abstract class AbstractMensajeNotificacion  implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	// Fields  
     private Integer id;
     private Compania compania;
     private String codigo;
     private String asunto;
     private String mensaje;


    // Constructors

    /** default constructor */
    public AbstractMensajeNotificacion() {
    }

    
    /** full constructor */
    public AbstractMensajeNotificacion(Compania compania, String codigo, String asunto, String mensaje) {
        this.compania = compania;
        this.codigo = codigo;
        this.asunto = asunto;
        this.mensaje = mensaje;
    }

   
    // Property accessors
    @Id @GeneratedValue    
    @Column(name="id", unique=true, nullable=false)
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="compania_id", nullable=false)
    public Compania getCompania() {
        return this.compania;
    }
    
    public void setCompania(Compania compania) {
        this.compania = compania;
    }
    
    @Column(name="codigo", nullable=false, length=20)
    public String getCodigo() {
        return this.codigo;
    }
    
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
    
    @Column(name="asunto", nullable=false, length=100)
    public String getAsunto() {
        return this.asunto;
    }
    
    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }
    
    @Column(name="mensaje", nullable=false)
    public String getMensaje() {
        return this.mensaje;
    }
    
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

}