package com.sh.pvs.model.dto.configuracion;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import com.sh.pvs.model.dto.seguridad.Compania;


/**
 * AbstractConcesionario entity provides the base persistence definition of the Concesionario entity. @author MyEclipse Persistence Tools
 */
@MappedSuperclass
public abstract class AbstractConcesionario  implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	// Fields
     private Integer id;
     private Compania compania;
     private String nombre;
     private String descripcion;
     private Boolean esActivo;
     private String nit;
     private String telefono;
     private String contacto;
     private String numeroCuenta;
     private Boolean aplicaRetoma;

    // Constructors
     /** default constructor */
     public AbstractConcesionario() {
     }
     
     /** minimal constructor */
     public AbstractConcesionario(String nombre, String descripcion, Boolean esActivo) {
 		this.nombre = nombre;
 		this.descripcion = descripcion;
 		this.esActivo = esActivo;
 	}
   
    // Property accessors
    @Id @GeneratedValue    
    @Column(name="id", unique=true, nullable=false)
    public Integer getId() {
        return this.id;
    }
    
	public void setId(Integer id) {
        this.id = id;
    }
    
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="compania_id", nullable=false)
    public Compania getCompania() {
        return this.compania;
    }
    
    public void setCompania(Compania companias) {
        this.compania = companias;
    }
    
    @Column(name="nombre", nullable=false, length=100)
    public String getNombre() {
        return this.nombre;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    @Column(name="descripcion")
    public String getDescripcion() {
        return this.descripcion;
    }
    
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
   
    @Column(name="es_activo", nullable=false)
    public Boolean getEsActivo() {
        return this.esActivo;
    }
    
    public void setEsActivo(Boolean esActivo) {
        this.esActivo = esActivo;
    }
	
    @Column(name="nit", length=45)
    public String getNit() {
		return nit;
	}

	public void setNit(String nit) {
		this.nit = nit;
	}
	
	@Column(name="telefono", length=45)
	public String getTelefono() {
		return telefono;
	}
	
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	
	@Column(name="contacto", length=200)
	public String getContacto() {
		return contacto;
	}
	
	public void setContacto(String contacto) {
		this.contacto = contacto;
	}
	
	@Column(name="numero_cuenta", length=45)
	public String getNumeroCuenta() {
		return numeroCuenta;
	}

	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}
   
	@Column(name="aplica_retoma", nullable=false)
    public Boolean getAplicaRetoma() {
        return this.aplicaRetoma;
    }
    
    public void setAplicaRetoma(Boolean aplicaRetoma) {
        this.aplicaRetoma = aplicaRetoma;
    }
    
}