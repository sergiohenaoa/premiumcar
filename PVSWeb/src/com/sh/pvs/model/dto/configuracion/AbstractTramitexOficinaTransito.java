package com.sh.pvs.model.dto.configuracion;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;


/**
 * AbstractTramitexOficinaTransito entity provides the base persistence definition of the TramitexOficinaTransito entity. @author MyEclipse Persistence Tools
 */
@MappedSuperclass
public abstract class AbstractTramitexOficinaTransito  implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	// Fields
     private Integer id;
     private OficinaTransito oficinaTransito;
     private Gasto gasto;
     private String descripcion;
     private String asignadoA;
     private Double valor;

    // Constructors
     /** default constructor */
     public AbstractTramitexOficinaTransito() {
     }
     
     /** minimal constructor */
     public AbstractTramitexOficinaTransito(String descripcion, Double valor) {
 		this.descripcion = descripcion;
 		this.valor = valor;
 	}
   
    // Property accessors
    @Id @GeneratedValue    
    @Column(name="id", unique=true, nullable=false)
    public Integer getId() {
        return this.id;
    }
    
	public void setId(Integer id) {
        this.id = id;
    }
    
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="oficinaTransito_id", nullable=false)
    public OficinaTransito getOficinaTransito() {
        return this.oficinaTransito;
    }
    
    public void setOficinaTransito(OficinaTransito oficinaTransito) {
        this.oficinaTransito = oficinaTransito;
    }
    
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="gasto_id", nullable=false)
	public Gasto getGasto() {
	    return this.gasto;
	}
	
	public void setGasto(Gasto gasto) {
	    this.gasto = gasto;
	}
    
    @Column(name="descripcion")
    public String getDescripcion() {
        return this.descripcion;
    }
    
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
   
    @Column(name="valor", nullable=false)
    public Double getValor() {
        return this.valor;
    }
    
    public void setValor(Double valor) {
        this.valor = valor;
    }

	
    @Column(name="asignado_a", nullable=false, length=2)
    public String getAsignadoA() {
		return asignadoA;
	}
    

	public void setAsignadoA(String asignadoA) {
		this.asignadoA = asignadoA;
	}
}