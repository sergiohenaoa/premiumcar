package com.sh.pvs.model.dto.configuracion;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * FotoxVehiculo entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "tb_fotosxvehiculo", catalog = "db_pvs")
public class FotoxVehiculo extends AbstractFotoxVehiculo implements
		java.io.Serializable {

	private static final long serialVersionUID = 1L;

	// Constructors

	/** default constructor */
	public FotoxVehiculo() {
	}

}
