package com.sh.pvs.model.dto.configuracion;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Cliente entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "tb_clientes", catalog = "db_pvs")
public class Cliente extends AbstractCliente implements
		java.io.Serializable {

	private static final long serialVersionUID = 1L;

	// Constructors

	/** default constructor */
	public Cliente() {
	}

	/** minimal constructor */
	public Cliente(String cedula, String nombre, String observaciones) {
		super(cedula, nombre, observaciones);
	}

}
