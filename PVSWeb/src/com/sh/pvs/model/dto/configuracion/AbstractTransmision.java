package com.sh.pvs.model.dto.configuracion;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import com.sh.pvs.model.dto.seguridad.Compania;


/**
 * AbstractTransmision entity provides the base persistence definition of the Transmision entity. @author MyEclipse Persistence Tools
 */
@MappedSuperclass
public abstract class AbstractTransmision  implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	// Fields
     private Integer id;
     private Compania compania;
     private String nombre;
     private String descripcion;
     private Boolean esActivo;

    // Constructors
     /** default constructor */
     public AbstractTransmision() {
     }
     
     /** minimal constructor */
     public AbstractTransmision(String nombre, String descripcion, Boolean esActivo) {
 		this.nombre = nombre;
 		this.descripcion = descripcion;
 		this.esActivo = esActivo;
 	}
   
    // Property accessors
    @Id @GeneratedValue    
    @Column(name="id", unique=true, nullable=false)
    public Integer getId() {
        return this.id;
    }
    
	public void setId(Integer id) {
        this.id = id;
    }
    
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="compania_id", nullable=false)
    public Compania getCompania() {
        return this.compania;
    }
    
    public void setCompania(Compania companias) {
        this.compania = companias;
    }
    
    @Column(name="nombre", nullable=false, length=100)
    public String getNombre() {
        return this.nombre;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    @Column(name="descripcion")
    public String getDescripcion() {
        return this.descripcion;
    }
    
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
   
    @Column(name="es_activo", nullable=false)
    public Boolean getEsActivo() {
        return this.esActivo;
    }
    
    public void setEsActivo(Boolean esActivo) {
        this.esActivo = esActivo;
    }
    
}