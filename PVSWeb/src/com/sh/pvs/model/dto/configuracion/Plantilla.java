package com.sh.pvs.model.dto.configuracion;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Plantilla entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "tb_plantillas", catalog = "db_pvs")
public class Plantilla extends AbstractPlantilla implements
		java.io.Serializable {

	private static final long serialVersionUID = 1L;

	// Constructors

	/** default constructor */
	public Plantilla() {
	}

	/** minimal constructor */
	public Plantilla(String nombre, String descripcion, Boolean esActivo) {
		super(nombre, descripcion, esActivo);
	}

}
