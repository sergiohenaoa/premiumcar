package com.sh.pvs.model.dto.configuracion;

import com.sh.pvs.model.dto.seguridad.Compania;

import javax.persistence.*;


/**
 * AbstractCliente entity provides the base persistence definition of the Cliente entity. @author MyEclipse Persistence Tools
 */
@MappedSuperclass
public abstract class AbstractCliente  implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	// Fields
     private Integer id;
     private Compania compania;
     private String cedula;
     private String nombres;
     private String apellidos;
     private String telefono1;
     private String celular1;
     private String correoElectronico;
     private String telefono2;
     private String celular2;
     private String empresa;
     private String cargo;
     private String observaciones;
     private String direccion;
     private String ciudad;
	  private Boolean esEmpresa = false;

    // Constructors
     /** default constructor */
     public AbstractCliente() {
     }
     
     /** minimal constructor */
     public AbstractCliente(String cedula, String nombres, String observaciones) {
 		this.cedula = cedula;
    	this.nombres = nombres;
 		this.observaciones = observaciones;
 	}
   
    // Property accessors
    @Id @GeneratedValue    
    @Column(name="id", unique=true, nullable=false)
    public Integer getId() {
        return this.id;
    }
    
	public void setId(Integer id) {
        this.id = id;
    }
    
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="compania_id", nullable=false)
    public Compania getCompania() {
        return this.compania;
    }
    
    public void setCompania(Compania companias) {
        this.compania = companias;
    }

	
    @Column(name="cedula", nullable=false, length=45)
    public String getCedula() {
		return cedula;
	}
    

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}
	
	@Column(name="nombres", nullable=false, length=200)
	public String getNombres() {
		return nombres;
	}
	
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	
	@Column(name="apellidos", nullable=false, length=200)
	public String getApellidos() {
		return apellidos;
	}
	
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	
	@Column(name="telefono_1", nullable=true, length=45)
	public String getTelefono1() {
		return telefono1;
	}
	

	public void setTelefono1(String telefono1) {
		this.telefono1 = telefono1;
	}
	
	@Column(name="celular_1", nullable=false, length=45)
	public String getCelular1() {
		return celular1;
	}
	

	public void setCelular1(String celular1) {
		this.celular1 = celular1;
	}
	
	@Column(name="correo_electronico", length=200)
	public String getCorreoElectronico() {
		return correoElectronico;
	}
	

	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}
	
	@Column(name="telefono_2", length=45)
	public String getTelefono2() {
		return telefono2;
	}
	

	public void setTelefono2(String telefono2) {
		this.telefono2 = telefono2;
	}
	
	@Column(name="celular_2", length=45)
	public String getCelular2() {
		return celular2;
	}
	

	public void setCelular2(String celular2) {
		this.celular2 = celular2;
	}
	
	@Column(name="empresa", length=200)
	public String getEmpresa() {
		return empresa;
	}
	

	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}
	
	@Column(name="cargo", length=200)
	public String getCargo() {
		return cargo;
	}
	
	public void setCargo(String cargo) {
		this.cargo = cargo;
	}
	
	@Column(name="observaciones")
	public String getObservaciones() {
		return observaciones;
	}
	
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	
	@Column(name="direccion", length=500)
	public String getDireccion() {
		return direccion;
	}
	
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	
	@Column(name="ciudad", length=200)
	public String getCiudad() {
		return ciudad;
	}
	
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	
	@Column(name="esEmpresa", nullable=false)
	public Boolean getEsEmpresa()
	{
		return esEmpresa;
	}
	
	public void setEsEmpresa(Boolean esEmpresa)
	{
		this.esEmpresa = esEmpresa;
	}
}
