package com.sh.pvs.model.dto.configuracion;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Transmision entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "tb_transmisiones", catalog = "db_pvs")
public class Transmision extends AbstractTransmision implements
		java.io.Serializable {

	private static final long serialVersionUID = 1L;

	// Constructors

	/** default constructor */
	public Transmision() {
	}

	/** minimal constructor */
	public Transmision(String nombre, String descripcion, Boolean esActivo) {
		super(nombre, descripcion, esActivo);
	}

}
