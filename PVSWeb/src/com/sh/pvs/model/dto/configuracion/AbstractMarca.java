package com.sh.pvs.model.dto.configuracion;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;

import com.sh.pvs.model.dto.seguridad.Compania;


/**
 * AbstractMarca entity provides the base persistence definition of the Marca entity. @author MyEclipse Persistence Tools
 */
@MappedSuperclass
public abstract class AbstractMarca  implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	// Fields
     private Integer id;
     private Compania compania;
     private String nombre;
     private String descripcion;
     private Boolean esActivo;
     private Set<TipoVehiculoxMarca> tiposvehiculo = new HashSet<TipoVehiculoxMarca>(0);

    // Constructors
     /** default constructor */
     public AbstractMarca() {
     }
     
     /** minimal constructor */
     public AbstractMarca(String nombre, String descripcion, Boolean esActivo) {
 		this.nombre = nombre;
 		this.descripcion = descripcion;
 		this.esActivo = esActivo;
 	}
   
    // Property accessors
    @Id @GeneratedValue    
    @Column(name="id", unique=true, nullable=false)
    public Integer getId() {
        return this.id;
    }
    
	public void setId(Integer id) {
        this.id = id;
    }
    
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="compania_id", nullable=false)
    public Compania getCompania() {
        return this.compania;
    }
    
    public void setCompania(Compania companias) {
        this.compania = companias;
    }
    
    @Column(name="nombre", nullable=false, length=100)
    public String getNombre() {
        return this.nombre;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    @Column(name="descripcion")
    public String getDescripcion() {
        return this.descripcion;
    }
    
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
   
    @Column(name="es_activo", nullable=false)
    public Boolean getEsActivo() {
        return this.esActivo;
    }
    
    public void setEsActivo(Boolean esActivo) {
        this.esActivo = esActivo;
    }

	
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "marca")
    public Set<TipoVehiculoxMarca> getTiposvehiculo() {
		return tiposvehiculo;
	}
    

	public void setTiposvehiculo(Set<TipoVehiculoxMarca> tiposvehiculo) {
		this.tiposvehiculo = tiposvehiculo;
	}
    
}