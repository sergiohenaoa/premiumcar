package com.sh.pvs.model.dto.configuracion;

import com.sh.pvs.model.dto.seguridad.Compania;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;


/**
 * AbstractVehiculo entity provides the base persistence definition of the Vehiculo entity. @author MyEclipse Persistence Tools
 */
@MappedSuperclass
public abstract class AbstractVehiculo  implements java.io.Serializable, Comparable<Vehiculo>   {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	// Fields    

     private Integer id;
     private Transmision transmision;
     private Carroceria carroceria;
     private Marca marca;
     private Compania compania;
     private TipoVehiculo tipovehiculo;
     private Color color;
     private String modelo;
     private String linea;
     private String motor;
     private String placa;
     private String chasis;
     private String serie;
     private Integer cilindraje;
     private Boolean esActivoInventario = false;
	  private String servicio;
     private Set<FotoxVehiculo> fotos = new HashSet<FotoxVehiculo>(0);
     
     private Double precioTmp;
     private Integer kilometrosTmp;


    // Constructors

    /** default constructor */
    public AbstractVehiculo() {
    }

    
    /** full constructor */
    public AbstractVehiculo(Transmision transmision, Carroceria carroceria, Marca marca, Compania compania, TipoVehiculo tipovehiculo, Color color, String modelo, String linea, String motor, String placa, String chasis, String serie, Boolean esActivoInventario) {
        this.transmision = transmision;
        this.carroceria = carroceria;
        this.marca = marca;
        this.compania = compania;
        this.tipovehiculo = tipovehiculo;
        this.color = color;
        this.modelo = modelo;
        this.linea = linea;
        this.motor = motor;
        this.placa = placa;
        this.chasis = chasis;
        this.serie = serie;
        this.esActivoInventario = esActivoInventario;
    }

   
    // Property accessors
    @Id @GeneratedValue
    
    @Column(name="id", unique=true, nullable=false)

    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="transmision_id", nullable=false)

    public Transmision getTransmision() {
        return this.transmision;
    }
    
    public void setTransmision(Transmision transmision) {
        this.transmision = transmision;
    }
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="carroceria_id", nullable=false)

    public Carroceria getCarroceria() {
        return this.carroceria;
    }
    
    public void setCarroceria(Carroceria carroceria) {
        this.carroceria = carroceria;
    }
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="marca_id", nullable=false)

    public Marca getMarca() {
        return this.marca;
    }
    
    public void setMarca(Marca marca) {
        this.marca = marca;
    }
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="compania_id", nullable=false)

    public Compania getCompania() {
        return this.compania;
    }
    
    public void setCompania(Compania compania) {
        this.compania = compania;
    }
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="tipo_vehiculo_id", nullable=false)

    public TipoVehiculo getTipoVehiculo() {
        return this.tipovehiculo;
    }
    
    public void setTipoVehiculo(TipoVehiculo tipovehiculo) {
        this.tipovehiculo = tipovehiculo;
    }
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="color_id", nullable=false)

    public Color getColor() {
        return this.color;
    }
    
    public void setColor(Color color) {
        this.color = color;
    }
    
    @Column(name="modelo", nullable=false, length=4)

    public String getModelo() {
        return this.modelo;
    }
    
    public void setModelo(String modelo) {
        this.modelo = modelo;
    }
    
    @Column(name="linea", nullable=false, length=200)

    public String getLinea() {
        return this.linea;
    }
    
    public void setLinea(String linea) {
        this.linea = linea;
    }
    
    @Column(name="motor", nullable=false, length=200)

    public String getMotor() {
        return this.motor;
    }
    
    public void setMotor(String motor) {
        this.motor = motor;
    }
    
    @Column(name="placa", nullable=false, length=10)

    public String getPlaca() {
        return this.placa;
    }
    
    public void setPlaca(String placa) {
        this.placa = placa;
    }
    
    @Column(name="chasis", nullable=false, length=200)

    public String getChasis() {
        return this.chasis;
    }
    
    public void setChasis(String chasis) {
        this.chasis = chasis;
    }
    
    @Column(name="serie", nullable=false, length=200)

    public String getSerie() {
        return this.serie;
    }
    
    public void setSerie(String serie) {
        this.serie = serie;
    }
    
    @Column(name="es_activo_inventario", nullable=false)

    public Boolean getEsActivoInventario() {
        return this.esActivoInventario;
    }
    
    public void setEsActivoInventario(Boolean esActivoInventario) {
        this.esActivoInventario = esActivoInventario;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "vehiculo")
    public Set<FotoxVehiculo> getFotos() {
		return fotos;
	}
    
	public void setFotos(Set<FotoxVehiculo> fotos) {
		this.fotos = fotos;
	}

	@Transient
	public Integer getMainPhotoId(){
		return 20;
	}

	 @Override
	 public int compareTo(final Vehiculo o) {
		 return Integer.compare(this.getId(), o.getId());
	 }

	 @Transient
	public Double getPrecioTmp() {
		return precioTmp;
	}


	public void setPrecioTmp(Double precioTmp) {
		this.precioTmp = precioTmp;
	}

	@Transient
	public Integer getKilometrosTmp() {
		return kilometrosTmp;
	}


	public void setKilometrosTmp(Integer kilometrosTmp) {
		this.kilometrosTmp = kilometrosTmp;
	}


	
	@Column(name="cilindraje", nullable=false)
	public Integer getCilindraje() {
		return cilindraje;
	}
	


	public void setCilindraje(Integer cilindraje) {
		this.cilindraje = cilindraje;
	}
	
	@Column(name="servicio", nullable=false)
	public String getServicio()
	{
		return servicio;
	}
	
	public void setServicio(String servicio)
	{
		this.servicio = servicio;
	}
}
