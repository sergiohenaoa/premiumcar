package com.sh.pvs.model.dto.configuracion;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.sh.pvs.model.dto.seguridad.Compania;


/**
 * Vehiculo entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="tb_vehiculos"
    ,catalog="db_pvs"
)
public class Vehiculo extends AbstractVehiculo implements java.io.Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	// Constructors

    /** default constructor */
    public Vehiculo() {
    }

    
    /** full constructor */
    public Vehiculo(Transmision tbTransmisiones, Carroceria tbCarrocerias, Marca tbMarcas, Compania tbCompanias, TipoVehiculo tbTiposvehiculo, Color tbColores, String modelo, String linea, String motor, String placa, String chasis, String serie, Boolean esActivoInventario) {
        super(tbTransmisiones, tbCarrocerias, tbMarcas, tbCompanias, tbTiposvehiculo, tbColores, modelo, linea, motor, placa, chasis, serie, esActivoInventario);        
    }
   
}
