package com.sh.pvs.model.dto.configuracion;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * OficinaTransito entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "tb_oficinastransito", catalog = "db_pvs")
public class OficinaTransito extends AbstractOficinaTransito implements
		java.io.Serializable {

	private static final long serialVersionUID = 1L;

	// Constructors

	/** default constructor */
	public OficinaTransito() {
	}

	/** minimal constructor */
	public OficinaTransito(String nombre, String descripcion, Boolean esActivo) {
		super(nombre, descripcion, esActivo);
	}

}
