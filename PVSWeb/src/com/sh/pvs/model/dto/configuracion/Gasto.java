package com.sh.pvs.model.dto.configuracion;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.sh.pvs.model.dto.seguridad.Compania;


/**
 * Gasto entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="tb_gastos"
    ,catalog="db_pvs"
)
public class Gasto extends AbstractGasto implements java.io.Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// Constructors

    /** default constructor */
    public Gasto() {
    }

	/** minimal constructor */
    public Gasto(Compania compania, String nombre, Boolean esActivo, Boolean esDefectoConsignacion) {
        super(compania, nombre, esActivo, esDefectoConsignacion);        
    }
    
    /** full constructor */
    public Gasto(Compania compania, String nombre, String descripcion, Boolean esActivo, Double valor, Boolean esDefectoConsignacion) {
        super(compania, nombre, descripcion, esActivo, valor, esDefectoConsignacion);        
    }
   
}
