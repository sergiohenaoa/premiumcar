package com.sh.pvs.model.dto.configuracion;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import com.sh.pvs.model.dto.seguridad.Compania;


/**
 * AbstractGasto entity provides the base persistence definition of the Gasto entity. @author MyEclipse Persistence Tools
 */
@MappedSuperclass

public abstract class AbstractGasto  implements java.io.Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	// Fields    

     private Integer id;
     private Compania compania;
     private String nombre;
     private String descripcion;
     private String tipoGasto;
     private Boolean esActivo;
     private Double valor;
     private Boolean esDefectoConsignacion;


    // Constructors

    /** default constructor */
    public AbstractGasto() {
    }

	/** minimal constructor */
    public AbstractGasto(Compania compania, String nombre, Boolean esActivo, Boolean esDefectoConsignacion) {
        this.compania = compania;
        this.nombre = nombre;
        this.esActivo = esActivo;
        this.esDefectoConsignacion = esDefectoConsignacion;
    }
    
    /** full constructor */
    public AbstractGasto(Compania compania, String nombre, String descripcion, Boolean esActivo, Double valor, Boolean esDefectoConsignacion) {
        this.compania = compania;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.esActivo = esActivo;
        this.valor = valor;
        this.esDefectoConsignacion = esDefectoConsignacion;
    }

   
    // Property accessors
    @Id @GeneratedValue
    
    @Column(name="id", unique=true, nullable=false)

    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="compania_id", nullable=false)

    public Compania getCompania() {
        return this.compania;
    }
    
    public void setCompania(Compania compania) {
        this.compania = compania;
    }
    
    @Column(name="nombre", nullable=false, length=100)
    public String getNombre() {
        return this.nombre;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    @Column(name="tipo_gasto", nullable=false, length=2)
    public String getTipoGasto() {
        return this.tipoGasto;
    }
    
    public void setTipoGasto(String tipoGasto) {
        this.tipoGasto = tipoGasto;
    }
    
    @Column(name="descripcion")

    public String getDescripcion() {
        return this.descripcion;
    }
    
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    @Column(name="es_activo", nullable=false)
    public Boolean getEsActivo() {
        return this.esActivo;
    }
    
    public void setEsActivo(Boolean esActivo) {
        this.esActivo = esActivo;
    }
    
    @Column(name="valor", precision=17, scale=4)
    public Double getValor() {
        return this.valor;
    }
    
    public void setValor(Double valor) {
        this.valor = valor;
    }
    
    @Column(name="es_defecto_consignacion", nullable=false)
    public Boolean getEsDefectoConsignacion() {
        return this.esDefectoConsignacion;
    }
    
    public void setEsDefectoConsignacion(Boolean esDefectoConsignacion) {
        this.esDefectoConsignacion = esDefectoConsignacion;
    }
   
    
}