package com.sh.pvs.model.dto.configuracion;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * TipoVehiculo entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "tb_tiposvehiculo", catalog = "db_pvs")
public class TipoVehiculo extends AbstractTipoVehiculo implements
		java.io.Serializable {

	private static final long serialVersionUID = 1L;

	// Constructors

	/** default constructor */
	public TipoVehiculo() {
	}

	/** minimal constructor */
	public TipoVehiculo(String nombre, String descripcion, Boolean esActivo) {
		super(nombre, descripcion, esActivo);
	}

}
