package com.sh.pvs.model.dto.configuracion;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Accesorio entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "tb_accesorios", catalog = "db_pvs")
public class Accesorio extends AbstractAccesorio implements
		java.io.Serializable {

	private static final long serialVersionUID = 1L;

	// Constructors

	/** default constructor */
	public Accesorio() {
	}

	/** minimal constructor */
	public Accesorio(String nombre, String descripcion, Boolean esActivo) {
		super(nombre, descripcion, esActivo);
	}

}
