package com.sh.pvs.model.dto.configuracion;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Cuenta entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "tb_cuentas", catalog = "db_pvs")
public class Cuenta extends AbstractCuenta implements
		java.io.Serializable {

	private static final long serialVersionUID = 1L;

	// Constructors

	/** default constructor */
	public Cuenta() {
	}

	/** minimal constructor */
	public Cuenta(String nombre, String descripcion, Boolean esActivo) {
		super(nombre, descripcion, esActivo);
	}

}
