package com.sh.pvs.model.dto.configuracion;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Concesionario entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "tb_concesionarios", catalog = "db_pvs")
public class Concesionario extends AbstractConcesionario implements
		java.io.Serializable {

	private static final long serialVersionUID = 1L;

	// Constructors

	/** default constructor */
	public Concesionario() {
	}

	/** minimal constructor */
	public Concesionario(String nombre, String descripcion, Boolean esActivo) {
		super(nombre, descripcion, esActivo);
	}

}
