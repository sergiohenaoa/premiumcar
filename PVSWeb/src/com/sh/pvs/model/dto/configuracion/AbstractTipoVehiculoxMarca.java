package com.sh.pvs.model.dto.configuracion;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;


/**
 * AbstractTipoVehiculoxMarca entity provides the base persistence definition of the TipoVehiculoxMarca entity. @author MyEclipse Persistence Tools
 */
@MappedSuperclass
public abstract class AbstractTipoVehiculoxMarca  implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	// Fields
     private Integer id;
     private Marca marca;
     private TipoVehiculo tipoVehiculo;

    // Constructors
     /** default constructor */
     public AbstractTipoVehiculoxMarca() {
     }
     
    // Property accessors
    @Id @GeneratedValue    
    @Column(name="id", unique=true, nullable=false)
    public Integer getId() {
        return this.id;
    }
    
	public void setId(Integer id) {
        this.id = id;
    }
    
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="marca_id", nullable=false)
    public Marca getMarca() {
        return this.marca;
    }
    
    public void setMarca(Marca marca) {
        this.marca = marca;
    }
    
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="tipovehiculo_id", nullable=false)
	public TipoVehiculo getTipoVehiculo() {
	    return this.tipoVehiculo;
	}
	
	public void setTipoVehiculo(TipoVehiculo tipoVehiculo) {
	    this.tipoVehiculo = tipoVehiculo;
	}
    
}