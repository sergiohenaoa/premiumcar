package com.sh.pvs.model.dto.configuracion;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Marca entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "tb_marcas", catalog = "db_pvs")
public class Marca extends AbstractMarca implements
		java.io.Serializable {

	private static final long serialVersionUID = 1L;

	// Constructors

	/** default constructor */
	public Marca() {
	}

	/** minimal constructor */
	public Marca(String nombre, String descripcion, Boolean esActivo) {
		super(nombre, descripcion, esActivo);
	}

}
