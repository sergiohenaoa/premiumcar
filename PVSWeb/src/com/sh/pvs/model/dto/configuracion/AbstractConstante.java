package com.sh.pvs.model.dto.configuracion;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import com.sh.pvs.model.dto.seguridad.Compania;


/**
 * AbstractConstante entity provides the base persistence definition of the Constante entity. @author MyEclipse Persistence Tools
 */
@MappedSuperclass
public abstract class AbstractConstante  implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	// Fields
     private Integer id;
     private Compania compania;
     private String codigo;
     private String valor;
     private String descripcion;


    // Constructors

    /** default constructor */
    public AbstractConstante() {
    }

	/** minimal constructor */
    public AbstractConstante(Compania companias, String codigo, String valor) {
        this.compania = companias;
        this.codigo = codigo;
        this.valor = valor;
    }
    
    /** full constructor */
    public AbstractConstante(Compania companias, String codigo, String valor, String descripcion) {
        this.compania = companias;
        this.codigo = codigo;
        this.valor = valor;
        this.descripcion = descripcion;
    }

   
    // Property accessors
    @Id @GeneratedValue    
    @Column(name="id", unique=true, nullable=false)
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="compania_id", nullable=false)
    public Compania getCompania() {
        return this.compania;
    }
    
    public void setCompania(Compania companias) {
        this.compania = companias;
    }
    
    @Column(name="codigo", nullable=false, length=20)
    public String getCodigo() {
        return this.codigo;
    }
    
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
    
    @Column(name="valor", nullable=false, length=200)
    public String getValor() {
        return this.valor;
    }
    
    public void setValor(String valor) {
        this.valor = valor;
    }
    
    @Column(name="descripcion", length=1000)
    public String getDescripcion() {
        return this.descripcion;
    }
    
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
   
}