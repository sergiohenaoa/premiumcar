package com.sh.pvs.model.dto.configuracion;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * TramitexOficinaTransito entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "tb_tramitesxoficinatransito", catalog = "db_pvs")
public class TramitexOficinaTransito extends AbstractTramitexOficinaTransito implements
		java.io.Serializable {

	private static final long serialVersionUID = 1L;

	// Constructors

	/** default constructor */
	public TramitexOficinaTransito() {
	}

	/** minimal constructor */
	public TramitexOficinaTransito(String descripcion, Double valor) {
		super(descripcion, valor);
	}

}
