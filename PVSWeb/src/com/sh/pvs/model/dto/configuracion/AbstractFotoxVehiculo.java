package com.sh.pvs.model.dto.configuracion;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;


/**
 * AbstractFotoxVehiculo entity provides the base persistence definition of the FotoxVehiculo entity. @author MyEclipse Persistence Tools
 */
@MappedSuperclass
public abstract class AbstractFotoxVehiculo  implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	// Fields
     private Integer id;
     private Vehiculo vehiculo;
     private String adjunto;
     private String NombreArchivo;
     private String TipoArchivo;
     private Integer SizeArchivo;

    // Constructors
     /** default constructor */
     public AbstractFotoxVehiculo() {
     }
     
    // Property accessors
    @Id @GeneratedValue    
    @Column(name="id", unique=true, nullable=false)
    public Integer getId() {
        return this.id;
    }
    
	public void setId(Integer id) {
        this.id = id;
    }
    
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="vehiculo_id", nullable=false)
    public Vehiculo getVehiculo() {
        return this.vehiculo;
    }
    
    public void setVehiculo(Vehiculo vehiculo) {
        this.vehiculo = vehiculo;
    }

    @Lob
	@Column(name="adjunto", nullable=false)
	public String getAdjunto() {
		return adjunto;
	}

	public void setAdjunto(String adjunto) {
		this.adjunto = adjunto;
	}

	@Column(name="nombre_archivo", nullable=false, length=200)
	public String getNombreArchivo() {
		return NombreArchivo;
	}
	
	public void setNombreArchivo(String nombreArchivo) {
		NombreArchivo = nombreArchivo;
	}
	
	@Column(name="tipo_archivo", nullable=false, length=100)
	public String getTipoArchivo() {
		return TipoArchivo;
	}

	public void setTipoArchivo(String tipoArchivo) {
		TipoArchivo = tipoArchivo;
	}
	
	@Column(name = "size_archivo", nullable = false)
	public Integer getSizeArchivo() {
		return SizeArchivo;
	}

	public void setSizeArchivo(Integer sizeArchivo) {
		SizeArchivo = sizeArchivo;
	}
	
}