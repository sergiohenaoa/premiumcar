package com.sh.pvs.model.dto.consignacion;

import com.sh.pvs.model.dto.configuracion.Cliente;
import com.sh.pvs.model.dto.configuracion.Vehiculo;
import com.sh.pvs.model.dto.seguridad.Compania;
import com.sh.pvs.model.utils.Constantes;

import javax.persistence.*;
import java.util.Date;


/**
 * AbstractConsignacion entity provides the base persistence definition of the Consignacion entity. @author MyEclipse Persistence Tools
 */
@MappedSuperclass

public abstract class AbstractConsignacion  implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	// Fields    

     private Integer id;
     private Cliente cliente;
     private Vehiculo vehiculo;
     private Compania compania;
     private String codigo;
     private Integer kilometraje;
     private Date fchVenceSoat;
     private Date fchVenceRevtm;
     private Double valorVehiculo;
     private Date fchConsignacion = new Date();
     private Boolean requiereDatosMatricula;
     private Boolean aplicaRevisionTM;
     private Boolean negociable;
     private String identificacionMatricula;
     private String nombreMatricula;
     private String estado = Constantes.CONS_ESTADO_INICIAL_COD;
    private Boolean tieneMatricula = false; 
    private Boolean tieneSoat = false;
    private Boolean tieneTecnoMecanica = false;
    private Boolean tieneCopiaLlave = false;
    private Boolean copiaLlaveEntregada = false;
    private Double precioMinimo;
    
    
    // Constructors

    /** default constructor */
    public AbstractConsignacion() {
    }

	/** minimal constructor */
    public AbstractConsignacion(Cliente cliente, Vehiculo vehiculo, String codigo, Double valorVehiculo, Date fchConsignacion, Boolean requiereDatosMatricula, String estado) {
        this.cliente = cliente;
        this.vehiculo = vehiculo;
        this.codigo = codigo;
        this.valorVehiculo = valorVehiculo;
        this.fchConsignacion = fchConsignacion;
        this.requiereDatosMatricula = requiereDatosMatricula;
        this.estado = estado;
    }
    
    /** full constructor */
    public AbstractConsignacion(Cliente cliente, Vehiculo vehiculo, String codigo, Integer kilometraje, Date fchVenceSoat, Date fchVenceRevtm, Double valorVehiculo, Date fchConsignacion, Boolean requiereDatosMatricula, String estado) {
        this.cliente = cliente;
        this.vehiculo = vehiculo;
        this.codigo = codigo;
        this.kilometraje = kilometraje;
        this.fchVenceSoat = fchVenceSoat;
        this.fchVenceRevtm = fchVenceRevtm;
        this.valorVehiculo = valorVehiculo;
        this.fchConsignacion = fchConsignacion;
        this.requiereDatosMatricula = requiereDatosMatricula;
        this.estado = estado;
    }

   
    // Property accessors
    @Id @GeneratedValue
    @Column(name="id", unique=true, nullable=false)
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="cliente_id", nullable=false)
    public Cliente getCliente() {
        return this.cliente;
    }
    
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
    
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="vehiculo_id", nullable=false)
    public Vehiculo getVehiculo() {
        return this.vehiculo;
    }
    
    public void setVehiculo(Vehiculo vehiculo) {
        this.vehiculo = vehiculo;
    }
    
    @ManyToOne(fetch=FetchType.EAGER)
	    @JoinColumn(name="compania_id", nullable=false)
	public Compania getCompania() {
	    return this.compania;
	}
	
	public void setCompania(Compania companias) {
	    this.compania = companias;
	}

    @Column(name="codigo", nullable=false, length=45)
    public String getCodigo() {
        return this.codigo;
    }
    
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
    
    @Column(name="kilometraje")
    public Integer getKilometraje() {
        return this.kilometraje;
    }
    
    public void setKilometraje(Integer kilometraje) {
        this.kilometraje = kilometraje;
    }
    
    @Column(name="fch_vence_soat", length=19)
    public Date getFchVenceSoat() {
        return this.fchVenceSoat;
    }
    
    public void setFchVenceSoat(Date fchVenceSoat) {
        this.fchVenceSoat = fchVenceSoat;
    }
    
    @Column(name="fch_vence_revtm", length=19)
    public Date getFchVenceRevtm() {
        return this.fchVenceRevtm;
    }
    
    public void setFchVenceRevtm(Date fchVenceRevtm) {
        this.fchVenceRevtm = fchVenceRevtm;
    }
    
    @Column(name="valor_vehiculo", nullable=false, precision=17, scale=4)
    public Double getValorVehiculo() {
        return this.valorVehiculo;
    }
    
    public void setValorVehiculo(Double valorVehiculo) {
        this.valorVehiculo = valorVehiculo;
    }

    @Column(name="fch_consignacion", nullable=false, length=19)
    public Date getFchConsignacion() {
        return this.fchConsignacion;
    }
    
    public void setFchConsignacion(Date fchConsignacion) {
        this.fchConsignacion = fchConsignacion;
    }
    
    @Column(name="requiere_datos_matricula", nullable=false)
    public Boolean getRequiereDatosMatricula() {
        return this.requiereDatosMatricula;
    }
    
    public void setRequiereDatosMatricula(Boolean requiereDatosMatricula) {
        this.requiereDatosMatricula = requiereDatosMatricula;
    }
    
    @Column(name="aplica_revision_tm", nullable=false)
    public Boolean getAplicaRevisionTM() {
		return aplicaRevisionTM;
	}

	public void setAplicaRevisionTM(Boolean aplicaRevisionTM) {
		this.aplicaRevisionTM = aplicaRevisionTM;
	}
    
	@Column(name="negociable", nullable=false)
    public Boolean getNegociable() {
        return this.negociable;
    }
    
    public void setNegociable(Boolean negociable) {
        this.negociable = negociable;
    }
    
    @Column(name="estado", nullable=false, length=2)
    public String getEstado() {
        return this.estado;
    }
    
    public void setEstado(String estado) {
        this.estado = estado;
    }

	
    @Column(name="identificacion_matricula", length=50)
    public String getIdentificacionMatricula() {
		return identificacionMatricula;
	}

	public void setIdentificacionMatricula(String identificacionMatricula) {
		this.identificacionMatricula = identificacionMatricula;
	}
	
	@Column(name="nombre_matricula", length=200)
	public String getNombreMatricula() {
		return nombreMatricula;
	}

	public void setNombreMatricula(String nombreMatricula) {
		this.nombreMatricula = nombreMatricula;
	}
    
    @Column(name="tieneMatricula", nullable=false)
    public Boolean getTieneMatricula()
    {
        return tieneMatricula;
    }
    
    public void setTieneMatricula(Boolean tieneMatricula)
    {
        this.tieneMatricula = tieneMatricula;
    }
    
    @Column(name="tieneSoat", nullable=false)
    public Boolean getTieneSoat()
    {
        return tieneSoat;
    }
    
    public void setTieneSoat(Boolean tieneSoat)
    {
        this.tieneSoat = tieneSoat;
    }
    
    @Column(name="tieneTecnoMecanica", nullable=false)
    public Boolean getTieneTecnoMecanica()
    {
        return tieneTecnoMecanica;
    }
    
    public void setTieneTecnoMecanica(Boolean tieneTecnoMecanica)
    {
        this.tieneTecnoMecanica = tieneTecnoMecanica;
    }
    
    @Column(name="tieneCopiaLlave", nullable=false)
    public Boolean getTieneCopiaLlave()
    {
        return tieneCopiaLlave;
    }
    
    public void setTieneCopiaLlave(Boolean tieneCopiaLlave)
    {
        this.tieneCopiaLlave = tieneCopiaLlave;
    }
    
    @Column(name="copiaLlaveEntregada", nullable=false)
    public Boolean getCopiaLlaveEntregada()
    {
        return copiaLlaveEntregada;
    }
    
    public void setCopiaLlaveEntregada(Boolean copiaLlaveEntregada)
    {
        this.copiaLlaveEntregada = copiaLlaveEntregada;
    }
    
    @Column(name="precioMinimo", precision=17, scale=4)
    public Double getPrecioMinimo()
    {
        return precioMinimo;
    }
    
    public void setPrecioMinimo(Double precioMinimo)
    {
        this.precioMinimo = precioMinimo;
    }
}
