package com.sh.pvs.model.dto.consignacion;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.sh.pvs.model.dto.configuracion.Cliente;
import com.sh.pvs.model.dto.configuracion.Vehiculo;
import com.sh.pvs.model.utils.Constantes;


/**
 * Consignacion entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="tb_consignaciones"
    ,catalog="db_pvs"
)
public class Consignacion extends AbstractConsignacion implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	// Constructors

    /** default constructor */
    public Consignacion() {
    }

	/** minimal constructor */
    public Consignacion(Cliente tbClientes, Vehiculo vehiculo, String codigo, Double valorVehiculo, Timestamp fchConsignacion, Boolean requiereDatosMatricula, String estado) {
        super(tbClientes, vehiculo, codigo, valorVehiculo, fchConsignacion, requiereDatosMatricula, estado);        
    }
    
    /** full constructor */
    public Consignacion(Cliente tbClientes, Vehiculo vehiculo, String codigo, Integer kilometraje, Timestamp fchVenceSoat, Timestamp fchVenceRevtm, Double valorVehiculo, Timestamp fchConsignacion, Boolean requiereDatosMatricula, String estado) {
        super(tbClientes, vehiculo, codigo, kilometraje, fchVenceSoat, fchVenceRevtm, valorVehiculo, fchConsignacion, requiereDatosMatricula, estado);        
    }
  
    @Transient
    public String getEstadoLbl(){
    	if(this.getEstado().equals(Constantes.CONS_ESTADO_INICIAL_COD))
    		return Constantes.CONS_ESTADO_INICIAL_LBL;
    	if(this.getEstado().equals(Constantes.CONS_ESTADO_ENPROCESO_COD))
    		return Constantes.CONS_ESTADO_ENPROCESO_LBL;
    	if(this.getEstado().equals(Constantes.CONS_ESTADO_FINALIZADA_COD))
    		return Constantes.CONS_ESTADO_FINALIZADA_LBL;
    	if(this.getEstado().equals(Constantes.CONS_ESTADO_DECLINADA_COD))
    		return Constantes.CONS_ESTADO_DECLINADA_LBL;
    	return "";
    }

}
