package com.sh.pvs.model.dto.contabilidad;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.sh.pvs.model.dto.configuracion.Accesorio;
import com.sh.pvs.model.dto.seguridad.Compania;


/**
 * InventarioAccesorios entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="tb_inventarioaccesorios"
    ,catalog="db_pvs"
)
public class InventarioAccesorios extends AbstractInventarioAccesorios implements java.io.Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// Constructors

    /** default constructor */
    public InventarioAccesorios() {
    }

	/** minimal constructor */
    public InventarioAccesorios(Compania compania, Accesorio accesorio, Boolean esActivo) {
        super(compania, accesorio, esActivo);        
    }
    
    /** full constructor */
    public InventarioAccesorios(Compania compania, Accesorio accesorio, String descripcion, Boolean esActivo) {
        super(compania, accesorio, descripcion, esActivo);        
    }
   
}
