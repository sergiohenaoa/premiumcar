package com.sh.pvs.model.dto.contabilidad;

import java.util.Date;

public abstract class AbstractReporteCarteraRow  implements java.io.Serializable {


    // Fields    
	private static final long serialVersionUID = 1L;
	
	private String origen;
	private Date fecha;
	private String descripcion;
	private Integer plazo;
	private Double valor;
	private Date fechaV;
     
    // Constructors

    /** default constructor */
    public AbstractReporteCarteraRow() {
    }

    /** full constructor */
    public AbstractReporteCarteraRow(String origen, Date fecha, String descripcion,
			Integer plazo, Double valor) {
		super();
		this.origen = origen;
		this.fecha = fecha;
		this.descripcion = descripcion;
		this.plazo = plazo;
		this.valor = valor;
	}
    
    /*GETTERS AND SETTERS*/

	public String getOrigen() {
		return origen;
	}

	public void setOrigen(String origen) {
		this.origen = origen;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	
	public Integer getPlazo() {
		return plazo;
	}
	

	public void setPlazo(Integer plazo) {
		this.plazo = plazo;
	}
	

	public Double getValor() {
		return valor;
	}
	

	public void setValor(Double valor) {
		this.valor = valor;
	}

	
	public Date getFechaV() {
		return fechaV;
	}
	

	public void setFechaV(Date fechaV) {
		this.fechaV = fechaV;
	}
	
}