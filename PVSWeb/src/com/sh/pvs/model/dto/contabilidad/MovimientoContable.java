package com.sh.pvs.model.dto.contabilidad;


import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.sh.pvs.model.dto.configuracion.Cuenta;
import com.sh.pvs.model.dto.seguridad.Usuario;


/**
 * MovimientoContable entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="tb_movimientoscontables"
    ,catalog="db_pvs"
)
public class MovimientoContable extends AbstractMovimientoContable implements java.io.Serializable{

	private static final long serialVersionUID = 1L;

	// Constructors

    /** default constructor */
    public MovimientoContable() {
    }

    /** full constructor */
	public MovimientoContable(String codigo, String descripcion, Date fecha,
			Double valor, Cuenta cuenta, Usuario usuario, Boolean esIngreso) {
		super(codigo, descripcion, fecha, valor, cuenta, usuario, esIngreso);
	}
	
}
