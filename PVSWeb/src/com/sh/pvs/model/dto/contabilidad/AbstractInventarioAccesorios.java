package com.sh.pvs.model.dto.contabilidad;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import com.sh.pvs.model.dto.configuracion.Accesorio;
import com.sh.pvs.model.dto.seguridad.Compania;


/**
 * AbstractInventarioAccesorios entity provides the base persistence definition of the InventarioAccesorios entity. @author MyEclipse Persistence Tools
 */
@MappedSuperclass

public abstract class AbstractInventarioAccesorios  implements java.io.Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	// Fields    

     private Integer id;
     private Compania compania;
     private Accesorio accesorio;
     private String descripcion;
     private Boolean esActivo;
     private Double cantidadDisponible;


    // Constructors

    /** default constructor */
    public AbstractInventarioAccesorios() {
    }

	/** minimal constructor */
    public AbstractInventarioAccesorios(Compania compania, Accesorio accesorio, Boolean esActivo) {
        this.compania = compania;
        this.accesorio = accesorio;
        this.esActivo = esActivo;
    }
    
    /** full constructor */
    public AbstractInventarioAccesorios(Compania compania, Accesorio accesorio, String descripcion, Boolean esActivo) {
        this.compania = compania;
        this.accesorio = accesorio;
        this.descripcion = descripcion;
        this.esActivo = esActivo;
    }

   
    // Property accessors
    @Id @GeneratedValue
    
    @Column(name="id", unique=true, nullable=false)

    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
	
    @ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="compania_id", nullable=false)

    public Compania getCompania() {
        return this.compania;
    }
    
    public void setCompania(Compania compania) {
        this.compania = compania;
    }
    
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="accesorio_id", nullable=false)
	public Accesorio getAccesorio() {
	    return this.accesorio;
	}
	
	public void setAccesorio(Accesorio accesorio) {
	    this.accesorio = accesorio;
	}
    
    @Column(name="descripcion")

    public String getDescripcion() {
        return this.descripcion;
    }
    
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    @Column(name="es_activo", nullable=false)
    public Boolean getEsActivo() {
        return this.esActivo;
    }
    
    public void setEsActivo(Boolean esActivo) {
        this.esActivo = esActivo;
    }
    
    @Column(name="cantidad_disponible", precision=17, scale=4)
    public Double getCantidadDisponible() {
        return this.cantidadDisponible;
    }
    
    public void setCantidadDisponible(Double cantidadDisponible) {
        this.cantidadDisponible = cantidadDisponible;
    }
    
}