package com.sh.pvs.model.dto.contabilidad;


import javax.persistence.Entity;
import javax.persistence.Table;

import com.sh.pvs.model.dto.configuracion.Accesorio;


/**
 * AccesorioxCompraventa entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="tb_accesoriosxcompraventa"
    ,catalog="db_pvs"
)
public class AccesorioxCompraVenta extends AbstractAccesorioxCompraVenta implements java.io.Serializable, Comparable<AccesorioxCompraVenta> {

	private static final long serialVersionUID = 1L;

	// Constructors

    /** default constructor */
    public AccesorioxCompraVenta() {
    }

    /** full constructor */
    public AccesorioxCompraVenta(CompraVentaAccesorios compraVentaAccesorios,
			Accesorio accesorio, Double cantidad, Double precio) {
		super(compraVentaAccesorios, accesorio, cantidad, precio);
	}
	
    @Override
    public int compareTo(final AccesorioxCompraVenta o) {
		if(this.getId() != null && o.getId() != null)
			return Integer.compare(this.getId(), o.getId());
		return 0;
    }
}
