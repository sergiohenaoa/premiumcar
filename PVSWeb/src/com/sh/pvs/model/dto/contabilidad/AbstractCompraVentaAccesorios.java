package com.sh.pvs.model.dto.contabilidad;

import com.sh.pvs.model.dto.configuracion.Cuenta;
import com.sh.pvs.model.dto.seguridad.Compania;
import com.sh.pvs.model.dto.seguridad.Usuario;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * AbstractCompraVentaAccesorios entity provides the base persistence definition
 * of the HojaNegocio entity. @author MyEclipse Persistence Tools
 */
@MappedSuperclass

public abstract class AbstractCompraVentaAccesorios implements java.io.Serializable {

	// Fields
	private static final long serialVersionUID = 1L;

	private Integer id;
	private Date fecha = new Date();
	private Boolean esCompra;
	private Boolean esPagado = false;
	private String nroFactura;
	private String proveedor;
	private String observaciones;
	private Double valor;
	private Integer plazo;
	private Cuenta cuenta = new Cuenta();
	private Usuario usuario = new Usuario();
	private Compania compania = new Compania();
	private Date fchVencimiento;
	private String nitProveedor;
	private Double descuento;
	private Double iva;
	private Double total;
	private Set<AccesorioxCompraVenta> accesorios = new HashSet<AccesorioxCompraVenta>(0);

	// Constructors

	/** default constructor */
	public AbstractCompraVentaAccesorios() {
	}

	/** full constructor */
	public AbstractCompraVentaAccesorios(String nroFactura, String observaciones, Date fecha, Double valor,
			Cuenta cuenta, Usuario usuario, Boolean esCompra) {
		super();
		this.nroFactura = nroFactura;
		this.observaciones = observaciones;
		this.fecha = fecha;
		this.valor = valor;
		this.cuenta = cuenta;
		this.usuario = usuario;
		this.esCompra = esCompra;
	}

	// Property accessors
	@Id
	@GeneratedValue
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "nrofactura", nullable = false, length = 200)
	public String getNroFactura() {
		return nroFactura;
	}

	public void setNroFactura(String nrofactura) {
		this.nroFactura = nrofactura;
	}

	@Column(name = "proveedor", nullable = false, length = 500)
	public String getProveedor() {
		return proveedor;
	}

	public void setProveedor(String proveedor) {
		this.proveedor = proveedor;
	}

	@Column(name = "observaciones", nullable = false, length = 500)
	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	@Column(name = "fecha", nullable = false, length = 19)
	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	@Column(name = "valor", nullable = false, precision = 17, scale = 4)
	public Double getValor() {
		valor = 0.0;
		if(accesorios != null){
			for (Iterator<AccesorioxCompraVenta> itAcc = accesorios.iterator(); itAcc.hasNext();) {
				AccesorioxCompraVenta accesorioxCompraVenta = itAcc.next();
				valor += accesorioxCompraVenta.getPrecio()*accesorioxCompraVenta.getCantidad();
			}
		}
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "cuenta_id")
	public Cuenta getCuenta() {
		return cuenta;
	}

	public void setCuenta(Cuenta cuenta) {
		this.cuenta = cuenta;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "usuario_id")
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "compania_id", nullable = false)
	public Compania getCompania() {
		return this.compania;
	}

	public void setCompania(Compania compania) {
		this.compania = compania;
	}

	@Column(name = "es_compra", nullable = false)
	public Boolean getEsCompra() {
		return esCompra;
	}

	public void setEsCompra(Boolean esCompra) {
		this.esCompra = esCompra;
	}

	@Column(name = "es_pagado", nullable = false)
	public Boolean getEsPagado() {
		return esPagado;
	}

	public void setEsPagado(Boolean esPagado) {
		this.esPagado = esPagado;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "compraVentaAccesorios")
	public Set<AccesorioxCompraVenta> getAccesorios() {
		return accesorios;
	}

	public void setAccesorios(Set<AccesorioxCompraVenta> accesorios) {
		this.accesorios = accesorios;
	}

	@Column(name = "plazo", nullable = false)
	public Integer getPlazo() {
		return plazo;
	}

	public void setPlazo(Integer plazo) {
		this.plazo = plazo;
	}

	
	@Column(name = "fch_vencimiento", length = 19)
	public Date getFchVencimiento() {
		return fchVencimiento;
	}
	

	public void setFchVencimiento(Date fchVencimiento) {
		this.fchVencimiento = fchVencimiento;
	}
	
	@Column(name = "nit_proveedor")
	public String getNitProveedor() {
		return nitProveedor;
	}
	

	public void setNitProveedor(String nitProveedor) {
		this.nitProveedor = nitProveedor;
	}
	
	@Column(name = "descuento", precision = 17, scale = 4)
	public Double getDescuento() {
		return descuento;
	}
	

	public void setDescuento(Double descuento) {
		this.descuento = descuento;
	}
	
	@Column(name = "iva", precision = 17, scale = 4)
	public Double getIva() {
		return iva;
	}
	

	public void setIva(Double iva) {
		this.iva = iva;
	}
	
	@Column(name = "total", precision = 17, scale = 4)
	public Double getTotal() {
		total = 0.0;
		if(this.getValor() != null){
			total = this.getValor();
		}
		if(this.getDescuento() != null){
			total = total - this.getDescuento();
		}
		if(this.getIva() != null){
			total = total + this.getIva();
		}
		
		
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}
	
	@Transient
	public Double getTotalCalculado() {
		Double total = 0.0;
		if(this.getValor() != null){
			total = this.getValor();
		}
		if(this.getDescuento() != null){
			total = total - this.getDescuento();
		}
		if(this.getIva() != null){
			total = total + this.getIva();
		}
		return total;
	}
}
