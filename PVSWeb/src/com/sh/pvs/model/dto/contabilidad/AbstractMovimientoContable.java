package com.sh.pvs.model.dto.contabilidad;

import com.sh.pvs.model.dto.configuracion.Cuenta;
import com.sh.pvs.model.dto.seguridad.Compania;
import com.sh.pvs.model.dto.seguridad.Usuario;

import javax.persistence.*;
import java.util.Date;


/**
 * AbstractMovimientoContable entity provides the base persistence definition of the HojaNegocio entity. @author MyEclipse Persistence Tools
 */
@MappedSuperclass

public abstract class AbstractMovimientoContable  implements java.io.Serializable {


    // Fields    
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private String codigo;
	private String descripcion;
	private Date fecha;
	private Double valor;
	private Cuenta cuenta = new Cuenta();
	private Usuario usuario = new Usuario();
	private Compania compania = new Compania();
	private Boolean esIngreso;
	private Boolean esPendiente;
     
    // Constructors

    /** default constructor */
    public AbstractMovimientoContable() {
    }

    /** full constructor */
    public AbstractMovimientoContable(String codigo, String descripcion,
			Date fecha, Double valor, Cuenta cuenta, Usuario usuario,
			Boolean esIngreso) {
		super();
		this.codigo = codigo;
		this.descripcion = descripcion;
		this.fecha = fecha;
		this.valor = valor;
		this.cuenta = cuenta;
		this.usuario = usuario;
		this.esIngreso = esIngreso;
	}
   
    // Property accessors
    @Id @GeneratedValue
    @Column(name="id", unique=true, nullable=false)
    public Integer getId() {
        return this.id;
    }

	public void setId(Integer id) {
        this.id = id;
    }
	
	@Column(name="codigo", nullable=true, length=45)
	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	@Column(name="descripcion", nullable=false, length=500)
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Column(name="fecha", nullable=false, length=19)
	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	@Column(name="valor", nullable=false, precision=17, scale=4)
	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="cuenta_id")
	public Cuenta getCuenta() {
		return cuenta;
	}

	public void setCuenta(Cuenta cuenta) {
		this.cuenta = cuenta;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="usuario_id")
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="compania_id", nullable=false)
	public Compania getCompania() {
	    return this.compania;
	}
	
	public void setCompania(Compania compania) {
	    this.compania = compania;
	}
	
	@Column(name="es_ingreso", nullable=false)
	public Boolean getEsIngreso() {
		return esIngreso;
	}

	public void setEsIngreso(Boolean esIngreso) {
		this.esIngreso = esIngreso;
	}
	
	@Column(name="es_pendiente", nullable=false)
	public Boolean getEsPendiente()
	{
		return esPendiente;
	}
	
	public void setEsPendiente(Boolean esPendiente)
	{
		this.esPendiente = esPendiente;
	}
}
