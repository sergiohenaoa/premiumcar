package com.sh.pvs.model.dto.contabilidad;

import java.util.Date;

import com.sh.pvs.model.dto.configuracion.Cuenta;

public abstract class AbstractReporteContableRow  implements java.io.Serializable {


    // Fields    
	private static final long serialVersionUID = 1L;
	
	private String origen;
	private Date fecha;
	private String descripcion;
	private Cuenta cuenta;
	private Double ingreso;
	private Double egreso;
     
    // Constructors

    /** default constructor */
    public AbstractReporteContableRow() {
    }

    /** full constructor */
    public AbstractReporteContableRow(String origen, Date fecha, String descripcion,
			Cuenta cuenta, Double ingreso, Double egreso) {
		super();
		this.origen = origen;
		this.fecha = fecha;
		this.descripcion = descripcion;
		this.cuenta = cuenta;
		this.ingreso = ingreso;
		this.egreso = egreso;
	}
    
    /*GETTERS AND SETTERS*/

	public String getOrigen() {
		return origen;
	}

	public void setOrigen(String origen) {
		this.origen = origen;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Cuenta getCuenta() {
		return cuenta;
	}

	public void setCuenta(Cuenta cuenta) {
		this.cuenta = cuenta;
	}

	public Double getIngreso() {
		return ingreso;
	}

	public void setIngreso(Double ingreso) {
		this.ingreso = ingreso;
	}

	public Double getEgreso() {
		return egreso;
	}

	public void setEgreso(Double egreso) {
		this.egreso = egreso;
	}

}