package com.sh.pvs.model.dto.contabilidad;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import com.sh.pvs.model.dto.configuracion.Accesorio;


/**
 * AbstractAccesorioxCompraventa entity provides the base persistence definition of the HojaNegocio entity. @author MyEclipse Persistence Tools
 */
@MappedSuperclass

public abstract class AbstractAccesorioxCompraVenta  implements java.io.Serializable {


    // Fields    
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private CompraVentaAccesorios compraVentaAccesorios;
	private Accesorio accesorio;
	private Double cantidad;
	private Double precio;
     
    // Constructors

    /** default constructor */
    public AbstractAccesorioxCompraVenta() {
    }

    /** full constructor */
    public AbstractAccesorioxCompraVenta(
			CompraVentaAccesorios compraVentaAccesorios, Accesorio accesorio,
			Double cantidad, Double precio) {
		super();
		this.compraVentaAccesorios = compraVentaAccesorios;
		this.accesorio = accesorio;
		this.cantidad = cantidad;
		this.precio = precio;
	}

    // Property accessors
    @Id @GeneratedValue
    @Column(name="id", unique=true, nullable=false)
    public Integer getId() {
        return this.id;
    }

	public void setId(Integer id) {
        this.id = id;
    }
	
	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="compraventa_id", nullable=false)
	public CompraVentaAccesorios getCompraVentaAccesorios() {
		return compraVentaAccesorios;
	}

	public void setCompraVentaAccesorios(CompraVentaAccesorios compraVentaAccesorios) {
		this.compraVentaAccesorios = compraVentaAccesorios;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="accesorio_id", nullable=false)
	public Accesorio getAccesorio() {
		return accesorio;
	}

	public void setAccesorio(Accesorio accesorio) {
		this.accesorio = accesorio;
	}
	
	@Column(name="cantidad", nullable=false, precision=17, scale=4)
	public Double getCantidad() {
		return cantidad;
	}

	public void setCantidad(Double cantidad) {
		this.cantidad = cantidad;
	}
	
	@Column(name="precio", nullable=false, precision=17, scale=4)
	public Double getPrecio() {
		return precio;
	}

	public void setPrecio(Double precio) {
		this.precio = precio;
	}
	
}