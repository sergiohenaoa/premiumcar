package com.sh.pvs.model.dto.contabilidad;

import java.util.Date;

import com.sh.pvs.model.dto.configuracion.Cuenta;

public class ReporteContableRow extends AbstractReporteContableRow implements java.io.Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// Constructors

    /** default constructor */
    public ReporteContableRow() {
    }

    /** full constructor */
    public ReporteContableRow(String origen, Date fecha, String descripcion,
			Cuenta cuenta, Double ingreso, Double egreso) {
		super(origen, fecha, descripcion, cuenta, ingreso, egreso);
	}
    
}
