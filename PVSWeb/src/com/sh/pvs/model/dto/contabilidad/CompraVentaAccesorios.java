package com.sh.pvs.model.dto.contabilidad;


import java.util.ArrayList;
import java.util.Date;
import java.util.TreeSet;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.sh.pvs.model.dto.configuracion.Cuenta;
import com.sh.pvs.model.dto.seguridad.Usuario;


/**
 * CompraVentaAccesorios entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="tb_compraventaaccesorios"
    ,catalog="db_pvs"
)
public class CompraVentaAccesorios extends AbstractCompraVentaAccesorios implements java.io.Serializable{

	private static final long serialVersionUID = 1L;

	// Constructors

    /** default constructor */
    public CompraVentaAccesorios() {
    }

    /** full constructor */
    public CompraVentaAccesorios(String nroFactura, String observaciones,
			Date fecha, Double valor, Cuenta cuenta, Usuario usuario,
			Boolean esCompra) {
		super(nroFactura, observaciones, fecha, valor, cuenta, usuario, esCompra);
	}
	
    @Transient
	public ArrayList<AccesorioxCompraVenta> accesoriosXCVAList(){
		return new ArrayList<AccesorioxCompraVenta>(new TreeSet<AccesorioxCompraVenta>(this.getAccesorios()));
	}
}
