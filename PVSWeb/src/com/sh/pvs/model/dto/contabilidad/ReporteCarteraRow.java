package com.sh.pvs.model.dto.contabilidad;

import java.util.Date;

public class ReporteCarteraRow extends AbstractReporteCarteraRow implements java.io.Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// Constructors

    /** default constructor */
    public ReporteCarteraRow() {
    }

	

    /** full constructor */
    public ReporteCarteraRow(String origen, Date fecha, String descripcion,
			Integer plazo, Double valor) {
		super(origen, fecha, descripcion, plazo, valor);
	}
    
}
