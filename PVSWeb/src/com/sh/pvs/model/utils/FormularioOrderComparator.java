package com.sh.pvs.model.utils;

import java.util.Comparator;

import com.sh.pvs.model.dto.seguridad.Formulario;

public class FormularioOrderComparator implements Comparator<Formulario>{

	@Override
    public int compare(Formulario o1, Formulario o2) {
		if(o1.getOrden() < o2.getOrden())
			return -1;
		else if(o1.getOrden() > o2.getOrden())
			return 1;
		else{
			if(o1.getNombreDescriptivo().compareTo(o2.getNombreDescriptivo()) > 0)
				return -1;
			if(o1.getNombreDescriptivo().compareTo(o2.getNombreDescriptivo()) < 0)
				return 1;
			else
				return 0;
		}
    }
	
}


 
