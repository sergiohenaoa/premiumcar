/**
 * Condition.java:
 * 
 * 	Condiciones para el control de consultas
 *
 * @author Sergio Henao, 06/03/2014
 * @Modifier Sergio Henao, 06/03/2014
 * @version 1.0
 * 
 */
package com.sh.pvs.model.utils;

public class Condition {
	
	/*------------------ATRIBUTOS DE LA CONDICIÓN------------------*/
	private String propiedad = "";
	private String operador = "";
	private Object valor = "";
	
	/*------------------OPERADORES PARA QUERY------------------*/
	public static final String DIFERENTE = " <> ";
	public static final String LIKE = " LIKE ";
	public static final String IGUAL = " = ";
	public static final String ACTIVO = " A ";
	public static final String AND = " AND ";
	public static final String OR = " OR ";
	private static final String QUESTION = "?";
	public static final String MAYOR = " > ";
	public static final String MENOR = " < ";
	public static final String MAYOROIGUAL = " >= ";
	public static final String MENOROIGUAL = " <= ";
	public static final String ISNULL = " is null ";
	public static final String ISNOTNULL = " is not null ";
	
	/*------------------OPERADORES PARA ORDERBY------------------*/
	public static final String ASC = " ASC ";
	public static final String DESC = " DESC ";
	
	/*------------------CONSTRUCTORES------------------*/	
	/**
	 * Constructor vacio
	 */
	public Condition() {
		
	}
	
	/**
	 * Constructor
	 * @param propiedad
	 * @param operador
	 * @param value
	 */
	public Condition(String propiedad, String operador, Object valor) {
		super();
		this.propiedad = propiedad;
		this.operador = operador;
		this.valor = valor;
	}

	//------------------GETTERS AND SETTERS------------------	
	/**
	 * @return the propiedad
	 */
	public String getPropiedad() {
		return propiedad;
	}

	/**
	 * @param propiedad the propiedad to set
	 */
	public void setPropiedad(String propiedad) {
		this.propiedad = propiedad;
	}

	/**
	 * @return the operador
	 */
	public String getOperador() {
		return operador;
	}

	/**
	 * @param operador the operador to set
	 */
	public void setOperador(String operador) {
		this.operador = operador;
	}

	/**
	 * @return the value
	 */
	public Object getValor() {
		return valor;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(Object valor) {
		this.valor = valor;
	}

	/**
	 * Concatena la propiedad con el operador y el signo ?
	 * @return la condicion
	 */
	public String getCondicion(){
		return propiedad + operador + QUESTION;
	}

}
