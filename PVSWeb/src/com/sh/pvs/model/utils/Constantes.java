package com.sh.pvs.model.utils;

public class Constantes {

	/*ESTADOS CONSIGNACION*/
	public static final String CONS_ESTADO_INICIAL_COD = "CI";
	public static final String CONS_ESTADO_INICIAL_LBL = "Inicial";
	public static final String CONS_ESTADO_ENPROCESO_COD = "EP";
	public static final String CONS_ESTADO_ENPROCESO_LBL = "En Proceso";
	public static final String CONS_ESTADO_FINALIZADA_COD = "FI";
	public static final String CONS_ESTADO_FINALIZADA_LBL = "Finalizada";
	public static final String CONS_ESTADO_DECLINADA_COD = "DC";
	public static final String CONS_ESTADO_DECLINADA_LBL = "Declinada";
	
	/*ESTADOS RETOMA*/
	public static final String RETOMA_ESTADO_INICIAL_COD = "CI";
	public static final String RETOMA_ESTADO_INICIAL_LBL = "Inicial";
	public static final String RETOMA_ESTADO_ENPROCESO_COD = "EP";
	public static final String RETOMA_ESTADO_ENPROCESO_LBL = "En Proceso";
	public static final String RETOMA_ESTADO_FINALIZADA_COD = "FI";
	public static final String RETOMA_ESTADO_FINALIZADA_LBL = "Finalizada";
	public static final String RETOMA_ESTADO_DECLINADA_COD = "DC";
	public static final String RETOMA_ESTADO_DECLINADA_LBL = "Declinada";
	
	/*ESTADOS RETOMA*/
	public static final String VENTA_ESTADO_INICIAL_COD = "CI";
	public static final String VENTA_ESTADO_INICIAL_LBL = "Inicial";
	public static final String VENTA_ESTADO_ENPROCESO_COD = "EP";
	public static final String VENTA_ESTADO_ENPROCESO_LBL = "En Proceso";
	public static final String VENTA_ESTADO_FINALIZADA_COD = "FI";
	public static final String VENTA_ESTADO_FINALIZADA_LBL = "Finalizada";
	public static final String VENTA_ESTADO_DECLINADA_COD = "DC";
	public static final String VENTA_ESTADO_DECLINADA_LBL = "Declinada";
	
	/*ESTADOS HOJA DE NEGOCIO*/
	public static final String HN_EST_ENPROCESO = "EP";
	public static final String HN_EST_ENPROCESO_LBL = "En Proceso";
	public static final String HN_EST_FINALIZADA = "FI";
	public static final String HN_EST_FINALIZADA_LBL = "Finalizada";
	
}
