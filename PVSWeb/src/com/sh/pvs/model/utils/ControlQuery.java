/**
 * Condition.java:
 * 
 *	Encargado de construir una consulta con estructura SQL
 *
 * @author Sergio Henao, 06/03/2014
 * @Modifier Sergio Henao, 06/03/2014
 * @version 1.0
 * 
 */

package com.sh.pvs.model.utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

public class ControlQuery implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/*------------------CONSTANTES PARA ORDENAR------------------*/
	public static final String ASC = "asc";
	public static final String DESC = "desc";
	
	/*------------------ATRIBUTOS DEL QUERY------------------*/
	private String alias = "alias";
	private String join = "";	
	private String where = "";
	private ArrayList<String> joinAttr = new ArrayList<String>(0);
	private ArrayList<String> selectAttr = new ArrayList<String>(0);
	private ArrayList<String[]> orderbyAttr = new ArrayList<String[]>(0);
	private ArrayList<Object> valores = new ArrayList<Object>(0);
	 
	/*------------------CONSTRUCTORES------------------*/
	/**
	 * Constructor vacio
	 */
	public ControlQuery(){
		
	} 
	
	/**
	 * Constructor
	 * @param cond
	 */
	public ControlQuery(Condition cond){
		add(cond);
	}
	
	/*------------------METHODS------------------*/
	/**
	 * 
	 * @param cond
	 */
	public void add(Condition cond){
		if(cond!=null){
			if((cond.getPropiedad() != null && !cond.getPropiedad().equals("")) &&
					(cond.getOperador() != null && !cond.getOperador().equals(""))
					&& (cond.getValor() != null && !cond.getValor().toString().trim().equals(""))){
				
				if(!where.equals(""))
					where += Condition.AND; //Asignación por defecto para no crear errores				
				where += cond.getCondicion();								
				valores.add(cond.getValor());
			}	
		}	
	}
	
	/**
	 * 
	 * @param cq
	 */
	public void add(ControlQuery cq){
		
		if(cq!=null && !cq.getWhere().equals("")){
			if(!where.equals(""))
				where += Condition.AND;
			where += "(" + cq.getWhere() + ")";
			
			for (Iterator<Object> iter = cq.getValores().iterator(); iter.hasNext();) {
				Object value = iter.next();
				valores.add(value);
			}
		}	
	}
	
	/**
	 * 
	 * @param cond
	 */
	public void addAND(Condition cond){
		if(cond!=null){
			if((cond.getPropiedad() != null && !cond.getPropiedad().equals("")) &&
					(cond.getOperador() != null && !cond.getOperador().equals(""))
					&& (cond.getValor() != null && !cond.getValor().toString().trim().equals(""))){
				
				if(!where.equals(""))
					where += Condition.AND;
				where += cond.getCondicion();
					
				valores.add(cond.getValor());
								
			}	
		}
	}
	
	/**
	 * 
	 * @param cq
	 */
	public void addAND(ControlQuery cq){
		
		if(cq!=null && !cq.getWhere().equals("")){
			if(!where.equals(""))
				where += Condition.AND;
			where += "(" + cq.getWhere() + ")";
			
			for (Iterator<Object> iter = cq.getValores().iterator(); iter.hasNext();) {
				Object value = iter.next();
				valores.add(value);
			}
		}	
	}

	/**
	 * 
	 * @param cond
	 */
    public void addOR(Condition cond){
    	if(cond!=null){
			if((cond.getPropiedad() != null && !cond.getPropiedad().equals("")) &&
					(cond.getOperador() != null && !cond.getOperador().equals(""))
					&& (cond.getValor() != null && !cond.getValor().toString().trim().equals(""))){
				
				if(!where.equals(""))
					where += Condition.OR;
				where += cond.getCondicion();
				valores.add(cond.getValor());							
			}	
		}
    }
    
    /**
	 * 
	 * @param cq
	 */
	public void addOR(ControlQuery cq){
		
		if(cq!=null && !cq.getWhere().equals("")){
			if(!where.equals(""))
				where += Condition.OR;
			where += "(" + cq.getWhere() + ")";
			
			for (Iterator<Object> iter = cq.getValores().iterator(); iter.hasNext();) {
				Object value = iter.next();
				valores.add(value);
			}
		}	
	}
	
	/**
	 * Crea el query con los otros elementos: select, where y orderby
	 * @param from
	 * @return
	 */
	public String getQuery(String from){
		String query = "";
		
		//se crea el select
		if(createSelect() != null)
			query = createSelect();
		
		//establece la entidad de donde se toman los datos		
		query += " FROM " + from + " " + alias + " ";
		if(!this.join.equals("")){
			query += this.join;
		}else{
			//se crean los join debe ser siempre después del from
			if (joinAttr.size() > 0) {
				query += createJoins();
			}
		}
		
		//se crea el where
		if(!where.equals("")){
			query += " WHERE " + where;
		}
		//se crea el order by
		if (orderbyAttr.size() > 0) {
			query += createOrderBy();
		}
		return query;
	}
	
	/**
	 * Crea el select del query
	 * @return
	 */
	public String createSelect(){
		String select = null;
		
		if(selectAttr.size() > 0){
			select = "SELECT ";
			for(int i=0; i < selectAttr.size(); i++){
				select += selectAttr.get(i) + ", ";
				//se quita la coma del ultimo atributo
				if(i == selectAttr.size()-1){
					select = select.substring(0, select.length() - 2);
				}
			}
		}
		return select;
	}
	
	/**
	 * Crea el order by del query
	 * @return
	 */
	public String createOrderBy(){
		String order = null;
		
		if (orderbyAttr.size() > 0) {
			String[] attr = new String[2];
			order = " ORDER BY ";
			for (int i = 0; i < orderbyAttr.size(); i++) {
				attr = orderbyAttr.get(i);
				order += attr[0] + " " + attr[1] + ", ";
				//se quita la coma del ultimo atributo
				if (i == orderbyAttr.size() - 1)
					order = order.substring(0, order.length() - 2);				
			}
		}
		return order;
	}
	
	/**
	 * Crea el left join fetch
	 * @return
	 */
	public String createJoins(){
		String join = "";
		
		if(joinAttr.size() > 0){			
			for(int i=0; i < joinAttr.size(); i++){
				join += "left join "+joinAttr.get(i) + " ";				
			}
		}
		return join;
	}
	
	/**
	 * @return the where
	 */
	public String getWhere() {
		return where;
	}

	/**
	 * @param where the where to set
	 */
	public void setWhere(String where) {
		this.where = where;
	}

	/**
	 * @return the valores
	 */
	public ArrayList<Object> getValores() {
		return valores;
	}

	/**
	 * @param valores the valores to set
	 */
	public void setValores(ArrayList<Object> valores) {
		this.valores = valores;
	}

	/**
	 * @return the select
	 */
	public ArrayList<String> getSelect() {
		return selectAttr;
	}
	
	/**
	 * @param select the select to set
	 */
	public void setSelect(ArrayList<String> select) {
		this.selectAttr = select;
	}

	/**
	 * @return the orderby
	 */
	public ArrayList<String[]> getOrderby() {
		return orderbyAttr;
	}

	/**
	 * @param orderby the orderby to set
	 */
	public void setOrderby(ArrayList<String[]> orderby) {
		this.orderbyAttr = orderby;
	}
	
	/**
	 * @return the from
	 */
	public String getJoin() {
		return join;
	}

	/**
	 * @param join the join to set
	 */
	public void setJoin(String join) {
		this.join = join;
	}

	/**
	 * @return the joinAttr
	 */
	public ArrayList<String> getJoinAttr() {
		return joinAttr;
	}

	/**
	 * @param joinAttr the joinAttr to set
	 */
	public void setJoinAttr(ArrayList<String> joinAttr) {
		this.joinAttr = joinAttr;
	}

	/**
	 * @return the alias
	 */
	public String getAlias() {
		return alias;
	}

	/**
	 * @param alias the alias to set
	 */
	public void setAlias(String alias) {
		this.alias = alias;
	}
	
	

}
