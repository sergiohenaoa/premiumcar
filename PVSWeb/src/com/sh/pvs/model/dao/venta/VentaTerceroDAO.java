package com.sh.pvs.model.dao.venta;

public class VentaTerceroDAO{

	public static final String CODIGO = "codigo";
	public static final String COMISION = "comision";
	public static final String IDCOMPANIA = "compania.id";
	public static final String IDVEHICULO = "vehiculo.id";
	public static final String FCHVENTA = "fchVenta";

}