package com.sh.pvs.model.dao.venta;

public class VentaDAO{

	public static final String CODIGO = "codigo";
	public static final String VALOR_VEHICULO = "valorVehiculo";
	public static final String REQUIERE_DATOS_MATRICULA = "requiereDatosMatricula";
	public static final String ESTADO = "estado";
	public static final String IDCOMPANIA = "compania.id";
	public static final String FCHVENTA = "fchVenta";

}