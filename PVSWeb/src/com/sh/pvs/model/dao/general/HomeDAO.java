/**
 * HomeDAO.java:
 * 
 * 	Métodos para DAO genéricos
 *
 * @author Sergio Henao, 06/03/2014
 * @Modifier Sergio Henao, 06/03/2014
 * @version 1.0
 * 
 */
package com.sh.pvs.model.dao.general;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;

import com.sh.pvs.model.hibernate.BaseHibernateDAO;
import com.sh.pvs.model.utils.ControlQuery;
import com.sh.pvs.utils.exceptioncontrol.ControlException;


public class HomeDAO<T> extends BaseHibernateDAO{

	/*------------------ATRIBUTOS------------------*/
	protected Class<T> entityClass;

	/*------------------CONSTRUCTORES------------------*/
	/**
	 * Default constructor
	 * @param entityClass
	 */
	public HomeDAO(Class<T> entityClass) {
		this.entityClass = entityClass;
	}	

	/*------------------METHODS------------------*/
	/**
	 * Método para listar todos los registros de un elemento en específico 
	 * @return Lista de elementos
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<T> findAll() throws Exception{
		List<T> ents = null;
		try {
			getSession().clear();
			String queryString = "from "+entityClass.getSimpleName();
			Query queryObject = getSession().createQuery(queryString);
			ents = queryObject.list();			
			//getSession().refresh(ents);
		} catch (RuntimeException re) {
			throw ControlException.formatException("TECNICA", HomeDAO.class.getName(), entityClass.getSimpleName(), "findAll", re);
		} catch (Exception e) {
			throw ControlException.formatException("TECNICA", HomeDAO.class.getName(), entityClass.getSimpleName(), "findAll", e);
		}
		return ents;
	}
	
	/**
	 * Método para listar todos los registros de un elemento en específico
	 * utilizando paginación
	 * 
	 * @param start Dato donde comienza la paginacion
	 * @param countData Cantidad de resultados
	 * @return Lista de elementos
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<T> findAll(int start,int countData) throws Exception{
		List<T> ents = null;
		try {
			getSession().clear();
			String queryString = "from "+entityClass.getSimpleName();
			Query queryObject = getSession().createQuery(queryString);
			queryObject.setFirstResult(start);
			queryObject.setMaxResults(countData);
			ents = queryObject.list();			
			//getSession().refresh(ents);
		} catch (RuntimeException re) {
			throw ControlException.formatException("TECNICA", HomeDAO.class.getName(), entityClass.getSimpleName(), "findAll", re);
		} catch (Exception e) {
			throw ControlException.formatException("TECNICA", HomeDAO.class.getName(), entityClass.getSimpleName(), "findAll", e);
		}
		return ents;
	}
	
	/**
	 * Método para listar todos los registros de un elemento en específico según criterios
	 * @param cq
	 * @return Lista de elementos
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<T> findAll(ControlQuery cq) throws Exception{
		List<T> ents = null;
		try {
			getSession().clear();
			//Construcción del query 
			String queryString = cq.getQuery(entityClass.getSimpleName());

			//Logica de bases de datos
			Query queryObject = getSession().createQuery(queryString);

			//set de los parametros
			if (cq != null){
				for (int i = 0; i < cq.getValores().size(); i++) {
					ArrayList<Object> valores = cq.getValores();
					queryObject.setParameter(i, valores.get(i));
				}
			}
			ents = queryObject.list();			
			
		} catch (RuntimeException re) {
			throw ControlException.formatException("TECNICA", HomeDAO.class.getName(), entityClass.getSimpleName(), "findAll(cq)", re);
		} catch (Exception e) {
			throw ControlException.formatException("TECNICA", HomeDAO.class.getName(), entityClass.getSimpleName(), "findAll(cq)", e);
		}
		return ents;
	}
	
	/**
	 * Método para listar todos los registros de un elemento en específico según criterios
	 * utilizando Paginación
	 * 
	 * @param cq ControlQuery
	 * @param start Dato donde comienza la paginacion
	 * @param countData Cantidad de resultados
	 * @return Lista de elementos
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<T> findAll(ControlQuery cq,int start,int countData) throws Exception{
		List<T> ents = null;
		try {
			getSession().clear();
			//Construcción del query 
			String queryString = cq.getQuery(entityClass.getSimpleName());

			//Logica de bases de datos
			Query queryObject = getSession().createQuery(queryString);

			//set de los parametros
			if (cq != null){
				for (int i = 0; i < cq.getValores().size(); i++) {
					ArrayList<Object> valores = cq.getValores();
					queryObject.setParameter(i, valores.get(i));
				}
			}
			queryObject.setFirstResult(start);
			queryObject.setMaxResults(countData);
			ents = queryObject.list();			
			
		} catch (RuntimeException re) {
			throw ControlException.formatException("TECNICA", HomeDAO.class.getName(), entityClass.getSimpleName(), "findAll(cq)", re);
		} catch (Exception e) {
			throw ControlException.formatException("TECNICA", HomeDAO.class.getName(), entityClass.getSimpleName(), "findAll(cq)", e);
		}
		return ents;
	}

	/**
	 * Busqueda de todas las entidades
	 * @param query
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> findAllByNativeQuery(String query)  throws Exception{

		List<Object[]> ents = null;
				
		try {

			//Logica de bases de datos
			Query queryObject = getSession().createQuery(query);						

			ents = queryObject.list();

		} catch (RuntimeException re) {
			throw ControlException.formatException("TECNICA", HomeDAO.class.getName(), entityClass.getSimpleName(), "findAllByNativeQuery(query)", re);
		} catch (Exception e) {
			throw ControlException.formatException("TECNICA", HomeDAO.class.getName(), entityClass.getSimpleName(), "findAllByNativeQuery(query)", e);
		}

		return ents;
	}
	
	/**
	 * Busqueda de todas las entidades
	 * @param query
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> findAllByNativeQuery(String query, int maxresults)  throws Exception{

		List<Object[]> ents = null;
				
		try {

			//Logica de bases de datos
			Query queryObject = getSession().createQuery(query).setMaxResults(maxresults);						

			ents = queryObject.list();

		} catch (RuntimeException re) {
			throw ControlException.formatException("TECNICA", HomeDAO.class.getName(), entityClass.getSimpleName(), "findAllByNativeQuery(query)", re);
		} catch (Exception e) {
			throw ControlException.formatException("TECNICA", HomeDAO.class.getName(), entityClass.getSimpleName(), "findAllByNativeQuery(query)", e);
		}

		return ents;
	}
	
	/**
	 * Consulta de una entidada por Id
	 * @param id
	 * @return registro
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public T findById(Integer id) throws Exception{
		try {
			getSession().clear();
			T instance = (T) getSession().get(entityClass, id);			
					
			return instance;
		} catch (RuntimeException re) {
			throw ControlException.formatException("TECNICA", HomeDAO.class.getName(), entityClass.getSimpleName(), "findById(id)", re);
		} catch (Exception e) {
			throw ControlException.formatException("TECNICA", HomeDAO.class.getName(), entityClass.getSimpleName(), "findById(id)", e);
		}
	}
	
	/**
	 * Consulta de una entidada por Id
	 * @param id
	 * @return registro
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public T findById(String id) throws Exception{
		try {
			getSession().clear();
			T instance = (T) getSession().get(entityClass, id);			
					
			return instance;
		} catch (RuntimeException re) {
			throw ControlException.formatException("TECNICA", HomeDAO.class.getName(), entityClass.getSimpleName(), "findById(id)", re);
		} catch (Exception e) {
			throw ControlException.formatException("TECNICA", HomeDAO.class.getName(), entityClass.getSimpleName(), "findById(id)", e);
		}
	}
	
	/**
	 * Consulta de entidades por una propiedad en específico
	 * @param propertyName
	 * @param value
	 * @return lista de registros
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<T> findByProperty(String propertyName, Object value) throws Exception{
		
		List<T> ents = null;
		
		try {
			getSession().clear();
			String queryString = "from "+entityClass.getSimpleName()+" as model where model." + propertyName + "= :propertyValue";
			Query queryObject = getSession().createQuery(queryString);
			queryObject.setParameter("propertyValue", value);
			ents = queryObject.list();
			
		} catch (RuntimeException re) {
			throw ControlException.formatException("TECNICA", HomeDAO.class.getName(), entityClass.getSimpleName(), "findByProperty", re);
		} catch (Exception e) {
			throw ControlException.formatException("TECNICA", HomeDAO.class.getName(), entityClass.getSimpleName(), "findByProperty", e);
		}
		
		return ents;
	}
	
	/**
	 * Almacenamiento de una entidad en base de datos
	 * @param entity
	 * @return Entidad creada
	 * @throws Exception
	 */
	public T save(T entity) throws Exception{	
		try {
			getSession().save(entity);
			return entity;
		} catch (RuntimeException re) {
			throw ControlException.formatException("TECNICA", HomeDAO.class.getName(), entityClass.getSimpleName(), "save", re);
		}  catch (Exception e) {
			throw ControlException.formatException("TECNICA", HomeDAO.class.getName(), entityClass.getSimpleName(), "save", e);
		}
	}

	/**
	 * Actualización de una entidad
	 * @param entity
	 * @return Entidad actualizada
	 * @throws Exception
	 */
	public T merge(T entity) throws Exception{
		try {
			getSession().merge(entity);
			return entity;
		} catch (RuntimeException re) {
			throw ControlException.formatException("TECNICA", HomeDAO.class.getName(), entityClass.getSimpleName(), "merge", re);
		} catch (Exception e) {
			throw ControlException.formatException("TECNICA", HomeDAO.class.getName(), entityClass.getSimpleName(), "merge", e);
		}
	}
	
	/**
	 * Actualización de una entidad
	 * @param entity
	 * @return Entidad actualizada
	 * @throws Exception
	 */
	public T update(T entity) throws Exception{
		try {
			getSession().update(entity);
			return entity;
		} catch (RuntimeException re) {
			throw ControlException.formatException("TECNICA", HomeDAO.class.getName(), entityClass.getSimpleName(), "update", re);
		} catch (Exception e) {
			throw ControlException.formatException("TECNICA", HomeDAO.class.getName(), entityClass.getSimpleName(), "update", e);
		}
	}

	/**
	 * Eliminación de una entidad
	 * @param entity
	 * @throws Exception
	 */
	public void delete(T entity) throws Exception{
		try {
			getSession().delete(entity);
		} catch (RuntimeException re) {
			throw ControlException.formatException("TECNICA", HomeDAO.class.getName(), entityClass.getSimpleName(), "delete", re);
		} catch (Exception e) {
			throw ControlException.formatException("TECNICA", HomeDAO.class.getName(), entityClass.getSimpleName(), "delete", e);
		}
	}

}
