package com.sh.pvs.model.dao.hojanegocio;

public class HojaNegocioDAO {

	/*------------------ATRIBUTOS------------------*/
	public static final String ID = "id"; 
	public static final String IDCONSIGNACION = "consignacion.id"; 
	public static final String IDRETOMA = "retoma.id";
	public static final String IDVENTA = "venta.id";
	public static final String ESTADO = "estado";
	public static final String FCHCONSIGNACION = "consignacion.fchConsignacion";
	public static final String IDCIACONSIGNACION = "consignacion.compania.id";

}
