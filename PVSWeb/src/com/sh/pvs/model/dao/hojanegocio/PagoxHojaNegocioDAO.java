package com.sh.pvs.model.dao.hojanegocio;


public class PagoxHojaNegocioDAO {

	/*------------------ATRIBUTOS------------------*/
	public static final String ID = "id"; 
	public static final String CONSECUTIVO = "consecutivo"; 
	public static final String FCHPAGO = "fchPago"; 
	public static final String PAGADOPOR = "pagadoPor"; 
	public static final String PAGADOA = "pagadoA"; 
	public static final String CONCEPTO = "concepto"; 
	public static final String VALOR = "valor"; 
	public static final String HOJANEGOCIO = "hojaNegocio"; 
	public static final String CUENTASALIDA = "cuentaSalida"; 
	public static final String CUENTAENTRADA = "cuentaEntrada"; 
	public static final String FORMAPAGO = "formaPago"; 
	public static final String GASTOCONSIGNACION = "gastoConsignacion"; 
	public static final String GASTORETOMA = "gastoRetoma"; 
	public static final String GASTOVENTA = "gastoVenta"; 
	public static final String IDCUENTASALIDA = "cuentaSalida.id"; 
	public static final String IDCUENTAENTRADA = "cuentaEntrada.id"; 

}