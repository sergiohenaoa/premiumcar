package com.sh.pvs.model.dao.seguridad;

/**
 * FormularioDAO.java:
 * 
 * 	Métodos para DAO de la entidad Formulario
 *
 * @author Sergio Henao, 06/03/2014
 * @Modifier Sergio Henao, 06/03/2014
 * @version 1.0
 * 
 */
public class FormularioDAO{

	/*------------------ATRIBUTOS------------------*/
	public static final String ID = "id";
	public static final String IDFORMULARIOPADRE = "formularioPadre.Id";
	public static final String NOMBREDESCRIPTIVO = "nombreDescriptivo";
	public static final String NOMBRECLASE = "nombreClase";
	public static final String URL = "url";
	public static final String ESTADO = "esActivo";
	public static final String FORMULARIO = "esFormulario";
	public static final String ORDEN = "orden";
	public static final String IDCOMPANIA = "compania.id";
	
}
