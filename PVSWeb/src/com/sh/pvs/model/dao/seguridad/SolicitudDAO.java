package com.sh.pvs.model.dao.seguridad;
// default package


public class SolicitudDAO  {
		//property constants
	public static final String ID = "id";
	public static final String NIT_CLIENTE = "nitCliente";
	public static final String NOMBRE_CLIENTE = "nombreCliente";
	public static final String CEDULA_SOLICITANTE = "cedulaSolicitante";
	public static final String NOMBRE_SOLICITANTE = "nombreSolicitante";
	public static final String OBSERVACIONES = "observaciones";
	public static final String FECHA_TEXT = "fechaText";
	public static final String ESTADO = "estado";
	public static final String COMPANIA = "compania";


}