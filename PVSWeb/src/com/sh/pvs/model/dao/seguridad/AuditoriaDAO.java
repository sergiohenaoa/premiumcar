package com.sh.pvs.model.dao.seguridad;

/**
 * @author andress
 *
 */
public class AuditoriaDAO {
	
	public static final String IDAUDITORIA = "id";
	public static final String USUARIO = "usuario";
	public static final String FORMULARIO = "formulario";
	public static final String REGISTRO = "registro";
	public static final String ACCION = "accion";
	public static final String FECHA = "fecha";
	public static final String OBSERVACION = "observaciones";
	public static final String COMPANIA = "compania";
	public static final String COMPANIAID = "companiaId";


}
