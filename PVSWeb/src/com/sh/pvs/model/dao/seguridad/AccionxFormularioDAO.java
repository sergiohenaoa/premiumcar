package com.sh.pvs.model.dao.seguridad;


/**
 * AccionxFormularioDAO.java:
 * 
 * 	Métodos para DAO de la entidad Acción por formulario
 *
 * @author Sergio Henao, 06/03/2014
 * @Modifier Sergio Henao, 06/03/2014
 * @version 1.0
 * 
 */
public class AccionxFormularioDAO {
	
	/*------------------ATRIBUTOS------------------*/
	public static final String ID = "id";
	public static final String IDACCION = "accion.id";
	public static final String IDFORMULARIO = "formulario.id";
	public static final String ESTADO = "estado";

}