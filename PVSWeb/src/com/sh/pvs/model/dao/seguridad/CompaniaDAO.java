package com.sh.pvs.model.dao.seguridad;

/**
 * CompaniaDAO.java:
 * 
 * 	Métodos para DAO de la entidad Formulario
 *
 * @author Sergio Henao, 06/03/2014
 * @Modifier Sergio Henao, 06/03/2014
 * @version 1.0
 * 
 */
public class CompaniaDAO{

	/*------------------ATRIBUTOS------------------*/
	public static final String ID = "id";
	public static final String CODIGO = "codigo";
	public static final String NOMBRE = "nombre";
	public static final String ESTADO = "esActiva";
	
	
}
