package com.sh.pvs.model.dao.seguridad;

/**
 * AccionDAO.java:
 * 
 * 	Métodos para DAO de la entidad Acción
 *
 * @author Sergio Henao, 06/03/2014
 * @Modifier Sergio Henao, 06/03/2014
 * @version 1.0
 * 
 */

public class AccionDAO {
	
	/*------------------ATRIBUTOS------------------*/
	public static final String ID = "id";
	public static final String NOMBRE = "nombre";

}