package com.sh.pvs.model.dao.seguridad;

/**
 * UsuarioDAO.java:
 * 
 * 	Métodos para DAO de la entidad Usuario
 *
 * @author Sergio Henao, 06/03/2014
 * @Modifier Sergio Henao, 06/03/2014
 * @version 1.0
 * 
 */

public class UsuarioDAO {

	/*------------------ATRIBUTOS------------------*/
	public static final String USUARIO = "usuario";
	public static final String NOMBRE = "nombre";
	public static final String CLAVE = "clave";
	public static final String EMAIL = "email";
	public static final String ESTADO = "esActivo";
	public static final String IDCOMPANIA = "compania.id";
	public static final String ESTADOCOMPANIA = "compania.esActiva";
	public static final String SOLICITUD = "solicitud";
	
}
