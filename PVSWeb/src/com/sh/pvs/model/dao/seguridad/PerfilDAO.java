package com.sh.pvs.model.dao.seguridad;

/**
 * PerfilDAO.java:
 * 
 * 	Métodos para DAO de la entidad Usuario
 *
 * @author Sergio Henao, 06/03/2014
 * @Modifier Sergio Henao, 06/03/2014
 * @version 1.0
 * 
 */
public class PerfilDAO {

	/*------------------ATRIBUTOS------------------*/
	public static final String ID = "id";
	public static final String NOMBRE = "nombre";
	public static final String DESCRIPCION = "descripcion";
	public static final String ESTADO = "esActivo";
	public static final String COMPANIA = "compania.id";
	
}
