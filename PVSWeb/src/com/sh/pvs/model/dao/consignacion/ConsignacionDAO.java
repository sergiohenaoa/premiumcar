package com.sh.pvs.model.dao.consignacion;

public class ConsignacionDAO{

	public static final String CODIGO = "codigo";
	public static final String KILOMETRAJE = "kilometraje";
	public static final String VALOR_VEHICULO = "valorVehiculo";
	public static final String REQUIERE_DATOS_MATRICULA = "requiereDatosMatricula";
	public static final String ESTADO = "estado";
	public static final String IDCOMPANIA = "compania.id";
	public static final String IDVEHICULO = "vehiculo.id";
	public static final String FCHCONSIGNACION = "fchConsignacion";

}