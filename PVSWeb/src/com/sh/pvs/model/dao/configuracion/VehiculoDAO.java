package com.sh.pvs.model.dao.configuracion;

public class VehiculoDAO{

	/*------------------ATRIBUTOS------------------*/
	public static final String MODELO = "modelo";
	public static final String LINEA = "linea";
	public static final String MOTOR = "motor";
	public static final String PLACA = "placa";
	public static final String CHASIS = "chasis";
	public static final String SERIE = "serie";
	public static final String ES_ACTIVO_INVENTARIO = "esActivoInventario";
	public static final String IDCOMPANIA = "compania.id";

}