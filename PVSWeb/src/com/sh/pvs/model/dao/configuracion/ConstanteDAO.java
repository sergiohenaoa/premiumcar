package com.sh.pvs.model.dao.configuracion;

public class ConstanteDAO {
	
	/*------------------ATRIBUTOS------------------*/
	public static final String ID = "id"; 
	public static final String CODIGO = "codigo";  
	public static final String VALOR = "valor";
	public static final String DESCRIPCION = "descripcion";
	public static final String IDCOMPANIA = "compania.id";
	
}
