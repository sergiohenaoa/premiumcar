package com.sh.pvs.model.dao.configuracion;

public class ConcesionarioDAO {
	
	/*------------------ATRIBUTOS------------------*/
	public static final String ID = "id"; 
	public static final String NOMBRE = "nombre"; 
	public static final String NIT = "nit"; 
	public static final String DESCRIPCION = "descripcion";
	public static final String ESTADO = "esActivo";
	public static final String IDCOMPANIA = "compania.id";
	
}
