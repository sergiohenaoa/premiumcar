package com.sh.pvs.model.dao.configuracion;

import com.sh.pvs.model.dao.general.HomeDAO;
import com.sh.pvs.model.dto.configuracion.TramitexOficinaTransito;

public class TramitexOficinaTransitoDAO extends HomeDAO<TramitexOficinaTransito> {
	
	/*------------------ATRIBUTOS------------------*/
	public static final String ID = "id"; 
	public static final String NOMBRE = "codigo";  
	public static final String DESCRIPCION = "descripcion";
	public static final String ESTADO = "esActivo";
	public static final String IDOFICINATRANSITO = "oficinaTransito.id";
	
	/*-------------------CONSTRUCTOR----------------------*/
	public TramitexOficinaTransitoDAO() {
		super(TramitexOficinaTransito.class);
	}
	
	public TramitexOficinaTransitoDAO(Class<TramitexOficinaTransito> entityClass) {
		super(entityClass);
	}
	
}
