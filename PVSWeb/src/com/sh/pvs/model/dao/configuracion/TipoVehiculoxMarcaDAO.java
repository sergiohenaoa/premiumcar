package com.sh.pvs.model.dao.configuracion;

import com.sh.pvs.model.dao.general.HomeDAO;
import com.sh.pvs.model.dto.configuracion.TipoVehiculoxMarca;

public class TipoVehiculoxMarcaDAO extends HomeDAO<TipoVehiculoxMarca> {
	
	/*------------------ATRIBUTOS------------------*/
	public static final String ID = "id"; 
	public static final String IDMARCA = "marca.id";
	public static final String IDCIAMARCA = "marca.compania.id";
	public static final String ESTADOMARCA = "marca.esActivo";
	public static final String NOMBREMARCA = "marca.nombre";
	public static final String IDTIPOVEHICULO = "tipoVehiculo.id";
	
	/*-------------------CONSTRUCTOR----------------------*/
	public TipoVehiculoxMarcaDAO() {
		super(TipoVehiculoxMarca.class);
	}
	
	public TipoVehiculoxMarcaDAO(Class<TipoVehiculoxMarca> entityClass) {
		super(entityClass);
	}
	
}
