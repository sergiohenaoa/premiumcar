package com.sh.pvs.model.dao.configuracion;

public class GastoDAO {
	
	/*------------------ATRIBUTOS------------------*/
	public static final String NOMBRE = "nombre";
	public static final String DESCRIPCION = "descripcion";
	public static final String ES_ACTIVO = "esActivo";
	public static final String TIPOGASTO = "tipoGasto";
	public static final String VALOR = "valor";
	public static final String ES_DEFECTO_CONSIGNACION = "esDefectoConsignacion";
	public static final String IDCOMPANIA = "compania.id";

}