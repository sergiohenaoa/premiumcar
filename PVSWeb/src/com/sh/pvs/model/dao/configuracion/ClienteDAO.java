package com.sh.pvs.model.dao.configuracion;

public class ClienteDAO {

	/*------------------ATRIBUTOS------------------*/
	public static final String ID = "id"; 
	public static final String CEDULA = "cedula"; 
	public static final String NOMBRE = "nombres"; 
	public static final String TELEFONO1 = "telefono1"; 
	public static final String CELULAR1 = "celular1";
	public static final String CORREOELECTRONICO = "correoElectronico"; 
	public static final String TELEFONO2 = "telefono2"; 
	public static final String CELULAR2 = "celular2"; 
	public static final String EMPRESA = "empresa"; 
	public static final String CARGO = "cargo"; 
	public static final String OBSERVACIONES = "observaciones"; 
	public static final String IDCOMPANIA = "compania.id"; 

}