package com.sh.pvs.model.dao.configuracion;

public class MarcaDAO {
	
	/*------------------ATRIBUTOS------------------*/
	public static final String ID = "id"; 
	public static final String NOMBRE = "codigo";  
	public static final String DESCRIPCION = "descripcion";
	public static final String ESTADO = "esActivo";
	public static final String IDCOMPANIA = "compania.id";
	public static final String IDTIPOVEHICULO = "tipoVehiculo.id";
	
}
