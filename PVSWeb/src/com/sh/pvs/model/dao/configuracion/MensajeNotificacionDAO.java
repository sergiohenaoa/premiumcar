package com.sh.pvs.model.dao.configuracion;

public class MensajeNotificacionDAO {
	
	/*------------------ATRIBUTOS------------------*/
	public static final String ID = "id"; 
	public static final String CODIGO = "codigo";  
	public static final String ASUNTO = "asunto";
	public static final String MENSAJE = "mensaje";
	public static final String IDCOMPANIA = "compania.id";
	
}
