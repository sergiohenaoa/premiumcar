package com.sh.pvs.model.dao.contabilidad;



public class MovimientoContableDAO {

	/*------------------ATRIBUTOS------------------*/
	public static final String ID = "id"; 
	public static final String CODIGO = "codigo"; 
	public static final String DESCRIPCION = "descripcion"; 
	public static final String FECHA = "fecha"; 
	public static final String VALOR = "valor"; 
	public static final String IDCUENTA = "cuenta.id"; 
	public static final String IDUSUARIO = "usuario.id"; 
	public static final String ESINGRESO = "esIngreso"; 
	public static final String IDCOMPANIA = "compania.id";

}