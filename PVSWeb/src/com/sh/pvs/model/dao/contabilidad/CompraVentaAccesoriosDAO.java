package com.sh.pvs.model.dao.contabilidad;


public class CompraVentaAccesoriosDAO {

	/*------------------ATRIBUTOS------------------*/
	public static final String ID = "id"; 
	public static final String FECHA = "fecha"; 
	public static final String ESCOMPRA = "esCompra"; 
	public static final String ESPAGADO = "esPagado"; 
	public static final String NROFACTURA = "nroFactura"; 
	public static final String PROVEEDOR = "proveedor"; 
	public static final String OBSERVACIONES = "observaciones"; 
	public static final String VALOR = "valor";
	public static final String PLAZO = "plazo"; 
	
	public static final String IDCUENTA = "cuenta.id";
	public static final String IDUSUARIO = "usuario.id";
	public static final String IDCIA = "compania.id";
	
}