package com.sh.pvs.model.dao.contabilidad;


public class AccesoriosxCompraVentaDAO {

	/*------------------ATRIBUTOS------------------*/
	public static final String ID = "id"; 
	public static final String IDCOMPRAVENTA = "compraVentaAccesorios.id";
	public static final String IDACCESORIO = "accesorio.id";
	public static final String PRECIO = "precio"; 
	public static final String CANTIDAD = "cantidad"; 
	
}