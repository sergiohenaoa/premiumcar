package com.sh.pvs.model.dao.contabilidad;

public class InventarioAccesoriosDAO {
	
	/*------------------ATRIBUTOS------------------*/
	public static final String NOMBRE = "nombre";
	public static final String DESCRIPCION = "descripcion";
	public static final String ES_ACTIVO = "esActivo";
	public static final String IDCOMPANIA = "compania.id";
	public static final String CANTIDADDISPONIBLE = "cantidadDisponible";
	public static final String IDACCESORIO = "accesorio.id";

}