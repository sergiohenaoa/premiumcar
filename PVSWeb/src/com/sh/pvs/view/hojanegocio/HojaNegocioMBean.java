/**
	 * 	HojaNegocioMBean.java:
	 * 
	 * 	Esta clase corresponde al Managed Bean para controlar el CRUD de 
	 * 	la entidad HojaNegocio
	 *
	 * @author Sergio Henao - sergiohenaoa@gmail.com - 11/04/2015
	 * @Modifier Sergio Henao - sergiohenaoa@gmail.com - 11/04/2015
	 * @version 1.0
	 * 
	 */
package com.sh.pvs.view.hojanegocio;

import com.sh.pvs.core.interfaces.configuracion.ICuentaServices;
import com.sh.pvs.core.interfaces.configuracion.IFormaPagoServices;
import com.sh.pvs.core.interfaces.configuracion.IGastoServices;
import com.sh.pvs.core.interfaces.configuracion.IOficinaTransitoServices;
import com.sh.pvs.core.interfaces.hojanegocio.IGastoxHojaNegocioServices;
import com.sh.pvs.core.interfaces.hojanegocio.IHojaNegocioServices;
import com.sh.pvs.core.interfaces.hojanegocio.IPagoxHojaNegocioServices;
import com.sh.pvs.core.services.configuracion.CuentaServices;
import com.sh.pvs.core.services.configuracion.FormaPagoServices;
import com.sh.pvs.core.services.configuracion.GastoServices;
import com.sh.pvs.core.services.configuracion.OficinaTransitoServices;
import com.sh.pvs.core.services.hojanegocio.GastoxHojaNegocioServices;
import com.sh.pvs.core.services.hojanegocio.HojaNegocioServices;
import com.sh.pvs.core.services.hojanegocio.PagoxHojaNegocioServices;
import com.sh.pvs.model.dto.configuracion.Cuenta;
import com.sh.pvs.model.dto.configuracion.FormaPago;
import com.sh.pvs.model.dto.configuracion.Gasto;
import com.sh.pvs.model.dto.configuracion.OficinaTransito;
import com.sh.pvs.model.dto.hojanegocio.GastoxHojaNegocio;
import com.sh.pvs.model.dto.hojanegocio.HojaNegocio;
import com.sh.pvs.model.dto.hojanegocio.PagoxHojaNegocio;
import com.sh.pvs.model.dto.seguridad.Usuario;
import com.sh.pvs.view.utils.Constantes;
import com.sh.pvs.view.utils.ControlException;
import com.sh.pvs.view.utils.Metodos;
import org.primefaces.context.RequestContext;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;
import java.io.Serializable;
import java.util.*;

@ManagedBean
@ViewScoped
public class HojaNegocioMBean implements Serializable {

	/*Serializable Attribute*/
	private static final long serialVersionUID = 1L;
	
	/*Variables*/
	private HojaNegocio entity; 							// ViewForm - EditForm 	
 	private List<HojaNegocio> entityList; 					// Lista de registros del datatable
 	private List<HojaNegocio> entityFilterList;				// Lista de registros filtrados del datatable
 	private List<OficinaTransito> oficinasTransitoDropDownList;
 	private GastoxHojaNegocio gastoxHojaNegocio;
 	private List<Gasto> gastosDropDownList;
 	private PagoxHojaNegocio pagoxHojaNegocio;
 	private List<Cuenta> cuentasDropDownList;
 	private List<FormaPago> formasPagoDropDownList;
 	private Usuario loggedUser;								//Usuario logeado (En Sesión)
 	
 	/*Navegación*/
 	private String mode;									// view - edit - list - create ==> Modo para identificar la vista
 	
 	/*Servicios*/
 	private IHojaNegocioServices entityService;			// Servicios para la gestión de la entidad
	
 	/*Variables para navegación*/
 	public static String openDialog = "/hojanegocio/hojaNegocioForm.faces";
 	public static String openWebMobile = "hojaNegocioForm.faces";
 	public static String closeRedirectMobile = "hojaNegocioList.faces";
	public static String closeRedirectWeb = "/hojanegocio/hojaNegocioList.faces?faces-redirect=true";
	
	public static String openDialogGasto = "/hojanegocio/gastoxHojaNegocioForm.faces";
 	public static String openWebMobileGasto = "gastoxHojaNegocioForm.faces";
 	
 	public static String openDialogPago = "/hojanegocio/pagoxHojaNegocioForm.faces";
 	public static String openWebMobilePago = "pagoxHojaNegocioForm.faces";
 	
 	/*Constructors*/	
	/**	Default constructor */
	public HojaNegocioMBean() {

	}
	
	/*METHODS*/
	
	/**
	 * Método para inicializar las variables
	 * 
	 * */
	@PostConstruct
	private void init(){
		
		FacesContext context = FacesContext.getCurrentInstance();
		
		try {
			
			Metodos.loadSessionMesagges();
			
			loggedUser = Metodos.getUserSession();
			
			if(context.getExternalContext().getRequestParameterMap().get("mode") != null)
				mode = context.getExternalContext().getRequestParameterMap().get("mode");
			else
				mode = Constantes.MODELIST;
			
			if(mode.equals(Constantes.MODECREATE)){
				this.entity = new HojaNegocio();
				return;
			}
			
			if(mode.equals(Constantes.MODELIST)){
				fillEntityList();
				return;
			}
						
			if(mode.equals(Constantes.MODEVIEW) || mode.equals(Constantes.MODEEDIT)){
				
				try {
					
					String idParam = context.getExternalContext().getRequestParameterMap().get("id");
					entityService = new HojaNegocioServices();
					entity = entityService.findById(Integer.parseInt(idParam));
					
					if(entity.getOficinaTransito() == null)
						entity.setOficinaTransito(new OficinaTransito());
					
					fillOficinasTransitoDropDown();
							
				} catch (Exception e) {
					context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
				}
				
				return;
			}
			
			//Si es la consulta de un gasto traspaso o adicional del vendedor o del comprador
			if(mode.equals("GTOTRVENVIEW") || mode.equals("GTOADVENVIEW") 
					|| mode.equals("GTOTRCOMVIEW") || mode.equals("GTOADCOMVIEW")
					|| mode.equals("GTOADCIAVIEW")){
				
				IGastoxHojaNegocioServices gastoSrv = new GastoxHojaNegocioServices();
				String idgasto = context.getExternalContext().getRequestParameterMap().get("idgasto");
				gastoxHojaNegocio = gastoSrv.findById(Integer.parseInt(idgasto));
				
				entityService = new HojaNegocioServices();
				entity = entityService.findById(gastoxHojaNegocio.getHojaNegocio().getId());
				
			}
			
			//Si es la creación de un gasto traspaso o adicionale del vendedor o del comprador
			if(mode.equals("GTOTRVENNEW") || mode.equals("GTOTRCOMNEW")
					|| mode.equals("GTOADVENNEW") || mode.equals("GTOADCOMNEW")
					|| mode.equals("GTOADCIANEW")){
				
				entityService = new HojaNegocioServices();
				String idHN = context.getExternalContext().getRequestParameterMap().get("idHN");
				entity = entityService.findById(Integer.parseInt(idHN));
				
				gastoxHojaNegocio = new GastoxHojaNegocio();
				gastoxHojaNegocio.setHojaNegocio(entity);
				gastoxHojaNegocio.setGasto(new Gasto());
				
				if(mode.equals("GTOTRVENNEW")){
					gastoxHojaNegocio.setAsignadoA(com.sh.pvs.core.utils.Constantes.CODVENDEDOR);
					gastoxHojaNegocio.setTipoGasto(com.sh.pvs.core.utils.Constantes.CODTIPOGASTOTRASPASO);
				}
				if(mode.equals("GTOTRCOMNEW")){
					gastoxHojaNegocio.setAsignadoA(com.sh.pvs.core.utils.Constantes.CODCOMPRADOR);
					gastoxHojaNegocio.setTipoGasto(com.sh.pvs.core.utils.Constantes.CODTIPOGASTOTRASPASO);
				}
				if(mode.equals("GTOADVENNEW")){
					gastoxHojaNegocio.setAsignadoA(com.sh.pvs.core.utils.Constantes.CODVENDEDOR);
					gastoxHojaNegocio.setTipoGasto(com.sh.pvs.core.utils.Constantes.CODTIPOGASTOADICIONAL);
				}
				if(mode.equals("GTOADCOMNEW")){
					gastoxHojaNegocio.setAsignadoA(com.sh.pvs.core.utils.Constantes.CODCOMPRADOR);
					gastoxHojaNegocio.setTipoGasto(com.sh.pvs.core.utils.Constantes.CODTIPOGASTOADICIONAL);
				}
				if(mode.equals("GTOADCIANEW")){
					gastoxHojaNegocio.setAsignadoA(com.sh.pvs.core.utils.Constantes.CODCIA);
					gastoxHojaNegocio.setTipoGasto(com.sh.pvs.core.utils.Constantes.CODTIPOGASTOADICIONAL);
				}
				
				fillCuentasDropDown();
				fillGastosDropDown();				
			}
			
			//Si es la consulta de un abono del vendedor o del comprador o de la cia
			if(mode.equals("ABONOVENVIEW") || mode.equals("ABONOCOMVIEW") || mode.equals("ABONOCIAVIEW")){
				
				IPagoxHojaNegocioServices pagoSrv = new PagoxHojaNegocioServices();
				String idpago = context.getExternalContext().getRequestParameterMap().get("idpago");
				pagoxHojaNegocio = pagoSrv.findById(Integer.parseInt(idpago));
				
				entityService = new HojaNegocioServices();
				entity = entityService.findById(pagoxHojaNegocio.getHojaNegocio().getId());
				
			}
			
			//Si es la creación de un abono del vendedor o del comprador o de la cia
			if(mode.equals("ABONOVENNEW") || mode.equals("ABONOCOMNEW") || mode.equals("ABONOCIANEW")){
				
				entityService = new HojaNegocioServices();
				String idHN = context.getExternalContext().getRequestParameterMap().get("idHN");
				entity = entityService.findById(Integer.parseInt(idHN));
				
				pagoxHojaNegocio = new PagoxHojaNegocio();
				if(pagoxHojaNegocio.getCuentaEntrada() == null)
					pagoxHojaNegocio.setCuentaEntrada(new Cuenta());
				if(pagoxHojaNegocio.getCuentaSalida() == null)
					pagoxHojaNegocio.setCuentaSalida(new Cuenta());
				
				pagoxHojaNegocio.setHojaNegocio(entity);
				pagoxHojaNegocio.setFchPago(new Date());
				
				if(mode.equals("ABONOVENNEW"))
					pagoxHojaNegocio.setPagadoPor(com.sh.pvs.core.utils.Constantes.CODVENDEDOR);
				if(mode.equals("ABONOCOMNEW"))
					pagoxHojaNegocio.setPagadoPor(com.sh.pvs.core.utils.Constantes.CODCOMPRADOR);
				if(mode.equals("ABONOCIANEW"))
					pagoxHojaNegocio.setPagadoPor(com.sh.pvs.core.utils.Constantes.CODCIA);
				
				fillCuentasDropDown();
				fillFormasPagoDropDown();
				
			}
			
			
		} catch (Exception e) {
			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
		}
		
	}
	
	/**
	 * Select an existing HojaNegocio entity
	 * 
	 */
	public String selectEntity(Integer idKey) throws Exception {			
		
		FacesContext context = FacesContext.getCurrentInstance(); 
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		response.sendRedirect(openWebMobile + "?id=" + idKey + "&mode=" + Constantes.MODEVIEW);
		
		return "";
	}
	
	/**
	 * Edit an existing HojaNegocio entity
	 * 
	 */
	public String editEntity(Integer idKey, boolean fromList) throws Exception {
		
		FacesContext context = FacesContext.getCurrentInstance(); 
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		response.sendRedirect(openWebMobile + "?id=" + idKey + "&mode=" + Constantes.MODEEDIT);
		
		return "";
		
	}
	
	/**
	 * Delete an existing HojaNegocio entity
	 * 
	 */
	public String deleteEntity(Integer idKey, boolean fromList) {
		
		FacesContext context = FacesContext.getCurrentInstance(); 
		
		try {
			entityService = new HojaNegocioServices();
			entity = entityService.findById(idKey);
			entityService.deleteTX(entity);
			
			Metodos.auditLog(Constantes.ACTIONDELETE, HojaNegocioMBean.class.getSimpleName(), entity.getVehiculo().getPlaca(), "");
			
			String msg = Metodos.getMessageResourceString("generics_eliminarOK", new Object[]{entity.getVehiculo().getPlaca()});
			if(!fromList || (!Metodos.isMobile() && Metodos.isDlg())){
				Metodos.setSessionMesagges(Constantes.SESSIONMSGINFO, msg);
			}
				
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
				
			fillEntityList();
		} catch (Exception e) {
			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
		}
		return  Metodos.close(closeRedirectWeb, closeRedirectMobile, fromList);
		
	}
	
	/**
	 * Metodo que retorna las listas
	 * @return
	 */
	public void fillEntityList(){
		FacesContext context = FacesContext.getCurrentInstance(); 
		try {
			entityService = new HojaNegocioServices();
			entityList = entityService.findAll(loggedUser.getCompania().getId());
			
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}		
	}
	
	/**
	 * Metodo para guardar
	 */
	public String saveEntity(){
		
		FacesContext context = FacesContext.getCurrentInstance(); 
		
		try{
			if(mode != null && mode.equals(Constantes.MODECREATE)
					&& hasPermissionTo(Constantes.ACTIONCREATE)){
				
				entityService = new HojaNegocioServices();
				entityService.createTX(entity);
				
				Metodos.auditLog(Constantes.ACTIONCREATE, HojaNegocioMBean.class.getSimpleName(), entity.getVehiculo().getPlaca(), "");
				
				String msg = Metodos.getMessageResourceString("generics_guardarOK", new Object[]{entity.getVehiculo().getPlaca()});
				
				if(!Metodos.isMobile()){
					Metodos.setSessionMesagges(Constantes.SESSIONMSGINFO, msg);
				}
					
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
					
				return  Metodos.close(closeRedirectWeb, closeRedirectMobile, true);
			}
			
			else if(hasPermissionTo(Constantes.ACTIONUPDATE)){
				
				if(entity.getOficinaTransito() != null && entity.getOficinaTransito().getId() == null){
					entity.setOficinaTransito(null);
				}
				
				entityService = new HojaNegocioServices();
				entityService.updateTX(entity);
				
				Metodos.auditLog(Constantes.ACTIONUPDATE, HojaNegocioMBean.class.getSimpleName(), entity.getVehiculo().getPlaca(), "");
				
				String msg = Metodos.getMessageResourceString("generics_actualizarOK", new Object[]{entity.getVehiculo().getPlaca()});
				
				if(!Metodos.isMobile()){
					Metodos.setSessionMesagges(Constantes.SESSIONMSGINFO, msg);
				}
					
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
					
				return  "";
			}
					
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}
		
		return "";
	}

	/**
	 * Metodo que se ejecuta cuando se cierra una modal
	 * @return
	 */
	public void dialogListener(){
		fillEntityList();
		Metodos.loadSessionMesagges();
	}
	
	/**
	 * Update an existing HojaNegocio entity
	 * 
	 */
	public String assignOficinaTransito() throws Exception {
		
		FacesContext context = FacesContext.getCurrentInstance(); 
		
		try{
			if(hasPermissionTo(Constantes.ACTIONUPDATE)){
				
				entityService = new HojaNegocioServices();
				
				entityService.assignOficinaTransito(entity);
				
				entity = entityService.findById(entity.getId());
				
				Metodos.auditLog(Constantes.ACTIONUPDATE, HojaNegocioMBean.class.getSimpleName(), entity.getId().toString(), "Oficina de tránsito asignada: " + entity.getOficinaTransito().getNombre());
				
				String msg = Metodos.getMessageResourceString("generics_actualizarOK", new Object[]{entity.getId().toString()});
				if(!Metodos.isMobile()){
					Metodos.setSessionMesagges(Constantes.SESSIONMSGINFO, msg);
				}
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
					
			}
			
			HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
			response.sendRedirect(openWebMobile + "?id=" + entity.getId() + "&mode=" + Constantes.MODEEDIT);
			
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}
		
		return "";
		
	}
	
	/**
	 * Update an existing HojaNegocio entity
	 * 
	 */
	public String updateOficinaTransito() throws Exception {
		
		FacesContext context = FacesContext.getCurrentInstance(); 
		
		try{
			if(hasPermissionTo(Constantes.ACTIONUPDATE)){
				
				entityService = new HojaNegocioServices();
				
				entityService.updateTX(entity);
				
				entity = entityService.findById(entity.getId());
				
				Metodos.auditLog(Constantes.ACTIONUPDATE, HojaNegocioMBean.class.getSimpleName(), entity.getId().toString(), "Oficina de tránsito actualizada: " + entity.getOficinaTransito().getNombre());
				
				String msg = Metodos.getMessageResourceString("generics_actualizarOK", new Object[]{entity.getId().toString()});
				if(!Metodos.isMobile()){
					Metodos.setSessionMesagges(Constantes.SESSIONMSGINFO, msg);
				}
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
					
			}
			
			HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
			response.sendRedirect(openWebMobile + "?id=" + entity.getId() + "&mode=" + Constantes.MODEEDIT);
			
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}
		
		return "";
		
	}
	
	/**
	 * Método que llena la lista de oficinas de tránsito activas para el selector
	 * @param 
	 */
	private void fillOficinasTransitoDropDown(){
		FacesContext context = FacesContext.getCurrentInstance(); 
		oficinasTransitoDropDownList = null;
		try {
			IOficinaTransitoServices srv = new OficinaTransitoServices();
			oficinasTransitoDropDownList = srv.findAllActivos(loggedUser.getCompania().getId());
		} catch (Exception e) {
			oficinasTransitoDropDownList = null;
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}		
	}
	
	/**
	 * Metodo para guardar una entidad de asociación
	 */
	public String saveGasto(){
		
		FacesContext context = FacesContext.getCurrentInstance(); 
		
		try{
			if(entity.getEstado() != null && entity.getEstado().equals("EP")
					&& hasPermissionTo(Constantes.ACTIONUPDATE)){
				
				entityService = new HojaNegocioServices();
				entityService.addGasto(gastoxHojaNegocio, loggedUser);
				entity = entityService.findById(entity.getId());
				
				Metodos.auditLog(Constantes.ACTIONUPDATE, HojaNegocioMBean.class.getSimpleName(), entity.getId().toString(), "Gasto asociado: " + gastoxHojaNegocio.getGasto().getNombre());
				gastoxHojaNegocio = new GastoxHojaNegocio();
				gastoxHojaNegocio.setGasto(new Gasto());
				
				String msg = Metodos.getMessageResourceString("generics_actualizarOK", new Object[]{entity.getId()});
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
				
				if(!Metodos.isMobile()){
					Metodos.setSessionMesagges(Constantes.SESSIONMSGINFO, msg);
				}
					
			}
					
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}
		
		return  Metodos.close(closeRedirectWeb, closeRedirectMobile, false);
	}
	
	/**
	 * Metodo para eliminar una entidad de asociación
	 */
	public String deleteGasto(GastoxHojaNegocio gastoToDelete){
		
		FacesContext context = FacesContext.getCurrentInstance(); 
		
		try{
			if(entity.getEstado() != null && entity.getEstado().equals("EP")
					&& hasPermissionTo(Constantes.ACTIONUPDATE)){
				
				entityService = new HojaNegocioServices();
				entityService.deleteGasto(gastoToDelete, loggedUser);
				entity = entityService.findById(entity.getId());
				
				Metodos.auditLog(Constantes.ACTIONUPDATE, HojaNegocioMBean.class.getSimpleName(), entity.getId().toString(), "Gasto eliminado: " + gastoToDelete.getDescripcion());
				
				String msg = Metodos.getMessageResourceString("generics_actualizarOK", new Object[]{entity.getId()});
				
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
				
				if(!Metodos.isMobile() && Metodos.isDlg()){
					Metodos.setSessionMesagges(Constantes.SESSIONMSGINFO, msg);
				}
					
			}
					
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}
		
		if(Metodos.isDlg()){
		    RequestContext.getCurrentInstance().closeDialog(null);
	    }	
		return "";
	}
	
	public void onGastoEdit(GastoxHojaNegocio gasto) {
		FacesContext context = FacesContext.getCurrentInstance(); 
		
		try{
			if(entity.getEstado() != null && entity.getEstado().equals("EP")
					&& hasPermissionTo(Constantes.ACTIONUPDATE)){
				
				entityService = new HojaNegocioServices();
				entityService.updateGasto(gasto, loggedUser);
				entity = entityService.findById(entity.getId());
				
				Metodos.auditLog(Constantes.ACTIONUPDATE, HojaNegocioMBean.class.getSimpleName(), entity.getId().toString(), "Gasto actualizado: " + gasto.getDescripcion());
				
				String msg = Metodos.getMessageResourceString("generics_actualizarOK", new Object[]{entity.getId()});
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
					
			}
					
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}
		
    }
	
	/**
	 * Método que llena la lista de gastos activos para el selector
	 * @param 
	 */
	private void fillGastosDropDown(){
		FacesContext context = FacesContext.getCurrentInstance(); 
		gastosDropDownList = null;
		try {
			IGastoServices gastoSrv = new GastoServices();
			gastosDropDownList = gastoSrv.findAllActivosByCia(loggedUser.getCompania().getId());
		} catch (Exception e) {
			gastosDropDownList = null;
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}
	}
	
	/**
	 * Metodo que se ejecuta cuando se selecciona un gasto
	 * @return
	 */
	public void changeGastoListener(){
		try {
			IGastoServices gastoSrv = new GastoServices();
			Gasto gasto = gastoSrv.findById(gastoxHojaNegocio.getGasto().getId());
			if (gasto != null) {
				gastoxHojaNegocio.setDescripcion(gasto.getDescripcion());
				gastoxHojaNegocio.setValor(gasto.getValor());
			}
		} catch (Exception e) {
		}
	}
	
	/**
	 * Método que inicializa las variables y abre el formulario para agregar o consultar un gasto
	 * 
	 */
	public String openGastoForm(String section_id, Integer gasto_id) throws Exception{
		
		//Definición de los modos de las vistas
		String modeGasto = "";
		
		//Si se dio clic en el botón agregar gasto de la sección de gastos traspaso del vendedor
		if(section_id.equals("ADDGTOTRVEN")){
			
			//Si no hay un id del gasto, se supone que es la creación de uno nuevo
			if(gasto_id == null || gasto_id.equals(new Integer(0))){
				modeGasto = "GTOTRVENNEW"; //Nuevo gasto traspaso
			}else{
				modeGasto = "GTOTRVENVIEW"; //Consulta gasto traspaso
			}
		}
		
		//Si se dio clic en el botón agregar gasto de la sección de gastos adicionales del vendedor
		if(section_id.equals("ADDGTOADVEN")){
			
			//Si no hay un id del gasto, se supone que es la creación de uno nuevo
			if(gasto_id == null || gasto_id.equals(new Integer(0))){
				modeGasto = "GTOADVENNEW"; //Nuevo gasto adicional
			}else{
				modeGasto = "GTOADVENVIEW"; //Consulta gasto adicional
			}
		}
		
		//Si se dio clic en el botón agregar gasto de la sección de gastos traspaso del comprador
		if(section_id.equals("ADDGTOTRCOM")){
			
			//Si no hay un id del gasto, se supone que es la creación de uno nuevo
			if(gasto_id == null || gasto_id.equals(new Integer(0))){
				modeGasto = "GTOTRCOMNEW"; //Nuevo gasto traspaso
			}else{
				modeGasto = "GTOTRCOMVIEW"; //Consulta gasto traspaso
			}
		}
		
		//Si se dio clic en el botón agregar gasto de la sección de gastos adicionales del vendedor
		if(section_id.equals("ADDGTOADCOM")){
			
			//Si no hay un id del gasto, se supone que es la creación de uno nuevo
			if(gasto_id == null || gasto_id.equals(new Integer(0))){
				modeGasto = "GTOADCOMNEW"; //Nuevo gasto adicional
			}else{
				modeGasto = "GTOADCOMVIEW"; //Consulta gasto adicional
			}
		}
		
		//Si se dio clic en el botón agregar gasto de la sección de gastos adicionales de la compañía
		if(section_id.equals("ADDGTOADCIA")){
			
			//Si no hay un id del gasto, se supone que es la creación de uno nuevo
			if(gasto_id == null || gasto_id.equals(new Integer(0))){
				modeGasto = "GTOADCIANEW"; //Nuevo gasto adicional
			}else{
				modeGasto = "GTOADCIAVIEW"; //Consulta gasto adicional
			}
		}
		
		FacesContext context = FacesContext.getCurrentInstance(); 
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		
		if(!Metodos.isMobile()){
			
			Map<String,Object> options = new HashMap<String,Object>();
	        options.put("modal", true);
	        options.put("resizable", false);
	        options.put("contentHeight", 450);
	        options.put("contentWidth", 600);
	        
	        Map<String, List<String>> params = new HashMap<String, List<String>>();
		       
	        List<String> values = new ArrayList<String>();
	        values.add(modeGasto);
	        params.put("mode", values);
	        
	        List<String> valuesidgasto = new ArrayList<String>();	        
	        valuesidgasto.add(gasto_id.toString());
	        params.put("idgasto", valuesidgasto);
	        
	        List<String> valuesidHN = new ArrayList<String>();	        
	        valuesidHN.add(entity.getId().toString());
	        params.put("idHN", valuesidHN);
			
			RequestContext.getCurrentInstance().openDialog(openDialogGasto, options, params);
			
		}else{
			//Si la navegación es en dispoisitivos móviles, se redirecciona
			try {
				response.sendRedirect(openWebMobileGasto + "?idgasto=" + gasto_id + "&mode=" + modeGasto + "&idHN=" + entity.getId().toString());
			} catch (Exception e) {
				return "";
			}
			
		}
		
		return "";
		
	}
	
	/**
	 * Método que inicializa las variables y abre el formulario para agregar o consultar un abono
	 * 
	 */
	public String openAbonoForm(String section_id, Integer pago_id) throws Exception{
		
		//Definición de los modos de las vistas
		String modeAbono = "";
		
		//Si se dio clic en el botón agregar abono de la sección del vendedor
		if(section_id.equals("ADDABONOVEN")){
			
			//Si no hay un id del pago, se supone que es la creación de uno nuevo
			if(pago_id == null || pago_id.equals(new Integer(0))){
				modeAbono = "ABONOVENNEW"; //Nuevo abono
			}else{
				modeAbono = "ABONOVENVIEW"; //Consulta abono
			}
		}
		
		//Si se dio clic en el botón agregar abono de la sección del comprador
		if(section_id.equals("ADDABONOCOM")){
			
			//Si no hay un id del pago, se supone que es la creación de uno nuevo
			if(pago_id == null || pago_id.equals(new Integer(0))){
				modeAbono = "ABONOCOMNEW"; //Nuevo abono
			}else{
				modeAbono = "ABONOCOMVIEW"; //Consulta abono
			}
		}
		
		//Si se dio clic en el botón agregar abono de la sección de la compañía
		if(section_id.equals("ADDABONOCIA")){
			
			//Si no hay un id del pago, se supone que es la creación de uno nuevo
			if(pago_id == null || pago_id.equals(new Integer(0))){
				modeAbono = "ABONOCIANEW"; //Nuevo abono adicional
			}else{
				modeAbono = "ABONOCIAVIEW"; //Consulta abono adicional
			}
		}
		
		FacesContext context = FacesContext.getCurrentInstance(); 
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		
		if(!Metodos.isMobile()){
			
			Map<String,Object> options = new HashMap<String,Object>();
	        options.put("modal", true);
	        options.put("resizable", false);
	        options.put("contentHeight", 450);
	        options.put("contentWidth", 600);
	        
	        Map<String, List<String>> params = new HashMap<String, List<String>>();
		       
	        List<String> values = new ArrayList<String>();
	        values.add(modeAbono);
	        params.put("mode", values);
	        
	        List<String> valuesidpago = new ArrayList<String>();	        
	        valuesidpago.add(pago_id.toString());
	        params.put("idpago", valuesidpago);
	        
	        List<String> valuesidHN = new ArrayList<String>();	        
	        valuesidHN.add(entity.getId().toString());
	        params.put("idHN", valuesidHN);
			
			RequestContext.getCurrentInstance().openDialog(openDialogPago, options, params);
			
		}else{
			//Si la navegación es en dispoisitivos móviles, se redirecciona
			try {
				response.sendRedirect(openWebMobilePago + "?idpago=" + pago_id + "&mode=" + modeAbono + "&idHN=" + entity.getId().toString());
			} catch (Exception e) {
				return "";
			}
			
		}
		
		return "";
		
	}
	
	/**
	 * Metodo que se ejecuta cuando se cierra una modal
	 * @return
	 */
	public void gastoDialogListener() throws Exception{
		entityService = new HojaNegocioServices();
		entity = entityService.findById(entity.getId());
		Metodos.loadSessionMesagges();
	}
	
	/**
	 * Metodo que se ejecuta cuando se cierra una modal
	 * @return
	 */
	public void abonoDialogListener() throws Exception{
		entityService = new HojaNegocioServices();
		entity = entityService.findById(entity.getId());
		Metodos.loadSessionMesagges();
	}
	
	/**
	 * Metodo para guardar una entidad de asociación
	 */
	public String savePago(){
		
		FacesContext context = FacesContext.getCurrentInstance(); 
		
		try{
			if(entity.getEstado() != null && entity.getEstado().equals("EP")
					&& hasPermissionTo(Constantes.ACTIONUPDATE)){
				
				entityService = new HojaNegocioServices();
				entityService.addPago(pagoxHojaNegocio, loggedUser);
				entity = entityService.findById(entity.getId());
				
				Metodos.auditLog(Constantes.ACTIONUPDATE, HojaNegocioMBean.class.getSimpleName(), entity.getId().toString(), "Pago asociado: " + pagoxHojaNegocio.getConcepto());
				pagoxHojaNegocio = new PagoxHojaNegocio();
				
				String msg = Metodos.getMessageResourceString("generics_actualizarOK", new Object[]{entity.getId()});
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
				
				if(!Metodos.isMobile()){
					Metodos.setSessionMesagges(Constantes.SESSIONMSGINFO, msg);
				}
					
			}
					
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}
		
		return  Metodos.close(closeRedirectWeb, closeRedirectMobile, false);
	}
	
	/**
	 * Metodo para eliminar una entidad de asociación
	 */
	public String deletePago(PagoxHojaNegocio pagoToDelete){
		
		FacesContext context = FacesContext.getCurrentInstance(); 
		
		try{
			if(entity.getEstado() != null && entity.getEstado().equals("EP")
					&& hasPermissionTo(Constantes.ACTIONUPDATE)){
				
				entityService = new HojaNegocioServices();
				entityService.deletePago(pagoToDelete, loggedUser);
				entity = entityService.findById(entity.getId());
				
				Metodos.auditLog(Constantes.ACTIONUPDATE, HojaNegocioMBean.class.getSimpleName(), entity.getId().toString(), "Pago eliminado: " + pagoToDelete.getConcepto());
				
				String msg = Metodos.getMessageResourceString("generics_actualizarOK", new Object[]{entity.getId()});
				
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
				
				if(!Metodos.isMobile() && Metodos.isDlg()){
					Metodos.setSessionMesagges(Constantes.SESSIONMSGINFO, msg);
				}
					
			}
					
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}
		
		if(Metodos.isDlg()){
		    RequestContext.getCurrentInstance().closeDialog(null);
	    }	
		return "";
	}
	
	/**
	 * Método que llena la lista de cuentas activas para el selector
	 * @param 
	 */
	private void fillCuentasDropDown(){
		FacesContext context = FacesContext.getCurrentInstance(); 
		cuentasDropDownList = null;
		try {
			ICuentaServices cuentaSrv = new CuentaServices();
			cuentasDropDownList = cuentaSrv.findAllActivos(loggedUser.getCompania().getId());
		} catch (Exception e) {
			cuentasDropDownList = null;
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}		
	}
	
	/**
	 * Método que llena la lista de formas de pago activas para el selector
	 * @param 
	 */
	private void fillFormasPagoDropDown(){
		FacesContext context = FacesContext.getCurrentInstance(); 
		formasPagoDropDownList = null;
		try {
			IFormaPagoServices formaPagoSrv = new FormaPagoServices();
			formasPagoDropDownList = formaPagoSrv.findAllActivos(loggedUser.getCompania().getId());
		} catch (Exception e) {
			formasPagoDropDownList = null;
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}		
	}
	
	public void onAbonoEdit(PagoxHojaNegocio pago) {
		FacesContext context = FacesContext.getCurrentInstance(); 
		
		try{
			if(entity.getEstado() != null && entity.getEstado().equals("EP")
					&& hasPermissionTo(Constantes.ACTIONUPDATE)){
				
				entityService = new HojaNegocioServices();
				
				entityService.updatePago(pago, loggedUser);
				entity = entityService.findById(entity.getId());
				
				Metodos.auditLog(Constantes.ACTIONUPDATE, HojaNegocioMBean.class.getSimpleName(), entity.getId().toString(), "Pago actualizado: " + pago.getConcepto());
				
				String msg = Metodos.getMessageResourceString("generics_actualizarOK", new Object[]{entity.getId()});
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
					
			}
					
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}
		
    }
	
	public ArrayList<Double[]> getDatosValorVehiculoVendedor() {
		ArrayList<Double[]> datosValorVehiculo = new ArrayList<Double[]>();
		if(entity != null){
			datosValorVehiculo.add(new Double[]{entity.getVlrVehVendedor(), entity.getTotalAbonosxValorVehiculoAlVendedor(),entity.getSaldoVehiculo(com.sh.pvs.core.utils.Constantes.CODVENDEDOR)});
		}
			
		return datosValorVehiculo;
	}
	
	public void onVlrVehVendedorEdit() {
		FacesContext context = FacesContext.getCurrentInstance(); 
		
		try{
			if(entity.getEstado() != null && entity.getEstado().equals("EP")
					&& hasPermissionTo(Constantes.ACTIONUPDATE)){
				
				entityService = new HojaNegocioServices();
				entityService.updateTX(entity);
				entity = entityService.findById(entity.getId());
				
				Metodos.auditLog(Constantes.ACTIONUPDATE, HojaNegocioMBean.class.getSimpleName(), entity.getId().toString(), "Valor del vehículo actualizado: " + entity.getVlrVehVendedor());
				
				String msg = Metodos.getMessageResourceString("generics_actualizarOK", new Object[]{entity.getId()});
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
					
			}
					
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}
		
    }
	
	public ArrayList<Double[]> getDatosValorVehiculoComprador() {
		ArrayList<Double[]> datosValorVehiculo = new ArrayList<Double[]>();
		if(entity != null){
			datosValorVehiculo.add(new Double[]{entity.getVlrVehComprador(), entity.getTotalAbonosxValorVehiculoAlComprador(),entity.getSaldoVehiculo(com.sh.pvs.core.utils.Constantes.CODCOMPRADOR)});
		}
			
		return datosValorVehiculo;
	}
	
	public void onVlrVehCompradorEdit() {
		FacesContext context = FacesContext.getCurrentInstance(); 
		
		try{
			if(entity.getEstado() != null && entity.getEstado().equals("EP")
					&& hasPermissionTo(Constantes.ACTIONUPDATE)){
				
				entityService = new HojaNegocioServices();
				entityService.updateTX(entity);
				entity = entityService.findById(entity.getId());
				
				Metodos.auditLog(Constantes.ACTIONUPDATE, HojaNegocioMBean.class.getSimpleName(), entity.getId().toString(), "Valor de venta del vehículo actualizado: " + entity.getVlrVehVendedor());
				
				String msg = Metodos.getMessageResourceString("generics_actualizarOK", new Object[]{entity.getId()});
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
					
			}
					
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}
		
    }
	
	/**
	 * Open Plantillas Form
	 * 
	 */
	public String openGenerarPlantillaForm() {
		
		return Metodos.openGenerarPlantillaForm(com.sh.pvs.core.utils.Constantes.CODHOJANEGOCIO, entity.getId().toString());
		
	}
	 
	/**
	 * Metodo para finalizar una hoja de negocio
	 */
	public String finalizarHojaNegocio(){
		
		FacesContext context = FacesContext.getCurrentInstance(); 
		
		try{
			
			if(hasPermissionTo(Constantes.ACTIONFINALIZAR)){
				
				entityService = new HojaNegocioServices();
				entity = entityService.finalizarHojaNegocioTX(entity);
				
				Metodos.auditLog(Constantes.ACTIONFINALIZAR, HojaNegocioMBean.class.getSimpleName(), entity.getId().toString(), "");
				
				String msg = Metodos.getMessageResourceString("hojaNegocio_finalizar_result", null);
				
				if(!Metodos.isMobile()){
					Metodos.setSessionMesagges(Constantes.SESSIONMSGINFO, msg);
				}
					
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
					
				return  Metodos.close(closeRedirectWeb, closeRedirectMobile, true);
			}
			
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}
		
		return "";
	}
	
	/* SECURITY METHODS*/
	/**
	 * Método que valida si el usuario en sesión puede ejecutar una acción sobre este documento
	 * 
	 * @return
	 */
	public boolean hasPermissionTo(String action){		
		return Metodos.hasPermission(HojaNegocioMBean.class.getSimpleName(), action);
	}
	
	/* GETTERS AND SETTERS */

	public HojaNegocio getEntity() {
		return entity;
	}

	public void setEntity(HojaNegocio entity) {
		this.entity = entity;
	}

	public List<HojaNegocio> getEntityList() {
		return entityList;
	}

	public void setEntityList(List<HojaNegocio> entityList) {
		this.entityList = entityList;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public List<HojaNegocio> getEntityFilterList() {
		return entityFilterList;
	}

	public void setEntityFilterList(List<HojaNegocio> entityFilterList) {
		this.entityFilterList = entityFilterList;
	}

	public List<OficinaTransito> getOficinasTransitoDropDownList() {
		return oficinasTransitoDropDownList;
	}

	public void setOficinasTransitoDropDownList(
			List<OficinaTransito> oficinasTransitoDropDownList) {
		this.oficinasTransitoDropDownList = oficinasTransitoDropDownList;
	}

	public GastoxHojaNegocio getGastoxHojaNegocio() {
		return gastoxHojaNegocio;
	}
	
	public void setGastoxHojaNegocio(GastoxHojaNegocio gastoxHojaNegocio) {
		this.gastoxHojaNegocio = gastoxHojaNegocio;
	}
	
	public PagoxHojaNegocio getPagoxHojaNegocio() {
		return pagoxHojaNegocio;
	}
	
	public void setPagoxHojaNegocio(PagoxHojaNegocio pagoxHojaNegocio) {
		this.pagoxHojaNegocio = pagoxHojaNegocio;
	}
	
	public List<Gasto> getGastosDropDownList() {
		return gastosDropDownList;
	}

	public void setGastosDropDownList(List<Gasto> gastosDropDownList) {
		this.gastosDropDownList = gastosDropDownList;
	}
	
	public List<Cuenta> getCuentasDropDownList() {
		return cuentasDropDownList;
	}
	
	public void setCuentasDropDownList(List<Cuenta> cuentasDropDownList) {
		this.cuentasDropDownList = cuentasDropDownList;
	}
	
	public List<FormaPago> getFormasPagoDropDownList() {
		return formasPagoDropDownList;
	}

	public void setFormasPagoDropDownList(List<FormaPago> formasPagoDropDownList) {
		this.formasPagoDropDownList = formasPagoDropDownList;
	}
	
}
