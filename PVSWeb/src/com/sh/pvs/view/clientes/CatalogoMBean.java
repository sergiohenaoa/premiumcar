/**
	 * 	CatalogoMBean.java:
	 * 
	 * 	Esta clase corresponde al Managed Bean para controlar el CRUD de 
	 * 	la entidad Catalogo
	 *
	 * @author Sergio Henao - sergiohenaoa@gmail.com - 11/04/2015
	 * @Modifier Sergio Henao - sergiohenaoa@gmail.com - 11/04/2015
	 * @version 1.0
	 * 
	 */
package com.sh.pvs.view.clientes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.sh.pvs.core.interfaces.clientes.ICatalogoServices;
import com.sh.pvs.core.interfaces.configuracion.ICarroceriaServices;
import com.sh.pvs.core.interfaces.configuracion.IColorServices;
import com.sh.pvs.core.interfaces.configuracion.IMarcaServices;
import com.sh.pvs.core.interfaces.configuracion.ITipoVehiculoServices;
import com.sh.pvs.core.interfaces.configuracion.ITransmisionServices;
import com.sh.pvs.core.services.clientes.CatalogoServices;
import com.sh.pvs.core.services.configuracion.CarroceriaServices;
import com.sh.pvs.core.services.configuracion.ColorServices;
import com.sh.pvs.core.services.configuracion.MarcaServices;
import com.sh.pvs.core.services.configuracion.TipoVehiculoServices;
import com.sh.pvs.core.services.configuracion.TransmisionServices;
import com.sh.pvs.model.dao.configuracion.CarroceriaDAO;
import com.sh.pvs.model.dao.configuracion.MarcaDAO;
import com.sh.pvs.model.dto.configuracion.Carroceria;
import com.sh.pvs.model.dto.configuracion.Color;
import com.sh.pvs.model.dto.configuracion.Marca;
import com.sh.pvs.model.dto.configuracion.TipoVehiculo;
import com.sh.pvs.model.dto.configuracion.Transmision;
import com.sh.pvs.model.dto.configuracion.Vehiculo;
import com.sh.pvs.model.dto.seguridad.Usuario;
import com.sh.pvs.view.utils.ControlException;
import com.sh.pvs.view.utils.Metodos;

@ManagedBean
@ViewScoped
public class CatalogoMBean implements Serializable {

	/*Serializable Attribute*/
	private static final long serialVersionUID = 1L;
	
	/*Variables*/
	private List<Vehiculo> vehiculos;
	private TipoVehiculo tipovehiculo = new TipoVehiculo();
	private List<TipoVehiculo> tiposVehiculoDropDownList;
 	private List<Marca> marcasDropDownList;
 	private List<Carroceria> carroceriasDropDownList;
 	private List<Color> coloresDropDownList;
 	private List<String> modelosDropDownList;
 	private List<Transmision> transmisionesDropDownList;
 	private Usuario loggedUser;	
 	
 	/*Navegación*/
 	
 	/*Servicios*/
	
 	/*Variables para navegación*/
 	
 	/*Constructors*/	
	/**	Default constructor */
	public CatalogoMBean() {

	}
	
	/*METHODS*/
	
	/**
	 * Método para inicializar las variables
	 * 
	 * */
	@PostConstruct
	private void init(){
		
		FacesContext context = FacesContext.getCurrentInstance();
		
		try {
			
			Metodos.loadSessionMesagges();
			
			loggedUser = Metodos.getUserSession();
			
			fillVehiculoActivosList();
			
			fillTiposVehiculoDropDown();
			fillMarcasDropDown();
			fillCarroceriasDropDown();
			fillColoresDropDown();
			fillModelosDropDown();
			fillTransmisionesDropDown();
			
		} catch (Exception e) {
			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
		}
		
	}
	
	/**
	 * Método que llena la lista de vehículos activos para el selector
	 * @param 
	 */
	public void fillVehiculoActivosList(){
		FacesContext context = FacesContext.getCurrentInstance(); 
		vehiculos = null;
		try {
			ICatalogoServices servicio = new CatalogoServices();
			vehiculos = servicio.findAllVehiculosToCatalogo(Metodos.getUserSession().getCompania().getId());
		} catch (Exception e) {
			vehiculos = null;
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}		
	}
	
	/**
	 * Método que llena la lista de tipos de vehículo activos para el selector
	 * @param 
	 */
	public void fillTiposVehiculoDropDown(){
		FacesContext context = FacesContext.getCurrentInstance(); 
		tiposVehiculoDropDownList = null;
		try {
			ITipoVehiculoServices tipoVSrv = new TipoVehiculoServices();
			tiposVehiculoDropDownList = tipoVSrv.findAllActivos(loggedUser.getCompania().getId());
		} catch (Exception e) {
			tiposVehiculoDropDownList = null;
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}		
	}
	
	/**
	 * Método que llena la lista de marcas activas para el selector
	 * @param 
	 */
	public void fillMarcasDropDown(){
		FacesContext context = FacesContext.getCurrentInstance(); 
		marcasDropDownList = null;
		try {
			
			if(tipovehiculo != null
					&& tipovehiculo.getId() != null){
				IMarcaServices marcaSrv = new MarcaServices();
				marcasDropDownList = marcaSrv.findByProperty(MarcaDAO.IDTIPOVEHICULO, tipovehiculo.getId());
			}
			
		} catch (Exception e) {
			marcasDropDownList = null;
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}		
	}
	
	/**
	 * Método que llena la lista de carrocerías activas para el selector
	 * @param 
	 */
	public void fillCarroceriasDropDown(){
		FacesContext context = FacesContext.getCurrentInstance(); 
		carroceriasDropDownList  = null;
		try {
			
			if(tipovehiculo != null
					&& tipovehiculo.getId() != null){
				ICarroceriaServices carroceriaSrv = new CarroceriaServices();
				carroceriasDropDownList = carroceriaSrv.findByProperty(CarroceriaDAO.IDTIPOVEHICULO, tipovehiculo.getId());
			}
			
		} catch (Exception e) {
			carroceriasDropDownList = null;
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}		
	}
	
	/**
	 * Método que llena la lista de modelos para el selector
	 * @param 
	 */
	public void fillModelosDropDown(){
		FacesContext context = FacesContext.getCurrentInstance(); 
		modelosDropDownList = null;
		try {
			modelosDropDownList = new ArrayList<String>();
			Calendar calendar = Calendar.getInstance();
			int actualYear = calendar.get(Calendar.YEAR);
			//Poner límites de los años en constantes
			for(int i = actualYear - 50; i < actualYear + 2; i++){
				modelosDropDownList.add(Integer.toString(i));
			}
		} catch (Exception e) {
			modelosDropDownList = null;
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}		
	}
	
	/**
	 * Método que llena la lista de transmisiones activas para el selector
	 * @param 
	 */
	public void fillTransmisionesDropDown(){
		FacesContext context = FacesContext.getCurrentInstance(); 
		transmisionesDropDownList = null;
		try {
			ITransmisionServices srv = new TransmisionServices();
			transmisionesDropDownList = srv.findAllActivos(loggedUser.getCompania().getId());
		} catch (Exception e) {
			transmisionesDropDownList = null;
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}		
	}
	
	/**
	 * Método que llena la lista de colores activos para el selector
	 * @param 
	 */
	public void fillColoresDropDown(){
		FacesContext context = FacesContext.getCurrentInstance(); 
		coloresDropDownList = null;
		try {
			IColorServices srv = new ColorServices();
			coloresDropDownList = srv.findAllActivos(loggedUser.getCompania().getId());
		} catch (Exception e) {
			coloresDropDownList = null;
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}		
	}
	
	/**
	 * Método que llena recalcula las listas dependientes del tipo de vehículo
	 * @param 
	 */
	public void fillDropDownsDependentTipoVehiculo(){
		fillMarcasDropDown();
		fillCarroceriasDropDown();
	}
	
	/* SECURITY METHODS*/
	/**
	 * Método que valida si el usuario en sesión puede ejecutar una acción sobre este documento
	 * 
	 * @return
	 */
	public boolean hasPermissionTo(String action){		
		return Metodos.hasPermission(CatalogoMBean.class.getSimpleName(), action);
	}

	/* GETTERS AND SETTERS */
	public List<Vehiculo> getVehiculos() {
		return vehiculos;
	}

	public void setVehiculos(List<Vehiculo> vehiculos) {
		this.vehiculos = vehiculos;
	}

	public TipoVehiculo getTipovehiculo() {
		return tipovehiculo;
	}

	public void setTipovehiculo(TipoVehiculo tipovehiculo) {
		this.tipovehiculo = tipovehiculo;
	}

	public List<TipoVehiculo> getTiposVehiculoDropDownList() {
		return tiposVehiculoDropDownList;
	}

	public void setTiposVehiculoDropDownList(
			List<TipoVehiculo> tiposVehiculoDropDownList) {
		this.tiposVehiculoDropDownList = tiposVehiculoDropDownList;
	}

	public List<Marca> getMarcasDropDownList() {
		return marcasDropDownList;
	}

	public void setMarcasDropDownList(List<Marca> marcasDropDownList) {
		this.marcasDropDownList = marcasDropDownList;
	}

	public List<Carroceria> getCarroceriasDropDownList() {
		return carroceriasDropDownList;
	}

	public void setCarroceriasDropDownList(List<Carroceria> carroceriasDropDownList) {
		this.carroceriasDropDownList = carroceriasDropDownList;
	}

	public List<Color> getColoresDropDownList() {
		return coloresDropDownList;
	}

	public void setColoresDropDownList(List<Color> coloresDropDownList) {
		this.coloresDropDownList = coloresDropDownList;
	}

	public List<String> getModelosDropDownList() {
		return modelosDropDownList;
	}

	public void setModelosDropDownList(List<String> modelosDropDownList) {
		this.modelosDropDownList = modelosDropDownList;
	}

	public List<Transmision> getTransmisionesDropDownList() {
		return transmisionesDropDownList;
	}

	public void setTransmisionesDropDownList(
			List<Transmision> transmisionesDropDownList) {
		this.transmisionesDropDownList = transmisionesDropDownList;
	}

	
	
	
}
