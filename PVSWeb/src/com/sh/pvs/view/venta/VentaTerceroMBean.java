/**
	 * 	VentaTerceroMBean.java:
	 * 
	 * 	Esta clase corresponde al Managed Bean para controlar el CRUD de 
	 * 	la entidad VentaTercero
	 *
	 * @author Sergio Henao - sergiohenaoa@gmail.com - 11/04/2015
	 * @Modifier Sergio Henao - sergiohenaoa@gmail.com - 11/04/2015
	 * @version 1.0
	 * 
	 */
package com.sh.pvs.view.venta;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.primefaces.context.RequestContext;

import com.sh.pvs.core.interfaces.configuracion.IClienteServices;
import com.sh.pvs.core.interfaces.configuracion.ICuentaServices;
import com.sh.pvs.core.interfaces.configuracion.IFormaPagoServices;
import com.sh.pvs.core.interfaces.configuracion.IVehiculoServices;
import com.sh.pvs.core.interfaces.venta.IVentaTerceroServices;
import com.sh.pvs.core.services.configuracion.ClienteServices;
import com.sh.pvs.core.services.configuracion.CuentaServices;
import com.sh.pvs.core.services.configuracion.FormaPagoServices;
import com.sh.pvs.core.services.configuracion.VehiculoServices;
import com.sh.pvs.core.services.venta.VentaTerceroServices;
import com.sh.pvs.model.dao.venta.VentaTerceroDAO;
import com.sh.pvs.model.dto.configuracion.Cliente;
import com.sh.pvs.model.dto.configuracion.Cuenta;
import com.sh.pvs.model.dto.configuracion.FormaPago;
import com.sh.pvs.model.dto.configuracion.Vehiculo;
import com.sh.pvs.model.dto.seguridad.Usuario;
import com.sh.pvs.model.dto.venta.VentaTercero;
import com.sh.pvs.model.utils.Condition;
import com.sh.pvs.model.utils.ControlQuery;
import com.sh.pvs.view.utils.Constantes;
import com.sh.pvs.view.utils.ControlException;
import com.sh.pvs.view.utils.Metodos;

@ManagedBean
@ViewScoped
public class VentaTerceroMBean implements Serializable {

	/*Serializable Attribute*/
	private static final long serialVersionUID = 1L;
	
	/*Variables*/
	private VentaTercero entity; 							// ViewForm - EditForm 	
 	private List<VentaTercero> entityList; 					// Lista de registros del datatable
 	private List<VentaTercero> entityFilterList;			// Lista de registros filtrados del datatable
 	private List<Cliente> clientesDropDownList;
 	private List<Vehiculo> vehiculosDropDownList;
 	private List<FormaPago> formasPagoDropDownList;
 	private List<Cuenta> cuentasDropDownList;
 	private Usuario loggedUser;								//Usuario logeado (En Sesión)
 	
 	/*Navegación*/
 	private String mode;									// view - edit - list - create ==> Modo para identificar la vista
 	
 	/*Servicios*/
 	private IVentaTerceroServices entityService;			// Servicios para la gestión de la entidad
	
 	/*Variables para navegación*/
 	public static String openDialog = "/venta/ventaTerceroForm.faces";
 	public static String openWebMobile = "ventaTerceroForm.faces";
 	public static String closeRedirectMobile = "ventaTerceroList.faces";
	public static String closeRedirectWeb = "/venta/ventaTerceroList.faces?faces-redirect=true";
 	
 	/*Constructors*/	
	/**	Default constructor */
	public VentaTerceroMBean() {

	}
	
	/*METHODS*/
	
	/**
	 * Método para inicializar las variables
	 * 
	 * */
	@PostConstruct
	private void init(){
		
		FacesContext context = FacesContext.getCurrentInstance();
		
		try {
			
			Metodos.loadSessionMesagges();
			
			loggedUser = Metodos.getUserSession();
			
			if(context.getExternalContext().getRequestParameterMap().get("mode") != null)
				mode = context.getExternalContext().getRequestParameterMap().get("mode");
			else
				mode = Constantes.MODELIST;
			
			if(mode.equals(Constantes.MODECREATE)){
				this.entity = new VentaTercero();
				entity.setCliente(new Cliente());
				entity.setVehiculo(new Vehiculo());
				fillClientesDropDown();
				fillVehiculosDropDown();
				fillFormasPagoDropDown();
				fillCuentasDropDown();
				return;
			}
			
			if(mode.equals(Constantes.MODELIST)){
				fillEntityList();
				return;
			}
						
			if(mode.equals(Constantes.MODEVIEW) || mode.equals(Constantes.MODEEDIT)){
				
				try {
					
					String idParam = context.getExternalContext().getRequestParameterMap().get("id");
					entityService = new VentaTerceroServices();
					this.entity = entityService.findById(Integer.parseInt(idParam));
					
					if(mode.equals(Constantes.MODEEDIT)){
						fillClientesDropDown();
						fillVehiculosDropDown();
						fillFormasPagoDropDown();
						fillCuentasDropDown();
					}
							
				} catch (Exception e) {
					context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
				}
				
				return;
			}
			
		} catch (Exception e) {
			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
		}
		
	}
	
	/**
	 * Create a new VentaTercero entity
	 * 
	 */
	public String newEntity() throws Exception{
		
		FacesContext context = FacesContext.getCurrentInstance(); 
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		response.sendRedirect(openWebMobile + "?mode=" + Constantes.MODECREATE);
		
		return "";
		
	}
	
	/**
	 * Select an existing VentaTercero entity
	 * 
	 */
	public String selectEntity(Integer idKey) throws Exception {				
		
		FacesContext context = FacesContext.getCurrentInstance(); 
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		response.sendRedirect(openWebMobile + "?id=" + idKey + "&mode=" + Constantes.MODEVIEW);
		
		return "";
		
	}
	
	/**
	 * Edit an existing VentaTercero entity
	 * 
	 */
	public String editEntity(Integer idKey, boolean fromList) throws Exception {
		
		FacesContext context = FacesContext.getCurrentInstance(); 
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		response.sendRedirect(openWebMobile + "?id=" + idKey + "&mode=" + Constantes.MODEEDIT);
		
		return "";
		
	}
	
	/**
	 * Delete an existing VentaTercero entity
	 * 
	 */
	public String deleteEntity(Integer idKey, boolean fromList) {
		
		FacesContext context = FacesContext.getCurrentInstance(); 
		
		try {
			entityService = new VentaTerceroServices();
			entity = entityService.findById(idKey);
			entityService.deleteTX(entity);
			
			Metodos.auditLog(Constantes.ACTIONDELETE, VentaTerceroMBean.class.getSimpleName(), entity.getCodigo(), "");
			
			String msg = Metodos.getMessageResourceString("generics_eliminarOK", new Object[]{entity.getCodigo()});
			if(!fromList || (!Metodos.isMobile() && Metodos.isDlg())){
				Metodos.setSessionMesagges(Constantes.SESSIONMSGINFO, msg);
			}
				
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
				
			fillEntityList();
		} catch (Exception e) {
			String msg = ControlException.controlException(e);
			if(!fromList || (!Metodos.isMobile() && Metodos.isDlg())){
				Metodos.setSessionMesagges(Constantes.SESSIONMSGFATAL, msg);
			}
				
			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,msg, ""));
		}
		return  Metodos.close(closeRedirectWeb, closeRedirectMobile, fromList);
		
	}
	
	/**
	 * Metodo que retorna las listas
	 * @return
	 */
	public void fillEntityList(){
		FacesContext context = FacesContext.getCurrentInstance(); 
		try {
			entityService = new VentaTerceroServices();
			entityList = null;
			ControlQuery cq = new ControlQuery();
			cq.add(new Condition(VentaTerceroDAO.IDCOMPANIA, Condition.IGUAL, loggedUser.getCompania().getId()));
			cq.getOrderby().add(new String[]{VentaTerceroDAO.FCHVENTA, Condition.DESC});
			cq.getOrderby().add(new String[]{VentaTerceroDAO.CODIGO, Condition.DESC});
			entityList = entityService.findAll(cq);
			
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}		
	}
	
	/**
	 * Método que llena la lista de clientes activos para el selector
	 * @param 
	 */
	private void fillClientesDropDown(){
		FacesContext context = FacesContext.getCurrentInstance(); 
		clientesDropDownList = null;
		try {
			IClienteServices service = new ClienteServices();
			clientesDropDownList = service.findAll(loggedUser.getCompania().getId());
		} catch (Exception e) {
			clientesDropDownList = null;
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}		
	}
	
	/**
	 * Método que llena la lista de vehículos activos para el selector
	 * @param 
	 */
	private void fillVehiculosDropDown(){
		FacesContext context = FacesContext.getCurrentInstance(); 
		vehiculosDropDownList = null;
		try {
			IVehiculoServices service = new VehiculoServices();
			vehiculosDropDownList = service.findAllNoActivosInventario(loggedUser.getCompania().getId());
		} catch (Exception e) {
			vehiculosDropDownList = null;
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}		
	}
	
	/**
	 * Metodo para guardar
	 */
	public String saveEntity(){
		
		FacesContext context = FacesContext.getCurrentInstance(); 
		
		try{
			if(mode != null && mode.equals(Constantes.MODECREATE)
					&& hasPermissionTo(Constantes.ACTIONCREATE)){
				
				entity.setCompania(loggedUser.getCompania());
				entityService = new VentaTerceroServices();
				entityService.createVentaTerceroTX(entity);
				
				Metodos.auditLog(Constantes.ACTIONCREATE, VentaTerceroMBean.class.getSimpleName(), entity.getCodigo(), "");
				
				String msg = Metodos.getMessageResourceString("generics_guardarOK", new Object[]{entity.getCodigo()});
				
				if(!Metodos.isMobile()){
					Metodos.setSessionMesagges(Constantes.SESSIONMSGINFO, msg);
				}
					
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
					
				return  Metodos.close(closeRedirectWeb, closeRedirectMobile, true);
			}
			
			else if(mode != null && mode.equals(Constantes.MODEEDIT)
					&& hasPermissionTo(Constantes.ACTIONUPDATE)){
				
				entityService = new VentaTerceroServices();
				entityService.updateTX(entity);
				
				Metodos.auditLog(Constantes.ACTIONUPDATE, VentaTerceroMBean.class.getSimpleName(), entity.getCodigo(), "");
				
				String msg = Metodos.getMessageResourceString("generics_actualizarOK", new Object[]{entity.getCodigo()});
				
				if(!Metodos.isMobile()){
					Metodos.setSessionMesagges(Constantes.SESSIONMSGINFO, msg);
				}
					
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
					
				return  Metodos.close(closeRedirectWeb, closeRedirectMobile, true);
			}
					
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}
		
		return "";
	}
	
	/**
	 * Método que carga los datos del cliente según el valor seleccionado
	 */
	public void loadCliente(){
		
		try {
			FacesContext context = FacesContext.getCurrentInstance(); 
			HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
			HttpSession session = request.getSession();
			
			IClienteServices servicio = new ClienteServices();
			
			String cedula = request.getParameter("frm:autoCompleteClienteID_input");
			
			if(cedula != null
					&& cedula.contains(" - ")){
				Cliente cliente = servicio.findClienteByCedulaCia(entity.getCliente().getCedula().split(" - ")[0], Metodos.getUserSession().getCompania().getId());
				
				if(cliente == null && session.getAttribute(Constantes.NEWCLIENTRETURN) != null){
					cedula = session.getAttribute(Constantes.NEWCLIENTRETURN).toString();
					cliente = servicio.findClienteByCedulaCia(cedula, Metodos.getUserSession().getCompania().getId());
				}
				if(cliente == null){
					cliente = new Cliente();
					cliente.setCedula("");
				}else{
					cliente.setCedula(cliente.getCedula() + " - " + cliente.getNombres() + " " + cliente.getApellidos());
				}
				entity.setCliente(cliente);
			}else{
				if(session.getAttribute(Constantes.NEWCLIENTRETURN) != null && !session.getAttribute(Constantes.NEWCLIENTRETURN).toString().equals("")){
					entity.getCliente().setCedula(session.getAttribute(Constantes.NEWCLIENTRETURN).toString());
					Cliente cliente = servicio.findClienteByCedulaCia(entity.getCliente().getCedula(), Metodos.getUserSession().getCompania().getId());
					cliente.setCedula(cliente.getCedula() + " - " + cliente.getNombres() + " " + cliente.getApellidos());
					entity.setCliente(cliente);
				}else{
					
					if(cedula != null && !cedula.trim().equals("")){
						entity.setCliente(new Cliente());
						entity.getCliente().setCedula(cedula);
						RequestContext.getCurrentInstance().execute("PF('dataChangeDlgCliente').show();");
					}
				}
				
			}
		} catch (Exception e) {
			entity.setCliente(new Cliente());
		}
	}
	
	/**
	 * Método que carga los datos del vehículo según el valor seleccionado
	 */
	public void loadVehiculo(){
		
		try {
			FacesContext context = FacesContext.getCurrentInstance(); 
			HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
			HttpSession session = request.getSession();
			IVehiculoServices servicio = new VehiculoServices();
			
			String placa = request.getParameter("frm:autoCompleteVehiculoID_input");
			
			if(placa != null && !placa.trim().equals("")){
				//Se valida que la placa esté en la lista de vehículos (Sea válida para crear consignación)
				boolean placaValida = false;
				for (Iterator<Vehiculo> itVehiculos = vehiculosDropDownList.iterator(); itVehiculos.hasNext();) {
					Vehiculo vehiculo = (Vehiculo) itVehiculos.next();
					if(vehiculo.getPlaca().equals(placa))
						placaValida = true;
				}
				
				if(placaValida){
					Vehiculo vehiculo = servicio.findVehiculoByPlacaCia(placa, Metodos.getUserSession().getCompania().getId());
					if(vehiculo != null){
						entity.setVehiculo(vehiculo);
					}else{
						
						if(session.getAttribute(Constantes.NEWVEHICLERETURN) != null && !session.getAttribute(Constantes.NEWVEHICLERETURN).toString().equals("")){
							entity.getVehiculo().setPlaca(session.getAttribute(Constantes.NEWVEHICLERETURN).toString());
							vehiculo = servicio.findVehiculoByPlacaCia(entity.getVehiculo().getPlaca(), Metodos.getUserSession().getCompania().getId());
							if(vehiculo != null)
								entity.setVehiculo(vehiculo);
							else
								entity.setVehiculo(new Vehiculo());
						}else{
							
							if(placa != null && !placa.trim().equals("")){
								entity.setVehiculo(new Vehiculo());
								entity.getVehiculo().setPlaca(placa);
								RequestContext.getCurrentInstance().execute("PF('dataChangeDlgVehiculo').show();");
							}
						}
					}
				}else{
					if(placa != null && !placa.trim().equals("")){
						entity.setVehiculo(new Vehiculo());
						entity.getVehiculo().setPlaca(placa);
						RequestContext.getCurrentInstance().execute("PF('dataChangeDlgVehiculo').show();");
					}
				}
				
			}
		} catch (Exception e) {
			entity.setCliente(new Cliente());
		}
	}
	
	/**
	 * Metodo que se ejecuta cuando se cierra una modal
	 * @return
	 */
	public void dialogListener(){
		fillEntityList();
		Metodos.loadSessionMesagges();
	}
	
	/**
	 * Metodo que se ejecuta cuando se cierra una modal
	 * @return
	 */
	public void dialogListenerCliente(){
		
		fillClientesDropDown();
		
		loadCliente();
		
		Metodos.loadSessionMesagges();
		
	}
	
	/**
	 * Metodo que se ejecuta cuando se cierra una modal
	 * @return
	 */
	public void dialogListenerVehiculo(){
		fillVehiculosDropDown();
		
		loadVehiculo();
		
		Metodos.loadSessionMesagges();
	}
	
	public ArrayList<String> autocompleteClientes(String query) {
		ArrayList<String> results = new ArrayList<String>();
		try {
			if(clientesDropDownList != null && !clientesDropDownList.isEmpty()){
				for (Iterator<Cliente> itClientes = clientesDropDownList.iterator(); itClientes.hasNext();) {
					Cliente cliente = (Cliente) itClientes.next();
					if((query != null && query.trim().equals(""))
							|| cliente.getNombres().toLowerCase().contains(query.toLowerCase())
							|| cliente.getCedula().toLowerCase().contains(query.toLowerCase())
							|| cliente.getApellidos().toLowerCase().contains(query.toLowerCase())){
						results.add(cliente.getCedula() + " - " + cliente.getNombres() + " " + cliente.getApellidos());
					}
				}
			}
			
		} catch (Exception e) {
			results = new ArrayList<String>();
		}
		
		return results;
	}
	
	public ArrayList<String> autocompleteVehiculos(String query) {
		ArrayList<String> results = new ArrayList<String>();
		try {
			if(vehiculosDropDownList != null && !vehiculosDropDownList.isEmpty()){
				for (Iterator<Vehiculo> itVehiculos = vehiculosDropDownList.iterator(); itVehiculos.hasNext();) {
					Vehiculo vehiculo = (Vehiculo) itVehiculos.next();
					if((query != null && query.trim().equals(""))
							|| vehiculo.getPlaca().toLowerCase().contains(query.toLowerCase())){
						results.add(vehiculo.getPlaca());
					}
				}
			}
			
		} catch (Exception e) {
			results = new ArrayList<String>();
		}
		
		return results;
	}

	/**
	 * Método que llena la lista de formas de pago activas para el selector
	 * @param 
	 */
	private void fillFormasPagoDropDown(){
		FacesContext context = FacesContext.getCurrentInstance(); 
		formasPagoDropDownList = null;
		try {
			IFormaPagoServices formaPagoSrv = new FormaPagoServices();
			formasPagoDropDownList = formaPagoSrv.findAllActivos(loggedUser.getCompania().getId());
		} catch (Exception e) {
			formasPagoDropDownList = null;
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}		
	}
	
	/**
	 * Método que llena la lista de cuentas activas para el selector
	 * @param 
	 */
	private void fillCuentasDropDown(){
		FacesContext context = FacesContext.getCurrentInstance(); 
		cuentasDropDownList = null;
		try {
			ICuentaServices cuentaSrv = new CuentaServices();
			cuentasDropDownList = cuentaSrv.findAllActivos(loggedUser.getCompania().getId());
		} catch (Exception e) {
			cuentasDropDownList = null;
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}		
	}
	
	/* SECURITY METHODS*/
	/**
	 * Método que valida si el usuario en sesión puede ejecutar una acción sobre este documento
	 * 
	 * @return
	 */
	public boolean hasPermissionTo(String action){		
		return Metodos.hasPermission(VentaTerceroMBean.class.getSimpleName(), action);
	}
	
	/* GETTERS AND SETTERS */

	public VentaTercero getEntity() {
		return entity;
	}

	public void setEntity(VentaTercero entity) {
		this.entity = entity;
	}

	public List<VentaTercero> getEntityList() {
		return entityList;
	}

	public void setEntityList(List<VentaTercero> entityList) {
		this.entityList = entityList;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public List<VentaTercero> getEntityFilterList() {
		return entityFilterList;
	}

	public void setEntityFilterList(List<VentaTercero> entityFilterList) {
		this.entityFilterList = entityFilterList;
	}

	public List<Cliente> getClientesDropDownList() {
		return clientesDropDownList;
	}

	public void setClientesDropDownList(List<Cliente> clientesDropDownList) {
		this.clientesDropDownList = clientesDropDownList;
	}

	public List<Vehiculo> getVehiculosDropDownList() {
		return vehiculosDropDownList;
	}

	public void setVehiculosDropDownList(List<Vehiculo> vehiculosDropDownList) {
		this.vehiculosDropDownList = vehiculosDropDownList;
	}

	public List<FormaPago> getFormasPagoDropDownList() {
		return formasPagoDropDownList;
	}

	public void setFormasPagoDropDownList(List<FormaPago> formasPagoDropDownList) {
		this.formasPagoDropDownList = formasPagoDropDownList;
	}

	
	public List<Cuenta> getCuentasDropDownList() {
		return cuentasDropDownList;
	}
	

	public void setCuentasDropDownList(List<Cuenta> cuentasDropDownList) {
		this.cuentasDropDownList = cuentasDropDownList;
	}

}
