/**
	 * 	VehiculoMBean.java:
	 * 
	 * 	Esta clase corresponde al Managed Bean para controlar el CRUD de 
	 * 	la entidad Vehiculo
	 *
	 * @author Sergio Henao - sergiohenaoa@gmail.com - 11/04/2015
	 * @Modifier Sergio Henao - sergiohenaoa@gmail.com - 11/04/2015
	 * @version 1.0
	 * 
	 */
package com.sh.pvs.view.configuracion;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;

import com.sh.pvs.core.interfaces.configuracion.ICarroceriaServices;
import com.sh.pvs.core.interfaces.configuracion.IColorServices;
import com.sh.pvs.core.interfaces.configuracion.IMarcaServices;
import com.sh.pvs.core.interfaces.configuracion.ITipoVehiculoServices;
import com.sh.pvs.core.interfaces.configuracion.ITransmisionServices;
import com.sh.pvs.core.interfaces.configuracion.IVehiculoServices;
import com.sh.pvs.core.services.configuracion.CarroceriaServices;
import com.sh.pvs.core.services.configuracion.ColorServices;
import com.sh.pvs.core.services.configuracion.MarcaServices;
import com.sh.pvs.core.services.configuracion.TipoVehiculoServices;
import com.sh.pvs.core.services.configuracion.TransmisionServices;
import com.sh.pvs.core.services.configuracion.VehiculoServices;
import com.sh.pvs.model.dao.configuracion.CarroceriaDAO;
import com.sh.pvs.model.dao.configuracion.VehiculoDAO;
import com.sh.pvs.model.dto.configuracion.Carroceria;
import com.sh.pvs.model.dto.configuracion.Color;
import com.sh.pvs.model.dto.configuracion.FotoxVehiculo;
import com.sh.pvs.model.dto.configuracion.Marca;
import com.sh.pvs.model.dto.configuracion.TipoVehiculo;
import com.sh.pvs.model.dto.configuracion.Transmision;
import com.sh.pvs.model.dto.configuracion.Vehiculo;
import com.sh.pvs.model.dto.seguridad.Usuario;
import com.sh.pvs.view.utils.Constantes;
import com.sh.pvs.view.utils.ControlException;
import com.sh.pvs.view.utils.Metodos;

@ManagedBean
@ViewScoped
public class VehiculoMBean implements Serializable {

	/*Serializable Attribute*/
	private static final long serialVersionUID = 1L;
	
	/*Variables*/
	private Vehiculo entity; 								// ViewForm - EditForm 	
 	private List<Vehiculo> entityList; 						// Lista de registros del datatable
 	private List<Vehiculo> entityFilterList;				// Lista de registros filtrados del datatable
 	private List<TipoVehiculo> tiposVehiculoDropDownList;
 	private List<Marca> marcasDropDownList;
 	private List<Carroceria> carroceriasDropDownList;
 	private List<Color> coloresDropDownList;
 	private List<String> modelosDropDownList;
 	private List<Transmision> transmisionesDropDownList;
 	private Usuario loggedUser;								//Usuario logeado (En Sesión) 	
 	
 	/*Navegación*/
 	private String mode;									// view - edit - list - create ==> Modo para identificar la vista
 	private boolean noOptions;
 	
 	/*Servicios*/
 	private IVehiculoServices entityService;			// Servicios para la gestión de la entidad
	
 	/*Variables para navegación*/
 	public static String openDialog = "/configuracion/vehiculo/vehiculoForm.faces";
 	public static String openWebMobile = "vehiculoForm.faces";
 	public static String closeRedirectMobile = "vehiculoList.faces";
	public static String closeRedirectWeb = "/configuracion/vehiculo/vehiculoList.faces?faces-redirect=true";
 	
 	/*Constructors*/	
	/**	Default constructor */
	public VehiculoMBean() {

	}
	
	/*METHODS*/
	
	/**
	 * Método para inicializar las variables
	 * 
	 * */
	@PostConstruct
	private void init(){
		
		FacesContext context = FacesContext.getCurrentInstance();
		
		try {
			
			Metodos.loadSessionMesagges();
			
			loggedUser = Metodos.getUserSession();
			
			if(context.getExternalContext().getRequestParameterMap().get("mode") != null)
				mode = context.getExternalContext().getRequestParameterMap().get("mode");
			else
				mode = Constantes.MODELIST;
			
			if(mode.equals(Constantes.MODECREATE)){
				
				this.entity = new Vehiculo();
				
				entity.setTipoVehiculo(new TipoVehiculo());
				entity.setMarca(new Marca());
				entity.setCarroceria(new Carroceria());
				entity.setColor(new Color());
				entity.setTransmision(new Transmision());
				
				fillTiposVehiculoDropDown();
				fillMarcasDropDown();
				fillCarroceriasDropDown();
				fillColoresDropDown();
				fillModelosDropDown();
				fillTransmisionesDropDown();
				
				if(context.getExternalContext().getRequestParameterMap().get("placa") != null)
					this.entity.setPlaca(context.getExternalContext().getRequestParameterMap().get("placa"));
				
				return;
			}
			
			if(mode.equals(Constantes.MODELIST)){
				fillEntityList();
				return;
			}
						
			if(mode.equals(Constantes.MODEVIEW) || mode.equals(Constantes.MODEEDIT)){
				
				try {
					
					String idParam = context.getExternalContext().getRequestParameterMap().get("id");
					entityService = new VehiculoServices();
					this.entity = entityService.findById(Integer.parseInt(idParam));
					
					String noOptionsStr = context.getExternalContext().getRequestParameterMap().get("noOptions");
					if(noOptionsStr != null && noOptionsStr.equals("true"))
						noOptions = true;
					
					if(mode.equals(Constantes.MODEEDIT)){
						fillTiposVehiculoDropDown();
						fillMarcasDropDown();
						fillCarroceriasDropDown();
						fillColoresDropDown();
						fillModelosDropDown();
						fillTransmisionesDropDown();
					}
					
				} catch (Exception e) {
					context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
				}
				
				return;
			}
			
		} catch (Exception e) {
			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
		}
		
	}
	
	/**
	 * Create a new Vehiculo entity
	 * 
	 */
	public String newEntity() {
		
		entity = new Vehiculo();
		return Metodos.newNavigation(openDialog, openWebMobile);
		
	}
	
	/**
	 * Create a new Vehiculo entity
	 * 
	 */
	public String newEntity(String placa) {
		
		entity = new Vehiculo();
		entity.setPlaca(placa);
		return this.newVehiculoNavigation(openDialog, openWebMobile, placa);
		
	}
	
	/**
	 * Select an existing Vehiculo entity
	 * 
	 */
	public String selectEntity(Integer idKey) {				
		
		FacesContext context = FacesContext.getCurrentInstance();
		
		try {
			
			entityService = new VehiculoServices();
			entity = entityService.findById(idKey);
			
		} catch (Exception e) {
			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
		}
		
		return Metodos.selectNavigation(idKey, openDialog, openWebMobile);
	}
	
	/**
	 * Select an existing Vehiculo entity
	 * 
	 */
	public String selectEntityFromConsignacion() {				
		
		FacesContext context = FacesContext.getCurrentInstance();
		
		Integer idKey = 0;
		try {
			entityService = new VehiculoServices();
			
			HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
			String placa = request.getParameter("frm:autoCompleteVehiculoID_input");
			
			if(placa != null && !placa.trim().equals("")
					&& placa != null){
				entity = entityService.findVehiculoByPlacaCia(placa, Metodos.getUserSession().getCompania().getId());
				idKey = entity.getId();
				noOptions = false;
			}else{
				placa = request.getParameter("frm:autoCompleteVehiculoIDView");
				
				if(placa != null && !placa.trim().equals("")
						&& placa != null){
					entity = entityService.findVehiculoByPlacaCia(placa, Metodos.getUserSession().getCompania().getId());
					idKey = entity.getId();
					noOptions = true;
				}else{
					return "";
				}
			}
			
		} catch (Exception e) {
			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
		}
		
		if(noOptions)
			return Metodos.selectNavigationNoOptions(idKey, openDialog, openWebMobile);
		
		return Metodos.selectNavigation(idKey, openDialog, openWebMobile);
	}
	
	/**
	 * Select an existing Vehiculo entity
	 * 
	 */
	public String selectEntityFromVenta() {				
		
		FacesContext context = FacesContext.getCurrentInstance();
		
		Integer idKey = 0;
		try {
			entityService = new VehiculoServices();
			
			HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
			String placa = request.getParameter("frm:autoCompleteVehiculoID_input");
			
			if(placa != null && !placa.trim().equals("")
					&& placa != null){
				entity = entityService.findVehiculoByPlacaCia(placa, Metodos.getUserSession().getCompania().getId());
				idKey = entity.getId();
				noOptions = false;
			}else{
				placa = request.getParameter("frm:autoCompleteVehiculoIDView");
				
				if(placa != null && !placa.trim().equals("")
						&& placa != null){
					entity = entityService.findVehiculoByPlacaCia(placa, Metodos.getUserSession().getCompania().getId());
					idKey = entity.getId();
					noOptions = true;
				}else{
					return "";
				}
			}
			
		} catch (Exception e) {
			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
		}
		
		if(noOptions)
			return Metodos.selectNavigationNoOptions(idKey, openDialog, openWebMobile);
		
		return Metodos.selectNavigation(idKey, openDialog, openWebMobile);
	}
	
	/**
	 * Select an existing Vehiculo entity
	 * 
	 */
	public String selectEntityFromRetoma() {				
		
FacesContext context = FacesContext.getCurrentInstance();
		
		Integer idKey = 0;
		try {
			entityService = new VehiculoServices();
			
			HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
			String placa = request.getParameter("frm:autoCompleteVehiculoID_input");
			
			if(placa != null && !placa.trim().equals("")
					&& placa != null){
				entity = entityService.findVehiculoByPlacaCia(placa, Metodos.getUserSession().getCompania().getId());
				idKey = entity.getId();
				noOptions = false;
			}else{
				placa = request.getParameter("frm:autoCompleteVehiculoIDView");
				
				if(placa != null && !placa.trim().equals("")
						&& placa != null){
					entity = entityService.findVehiculoByPlacaCia(placa, Metodos.getUserSession().getCompania().getId());
					idKey = entity.getId();
					noOptions = true;
				}else{
					return "";
				}
			}
			
		} catch (Exception e) {
			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
		}
		
		if(noOptions)
			return Metodos.selectNavigationNoOptions(idKey, openDialog, openWebMobile);
		
		return Metodos.selectNavigation(idKey, openDialog, openWebMobile);
	}
	
	/**
	 * Edit an existing Vehiculo entity
	 * 
	 */
	public String editEntity(Integer idKey, boolean fromList) {
		
		FacesContext context = FacesContext.getCurrentInstance();
		
		try {
			
			entityService = new VehiculoServices();
			entity = entityService.findById(idKey);
			
		} catch (Exception e) {
			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
		}
		
		return Metodos.editNavigation(idKey, fromList, openDialog, openWebMobile);
	}
	
	/**
	 * Delete an existing Vehiculo entity
	 * 
	 */
	public String deleteEntity(Integer idKey, boolean fromList) {
		
		FacesContext context = FacesContext.getCurrentInstance(); 
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		
		try {
			entityService = new VehiculoServices();
			entity = entityService.findById(idKey);
			entityService.deleteTX(entity);
			
			Metodos.auditLog(Constantes.ACTIONDELETE, VehiculoMBean.class.getSimpleName(), entity.getPlaca(), "");
			
			String msg = Metodos.getMessageResourceString("generics_eliminarOK", new Object[]{entity.getPlaca()});
			if(!fromList || (!Metodos.isMobile() && Metodos.isDlg())){
				Metodos.setSessionMesagges(Constantes.SESSIONMSGINFO, msg);
			}
				
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
			
			HttpSession session = request.getSession();
			session.setAttribute(Constantes.NEWVEHICLERETURN, "Borrado");//Control para la consignación cuando se elimina un elemento
				
			fillEntityList();
		} catch (Exception e) {
			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
		}
		return  Metodos.close(closeRedirectWeb, closeRedirectMobile, fromList);
		
	}
	
	/**
	 * Metodo que retorna las listas
	 * @return
	 */
	public void fillEntityList(){
		FacesContext context = FacesContext.getCurrentInstance(); 
		try {
			entityService = new VehiculoServices();
			entityList = null;
			entityList = entityService.findByProperty(VehiculoDAO.IDCOMPANIA , loggedUser.getCompania().getId());
			
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}		
	}
	
	/**
	 * Método que llena la lista de tipos de vehículo activos para el selector
	 * @param 
	 */
	public void fillTiposVehiculoDropDown(){
		FacesContext context = FacesContext.getCurrentInstance(); 
		tiposVehiculoDropDownList = null;
		try {
			ITipoVehiculoServices tipoVSrv = new TipoVehiculoServices();
			tiposVehiculoDropDownList = tipoVSrv.findAllActivos(loggedUser.getCompania().getId());
		} catch (Exception e) {
			tiposVehiculoDropDownList = null;
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}		
	}
	
	/**
	 * Método que llena la lista de marcas activas para el selector
	 * @param 
	 */
	public void fillMarcasDropDown(){
		FacesContext context = FacesContext.getCurrentInstance(); 
		marcasDropDownList = null;
		try {
			
			if(entity != null && entity.getTipoVehiculo() != null
					&& entity.getTipoVehiculo().getId() != null){
				IMarcaServices marcaSrv = new MarcaServices();
				marcasDropDownList = marcaSrv.findAllActivosByTipoVehiculo(loggedUser.getCompania().getId(), entity.getTipoVehiculo().getId());
			}
			
		} catch (Exception e) {
			marcasDropDownList = null;
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}		
	}
	
	/**
	 * Método que llena la lista de carrocerías activas para el selector
	 * @param 
	 */
	public void fillCarroceriasDropDown(){
		FacesContext context = FacesContext.getCurrentInstance(); 
		carroceriasDropDownList  = null;
		try {
			
			if(entity != null && entity.getTipoVehiculo() != null
					&& entity.getTipoVehiculo().getId() != null){
				ICarroceriaServices carroceriaSrv = new CarroceriaServices();
				carroceriasDropDownList = carroceriaSrv.findByProperty(CarroceriaDAO.IDTIPOVEHICULO, entity.getTipoVehiculo().getId());
			}
			
		} catch (Exception e) {
			carroceriasDropDownList = null;
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}		
	}
	
	/**
	 * Método que llena la lista de modelos para el selector
	 * @param 
	 */
	public void fillModelosDropDown(){
		FacesContext context = FacesContext.getCurrentInstance(); 
		modelosDropDownList = null;
		try {
			modelosDropDownList = new ArrayList<String>();
			Calendar calendar = Calendar.getInstance();
			int actualYear = calendar.get(Calendar.YEAR);
			//Poner límites de los años en constantes
			for(int i = actualYear - 50; i < actualYear + 2; i++){
				modelosDropDownList.add(Integer.toString(i));
			}
		} catch (Exception e) {
			modelosDropDownList = null;
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}		
	}
	
	/**
	 * Método que llena la lista de transmisiones activas para el selector
	 * @param 
	 */
	public void fillTransmisionesDropDown(){
		FacesContext context = FacesContext.getCurrentInstance(); 
		transmisionesDropDownList = null;
		try {
			ITransmisionServices srv = new TransmisionServices();
			transmisionesDropDownList = srv.findAllActivos(loggedUser.getCompania().getId());
		} catch (Exception e) {
			transmisionesDropDownList = null;
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}		
	}
	
	/**
	 * Método que llena la lista de colores activos para el selector
	 * @param 
	 */
	public void fillColoresDropDown(){
		FacesContext context = FacesContext.getCurrentInstance(); 
		coloresDropDownList = null;
		try {
			IColorServices srv = new ColorServices();
			coloresDropDownList = srv.findAllActivos(loggedUser.getCompania().getId());
		} catch (Exception e) {
			coloresDropDownList = null;
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}		
	}
	
	/**
	 * Método que llena recalcula las listas dependientes del tipo de vehículo
	 * @param 
	 */
	public void fillDropDownsDependentTipoVehiculo(){
		fillMarcasDropDown();
		fillCarroceriasDropDown();
	}
	
	/**
	 * Metodo para guardar
	 */
	public String saveEntity(){
		
		FacesContext context = FacesContext.getCurrentInstance(); 
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		
		try{
			if(mode != null && mode.equals(Constantes.MODECREATE)
					&& hasPermissionTo(Constantes.ACTIONCREATE)){
				
				entity.setCompania(loggedUser.getCompania());
				entityService = new VehiculoServices();
				entityService.createTX(entity);
				
				Metodos.auditLog(Constantes.ACTIONCREATE, VehiculoMBean.class.getSimpleName(), entity.getPlaca(), "");
				
				String msg = Metodos.getMessageResourceString("generics_guardarOK", new Object[]{entity.getPlaca()});
				
				if(!Metodos.isMobile()){
					Metodos.setSessionMesagges(Constantes.SESSIONMSGINFO, msg);
				}
					
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
				
				HttpSession session = request.getSession();
				session.setAttribute(Constantes.NEWVEHICLERETURN, entity.getPlaca());
					
				return  Metodos.close(closeRedirectWeb, closeRedirectMobile, true);
			}
			
			else if(mode != null && mode.equals(Constantes.MODEEDIT)
					&& hasPermissionTo(Constantes.ACTIONUPDATE)){
				
				entityService = new VehiculoServices();
				entityService.updateTX(entity);
				
				Metodos.auditLog(Constantes.ACTIONUPDATE, VehiculoMBean.class.getSimpleName(), entity.getPlaca(), "");
				
				String msg = Metodos.getMessageResourceString("generics_actualizarOK", new Object[]{entity.getPlaca()});
				
				if(!Metodos.isMobile()){
					Metodos.setSessionMesagges(Constantes.SESSIONMSGINFO, msg);
				}
					
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
				
				HttpSession session = request.getSession();
				session.setAttribute(Constantes.NEWVEHICLERETURN, entity.getPlaca());
					
				return  Metodos.close(closeRedirectWeb, closeRedirectMobile, true);
			}
					
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}
		
		return "";
	}

	/**
	 * Método que controla la carga de imágenes
	 * @param event
	 */
	public void handleFileUpload(FileUploadEvent event) {
        FacesContext context = FacesContext.getCurrentInstance(); 
		
		try {
			FotoxVehiculo foto = new FotoxVehiculo();
			foto.setNombreArchivo(event.getFile().getFileName());
			foto.setTipoArchivo(event.getFile().getContentType());
			foto.setSizeArchivo(Integer.parseInt(Long.toString(event.getFile().getSize())));
			foto.setAdjunto(com.sh.pvs.utils.Metodos.encodeFile(event.getFile().getInputstream(), event.getFile().getSize()));
			foto.setVehiculo(entity);
			entity.getFotos().add(foto);

		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}
    }
	
	/**
	 * Método que retorna el flujo de String del archivo a descargar
	 * @param itemProducto
	 * @return String
	 */
	public String descargarArchivo(Integer fotoId){
		FacesContext context = FacesContext.getCurrentInstance(); 
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		try {
			
			for (Iterator<FotoxVehiculo> itFotos = entity.getFotos().iterator(); itFotos.hasNext();) {
				FotoxVehiculo foto = (FotoxVehiculo) itFotos.next();
				if(foto.getId().equals(fotoId)){

					response.setContentType(foto.getTipoArchivo());
					response.setHeader("Content-disposition", "attachment;filename=" + foto.getNombreArchivo());
					
					byte[] decodedFile = com.sh.pvs.utils.Metodos.decodeFile(foto.getAdjunto());
					InputStream fileToDownload = new ByteArrayInputStream(decodedFile);
					
					PrintWriter out = response.getWriter();
		            int c;
		            while ((c = fileToDownload.read()) != -1) {
		                out.write(c);
		            }
		            
		            out.flush();
		            out.close();
		            fileToDownload.close();
		            context.responseComplete();
					
				}
			}

		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}
		return "";
	}
	
	/**
	 * 
	 */
	public String borrarArchivo(Integer fotoId){
		FacesContext context = FacesContext.getCurrentInstance(); 
		try {
			
			for (Iterator<FotoxVehiculo> itFotos = entity.getFotos().iterator(); itFotos.hasNext();) {
				FotoxVehiculo foto = (FotoxVehiculo) itFotos.next();
				if(foto.getId().equals(fotoId)){
					
					entity.getFotos().remove(foto);
					entity = entityService.actualizarFotos(entity);
					
					return "";
					
				}
			}

		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}
		return "";
	}
	
	/**
	 * Metodo que se ejecuta cuando se cierra una modal
	 * @return
	 */
	public void dialogListener(){
		fillEntityList();
		Metodos.loadSessionMesagges();
	}
	
	/**
	 * Método que define la navegación cuando se da clic en el botón nuevo
	 * @param openCreateDialog
	 * @param openCreateWebMobile
	 * @return
	 */
	public String newVehiculoNavigation(String openCreateDialog, String openCreateWebMobile, String placa){
		FacesContext context = FacesContext.getCurrentInstance(); 
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		if(!Metodos.isMobile()){
			
			//Si la navegación es en web se abren modales
			Map<String,Object> options = new HashMap<String, Object>();
	        options.put("modal", true);
	        options.put("resizable", false);
	        options.put("contentHeight", 450);
	        options.put("contentWidth", 600);
	        
	        Map<String, List<String>> params = new HashMap<String, List<String>>();
		       
	        List<String> values = new ArrayList<String>();
	        values.add(Constantes.MODECREATE);
	        params.put("mode", values);
	        
	        List<String> valuescedula = new ArrayList<String>();	        
	        valuescedula.add(placa);
	        params.put("placa", valuescedula);
	        
			RequestContext.getCurrentInstance().openDialog(openCreateDialog, options, params);
			
		}else{
			//Si la navegación es en dispositivos móviles, se redirecciona
			try {
				response.sendRedirect(openCreateWebMobile + "?mode=" + Constantes.MODECREATE + "&placa=" + placa);
			} catch (Exception e) {
				return "";
			}
			
		}
		
		return "";
	}
	
	/* SECURITY METHODS*/
	/**
	 * Método que valida si el usuario en sesión puede ejecutar una acción sobre este documento
	 * 
	 * @return
	 */
	public boolean hasPermissionTo(String action){		
		return Metodos.hasPermission(VehiculoMBean.class.getSimpleName(), action);
	}
	
	/* GETTERS AND SETTERS */

	public Vehiculo getEntity() {
		return entity;
	}

	public void setEntity(Vehiculo entity) {
		this.entity = entity;
	}

	public List<Vehiculo> getEntityList() {
		return entityList;
	}

	public void setEntityList(List<Vehiculo> entityList) {
		this.entityList = entityList;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public List<Vehiculo> getEntityFilterList() {
		return entityFilterList;
	}

	public void setEntityFilterList(List<Vehiculo> entityFilterList) {
		this.entityFilterList = entityFilterList;
	}

	public List<TipoVehiculo> getTiposVehiculoDropDownList() {
		return tiposVehiculoDropDownList;
	}

	public void setTiposVehiculoDropDownList(
			List<TipoVehiculo> tiposVehiculoDropDownList) {
		this.tiposVehiculoDropDownList = tiposVehiculoDropDownList;
	}

	public List<Marca> getMarcasDropDownList() {
		return marcasDropDownList;
	}

	public void setMarcasDropDownList(List<Marca> marcasDropDownList) {
		this.marcasDropDownList = marcasDropDownList;
	}

	public List<Carroceria> getCarroceriasDropDownList() {
		return carroceriasDropDownList;
	}

	public void setCarroceriasDropDownList(List<Carroceria> carroceriasDropDownList) {
		this.carroceriasDropDownList = carroceriasDropDownList;
	}

	public List<Color> getColoresDropDownList() {
		return coloresDropDownList;
	}

	public void setColoresDropDownList(List<Color> coloresDropDownList) {
		this.coloresDropDownList = coloresDropDownList;
	}

	public List<String> getModelosDropDownList() {
		return modelosDropDownList;
	}

	public void setModelosDropDownList(List<String> modelosDropDownList) {
		this.modelosDropDownList = modelosDropDownList;
	}

	public List<Transmision> getTransmisionesDropDownList() {
		return transmisionesDropDownList;
	}

	public void setTransmisionesDropDownList(
			List<Transmision> transmisionesDropDownList) {
		this.transmisionesDropDownList = transmisionesDropDownList;
	}

	
	public boolean isNoOptions() {
		return noOptions;
	}
	

	public void setNoOptions(boolean noOptions) {
		this.noOptions = noOptions;
	}
	
}
