/**
	 * 	FormaPagoMBean.java:
	 * 
	 * 	Esta clase corresponde al Managed Bean para controlar el CRUD de 
	 * 	la entidad FormaPago
	 *
	 * @author Sergio Henao - sergiohenaoa@gmail.com - 11/04/2015
	 * @Modifier Sergio Henao - sergiohenaoa@gmail.com - 11/04/2015
	 * @version 1.0
	 * 
	 */
package com.sh.pvs.view.configuracion;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import com.sh.pvs.core.interfaces.configuracion.IFormaPagoServices;
import com.sh.pvs.core.services.configuracion.FormaPagoServices;
import com.sh.pvs.model.dao.configuracion.FormaPagoDAO;
import com.sh.pvs.model.dto.configuracion.FormaPago;
import com.sh.pvs.view.seguridad.LoginMBean;
import com.sh.pvs.view.utils.Constantes;
import com.sh.pvs.view.utils.ControlException;
import com.sh.pvs.view.utils.Metodos;

@ManagedBean
@ViewScoped
public class FormaPagoMBean implements Serializable {

	/*Serializable Attribute*/
	private static final long serialVersionUID = 1L;
	
	/*Variables*/
	private FormaPago entity; 							// ViewForm - EditForm 	
 	private List<FormaPago> entityList; 					// Lista de registros del datatable
 	private List<FormaPago> entityFilterList;			// Lista de registros filtrados del datatable
 	
 	/*Navegación*/
 	private String mode;									// view - edit - list - create ==> Modo para identificar la vista
 	
 	/*Servicios*/
 	private IFormaPagoServices entityService;			// Servicios para la gestión de la entidad
	
 	/*Variables para navegación*/
 	public static String openDialog = "/configuracion/formaPago/formaPagoForm.faces";
 	public static String openWebMobile = "formaPagoForm.faces";
 	public static String closeRedirectMobile = "formaPagoList.faces";
	public static String closeRedirectWeb = "/configuracion/formaPago/formaPagoList.faces?faces-redirect=true";
 	
 	/*Constructors*/	
	/**	Default constructor */
	public FormaPagoMBean() {

	}
	
	/*METHODS*/
	
	/**
	 * Método para inicializar las variables
	 * 
	 * */
	@PostConstruct
	private void init(){
		
		FacesContext context = FacesContext.getCurrentInstance();
		
		try {
			
			Metodos.loadSessionMesagges();
			
			if(context.getExternalContext().getRequestParameterMap().get("mode") != null)
				mode = context.getExternalContext().getRequestParameterMap().get("mode");
			else
				mode = Constantes.MODELIST;
			
			if(mode.equals(Constantes.MODECREATE)){
				this.entity = new FormaPago();
				return;
			}
			
			if(mode.equals(Constantes.MODELIST)){
				fillEntityList();
				return;
			}
						
			if(mode.equals(Constantes.MODEVIEW) || mode.equals(Constantes.MODEEDIT)){
				
				try {
					
					String idParam = context.getExternalContext().getRequestParameterMap().get("id");
					entityService = new FormaPagoServices();
					this.entity = entityService.findById(Integer.parseInt(idParam));
							
				} catch (Exception e) {
					context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
				}
				
				return;
			}
			
		} catch (Exception e) {
			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
		}
		
	}
	
	/**
	 * Create a new FormaPago entity
	 * 
	 */
	public String newEntity() {
		
		entity = new FormaPago();
		return Metodos.newNavigation(openDialog, openWebMobile);
		
	}
	
	/**
	 * Select an existing FormaPago entity
	 * 
	 */
	public String selectEntity(Integer idKey) {				
		
		FacesContext context = FacesContext.getCurrentInstance();
		
		try {
			
			entityService = new FormaPagoServices();
			entity = entityService.findById(idKey);
			
		} catch (Exception e) {
			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
		}
		
		return Metodos.selectNavigation(idKey, openDialog, openWebMobile);
	}
	
	/**
	 * Edit an existing FormaPago entity
	 * 
	 */
	public String editEntity(Integer idKey, boolean fromList) {
		
		FacesContext context = FacesContext.getCurrentInstance();
		
		try {
			
			entityService = new FormaPagoServices();
			entity = entityService.findById(idKey);
			
		} catch (Exception e) {
			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
		}
		
		return Metodos.editNavigation(idKey, fromList, openDialog, openWebMobile);
	}
	
	/**
	 * Delete an existing FormaPago entity
	 * 
	 */
	public String deleteEntity(Integer idKey, boolean fromList) {
		
		FacesContext context = FacesContext.getCurrentInstance(); 
		
		try {
			entityService = new FormaPagoServices();
			entity = entityService.findById(idKey);
			entityService.deleteTX(entity);
			
			Metodos.auditLog(Constantes.ACTIONDELETE, FormaPagoMBean.class.getSimpleName(), entity.getNombre(), "");
			
			String msg = Metodos.getMessageResourceString("generics_eliminarOK", new Object[]{entity.getNombre()});
			if(!fromList || (!Metodos.isMobile() && Metodos.isDlg())){
				Metodos.setSessionMesagges(Constantes.SESSIONMSGINFO, msg);
			}
				
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
				
			fillEntityList();
		} catch (Exception e) {
			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
		}
		return  Metodos.close(closeRedirectWeb, closeRedirectMobile, fromList);
		
	}
	
	/**
	 * Metodo que retorna las listas
	 * @return
	 */
	public void fillEntityList(){
		FacesContext context = FacesContext.getCurrentInstance(); 
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		try {
			entityService = new FormaPagoServices();
			entityList = null;
			LoginMBean loginMBean = (LoginMBean)Metodos.getManagedBean(request, "loginMBean");
			entityList = entityService.findByProperty(FormaPagoDAO.IDCOMPANIA , loginMBean.getUsuario().getCompania().getId());
			
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}		
	}
	
	/**
	 * Metodo para guardar
	 */
	public String saveEntity(){
		
		FacesContext context = FacesContext.getCurrentInstance(); 
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		
		try{
			if(mode != null && mode.equals(Constantes.MODECREATE)
					&& hasPermissionTo(Constantes.ACTIONCREATE)){
				
				LoginMBean loginMBean = (LoginMBean)Metodos.getManagedBean(request, "loginMBean");
				entity.setCompania(loginMBean.getUsuario().getCompania());
				entityService = new FormaPagoServices();
				entityService.createTX(entity);
				
				Metodos.auditLog(Constantes.ACTIONCREATE, FormaPagoMBean.class.getSimpleName(), entity.getNombre(), "");
				
				String msg = Metodos.getMessageResourceString("generics_guardarOK", new Object[]{entity.getNombre()});
				
				if(!Metodos.isMobile()){
					Metodos.setSessionMesagges(Constantes.SESSIONMSGINFO, msg);
				}
					
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
					
				return  Metodos.close(closeRedirectWeb, closeRedirectMobile, true);
			}
			
			else if(mode != null && mode.equals(Constantes.MODEEDIT)
					&& hasPermissionTo(Constantes.ACTIONUPDATE)){
				
				entityService = new FormaPagoServices();
				entityService.updateTX(entity);
				
				Metodos.auditLog(Constantes.ACTIONUPDATE, FormaPagoMBean.class.getSimpleName(), entity.getNombre(), "");
				
				String msg = Metodos.getMessageResourceString("generics_actualizarOK", new Object[]{entity.getNombre()});
				
				if(!Metodos.isMobile()){
					Metodos.setSessionMesagges(Constantes.SESSIONMSGINFO, msg);
				}
					
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
					
				return  Metodos.close(closeRedirectWeb, closeRedirectMobile, true);
			}
					
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}
		
		return "";
	}

	/**
	 * Metodo que se ejecuta cuando se cierra una modal
	 * @return
	 */
	public void dialogListener(){
		fillEntityList();
		Metodos.loadSessionMesagges();
	}
	
	/* SECURITY METHODS*/
	/**
	 * Método que valida si el usuario en sesión puede ejecutar una acción sobre este documento
	 * 
	 * @return
	 */
	public boolean hasPermissionTo(String action){		
		return Metodos.hasPermission(FormaPagoMBean.class.getSimpleName(), action);
	}
	
	/* GETTERS AND SETTERS */

	public FormaPago getEntity() {
		return entity;
	}

	public void setEntity(FormaPago entity) {
		this.entity = entity;
	}

	public List<FormaPago> getEntityList() {
		return entityList;
	}

	public void setEntityList(List<FormaPago> entityList) {
		this.entityList = entityList;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public List<FormaPago> getEntityFilterList() {
		return entityFilterList;
	}

	public void setEntityFilterList(List<FormaPago> entityFilterList) {
		this.entityFilterList = entityFilterList;
	}
	
}
