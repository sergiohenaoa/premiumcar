/**
	 * 	PlantillaMBean.java:
	 * 
	 * 	Esta clase corresponde al Managed Bean para controlar el CRUD de 
	 * 	la entidad Plantilla
	 *
	 * @author Sergio Henao - sergiohenaoa@gmail.com - 11/04/2015
	 * @Modifier Sergio Henao - sergiohenaoa@gmail.com - 11/04/2015
	 * @version 1.0
	 * 
	 */
package com.sh.pvs.view.configuracion;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import com.sh.pvs.core.interfaces.configuracion.IPlantillaServices;
import com.sh.pvs.core.services.configuracion.PlantillaServices;
import com.sh.pvs.model.dao.configuracion.PlantillaDAO;
import com.sh.pvs.model.dto.configuracion.Plantilla;
import com.sh.pvs.view.seguridad.LoginMBean;
import com.sh.pvs.view.utils.Constantes;
import com.sh.pvs.view.utils.ControlException;
import com.sh.pvs.view.utils.Metodos;

@ManagedBean
@ViewScoped
public class PlantillaMBean implements Serializable {

	/*Serializable Attribute*/
	private static final long serialVersionUID = 1L;
	
	/*Variables*/
	private Plantilla entity; 							// ViewForm - EditForm 	
 	private List<Plantilla> entityList; 				// Lista de registros del datatable
 	private List<Plantilla> entityFilterList;			// Lista de registros filtrados del datatable
 	private UploadedFile archivoSubido;
 	
 	/*Navegación*/
 	private String mode;								// view - edit - list - create ==> Modo para identificar la vista
 	
 	/*Servicios*/
 	private IPlantillaServices entityService;			// Servicios para la gestión de la entidad
	
 	/*Variables para navegación*/
 	public static String openDialog = "/configuracion/plantilla/plantillaForm.faces";
 	public static String openWebMobile = "plantillaForm.faces";
 	public static String closeRedirectMobile = "plantillaList.faces";
	public static String closeRedirectWeb = "/configuracion/plantilla/plantillaList.faces?faces-redirect=true";
 	
 	/*Constructors*/	
	/**	Default constructor */
	public PlantillaMBean() {

	}
	
	/*METHODS*/
	
	/**
	 * Método para inicializar las variables
	 * 
	 * */
	@PostConstruct
	private void init(){
		
		FacesContext context = FacesContext.getCurrentInstance();
		
		try {
			
			Metodos.loadSessionMesagges();
			
			if(context.getExternalContext().getRequestParameterMap().get("mode") != null)
				mode = context.getExternalContext().getRequestParameterMap().get("mode");
			else
				mode = Constantes.MODELIST;
			
			if(mode.equals(Constantes.MODECREATE)){
				this.entity = new Plantilla();
				return;
			}
			
			if(mode.equals(Constantes.MODELIST)){
				fillEntityList();
				return;
			}
						
			if(mode.equals(Constantes.MODEVIEW) || mode.equals(Constantes.MODEEDIT)){
				
				try {
					
					String idParam = context.getExternalContext().getRequestParameterMap().get("id");
					entityService = new PlantillaServices();
					this.entity = entityService.findById(Integer.parseInt(idParam));
							
				} catch (Exception e) {
					context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
				}
				
				return;
			}
			
		} catch (Exception e) {
			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
		}
		
	}
	
	/**
	 * Create a new Plantilla entity
	 * 
	 */
	public String newEntity() {
		
		entity = new Plantilla();
		return Metodos.newNavigation(openDialog, openWebMobile);
		
	}
	
	/**
	 * Select an existing Plantilla entity
	 * 
	 */
	public String selectEntity(Integer idKey) {				
		
		FacesContext context = FacesContext.getCurrentInstance();
		
		try {
			
			entityService = new PlantillaServices();
			entity = entityService.findById(idKey);
			
		} catch (Exception e) {
			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
		}
		
		return Metodos.selectNavigation(idKey, openDialog, openWebMobile);
	}
	
	/**
	 * Edit an existing Plantilla entity
	 * 
	 */
	public String editEntity(Integer idKey, boolean fromList) {
		
		FacesContext context = FacesContext.getCurrentInstance();
		
		try {
			
			entityService = new PlantillaServices();
			entity = entityService.findById(idKey);
			
		} catch (Exception e) {
			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
		}
		
		return Metodos.editNavigation(idKey, fromList, openDialog, openWebMobile);
	}
	
	/**
	 * Delete an existing Plantilla entity
	 * 
	 */
	public String deleteEntity(Integer idKey, boolean fromList) {
		
		FacesContext context = FacesContext.getCurrentInstance(); 
		
		try {
			entityService = new PlantillaServices();
			entity = entityService.findById(idKey);
			entityService.deleteTX(entity);
			
			Metodos.auditLog(Constantes.ACTIONDELETE, PlantillaMBean.class.getSimpleName(), entity.getNombre(), "");
			
			String msg = Metodos.getMessageResourceString("generics_eliminarOK", new Object[]{entity.getNombre()});
			if(!fromList || (!Metodos.isMobile() && Metodos.isDlg())){
				Metodos.setSessionMesagges(Constantes.SESSIONMSGINFO, msg);
			}
				
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
				
			fillEntityList();
		} catch (Exception e) {
			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
		}
		return  Metodos.close(closeRedirectWeb, closeRedirectMobile, fromList);
		
	}
	
	/**
	 * Metodo que retorna las listas
	 * @return
	 */
	public void fillEntityList(){
		FacesContext context = FacesContext.getCurrentInstance(); 
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		try {
			entityService = new PlantillaServices();
			entityList = null;
			LoginMBean loginMBean = (LoginMBean)Metodos.getManagedBean(request, "loginMBean");
			entityList = entityService.findByProperty(PlantillaDAO.IDCOMPANIA , loginMBean.getUsuario().getCompania().getId());
			
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}		
	}
	
	/**
	 * Metodo para guardar
	 */
	public String saveEntity(){
		
		FacesContext context = FacesContext.getCurrentInstance(); 
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		
		try{
			if(mode != null && mode.equals(Constantes.MODECREATE)
					&& hasPermissionTo(Constantes.ACTIONCREATE)){
				
				LoginMBean loginMBean = (LoginMBean)Metodos.getManagedBean(request, "loginMBean");
				entity.setCompania(loginMBean.getUsuario().getCompania());
				entityService = new PlantillaServices();
				entityService.createTX(entity);
				
				Metodos.auditLog(Constantes.ACTIONCREATE, PlantillaMBean.class.getSimpleName(), entity.getNombre(), "");
				
				String msg = Metodos.getMessageResourceString("generics_guardarOK", new Object[]{entity.getNombre()});
				
				if(!Metodos.isMobile()){
					Metodos.setSessionMesagges(Constantes.SESSIONMSGINFO, msg);
				}
					
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
					
				return  Metodos.close(closeRedirectWeb, closeRedirectMobile, true);
			}
			
			else if(mode != null && mode.equals(Constantes.MODEEDIT)
					&& hasPermissionTo(Constantes.ACTIONUPDATE)){
				
				entityService = new PlantillaServices();
				entityService.updateTX(entity);
				
				Metodos.auditLog(Constantes.ACTIONUPDATE, PlantillaMBean.class.getSimpleName(), entity.getNombre(), "");
				
				String msg = Metodos.getMessageResourceString("generics_actualizarOK", new Object[]{entity.getNombre()});
				
				if(!Metodos.isMobile()){
					Metodos.setSessionMesagges(Constantes.SESSIONMSGINFO, msg);
				}
					
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
					
				return  Metodos.close(closeRedirectWeb, closeRedirectMobile, true);
			}
					
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}
		
		return "";
	}

	/**
	 * Metodo que se ejecuta cuando se cierra una modal
	 * @return
	 */
	public void dialogListener(){
		fillEntityList();
		Metodos.loadSessionMesagges();
	}
	
	public void fileListener(FileUploadEvent event){
		FacesContext context = FacesContext.getCurrentInstance(); 
		
		try {
			archivoSubido = event.getFile();
			entity.setNombreArchivo(archivoSubido.getFileName());
			entity.setTipoArchivo(archivoSubido.getContentType());
			entity.setSizeArchivo(Integer.parseInt(Long.toString(archivoSubido.getSize())));
			entity.setAdjunto(com.sh.pvs.utils.Metodos.encodeFile(archivoSubido.getInputstream(), archivoSubido.getSize()));

		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}
	}
	
	/**
	 * Método que retorna el flujo de String del archivo a descargar
	 * @param itemProducto
	 * @return String
	 */
	public String descargarArchivo(){
		FacesContext context = FacesContext.getCurrentInstance(); 
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		try {
			if(entity.getAdjunto() == null
					|| entity.getAdjunto().equals("")){
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se ha seleccionado ningún archivo", "")); // MENSAJE
				return "";
			}
			
			response.setContentType(entity.getTipoArchivo());
			response.setHeader("Content-disposition", "attachment;filename=" + entity.getNombreArchivo());
			
			
			byte[] decodedFile = com.sh.pvs.utils.Metodos.decodeFile(entity.getAdjunto());
			InputStream fileToDownload = new ByteArrayInputStream(decodedFile);
			
			PrintWriter out = response.getWriter();
            int c;
            while ((c = fileToDownload.read()) != -1) {
                out.write(c);
            }
            
            out.flush();
            out.close();
            fileToDownload.close();
            context.responseComplete();
            
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}
		return "";
	}
	
	/* SECURITY METHODS*/
	/**
	 * Método que valida si el usuario en sesión puede ejecutar una acción sobre este documento
	 * 
	 * @return
	 */
	public boolean hasPermissionTo(String action){		
		return Metodos.hasPermission(PlantillaMBean.class.getSimpleName(), action);
	}
	
	/* GETTERS AND SETTERS */

	public Plantilla getEntity() {
		return entity;
	}

	public void setEntity(Plantilla entity) {
		this.entity = entity;
	}

	public List<Plantilla> getEntityList() {
		return entityList;
	}

	public void setEntityList(List<Plantilla> entityList) {
		this.entityList = entityList;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public List<Plantilla> getEntityFilterList() {
		return entityFilterList;
	}

	public void setEntityFilterList(List<Plantilla> entityFilterList) {
		this.entityFilterList = entityFilterList;
	}
	
	public UploadedFile getArchivoSubido() {
		return archivoSubido;
	}
	
	public void setArchivoSubido(UploadedFile archivoSubido) {
		this.archivoSubido = archivoSubido;
	}
	
}
