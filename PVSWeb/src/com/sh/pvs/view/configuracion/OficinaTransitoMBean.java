/**
	 * 	OficinaTransitoMBean.java:
	 * 
	 * 	Esta clase corresponde al Managed Bean para controlar el CRUD de 
	 * 	la entidad OficinaTransito
	 *
	 * @author Sergio Henao - sergiohenaoa@gmail.com - 11/04/2015
	 * @Modifier Sergio Henao - sergiohenaoa@gmail.com - 11/04/2015
	 * @version 1.0
	 * 
	 */
package com.sh.pvs.view.configuracion;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.sh.pvs.core.interfaces.configuracion.IGastoServices;
import com.sh.pvs.core.interfaces.configuracion.IOficinaTransitoServices;
import com.sh.pvs.core.services.configuracion.GastoServices;
import com.sh.pvs.core.services.configuracion.OficinaTransitoServices;
import com.sh.pvs.model.dao.configuracion.OficinaTransitoDAO;
import com.sh.pvs.model.dto.configuracion.Gasto;
import com.sh.pvs.model.dto.configuracion.OficinaTransito;
import com.sh.pvs.model.dto.configuracion.TramitexOficinaTransito;
import com.sh.pvs.model.dto.seguridad.Usuario;
import com.sh.pvs.view.utils.Constantes;
import com.sh.pvs.view.utils.ControlException;
import com.sh.pvs.view.utils.Metodos;

@ManagedBean
@ViewScoped
public class OficinaTransitoMBean implements Serializable {

	/*Serializable Attribute*/
	private static final long serialVersionUID = 1L;
	
	/*Variables*/
	private OficinaTransito entity; 						// ViewForm - EditForm 	
 	private List<OficinaTransito> entityList; 				// Lista de registros del datatable
 	private List<OficinaTransito> entityFilterList;			// Lista de registros filtrados del datatable
 	private TramitexOficinaTransito tramite;
 	private List<Gasto> gastosDropDownList;
 	private Usuario loggedUser;								//Usuario logeado (En Sesión)
 	
 	/*Navegación*/
 	private String mode;									// view - edit - list - create ==> Modo para identificar la vista
 	
 	/*Servicios*/
 	private IOficinaTransitoServices entityService;			// Servicios para la gestión de la entidad
	
 	/*Variables para navegación*/
 	public static String openDialog = "/configuracion/oficinaTransito/oficinaTransitoForm.faces";
 	public static String openWebMobile = "oficinaTransitoForm.faces";
 	public static String closeRedirectMobile = "oficinaTransitoList.faces";
	public static String closeRedirectWeb = "/configuracion/oficinaTransito/oficinaTransitoList.faces?faces-redirect=true";
 	
 	/*Constructors*/	
	/**	Default constructor */
	public OficinaTransitoMBean() {

	}
	
	/*METHODS*/
	
	/**
	 * Método para inicializar las variables
	 * 
	 * */
	@PostConstruct
	private void init(){
		
		FacesContext context = FacesContext.getCurrentInstance();
		
		try {
			
			tramite = new TramitexOficinaTransito();
			tramite.setGasto(new Gasto());
			
			Metodos.loadSessionMesagges();
			
			loggedUser = Metodos.getUserSession();
			
			if(context.getExternalContext().getRequestParameterMap().get("mode") != null)
				mode = context.getExternalContext().getRequestParameterMap().get("mode");
			else
				mode = Constantes.MODELIST;
			
			if(mode.equals(Constantes.MODECREATE)){
				this.entity = new OficinaTransito();
				return;
			}
			
			if(mode.equals(Constantes.MODELIST)){
				fillEntityList();
				return;
			}
						
			if(mode.equals(Constantes.MODEVIEW) || mode.equals(Constantes.MODEEDIT)){
				
				try {
					
					String idParam = context.getExternalContext().getRequestParameterMap().get("id");
					entityService = new OficinaTransitoServices();
					this.entity = entityService.findById(Integer.parseInt(idParam));
					
					if(mode.equals(Constantes.MODEEDIT)){
						fillGastosDropDown();
					}
							
				} catch (Exception e) {
					context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
				}
				
				return;
			}
			
		} catch (Exception e) {
			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
		}
		
	}
	
	/**
	 * Create a new OficinaTransito entity
	 * 
	 */
	public String newEntity() {
		
		entity = new OficinaTransito();
		return Metodos.newNavigation(openDialog, openWebMobile);
		
	}
	
	/**
	 * Select an existing OficinaTransito entity
	 * 
	 */
	public String selectEntity(Integer idKey) {				
		
		FacesContext context = FacesContext.getCurrentInstance();
		
		try {
			
			entityService = new OficinaTransitoServices();
			entity = entityService.findById(idKey);
			
		} catch (Exception e) {
			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
		}
		
		return Metodos.selectNavigation(idKey, openDialog, openWebMobile);
	}
	
	/**
	 * Edit an existing OficinaTransito entity
	 * 
	 */
	public String editEntity(Integer idKey, boolean fromList) {
		
		FacesContext context = FacesContext.getCurrentInstance();
		
		try {
			
			entityService = new OficinaTransitoServices();
			entity = entityService.findById(idKey);
			
		} catch (Exception e) {
			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
		}
		
		return Metodos.editNavigation(idKey, fromList, openDialog, openWebMobile);
	}
	
	/**
	 * Delete an existing OficinaTransito entity
	 * 
	 */
	public String deleteEntity(Integer idKey, boolean fromList) {
		
		FacesContext context = FacesContext.getCurrentInstance(); 
		
		try {
			entityService = new OficinaTransitoServices();
			entity = entityService.findById(idKey);
			entityService.deleteTX(entity);
			
			Metodos.auditLog(Constantes.ACTIONDELETE, OficinaTransitoMBean.class.getSimpleName(), entity.getNombre(), "");
			
			String msg = Metodos.getMessageResourceString("generics_eliminarOK", new Object[]{entity.getNombre()});
			if(!fromList || (!Metodos.isMobile() && Metodos.isDlg())){
				Metodos.setSessionMesagges(Constantes.SESSIONMSGINFO, msg);
			}
				
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
				
			fillEntityList();
		} catch (Exception e) {
			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
		}
		return  Metodos.close(closeRedirectWeb, closeRedirectMobile, fromList);
		
	}
	
	/**
	 * Metodo que retorna las listas
	 * @return
	 */
	public void fillEntityList(){
		FacesContext context = FacesContext.getCurrentInstance(); 
		try {
			entityService = new OficinaTransitoServices();
			entityList = null;
			entityList = entityService.findByProperty(OficinaTransitoDAO.IDCOMPANIA , loggedUser.getCompania().getId());
			
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}		
	}
	
	/**
	 * Metodo para guardar
	 */
	public String saveEntity(){
		
		FacesContext context = FacesContext.getCurrentInstance(); 
		
		try{
			if(mode != null && mode.equals(Constantes.MODECREATE)
					&& hasPermissionTo(Constantes.ACTIONCREATE)){
				
				entity.setCompania(loggedUser.getCompania());
				entityService = new OficinaTransitoServices();
				entityService.createTX(entity);
				
				Metodos.auditLog(Constantes.ACTIONCREATE, OficinaTransitoMBean.class.getSimpleName(), entity.getNombre(), "");
				
				String msg = Metodos.getMessageResourceString("generics_guardarOK", new Object[]{entity.getNombre()});
				
				if(!Metodos.isMobile()){
					Metodos.setSessionMesagges(Constantes.SESSIONMSGINFO, msg);
				}
					
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
					
				return  Metodos.close(closeRedirectWeb, closeRedirectMobile, true);
			}
			
			else if(mode != null && mode.equals(Constantes.MODEEDIT)
					&& hasPermissionTo(Constantes.ACTIONUPDATE)){
				
				entityService = new OficinaTransitoServices();
				entityService.updateTX(entity);
				
				Metodos.auditLog(Constantes.ACTIONUPDATE, OficinaTransitoMBean.class.getSimpleName(), entity.getNombre(), "");
				
				String msg = Metodos.getMessageResourceString("generics_actualizarOK", new Object[]{entity.getNombre()});
				
				if(!Metodos.isMobile()){
					Metodos.setSessionMesagges(Constantes.SESSIONMSGINFO, msg);
				}
					
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
					
				return  Metodos.close(closeRedirectWeb, closeRedirectMobile, true);
			}
					
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}
		
		return "";
	}

	/**
	 * Metodo que se ejecuta cuando se cierra una modal
	 * @return
	 */
	public void dialogListener(){
		fillEntityList();
		Metodos.loadSessionMesagges();
	}
	
	/**
	 * Metodo para guardar una entidad de asociación
	 */
	public String saveTramite(){
		
		FacesContext context = FacesContext.getCurrentInstance(); 
		
		try{
			if(mode != null && mode.equals(Constantes.MODEEDIT)
					&& hasPermissionTo(Constantes.ACTIONUPDATE)){
				
				entityService = new OficinaTransitoServices();
				entityService.addTramite(entity, tramite);
				entity = entityService.findById(entity.getId());
				tramite = new TramitexOficinaTransito();
				tramite.setGasto(new Gasto());
				
				Metodos.auditLog(Constantes.ACTIONUPDATE, OficinaTransitoMBean.class.getSimpleName(), entity.getNombre(), "Trámite asociado: " + tramite.getGasto().getNombre());
				
				String msg = Metodos.getMessageResourceString("generics_actualizarOK", new Object[]{entity.getNombre()});
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
					
			}
					
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}
		
		return "";
	}
	
	/**
	 * Metodo para eliminar una entidad de asociación
	 */
	public String deleteTramite(TramitexOficinaTransito tramiteToDelete){
		
		FacesContext context = FacesContext.getCurrentInstance(); 
		
		try{
			if(mode != null && mode.equals(Constantes.MODEEDIT)
					&& hasPermissionTo(Constantes.ACTIONUPDATE)){
				
				entityService = new OficinaTransitoServices();
				entityService.deleteTramite(tramiteToDelete);
				entity = entityService.findById(entity.getId());
				
				Metodos.auditLog(Constantes.ACTIONUPDATE, OficinaTransitoMBean.class.getSimpleName(), entity.getNombre(), "Trámite eliminado: " + tramiteToDelete.getGasto().getNombre());
				
				String msg = Metodos.getMessageResourceString("generics_actualizarOK", new Object[]{entity.getNombre()});
				
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
					
			}
					
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}
		
		return "";
	}
	
	/**
	 * Método que llena la lista de gastos activos para el selector
	 * @param 
	 */
	private void fillGastosDropDown(){
		FacesContext context = FacesContext.getCurrentInstance(); 
		gastosDropDownList = null;
		try {
			IGastoServices gastoSrv = new GastoServices();
			gastosDropDownList = gastoSrv.findAllActivosByCiaAndTraspaso(loggedUser.getCompania().getId());
		} catch (Exception e) {
			gastosDropDownList = null;
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}		
	}
	
	/**
	 * Metodo que se ejecuta cuando se selecciona un gasto
	 * @return
	 */
	public void changeGastoListener(){
		try {
			IGastoServices gastoSrv = new GastoServices();
			Gasto gasto = gastoSrv.findById(tramite.getGasto().getId());
			if (gasto != null) {
				tramite.setDescripcion(gasto.getDescripcion());
				tramite.setValor(gasto.getValor());
			}
		} catch (Exception e) {
		}
	}
	
	/* SECURITY METHODS*/
	/**
	 * Método que valida si el usuario en sesión puede ejecutar una acción sobre este documento
	 * 
	 * @return
	 */
	public boolean hasPermissionTo(String action){		
		return Metodos.hasPermission(OficinaTransitoMBean.class.getSimpleName(), action);
	}
	
	/* GETTERS AND SETTERS */

	public OficinaTransito getEntity() {
		return entity;
	}

	public void setEntity(OficinaTransito entity) {
		this.entity = entity;
	}

	public List<OficinaTransito> getEntityList() {
		return entityList;
	}

	public void setEntityList(List<OficinaTransito> entityList) {
		this.entityList = entityList;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public List<OficinaTransito> getEntityFilterList() {
		return entityFilterList;
	}

	public void setEntityFilterList(List<OficinaTransito> entityFilterList) {
		this.entityFilterList = entityFilterList;
	}

	public TramitexOficinaTransito getTramite() {
		return tramite;
	}
	
	public void setTramite(TramitexOficinaTransito tramite) {
		this.tramite = tramite;
	}

	public List<Gasto> getGastosDropDownList() {
		return gastosDropDownList;
	}

	public void setGastosDropDownList(List<Gasto> gastosDropDownList) {
		this.gastosDropDownList = gastosDropDownList;
	}
	
}
