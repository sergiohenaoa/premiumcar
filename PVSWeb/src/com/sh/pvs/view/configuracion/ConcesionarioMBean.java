/**
	 * 	ConcesionarioMBean.java:
	 * 
	 * 	Esta clase corresponde al Managed Bean para controlar el CRUD de 
	 * 	la entidad Concesionario
	 *
	 * @author Sergio Henao - sergiohenaoa@gmail.com - 11/04/2015
	 * @Modifier Sergio Henao - sergiohenaoa@gmail.com - 11/04/2015
	 * @version 1.0
	 * 
	 */
package com.sh.pvs.view.configuracion;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.primefaces.context.RequestContext;

import com.sh.pvs.core.interfaces.configuracion.IConcesionarioServices;
import com.sh.pvs.core.services.configuracion.ConcesionarioServices;
import com.sh.pvs.model.dao.configuracion.ConcesionarioDAO;
import com.sh.pvs.model.dto.configuracion.Concesionario;
import com.sh.pvs.view.seguridad.LoginMBean;
import com.sh.pvs.view.utils.Constantes;
import com.sh.pvs.view.utils.ControlException;
import com.sh.pvs.view.utils.Metodos;

@ManagedBean
@ViewScoped
public class ConcesionarioMBean implements Serializable {

	/*Serializable Attribute*/
	private static final long serialVersionUID = 1L;
	
	/*Variables*/
	private Concesionario entity; 							// ViewForm - EditForm 	
 	private List<Concesionario> entityList; 					// Lista de registros del datatable
 	private List<Concesionario> entityFilterList;			// Lista de registros filtrados del datatable
 	
 	/*Navegación*/
 	private String mode;									// view - edit - list - create ==> Modo para identificar la vista
 	private boolean noOptions;
 	
 	/*Servicios*/
 	private IConcesionarioServices entityService;			// Servicios para la gestión de la entidad
	
 	/*Variables para navegación*/
 	public static String openDialog = "/configuracion/concesionario/concesionarioForm.faces";
 	public static String openWebMobile = "concesionarioForm.faces";
 	public static String closeRedirectMobile = "concesionarioList.faces";
	public static String closeRedirectWeb = "/configuracion/concesionario/concesionarioList.faces?faces-redirect=true";
 	
 	/*Constructors*/	
	/**	Default constructor */
	public ConcesionarioMBean() {

	}
	
	/*METHODS*/
	
	/**
	 * Método para inicializar las variables
	 * 
	 * */
	@PostConstruct
	private void init(){
		
		FacesContext context = FacesContext.getCurrentInstance();
		
		try {
			
			Metodos.loadSessionMesagges();
			
			if(context.getExternalContext().getRequestParameterMap().get("mode") != null)
				mode = context.getExternalContext().getRequestParameterMap().get("mode");
			else
				mode = Constantes.MODELIST;
			
			if(mode.equals(Constantes.MODECREATE)){
				this.entity = new Concesionario();
				if(context.getExternalContext().getRequestParameterMap().get("nit") != null)
					this.entity.setNit(context.getExternalContext().getRequestParameterMap().get("nit"));
				return;
			}
			
			if(mode.equals(Constantes.MODELIST)){
				fillEntityList();
				return;
			}
						
			if(mode.equals(Constantes.MODEVIEW) || mode.equals(Constantes.MODEEDIT)){
				
				try {
					
					String noOptionsStr = context.getExternalContext().getRequestParameterMap().get("noOptions");
					if(noOptionsStr != null && noOptionsStr.equals("true"))
						noOptions = true;
					
					String idParam = context.getExternalContext().getRequestParameterMap().get("id");
					entityService = new ConcesionarioServices();
					this.entity = entityService.findById(Integer.parseInt(idParam));
							
				} catch (Exception e) {
					context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
				}
				
				return;
			}
			
		} catch (Exception e) {
			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
		}
		
	}
	
	/**
	 * Create a new Concesionario entity
	 * 
	 */
	public String newEntity() {
		
		entity = new Concesionario();
		return Metodos.newNavigation(openDialog, openWebMobile);
		
	}
	
	/**
	 * Create a new Concesionario entity
	 * 
	 */
	public String newEntity(String nit) {
		
		entity = new Concesionario();
		entity.setNit(nit);
		return this.newConcesionarioNavigation(openDialog, openWebMobile, nit);
		
	}
	
	/**
	 * Select an existing Concesionario entity
	 * 
	 */
	public String selectEntity(Integer idKey) {				
		
		FacesContext context = FacesContext.getCurrentInstance();
		
		try {
			
			entityService = new ConcesionarioServices();
			entity = entityService.findById(idKey);
			
		} catch (Exception e) {
			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
		}
		
		return Metodos.selectNavigation(idKey, openDialog, openWebMobile);
	}
	
	/**
	 * Select an existing Concesionario entity
	 * 
	 */
	public String selectEntityFromRetoma() {
		
		FacesContext context = FacesContext.getCurrentInstance();
		
		Integer idKey = 0;
		try {
			entityService = new ConcesionarioServices();
			
			HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
			String nit = request.getParameter("frm:autoCompleteConcesionarioID_input");
			
			if(nit != null && !nit.trim().equals("")
					&& nit != null && nit.contains(" - ")){
				entity = entityService.findConcesionarioByNitCia(nit.split(" - ")[0], Metodos.getUserSession().getCompania().getId());
				idKey = entity.getId();
				noOptions = false;
			}else{
				nit = request.getParameter("frm:autoCompleteConcesionarioIDView");
				
				if(nit != null && !nit.trim().equals("")){
					entity = entityService.findConcesionarioByNitCia(nit, Metodos.getUserSession().getCompania().getId());
					idKey = entity.getId();
					noOptions = true;
				}else{
					return "";
				}
			}
			
		} catch (Exception e) {
			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
		}
		
		if(noOptions)
			return Metodos.selectNavigationNoOptions(idKey, openDialog, openWebMobile);
		
		return Metodos.selectNavigation(idKey, openDialog, openWebMobile);
		
	}
	
	/**
	 * Edit an existing Concesionario entity
	 * 
	 */
	public String editEntity(Integer idKey, boolean fromList) {
		
		FacesContext context = FacesContext.getCurrentInstance();
		
		try {
			
			entityService = new ConcesionarioServices();
			entity = entityService.findById(idKey);
			
		} catch (Exception e) {
			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
		}
		
		return Metodos.editNavigation(idKey, fromList, openDialog, openWebMobile);
	}
	
	/**
	 * Delete an existing Concesionario entity
	 * 
	 */
	public String deleteEntity(Integer idKey, boolean fromList) {
		
		FacesContext context = FacesContext.getCurrentInstance(); 
		
		try {
			entityService = new ConcesionarioServices();
			entity = entityService.findById(idKey);
			entityService.deleteTX(entity);
			
			Metodos.auditLog(Constantes.ACTIONDELETE, ConcesionarioMBean.class.getSimpleName(), entity.getNombre(), "");
			
			String msg = Metodos.getMessageResourceString("generics_eliminarOK", new Object[]{entity.getNombre()});
			if(!fromList || (!Metodos.isMobile() && Metodos.isDlg())){
				Metodos.setSessionMesagges(Constantes.SESSIONMSGINFO, msg);
			}
				
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
				
			fillEntityList();
		} catch (Exception e) {
			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
		}
		return  Metodos.close(closeRedirectWeb, closeRedirectMobile, fromList);
		
	}
	
	/**
	 * Metodo que retorna las listas
	 * @return
	 */
	public void fillEntityList(){
		FacesContext context = FacesContext.getCurrentInstance(); 
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		try {
			entityService = new ConcesionarioServices();
			entityList = null;
			LoginMBean loginMBean = (LoginMBean)Metodos.getManagedBean(request, "loginMBean");
			entityList = entityService.findByProperty(ConcesionarioDAO.IDCOMPANIA , loginMBean.getUsuario().getCompania().getId());
			
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}		
	}
	
	/**
	 * Metodo para guardar
	 */
	public String saveEntity(){
		
		FacesContext context = FacesContext.getCurrentInstance(); 
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		
		try{
			if(mode != null && mode.equals(Constantes.MODECREATE)
					&& hasPermissionTo(Constantes.ACTIONCREATE)){
				
				LoginMBean loginMBean = (LoginMBean)Metodos.getManagedBean(request, "loginMBean");
				entity.setCompania(loginMBean.getUsuario().getCompania());
				entityService = new ConcesionarioServices();
				entityService.createTX(entity);
				
				Metodos.auditLog(Constantes.ACTIONCREATE, ConcesionarioMBean.class.getSimpleName(), entity.getNombre(), "");
				
				String msg = Metodos.getMessageResourceString("generics_guardarOK", new Object[]{entity.getNombre()});
				
				if(!Metodos.isMobile()){
					Metodos.setSessionMesagges(Constantes.SESSIONMSGINFO, msg);
				}
					
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
					
				return  Metodos.close(closeRedirectWeb, closeRedirectMobile, true);
			}
			
			else if(mode != null && mode.equals(Constantes.MODEEDIT)
					&& hasPermissionTo(Constantes.ACTIONUPDATE)){
				
				entityService = new ConcesionarioServices();
				entityService.updateTX(entity);
				
				Metodos.auditLog(Constantes.ACTIONUPDATE, ConcesionarioMBean.class.getSimpleName(), entity.getNombre(), "");
				
				String msg = Metodos.getMessageResourceString("generics_actualizarOK", new Object[]{entity.getNombre()});
				
				if(!Metodos.isMobile()){
					Metodos.setSessionMesagges(Constantes.SESSIONMSGINFO, msg);
				}
					
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
					
				return  Metodos.close(closeRedirectWeb, closeRedirectMobile, true);
			}
					
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}
		
		return "";
	}

	/**
	 * Metodo que se ejecuta cuando se cierra una modal
	 * @return
	 */
	public void dialogListener(){
		fillEntityList();
		Metodos.loadSessionMesagges();
	}
	
	/**
	 * Método que define la navegación cuando se da clic en el botón nuevo
	 * @param openCreateDialog
	 * @param openCreateWebMobile
	 * @return
	 */
	public String newConcesionarioNavigation(String openCreateDialog, String openCreateWebMobile, String nit){
		FacesContext context = FacesContext.getCurrentInstance(); 
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		if(!Metodos.isMobile()){
			
			//Si la navegación es en web se abren modales
			Map<String,Object> options = new HashMap<String, Object>();
	        options.put("modal", true);
	        options.put("resizable", false);
	        options.put("contentHeight", 450);
	        options.put("contentWidth", 600);
	        
	        Map<String, List<String>> params = new HashMap<String, List<String>>();
		       
	        List<String> values = new ArrayList<String>();
	        values.add(Constantes.MODECREATE);
	        params.put("mode", values);
	        
	        List<String> valuesnit = new ArrayList<String>();	        
	        valuesnit.add(nit);
	        params.put("nit", valuesnit);
	        
			RequestContext.getCurrentInstance().openDialog(openCreateDialog, options, params);
			
		}else{
			//Si la navegación es en dispositivos móviles, se redirecciona
			try {
				response.sendRedirect(openCreateWebMobile + "?mode=" + Constantes.MODECREATE + "&nit=" + nit);
			} catch (Exception e) {
				return "";
			}
			
		}
		
		return "";
	}
	
	/* SECURITY METHODS*/
	/**
	 * Método que valida si el usuario en sesión puede ejecutar una acción sobre este documento
	 * 
	 * @return
	 */
	public boolean hasPermissionTo(String action){		
		return Metodos.hasPermission(ConcesionarioMBean.class.getSimpleName(), action);
	}
	
	/* GETTERS AND SETTERS */

	public Concesionario getEntity() {
		return entity;
	}

	public void setEntity(Concesionario entity) {
		this.entity = entity;
	}

	public List<Concesionario> getEntityList() {
		return entityList;
	}

	public void setEntityList(List<Concesionario> entityList) {
		this.entityList = entityList;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public List<Concesionario> getEntityFilterList() {
		return entityFilterList;
	}

	public void setEntityFilterList(List<Concesionario> entityFilterList) {
		this.entityFilterList = entityFilterList;
	}

	
	public boolean isNoOptions() {
		return noOptions;
	}
	

	public void setNoOptions(boolean noOptions) {
		this.noOptions = noOptions;
	}
	
}
