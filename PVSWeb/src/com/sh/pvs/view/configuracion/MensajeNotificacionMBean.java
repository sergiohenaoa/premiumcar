/**
	 * 	MensajeNotificacionMBean.java:
	 * 
	 * 	Esta clase corresponde al Managed Bean para controlar el CRUD de 
	 * 	la entidad MensajeNotificacion
	 *
	 * @author Jhoel Acosta - jhoel.acosta@e-deas.com.co - 11/03/2015
	 * @Modifier Jhoel Acosta - jhoel.acosta@e-deas.com.co - 11/03/2015
	 * @version 1.0
	 * 
	 */
package com.sh.pvs.view.configuracion;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import com.sh.pvs.core.interfaces.configuracion.IMensajeNotificacionServices;
import com.sh.pvs.core.services.configuracion.MensajeNotificacionServices;
import com.sh.pvs.model.dao.configuracion.MensajeNotificacionDAO;
import com.sh.pvs.model.dto.configuracion.MensajeNotificacion;
import com.sh.pvs.view.seguridad.LoginMBean;
import com.sh.pvs.view.utils.Constantes;
import com.sh.pvs.view.utils.ControlException;
import com.sh.pvs.view.utils.Metodos;

@ManagedBean
@ViewScoped
public class MensajeNotificacionMBean implements Serializable {

	/*Serializable Attribute*/
	private static final long serialVersionUID = 1L;
	
	/*Variables*/
	private MensajeNotificacion entity; 							// ViewForm - EditForm 	
 	private List<MensajeNotificacion> entityList; 					// Lista de registros del datatable
 	private List<MensajeNotificacion> entityFilterList;				// Lista de registros filtrados del datatable
 	
 	/*Navegación*/
 	private String mode;											// view - edit - list - create ==> Modo para identificar la vista
 	
 	/*Servicios*/
 	private IMensajeNotificacionServices entityService;				// Servicios para la gestión de la entidad
	
 	/*Variables para navegación*/
 	public static String openDialog = "/configuracion/mensajenotificacion/mensajeNotificacionForm.faces";
 	public static String openWebMobile = "mensajeNotificacionForm.faces";
 	public static String closeRedirectMobile = "mensajeNotificacionList.faces";
	public static String closeRedirectWeb = "/configuracion/mensajenotificacion/mensajeNotificacionList.faces?faces-redirect=true";
 	
 	/*Constructors*/	
	/**	Default constructor */
	public MensajeNotificacionMBean() {

	}
	
	/*METHODS*/
	
	/**
	 * Método para inicializar las variables
	 * 
	 * */
	@PostConstruct
	private void init(){
		
		FacesContext context = FacesContext.getCurrentInstance();
		
		try {
			
			Metodos.loadSessionMesagges();
			
			if(context.getExternalContext().getRequestParameterMap().get("mode") != null)
				mode = context.getExternalContext().getRequestParameterMap().get("mode");
			else
				mode = Constantes.MODELIST;
			
			if(mode.equals(Constantes.MODECREATE)){
				this.entity = new MensajeNotificacion();
				return;
			}
			
			if(mode.equals(Constantes.MODELIST)){
				fillEntityList();
				return;
			}
						
			if(mode.equals(Constantes.MODEVIEW) || mode.equals(Constantes.MODEEDIT)){
				
				try {
					
					String idParam = context.getExternalContext().getRequestParameterMap().get("id");
					entityService = new MensajeNotificacionServices();
					this.entity = entityService.findById(Integer.parseInt(idParam));
							
				} catch (Exception e) {
					context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
				}
				
				return;
			}
			
		} catch (Exception e) {
			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
		}
		
	}
	
	/**
	 * Create a new MensajeNotificacion entity
	 * 
	 */
	public String newEntity() {
		
		entity = new MensajeNotificacion();
		return Metodos.newNavigation(openDialog, openWebMobile);
		
	}
	
	/**
	 * Select an existing MensajeNotificacion entity
	 * 
	 */
	public String selectEntity(Integer idKey) {				
		
		FacesContext context = FacesContext.getCurrentInstance();
		
		try {
			
			entityService = new MensajeNotificacionServices();
			entity = entityService.findById(idKey);
			
		} catch (Exception e) {
			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
		}
		
		return Metodos.selectNavigation(idKey, openDialog, openWebMobile);
	}
	
	/**
	 * Edit an existing MensajeNotificacion entity
	 * 
	 */
	public String editEntity(Integer idKey, boolean fromList) {
		
		FacesContext context = FacesContext.getCurrentInstance();
		
		try {
			
			entityService = new MensajeNotificacionServices();
			entity = entityService.findById(idKey);
			
		} catch (Exception e) {
			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
		}
		
		return Metodos.editNavigation(idKey, fromList, openDialog, openWebMobile);
	}
	
	/**
	 * Delete an existing MensajeNotificacion entity
	 * 
	 */
	public String deleteEntity(Integer idKey, boolean fromList) {
		
		FacesContext context = FacesContext.getCurrentInstance(); 
		
		try {
			entityService = new MensajeNotificacionServices();
			entity = entityService.findById(idKey);
			entityService.deleteTX(entity);
			
			Metodos.auditLog(Constantes.ACTIONDELETE, MensajeNotificacionMBean.class.getSimpleName(), entity.getCodigo(), "");
			
			String msg = Metodos.getMessageResourceString("generics_eliminarOK", new Object[]{entity.getCodigo()});
			if(!fromList || (!Metodos.isMobile() && Metodos.isDlg())){
				Metodos.setSessionMesagges(Constantes.SESSIONMSGINFO, msg);
			}
				
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
				
			fillEntityList();
		} catch (Exception e) {
			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
		}
		return  Metodos.close(closeRedirectWeb, closeRedirectMobile, fromList);
		
	}
	
	/**
	 * Metodo que retorna las listas
	 * @return
	 */
	public void fillEntityList(){
		FacesContext context = FacesContext.getCurrentInstance(); 
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		try {
			entityService = new MensajeNotificacionServices();
			entityList = null;
			LoginMBean loginMBean = (LoginMBean)Metodos.getManagedBean(request, "loginMBean");
			entityList = entityService.findByProperty(MensajeNotificacionDAO.IDCOMPANIA , loginMBean.getUsuario().getCompania().getId());
			
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}		
	}
	
	/**
	 * Metodo para guardar
	 */
	public String saveEntity(){
		
		FacesContext context = FacesContext.getCurrentInstance(); 
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		
		try{
			if(mode != null && mode.equals(Constantes.MODECREATE)
					&& hasPermissionTo(Constantes.ACTIONCREATE)){
				
				LoginMBean loginMBean = (LoginMBean)Metodos.getManagedBean(request, "loginMBean");
				entity.setCompania(loginMBean.getUsuario().getCompania());
				entityService = new MensajeNotificacionServices();
				entityService.createTX(entity);
				
				Metodos.auditLog(Constantes.ACTIONCREATE, MensajeNotificacionMBean.class.getSimpleName(), entity.getCodigo(), "");
				
				String msg = Metodos.getMessageResourceString("generics_guardarOK", new Object[]{entity.getCodigo()});
				
				if(!Metodos.isMobile()){
					Metodos.setSessionMesagges(Constantes.SESSIONMSGINFO, msg);
				}
					
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
					
				return  Metodos.close(closeRedirectWeb, closeRedirectMobile, true);
			}
			
			if(mode != null && mode.equals(Constantes.MODEEDIT)
					&& hasPermissionTo(Constantes.ACTIONUPDATE)){
				
				entityService = new MensajeNotificacionServices();
				entityService.updateTX(entity);
				
				Metodos.auditLog(Constantes.ACTIONUPDATE, MensajeNotificacionMBean.class.getSimpleName(), entity.getCodigo(), "");
				
				String msg = Metodos.getMessageResourceString("generics_actualizarOK", new Object[]{entity.getCodigo()});
				
				if(!Metodos.isMobile()){
					Metodos.setSessionMesagges(Constantes.SESSIONMSGINFO, msg);
				}
					
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
					
				return  Metodos.close(closeRedirectWeb, closeRedirectMobile, true);
			}
					
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}
		
		return "";
	}

	/**
	 * Metodo que se ejecuta cuando se cierra una modal
	 * @return
	 */
	public void dialogListener(){
		fillEntityList();
		Metodos.loadSessionMesagges();
	}
	
	/* SECURITY METHODS*/
	/**
	 * Método que valida si el usuario en sesión puede ejecutar una acción sobre este documento
	 * 
	 * @return
	 */
	public boolean hasPermissionTo(String action){		
		return Metodos.hasPermission(MensajeNotificacionMBean.class.getSimpleName(), action);
	}
	
	/* GETTERS AND SETTERS */

	public MensajeNotificacion getEntity() {
		return entity;
	}

	public void setEntity(MensajeNotificacion entity) {
		this.entity = entity;
	}

	public List<MensajeNotificacion> getEntityList() {
		return entityList;
	}

	public void setEntityList(List<MensajeNotificacion> entityList) {
		this.entityList = entityList;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public List<MensajeNotificacion> getEntityFilterList() {
		return entityFilterList;
	}

	public void setEntityFilterList(List<MensajeNotificacion> entityFilterList) {
		this.entityFilterList = entityFilterList;
	}
	
}