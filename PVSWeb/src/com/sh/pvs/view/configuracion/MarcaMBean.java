/**
	 * 	MarcaMBean.java:
	 * 
	 * 	Esta clase corresponde al Managed Bean para controlar el CRUD de 
	 * 	la entidad Marca
	 *
	 * @author Sergio Henao - sergiohenaoa@gmail.com - 11/04/2015
	 * @Modifier Sergio Henao - sergiohenaoa@gmail.com - 11/04/2015
	 * @version 1.0
	 * 
	 */
package com.sh.pvs.view.configuracion;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.sh.pvs.core.interfaces.configuracion.IMarcaServices;
import com.sh.pvs.core.interfaces.configuracion.ITipoVehiculoServices;
import com.sh.pvs.core.services.configuracion.MarcaServices;
import com.sh.pvs.core.services.configuracion.TipoVehiculoServices;
import com.sh.pvs.model.dao.configuracion.MarcaDAO;
import com.sh.pvs.model.dto.configuracion.Marca;
import com.sh.pvs.model.dto.configuracion.TipoVehiculo;
import com.sh.pvs.model.dto.configuracion.TipoVehiculoxMarca;
import com.sh.pvs.model.dto.seguridad.Usuario;
import com.sh.pvs.view.utils.Constantes;
import com.sh.pvs.view.utils.ControlException;
import com.sh.pvs.view.utils.Metodos;

@ManagedBean
@ViewScoped
public class MarcaMBean implements Serializable {

	/*Serializable Attribute*/
	private static final long serialVersionUID = 1L;
	
	/*Variables*/
	private Marca entity; 							// ViewForm - EditForm 	
 	private List<Marca> entityList; 				// Lista de registros del datatable
 	private List<Marca> entityFilterList;			// Lista de registros filtrados del datatable
 	private TipoVehiculoxMarca tipoVehiculoxMarca;
 	private List<TipoVehiculo> tiposVehiculoDropDownList;
 	private Usuario loggedUser;						//Usuario logeado (En Sesión)
 	
 	/*Navegación*/
 	private String mode;									// view - edit - list - create ==> Modo para identificar la vista
 	
 	/*Servicios*/
 	private IMarcaServices entityService;			// Servicios para la gestión de la entidad
	
 	/*Variables para navegación*/
 	public static String openDialog = "/configuracion/marca/marcaForm.faces";
 	public static String openWebMobile = "marcaForm.faces";
 	public static String closeRedirectMobile = "marcaList.faces";
	public static String closeRedirectWeb = "/configuracion/marca/marcaList.faces?faces-redirect=true";
 	
 	/*Constructors*/	
	/**	Default constructor */
	public MarcaMBean() {

	}
	
	/*METHODS*/
	
	/**
	 * Método para inicializar las variables
	 * 
	 * */
	@PostConstruct
	private void init(){
		
		FacesContext context = FacesContext.getCurrentInstance();
		
		try {
			
			tipoVehiculoxMarca = new TipoVehiculoxMarca();
			tipoVehiculoxMarca.setTipoVehiculo(new TipoVehiculo());
			
			Metodos.loadSessionMesagges();
			
			loggedUser = Metodos.getUserSession();
			
			if(context.getExternalContext().getRequestParameterMap().get("mode") != null)
				mode = context.getExternalContext().getRequestParameterMap().get("mode");
			else
				mode = Constantes.MODELIST;
			
			if(mode.equals(Constantes.MODECREATE)){
				this.entity = new Marca();
				return;
			}
			
			if(mode.equals(Constantes.MODELIST)){
				fillEntityList();
				return;
			}
						
			if(mode.equals(Constantes.MODEVIEW) || mode.equals(Constantes.MODEEDIT)){
				
				try {
					
					String idParam = context.getExternalContext().getRequestParameterMap().get("id");
					entityService = new MarcaServices();
					this.entity = entityService.findById(Integer.parseInt(idParam));
					
					if(mode.equals(Constantes.MODEEDIT))
						fillTiposVehiculoDropDown();
							
				} catch (Exception e) {
					context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
				}
				
				return;
			}
			
		} catch (Exception e) {
			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
		}
		
	}
	
	/**
	 * Create a new Marca entity
	 * 
	 */
	public String newEntity() {
		
		entity = new Marca();
		return Metodos.newNavigation(openDialog, openWebMobile);
		
	}
	
	/**
	 * Select an existing Marca entity
	 * 
	 */
	public String selectEntity(Integer idKey) {				
		
		FacesContext context = FacesContext.getCurrentInstance();
		
		try {
			
			entityService = new MarcaServices();
			entity = entityService.findById(idKey);
			
		} catch (Exception e) {
			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
		}
		
		return Metodos.selectNavigation(idKey, openDialog, openWebMobile);
	}
	
	/**
	 * Edit an existing Marca entity
	 * 
	 */
	public String editEntity(Integer idKey, boolean fromList) {
		
		FacesContext context = FacesContext.getCurrentInstance();
		
		try {
			
			entityService = new MarcaServices();
			entity = entityService.findById(idKey);
			
		} catch (Exception e) {
			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
		}
		
		return Metodos.editNavigation(idKey, fromList, openDialog, openWebMobile);
	}
	
	/**
	 * Delete an existing Marca entity
	 * 
	 */
	public String deleteEntity(Integer idKey, boolean fromList) {
		
		FacesContext context = FacesContext.getCurrentInstance(); 
		
		try {
			entityService = new MarcaServices();
			entity = entityService.findById(idKey);
			entityService.deleteTX(entity);
			
			Metodos.auditLog(Constantes.ACTIONDELETE, MarcaMBean.class.getSimpleName(), entity.getNombre(), "");
			
			String msg = Metodos.getMessageResourceString("generics_eliminarOK", new Object[]{entity.getNombre()});
			if(!fromList || (!Metodos.isMobile() && Metodos.isDlg())){
				Metodos.setSessionMesagges(Constantes.SESSIONMSGINFO, msg);
			}
				
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
				
			fillEntityList();
		} catch (Exception e) {
			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
		}
		return  Metodos.close(closeRedirectWeb, closeRedirectMobile, fromList);
		
	}
	
	/**
	 * Metodo que retorna las listas
	 * @return
	 */
	public void fillEntityList(){
		FacesContext context = FacesContext.getCurrentInstance(); 
		try {
			entityService = new MarcaServices();
			entityList = null;
			entityList = entityService.findByProperty(MarcaDAO.IDCOMPANIA , loggedUser.getCompania().getId());
			
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}		
	}
	
	/**
	 * Metodo para guardar
	 */
	public String saveEntity(){
		
		FacesContext context = FacesContext.getCurrentInstance(); 
		
		try{
			if(mode != null && mode.equals(Constantes.MODECREATE)
					&& hasPermissionTo(Constantes.ACTIONCREATE)){
				
				entity.setCompania(loggedUser.getCompania());
				entityService = new MarcaServices();
				entityService.createTX(entity);
				
				Metodos.auditLog(Constantes.ACTIONCREATE, MarcaMBean.class.getSimpleName(), entity.getNombre(), "");
				
				String msg = Metodos.getMessageResourceString("generics_guardarOK", new Object[]{entity.getNombre()});
				
				if(!Metodos.isMobile()){
					Metodos.setSessionMesagges(Constantes.SESSIONMSGINFO, msg);
				}
					
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
					
				return  Metodos.close(closeRedirectWeb, closeRedirectMobile, true);
			}
			
			else if(mode != null && mode.equals(Constantes.MODEEDIT)
					&& hasPermissionTo(Constantes.ACTIONUPDATE)){
				
				entityService = new MarcaServices();
				entityService.updateTX(entity);
				
				Metodos.auditLog(Constantes.ACTIONUPDATE, MarcaMBean.class.getSimpleName(), entity.getNombre(), "");
				
				String msg = Metodos.getMessageResourceString("generics_actualizarOK", new Object[]{entity.getNombre()});
				
				if(!Metodos.isMobile()){
					Metodos.setSessionMesagges(Constantes.SESSIONMSGINFO, msg);
				}
					
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
					
				return  Metodos.close(closeRedirectWeb, closeRedirectMobile, true);
			}
					
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}
		
		return "";
	}

	/**
	 * Metodo que se ejecuta cuando se cierra una modal
	 * @return
	 */
	public void dialogListener(){
		fillEntityList();
		Metodos.loadSessionMesagges();
	}
	
	/**
	 * Metodo para guardar una entidad de asociación
	 */
	public String saveTipoVehiculo(){
		
		FacesContext context = FacesContext.getCurrentInstance(); 
		
		try{
			if(mode != null && mode.equals(Constantes.MODEEDIT)
					&& hasPermissionTo(Constantes.ACTIONUPDATE)){
				
				entityService = new MarcaServices();
				entityService.addTipoVehiculo(entity, tipoVehiculoxMarca);
				entity = entityService.findById(entity.getId());
				tipoVehiculoxMarca = new TipoVehiculoxMarca();
				tipoVehiculoxMarca.setTipoVehiculo(new TipoVehiculo());
				
				Metodos.auditLog(Constantes.ACTIONUPDATE, MarcaMBean.class.getSimpleName(), entity.getNombre(), "Tipo Vehículo asociado: " + tipoVehiculoxMarca.getTipoVehiculo().getNombre());
				
				String msg = Metodos.getMessageResourceString("generics_actualizarOK", new Object[]{entity.getNombre()});
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
					
			}
					
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}
		
		return "";
	}
	
	/**
	 * Metodo para eliminar una entidad de asociación
	 */
	public String deleteTipoVehiculo(TipoVehiculoxMarca tipoVToDelete){
		
		FacesContext context = FacesContext.getCurrentInstance(); 
		
		try{
			if(mode != null && mode.equals(Constantes.MODEEDIT)
					&& hasPermissionTo(Constantes.ACTIONUPDATE)){
				
				entityService = new MarcaServices();
				entityService.deleteTipoVehiculo(tipoVToDelete);
				entity = entityService.findById(entity.getId());
				
				Metodos.auditLog(Constantes.ACTIONUPDATE, MarcaMBean.class.getSimpleName(), entity.getNombre(), "Tipo Vehículo eliminado: " + tipoVToDelete.getTipoVehiculo().getNombre());
				
				String msg = Metodos.getMessageResourceString("generics_actualizarOK", new Object[]{entity.getNombre()});
				
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
					
			}
					
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}
		
		return "";
	}
	
	/**
	 * Método que llena la lista de tipos de vehículo activos para el selector
	 * @param 
	 */
	private void fillTiposVehiculoDropDown(){
		FacesContext context = FacesContext.getCurrentInstance(); 
		tiposVehiculoDropDownList = null;
		try {
			ITipoVehiculoServices tipoVSrv = new TipoVehiculoServices();
			tiposVehiculoDropDownList = tipoVSrv.findAllActivos(loggedUser.getCompania().getId());
		} catch (Exception e) {
			tiposVehiculoDropDownList = null;
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}		
	}
	
	/* SECURITY METHODS*/
	/**
	 * Método que valida si el usuario en sesión puede ejecutar una acción sobre este documento
	 * 
	 * @return
	 */
	public boolean hasPermissionTo(String action){		
		return Metodos.hasPermission(MarcaMBean.class.getSimpleName(), action);
	}
	
	/* GETTERS AND SETTERS */

	public Marca getEntity() {
		return entity;
	}

	public void setEntity(Marca entity) {
		this.entity = entity;
	}

	public List<Marca> getEntityList() {
		return entityList;
	}

	public void setEntityList(List<Marca> entityList) {
		this.entityList = entityList;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public List<Marca> getEntityFilterList() {
		return entityFilterList;
	}

	public void setEntityFilterList(List<Marca> entityFilterList) {
		this.entityFilterList = entityFilterList;
	}

	public List<TipoVehiculo> getTiposVehiculoDropDownList() {
		return tiposVehiculoDropDownList;
	}

	public void setTiposVehiculoDropDownList(
			List<TipoVehiculo> tiposVehiculoDropDownList) {
		this.tiposVehiculoDropDownList = tiposVehiculoDropDownList;
	}

	
	public TipoVehiculoxMarca getTipoVehiculoxMarca() {
		return tipoVehiculoxMarca;
	}
	

	public void setTipoVehiculoxMarca(TipoVehiculoxMarca tipoVehiculoxMarca) {
		this.tipoVehiculoxMarca = tipoVehiculoxMarca;
	}
	
}
