/**
 * Constantes.java:
 * 
 * 	Esta clase contiene diferentes constantes que serán utilizadas en 
 * 	el paquete de vista de la aplicación. (View Utils)
 *
 * @author Sergio Henao, 06/03/2014
 * @Modifier Sergio Henao, 06/03/2014
 * @version 1.0
 * 
 */
package com.sh.pvs.view.utils;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.model.SelectItem;

@ManagedBean(name = "utilityConstantes", eager = true)
@ApplicationScoped
public class Constantes {

	/*ARCHIVO DE PROPIEDADES*/
	public static final String I18N_FILENAME = "Messages";
	
	/*VARIABLES PARA ALMACENAMIENTO DE MENSAJES EN SESSIÓN*/
	public static final String SESSIONTOKENRESULT = "MULTI_PAGE_MESSAGES_SUPPORT_RESULT";
	public static final String SESSIONTOKENMESSAGE = "MULTI_PAGE_MESSAGES_SUPPORT_MESSAGE";
	public static final String NEWCLIENTRETURN = "ID_RETURN_NEW_CLIENT";
	public static final String NEWVEHICLERETURN = "ID_RETURN_NEW_VEHICLE";
	public static final String NEWCONCESIONARIORETURN = "ID_RETURN_NEW_CONCESIONARIO";
	
	/*NIVEL DE MENSAJES*/
	public static final String SESSIONMSGINFO = "INFO";
	public static final String SESSIONMSGFATAL = "FATAL";
	public static final String SESSIONMSGWARN = "WARN";
	public static final String SESSIONMSGERROR = "ERROR";
	
	/*ACCIONES PARA CONTROLAR LA SEGURIDAD*/
	public static final String ACTIONCREATE = "CREAR";
	public static final String ACTIONREAD = "LEER";
	public static final String ACTIONUPDATE = "EDITAR";
	public static final String ACTIONDELETE = "BORRAR";
	public static final String ACTIONIMPRIMIR = "IMPRIMIR";
	public static final String ACTIONDECLINAR = "DECLINAR";
	public static final String ACTIONFINALIZAR = "FINALIZAR";
	public static final String ACTIONRESTITUIRPASSWORD = "RESTITUIR PASSWORD";
	
	/*MODOS DE NAVEGACIÓN*/
	public static final String MODEVIEW = "view";
	public static final String MODEEDIT = "edit";
	public static final String MODELIST = "list";
	public static final String MODECREATE = "create";
	
	/*PROCESOS*/
	public static final String CONSIGNACIONLABEL = "Consignación";
	public static final String RETOMALABEL = "Retoma";
	public static final String VENTALABEL = "Venta";
	public static final String HOJANEGOCIOLABEL = "Hoja de negocio";
	
	/*ORIGEN MOVIMIENTO*/
	public static final String ORGMOV_MOVTOCONT = "Movimiento contable";
	public static final String ORGMOV_COMPRAVENTA = "Compra/Venta de accesorios";
	public static final String ORGMOV_MOVTOHN = "Movimiento hoja de negocios";
	public static final String ORGMOV_VENTA3 = "Venta terceros";
	
	/*REPORTECARTERA*/
	public static final String REPORTECARTERA_COMPRAPPAGO = "Compra pendiente de pago";
	public static final String REPORTECARTERA_VENTAPCOBRO = "Venta pendiente de cobro";
	public static final String REPORTECARTERA_PAGOPVENDEDOR = "Pagos pendientes al vendedor";
	public static final String REPORTECARTERA_COBROPCOMPRADOR = "Cobros pendientes al comprador";
	public static final String REPORTECARTERA_COBROPVENDEDOR = "Cobros pendientes al vendedor";
	public static final String REPORTECARTERA_PAGOPCOMPRADOR = "Pagos pendientes al comprador";
	
	public String getModeview() {
		return MODEVIEW;
	}
	
	public String getModeedit() {
		return MODEEDIT;
	}
	
	public String getModelist() {
		return MODELIST;
	}
	
	public String getModecreate() {
		return MODECREATE;
	}

	public String getActioncreate() {
		return ACTIONCREATE;
	}
	
	public String getActionread() {
		return ACTIONREAD;
	}
	
	public String getActionupdate() {
		return ACTIONUPDATE;
	}

	public String getActiondelete() {
		return ACTIONDELETE;
	}

	public String getActionrestituirpassword() {
		return ACTIONRESTITUIRPASSWORD;
	}	
	
	public String getActionimprimir() {
		return ACTIONIMPRIMIR;
	}
	
	public String getActiondeclinar() {
		return ACTIONDECLINAR;
	}
	
	public String getActionfinalizar() {
		return ACTIONFINALIZAR;
	}
	
	public List<SelectItem> getProcesos() {
		
		List<SelectItem> procesos = new ArrayList<SelectItem>();
		
		procesos.add(new SelectItem(com.sh.pvs.core.utils.Constantes.CODCONSIGNACION, CONSIGNACIONLABEL));
		procesos.add(new SelectItem(com.sh.pvs.core.utils.Constantes.CODRETOMA, RETOMALABEL));
		procesos.add(new SelectItem(com.sh.pvs.core.utils.Constantes.CODVENTA, VENTALABEL));
		procesos.add(new SelectItem(com.sh.pvs.core.utils.Constantes.CODHOJANEGOCIO, HOJANEGOCIOLABEL));
		
        return procesos;
    }
	
	public String getNombreProceso(String codigoProceso) {
		
		if(codigoProceso.equals(com.sh.pvs.core.utils.Constantes.CODCONSIGNACION))
				return CONSIGNACIONLABEL;
		if(codigoProceso.equals(com.sh.pvs.core.utils.Constantes.CODRETOMA))
			return RETOMALABEL;
		if(codigoProceso.equals(com.sh.pvs.core.utils.Constantes.CODVENTA))
			return VENTALABEL;
		if(codigoProceso.equals(com.sh.pvs.core.utils.Constantes.CODHOJANEGOCIO))
			return HOJANEGOCIOLABEL;
		
        return "";
    }
	
}