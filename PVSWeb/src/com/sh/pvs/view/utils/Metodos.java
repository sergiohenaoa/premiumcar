/**
 * Metodos.java:
 * 
 * 	Esta clase contiene diferentes métodos que serán utilizadas en 
 * 	el paquete de vista de la aplicación. (View Utils)
 *
 * @author Sergio Henao, 06/03/2014
 * @Modifier Sergio Henao, 06/03/2014
 * @version 1.0
 * 
 */
package com.sh.pvs.view.utils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.Set;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuModel;

import com.sh.pvs.core.interfaces.configuracion.IVehiculoServices;
import com.sh.pvs.core.interfaces.seguridad.IFormularioServices;
import com.sh.pvs.core.services.configuracion.VehiculoServices;
import com.sh.pvs.core.services.seguridad.AuditoriaServices;
import com.sh.pvs.core.services.seguridad.FormularioServices;
import com.sh.pvs.model.dto.configuracion.FotoxVehiculo;
import com.sh.pvs.model.dto.configuracion.Vehiculo;
import com.sh.pvs.model.dto.seguridad.AccionxFormulario;
import com.sh.pvs.model.dto.seguridad.Formulario;
import com.sh.pvs.model.dto.seguridad.PerfilxUsuario;
import com.sh.pvs.model.dto.seguridad.Permiso;
import com.sh.pvs.model.dto.seguridad.Usuario;
import com.sh.pvs.model.utils.FormularioOrderComparator;
import com.sh.pvs.view.seguridad.LoginMBean;

@ManagedBean(name = "utilityMethods", eager = true)
@SessionScoped
public class Metodos implements Serializable {

	/*Serializable Attribute*/
	private static final long serialVersionUID = 1L;
	
	/*CONSTRUCTORS*/
	/**
	 * Minimal constructor 
	 */
	public Metodos() {
		super();
	}

	/*METHODS*/
	/**
	 * Carga Recurso MESSAGE i18n
	 * 
	 * @param bundleName
	 * @param key
	 * @param params (Separated by ",") ==> Parámetros que son claves en el properties
	 * @param locale
	 * @return
	 */
	public String getMsgResourceWithResourceParams(String key, String params) {
		FacesContext context = FacesContext.getCurrentInstance(); 
		String text = "";
		Object paramsObj[] = null;
		ResourceBundle bundle = ResourceBundle.getBundle(Constantes.I18N_FILENAME, context.getViewRoot().getLocale(),
				getCurrentClassLoader(paramsObj));
		
		try {
			if(params != null && !params.equals("")){
				String[] paramArr = params.split(",");
				paramsObj = new Object[paramArr.length];
				for (int i = 0; i < paramArr.length; i++) {
					String param = bundle.getString(paramArr[i]);
					paramsObj[i] = param;
				}
			}
		} catch (Exception e) {
			paramsObj = null;
		}
		
		try {
			text = bundle.getString(key);
		} catch (MissingResourceException e) {
			text = "?? key " + key + " not found ??";
		}
		if (paramsObj != null) {
			MessageFormat mf = new MessageFormat(text, Locale.getDefault());
			text = mf.format(paramsObj, new StringBuffer(), null).toString();
		}
		return text;
		
	}
	
	/**
	 * Carga Recurso MESSAGE i18n
	 * 
	 * @param bundleName
	 * @param key
	 * @param params ==> Se reemplazan directamente
	 * @param locale
	 * @return
	 */
	public static String getMessageResourceString(String key, Object params[]) {
		String text = null;
		FacesContext context = FacesContext.getCurrentInstance(); 
		ResourceBundle bundle = ResourceBundle.getBundle(Constantes.I18N_FILENAME, context.getViewRoot().getLocale(),
				getCurrentClassLoader(params));
		try {
			text = bundle.getString(key);
		} catch (MissingResourceException e) {
			text = "?? key " + key + " not found ??";
		}
		if (params != null) {
			MessageFormat mf = new MessageFormat(text, Locale.getDefault());
			text = mf.format(params, new StringBuffer(), null).toString();
		}
		return text;
	}
	
	/**
	 * Carga clases
	 * 
	 * @param defaultObject
	 * @return
	 */
	protected static ClassLoader getCurrentClassLoader(Object defaultObject) {
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		if (loader == null) {
			loader = defaultObject.getClass().getClassLoader();
		}
		return loader;
	}

	/**
	 * Se obtiene el managedbean de la sesión
	 * 
	 * @param request
	 * @param beanName
	 * @return
	 * @throws Exception
	 */
	public static Object getManagedBean(HttpServletRequest request, String beanName) throws Exception{
		try {
			Object bean = request.getSession().getAttribute(beanName);
		    return bean;
		} catch (Exception e) {
			throw new Exception();
		}
	}

	/**
	 * Método que verifica si el usuario en sesión tiene permiso para
	 * acceder a la página que está solicitando
	 * @param uri
	 * @return
	 */
	public static boolean hasAccess(HttpServletRequest request, String uri) throws Exception{
		
		try {
			LoginMBean loginMBean = (LoginMBean)Metodos.getManagedBean(request, "loginMBean");
			
			for (Iterator<PerfilxUsuario> itPerfiles = loginMBean.getUsuario().getPerfilesxusuario().iterator(); itPerfiles.hasNext();) {
				PerfilxUsuario perfilxusuario = (PerfilxUsuario) itPerfiles.next();
				for (Iterator<Permiso> itPermisos = perfilxusuario.getPerfil().getPermisos().iterator(); itPermisos.hasNext();) {
					Permiso permiso = itPermisos.next();				
					String urlCarpetaPermisos = permiso.getAccionxFormulario().getFormulario().getNombreCarpeta();
					if(uri.contains(urlCarpetaPermisos)){
						return true;
					}
				}
			}
		} catch (Exception e) {
			throw com.sh.pvs.utils.exceptioncontrol.ControlException.formatException("TECNICA", Metodos.class.getName(), "", "tieneAcceso", e);
		}
		return false;
	}
	
	/**
	 * Método que verifica si el usuario en sesión tiene permiso para
	 * ejecutar alguna acción en el formulario actual
	 * @param uri
	 * @return
	 */
	public static boolean hasPermission(String clase, String accion){
		
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest)context.getExternalContext().getRequest();
		try {
			LoginMBean loginMBean = (LoginMBean)Metodos.getManagedBean(request, "loginMBean");
			for (Iterator<PerfilxUsuario> itPerfiles = loginMBean.getUsuario().getPerfilesxusuario().iterator(); itPerfiles.hasNext();) {
				PerfilxUsuario perfilxusuario = (PerfilxUsuario) itPerfiles.next();
				for (Iterator<Permiso> itPermisos = perfilxusuario.getPerfil().getPermisos().iterator(); itPermisos.hasNext();) {
					Permiso permiso = itPermisos.next();				
					AccionxFormulario accionxform = permiso.getAccionxFormulario();
					if(clase.equals(accionxform.getFormulario().getNombreClase())){
						if(accion != null && !accion.equals("") 
								&& accionxform.getAccion().getNombre() != null 
								&& !accionxform.getAccion().getNombre().equals("")){
							if(accion.toLowerCase().equals(accionxform.getAccion().getNombre().toLowerCase())){
								return true;
							}
						}
					}
				}
			}
		} catch (Exception e) {
			return false;
		}
		return false;
	}
	
	/**
	 * Método que verifica retorna el usuario que inicia sesión
	 * @param uri
	 * @return
	 */
	public static Usuario getUserSession(){
		
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest)context.getExternalContext().getRequest();
		try {
			LoginMBean loginMBean = (LoginMBean)Metodos.getManagedBean(request, "loginMBean");
			return loginMBean.getUsuario();
		} catch (Exception e) {
			return null;
		}
	}
	
	/**
	 * Método que construye el modelo para menú con base en los permisos del usuario
	 * @return
	 */
	public static MenuModel createMenuModel() throws Exception{
		
		try {
			FacesContext context = FacesContext.getCurrentInstance(); 
			HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
			LoginMBean loginMBean = (LoginMBean)Metodos.getManagedBean(request, "loginMBean");
			MenuModel model = new DefaultMenuModel();
			
			
			//Se obtienen los diferentes formularios a los que el usuario tiene acceso
			ArrayList<Formulario> formularios = new ArrayList<Formulario>();
			for (Iterator<PerfilxUsuario> itPerfiles = loginMBean.getUsuario().getPerfilesxusuario().iterator(); itPerfiles.hasNext();) {
				PerfilxUsuario perfilxusuario = (PerfilxUsuario) itPerfiles.next();
				for (Iterator<Permiso> itPermisos = perfilxusuario.getPerfil().getPermisos().iterator(); itPermisos.hasNext();) {
					Permiso permiso = itPermisos.next();				
					Formulario formulario = permiso.getAccionxFormulario().getFormulario();
					
					boolean formularioExiste = false;
					for (Iterator<Formulario> itFormularios = formularios.iterator(); itFormularios.hasNext();) {
						Formulario formTmp = itFormularios.next();
						if(formTmp.getId().equals(formulario.getId())){
							formularioExiste = true;
						}		
					}
					
					if(!formularioExiste && formulario.getEsActivo()){
						formularios.add(formulario);
					}
				}
			}
			
			IFormularioServices formSrvcs = new FormularioServices();
			
			//Se ordenan los formularios de acuerdo al campo Orden
			Collections.sort(formularios, new FormularioOrderComparator());
			
			//Se obtienen todos los encabezados para poder hacer la estructura árbol
			HashMap<Integer, DefaultSubMenu> encabezadosMap = new HashMap<Integer, DefaultSubMenu>();
			
			ArrayList<Formulario> encabezados = formSrvcs.findEncabezadosMenu(loginMBean.getUsuario().getCompania().getId());			
			for (Iterator<Formulario> itEncabezados = encabezados.iterator(); itEncabezados.hasNext();) {
				Formulario encabezado = itEncabezados.next();
				DefaultSubMenu opcionSinLink = new DefaultSubMenu(encabezado.getNombreDescriptivo());
				encabezadosMap.put(encabezado.getId(), opcionSinLink);
			}
			
			//Se cargan las opciones de encabezado para el menu (Se crean las dependencias entre encabezados)			
			for (Iterator<Formulario> itEncabezados = encabezados.iterator(); itEncabezados.hasNext();) {
				Formulario encabezado = itEncabezados.next();
				if(encabezado.getFormularioPadre() == null 
						|| encabezado.getFormularioPadre().getId() == null
						|| encabezado.getFormularioPadre().getId().equals(new Integer(0))){
					model.addElement(encabezadosMap.get(encabezado.getId()));
				}else{
					DefaultSubMenu opcionSinLink = encabezadosMap.get(encabezado.getId());
					DefaultSubMenu opcionPadre = encabezadosMap.get(encabezado.getFormularioPadre().getId());
					boolean tieneHijos = false;
					//Se valida si tiene al menos un hijo
					for (Iterator<Formulario> itFormularios = formularios.iterator(); itFormularios.hasNext();) {
						Formulario formulario = itFormularios.next();
				        if(formulario.getFormularioPadre() != null 
								&& formulario.getFormularioPadre().getId() != null
								&& !formulario.getFormularioPadre().getId().equals(new Integer(0))
								&& formulario.getFormularioPadre().getId().equals(encabezado.getId())){
				        	tieneHijos = true;
						}
					}
					if(tieneHijos)
						opcionPadre.addElement(opcionSinLink);
					
				}
			}
			
			//Se cargan las opciones de los formularios  a los que el usuario tiene acceso
			for (Iterator<Formulario> itFormularios = formularios.iterator(); itFormularios.hasNext();) {
				Formulario formulario = itFormularios.next();
				DefaultMenuItem item = new DefaultMenuItem(formulario.getNombreDescriptivo());  
		        item.setUrl(formulario.getUrl()); 
		        if(formulario.getFormularioPadre() == null 
						|| formulario.getFormularioPadre().getId() == null
						|| formulario.getFormularioPadre().getId().equals(new Integer(0))){
					model.addElement(item);
				}else{
					DefaultSubMenu opcionPadre = encabezadosMap.get(formulario.getFormularioPadre().getId());
					if(opcionPadre!=null)
						opcionPadre.addElement(item);
				}
			}
			
			//Se eliminan todos los encabezados que no tienen hijos
			Set<Integer> it = encabezadosMap.keySet();
			for (Iterator<Integer> itKeys = it.iterator(); itKeys.hasNext();) {
				Integer key = itKeys.next();
				DefaultSubMenu value = encabezadosMap.get(key);
				if(value.getElementsCount()==0){
					model.getElements().remove(value);
				}
			}
			
			return model;
		} catch (Exception e) {
			throw com.sh.pvs.utils.exceptioncontrol.ControlException.formatException("TECNICA", Metodos.class.getName(), "", "createMenuModel", e);
		}
		
	}

	/**
	 * Método para registrar la auditoría
	 */
	public static void auditLog(String accion, String formulario, String registro, String observaciones) throws Exception{
		try {
			FacesContext context = FacesContext.getCurrentInstance(); 
			HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
			LoginMBean loginMBean = (LoginMBean)Metodos.getManagedBean(request, "loginMBean");
			Usuario user = loginMBean.getUsuario();
			
			AuditoriaServices audServices = new AuditoriaServices();
			audServices.registrarAuditoria(user, accion, Metodos.getFormByBean(formulario), registro, observaciones);
		} catch (Exception e) {
			throw com.sh.pvs.utils.exceptioncontrol.ControlException.formatException("TECNICA", Metodos.class.getName(), "", "registrarAuditoria", e);
		}
		
	}
	
	/**
	 * Método para obtener el nombre del formulario según el Managed Bean
	 */
	public static String getFormByBean(String beanName){
		String formName = beanName;
		
		formName = Metodos.getMessageResourceString("aud_form_"+beanName, null);
	
		if(formName.endsWith("not found ??"))
			return beanName;
		
		return formName;
	}
	
	/**
	 * Método que indica si la aplicación está siendo vista desde un dispositivo móvil
	 */
	public static boolean isMobile(){
		
		FacesContext context = FacesContext.getCurrentInstance();
		
		HttpServletRequest request = (HttpServletRequest)context.getExternalContext().getRequest();
		if(request.getHeader("User-Agent").indexOf("Mobile") != -1) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * Método que indica si el contexto de la ventana actual, se encuentra desplegado en una modal
	 */
	public static boolean isDlg(){
		Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();		
	    String pfdlgcid = (String) params.get("pfdlgcid");
	    
	    return pfdlgcid!=null;
	}
	
	/**
	 * Método para finalizar las operaciones CRUD
	 * @return
	 */
	public static String close(String redirectWeb, String redirectMobile, boolean fromList){
		if(!isDlg()){
			try {
				if(isMobile()){
					if(fromList)
						return redirectMobile;
					else
						return redirectWeb;
				}else{
					return redirectWeb;
				}
			} catch (Exception e) {
				return "";
			}
	    }else{
		    RequestContext.getCurrentInstance().closeDialog(null);
	    }	
		return "";
	}
	
	/**
	 * Metodo que carga los mensajes de la sesión en el contexto de faces
	 * @return
	 */
	public static void loadSessionMesagges(){
		FacesContext context = FacesContext.getCurrentInstance(); 
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		HttpSession session = request.getSession();
		try {

			String result = session.getAttribute(Constantes.SESSIONTOKENRESULT).toString();
			String msg = session.getAttribute(Constantes.SESSIONTOKENMESSAGE).toString();
			
			if(result.equals(Constantes.SESSIONMSGINFO)){
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
			}
			if(result.equals(Constantes.SESSIONMSGFATAL)){
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, msg, "")); // MENSAJE
			}
			if(result.equals(Constantes.SESSIONMSGWARN)){
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, msg, "")); // MENSAJE
			}
			if(result.equals(Constantes.SESSIONMSGERROR)){
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, "")); // MENSAJE
			}
			
		} catch (Exception e) {
			
		}finally{
			session.removeAttribute(Constantes.SESSIONTOKENRESULT);
			session.removeAttribute(Constantes.SESSIONTOKENMESSAGE);
			session.removeAttribute(Constantes.NEWCLIENTRETURN);
			session.removeAttribute(Constantes.NEWVEHICLERETURN);
		}
	}
	
	/**
	 * Metodo que carga los mensajes en la sesión
	 * @return
	 */
	public static void setSessionMesagges(String result, String msg){
		
		FacesContext context = FacesContext.getCurrentInstance(); 
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		HttpSession session = request.getSession();
		
		session.setAttribute(Constantes.SESSIONTOKENRESULT, result);
		session.setAttribute(Constantes.SESSIONTOKENMESSAGE, msg);
			
	}
	
	/**
	 * Método que obtiene la sección de parámetros que llegan por URL
	 * @param request
	 * @return
	 */
	public static String getQueryString(HttpServletRequest request){
		String queryString = "";
		if ((request.getQueryString() != null) && (!request.getQueryString().equals(""))) {
			queryString = "?" + request.getQueryString();
		}
		
		return queryString;
	}
	
	/**
	 * Método que define la navegación cuando se da clic en el botón nuevo
	 * @param openCreateDialog
	 * @param openCreateWebMobile
	 * @return
	 */
	public static String newNavigation(String openCreateDialog, String openCreateWebMobile){
		FacesContext context = FacesContext.getCurrentInstance(); 
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		if(!Metodos.isMobile()){
			
			//Si la navegación es en web se abren modales
			Map<String,Object> options = new HashMap<String, Object>();
	        options.put("modal", true);
	        options.put("resizable", false);
	        options.put("contentHeight", 450);
	        options.put("contentWidth", 600);
	        
	        Map<String, List<String>> params = new HashMap<String, List<String>>();
		       
	        List<String> values = new ArrayList<String>();
	        values.add(Constantes.MODECREATE);
	        params.put("mode", values);
	        
			RequestContext.getCurrentInstance().openDialog(openCreateDialog, options, params);
			
		}else{
			//Si la navegación es en dispositivos móviles, se redirecciona
			try {
				response.sendRedirect(openCreateWebMobile + "?mode=" + Constantes.MODECREATE);
			} catch (Exception e) {
				return "";
			}
			
		}
		
		return "";
	}
	
	/**
	 * Método que define la navegación cuando se da clic en el botón editar
	 * @param openCreateDialog
	 * @param openCreateWebMobile
	 * @return
	 */
	public static String editNavigation(Integer idKey, boolean fromList, String openEditDialog, String openEditWebMobile){
		
		FacesContext context = FacesContext.getCurrentInstance(); 
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		
		if(fromList){
			//Si se dio editar desde la lista y es un dispositivo móvil
			//se redirecciona la página
			if(Metodos.isMobile()){
				try {
					response.sendRedirect(openEditWebMobile + "?id=" + idKey + "&mode=" + Constantes.MODEEDIT);
				} catch (Exception e) {
					return "";
				}
			}else{
				//Si se dio editar desde la lista y no es un dispositivo móvil
				//se abre el modal
				Map<String,Object> options = new HashMap<String, Object>();
		        options.put("modal", true);
		        options.put("resizable", false);
		        options.put("contentHeight", 450);
		        options.put("contentWidth", 600);
		       
		        Map<String, List<String>> params = new HashMap<String, List<String>>();
			       
		        List<String> values = new ArrayList<String>();
		        values.add(Constantes.MODEEDIT);
		        params.put("mode", values);
		        
		        List<String> valuesid = new ArrayList<String>();	        
		        valuesid.add(idKey.toString());
		        params.put("id", valuesid);
				
				RequestContext.getCurrentInstance().openDialog(openEditDialog, options, params);
			}
		}else{
			//Si se dio editar desde el view form y es un dispositivo movil
			//se redirecciona la página
			if(Metodos.isMobile()){
				try {
					response.sendRedirect(openEditWebMobile + "?id=" + idKey + "&mode=" + Constantes.MODEEDIT);
				} catch (Exception e) {
					return "";
				}
			}else{
				//Si se dio editar desde el view form y es un dialog
				if(Metodos.isDlg()){
					//Si se dio editar desde el view form y no es un dialog
					try {
						response.sendRedirect(openEditWebMobile + "?id=" + idKey + "&pfdlgcid=" + request.getParameter("pfdlgcid") + "&mode=" + Constantes.MODEEDIT);
					} catch (Exception e) {
						return "";
					}
				}else{
					//Si se dio editar desde el view form y no es un dialog
					try {
						response.sendRedirect(openEditWebMobile + "?id=" + idKey + "&mode=" + Constantes.MODEEDIT);
					} catch (Exception e) {
						return "";
					}
				}
				
			}
		}
		
		return "";
	}
	
	/**
	 * Método que define la navegación cuando se da clic en el botón ver
	 * @param openCreateDialog
	 * @param openCreateWebMobile
	 * @return
	 */
	public static String selectNavigation(Integer idKey, String openViewDialog, String openViewWebMobile){
		
		FacesContext context = FacesContext.getCurrentInstance(); 
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		
		if(!Metodos.isMobile()){
			
			Map<String,Object> options = new HashMap<String, Object>();
	        options.put("modal", true);
	        options.put("resizable", false);
	        options.put("contentHeight", 450);
	        options.put("contentWidth", 600);
	        
	        Map<String, List<String>> params = new HashMap<String, List<String>>();
		       
	        List<String> values = new ArrayList<String>();
	        values.add(Constantes.MODEVIEW);
	        params.put("mode", values);
	        
	        List<String> valuesid = new ArrayList<String>();	        
	        valuesid.add(idKey.toString());
	        params.put("id", valuesid);
			
			RequestContext.getCurrentInstance().openDialog(openViewDialog, options, params);
			
		}else{
			//Si la navegación es en dispositivos móviles, se redirecciona
			try {
				response.sendRedirect(openViewWebMobile + "?id=" + idKey + "&mode=" + Constantes.MODEVIEW);
			} catch (Exception e) {
				return "";
			}
			
		}
		
		return "";
	}
	
	/**
	 * Método que define la navegación cuando se da clic en el botón ver
	 * @param openCreateDialog
	 * @param openCreateWebMobile
	 * @return
	 */
	public static String selectNavigationNoOptions(Integer idKey, String openViewDialog, String openViewWebMobile){
		
		FacesContext context = FacesContext.getCurrentInstance(); 
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		
		if(!Metodos.isMobile()){
			
			Map<String,Object> options = new HashMap<String, Object>();
	        options.put("modal", true);
	        options.put("resizable", false);
	        options.put("contentHeight", 450);
	        options.put("contentWidth", 600);
	        
	        Map<String, List<String>> params = new HashMap<String, List<String>>();
		       
	        List<String> values = new ArrayList<String>();
	        values.add(Constantes.MODEVIEW);
	        params.put("mode", values);
	        
	        List<String> valuesid = new ArrayList<String>();	        
	        valuesid.add(idKey.toString());
	        params.put("id", valuesid);
	        
	        List<String> valuesNoOpt = new ArrayList<String>();	        
	        valuesNoOpt.add("true");
	        params.put("noOptions", valuesNoOpt);
			
			RequestContext.getCurrentInstance().openDialog(openViewDialog, options, params);
			
		}else{
			//Si la navegación es en dispositivos móviles, se redirecciona
			try {
				response.sendRedirect(openViewWebMobile + "?id=" + idKey + "&mode=" + Constantes.MODEVIEW + "&noOptions=true");
			} catch (Exception e) {
				return "";
			}
			
		}
		
		return "";
	}
	
	
	public StreamedContent getVehicleImage() throws IOException {
        
		try {
			FacesContext context = FacesContext.getCurrentInstance();
	        if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
	            // So, we're rendering the HTML. Return a stub StreamedContent so that it will generate right URL.
	            return new DefaultStreamedContent();
	        }
	        else {
	            // So, browser is requesting the image. Return a real StreamedContent with the image bytes.
	        	Integer vehiculoId = Integer.parseInt(context.getExternalContext().getRequestParameterMap().get("vehiculoId"));
	        	Integer fotoId = Integer.parseInt(context.getExternalContext().getRequestParameterMap().get("fotoId"));
	            
	        	IVehiculoServices vehicleService = new VehiculoServices();
	        	Vehiculo vehiculo = vehicleService.findById(vehiculoId);
	        	
	        	for (Iterator<FotoxVehiculo> itFotos = vehiculo.getFotos().iterator(); itFotos.hasNext();) {
					FotoxVehiculo foto = (FotoxVehiculo) itFotos.next();
					if(foto.getId().equals(fotoId)){
						return new DefaultStreamedContent(new ByteArrayInputStream(com.sh.pvs.utils.Metodos.decodeFile(foto.getAdjunto())), foto.getTipoArchivo(), foto.getNombreArchivo());
					}
				}
	        }
		} catch (Exception e) {
			return null;
		}
        
		return null;
    }

	/**
	 * Método que define la navegación cuando se da clic en el botón nuevo
	 * @param openCreateDialog
	 * @param openCreateWebMobile
	 * @return
	 */
	public static String openGenerarPlantillaForm(String proceso, String idElement){
		
		String openDialog = "/utils/generarPlantilla.faces";
	 	String openWebMobile = "generarPlantilla.faces";
	 	
		FacesContext context = FacesContext.getCurrentInstance(); 
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		if(!Metodos.isMobile()){
			
			//Si la navegación es en web se abren modales
			Map<String,Object> options = new HashMap<String, Object>();
	        options.put("modal", true);
	        options.put("resizable", false);
	        options.put("contentHeight", 450);
	        options.put("contentWidth", 600);
	        
	        Map<String, List<String>> params = new HashMap<String, List<String>>();
		       
	        List<String> values = new ArrayList<String>();
	        values.add(proceso);
	        params.put("proceso", values);
	        
	        List<String> valuesid = new ArrayList<String>();	        
	        valuesid.add(idElement);
	        params.put("id", valuesid);
	        
			RequestContext.getCurrentInstance().openDialog(openDialog, options, params);
			
		}else{
			//Si la navegación es en dispositivos móviles, se redirecciona
			try {
				response.sendRedirect(openWebMobile + "?proceso=" + proceso + "&id=" + idElement);
			} catch (Exception e) {
				return "";
			}
			
		}
		
		return "";
	}
}
