package com.sh.pvs.view.utils;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import com.sh.pvs.core.interfaces.configuracion.IPlantillaServices;
import com.sh.pvs.core.interfaces.consignacion.IConsignacionServices;
import com.sh.pvs.core.interfaces.hojanegocio.IHojaNegocioServices;
import com.sh.pvs.core.interfaces.retoma.IRetomaServices;
import com.sh.pvs.core.interfaces.venta.IVentaServices;
import com.sh.pvs.core.services.configuracion.PlantillaServices;
import com.sh.pvs.core.services.consignacion.ConsignacionServices;
import com.sh.pvs.core.services.hojanegocio.HojaNegocioServices;
import com.sh.pvs.core.services.retoma.RetomaServices;
import com.sh.pvs.core.services.venta.VentaServices;
import com.sh.pvs.model.dto.configuracion.Plantilla;
import com.sh.pvs.model.dto.consignacion.Consignacion;
import com.sh.pvs.model.dto.hojanegocio.HojaNegocio;
import com.sh.pvs.model.dto.retoma.Retoma;
import com.sh.pvs.model.dto.venta.Venta;
import com.sh.pvs.utils.wordtemplate.ConsignacionTagsReplacement;
import com.sh.pvs.utils.wordtemplate.HojaNegocioTagsReplacement;
import com.sh.pvs.utils.wordtemplate.RetomaTagsReplacement;
import com.sh.pvs.utils.wordtemplate.VentaTagsReplacement;

@ManagedBean
@ViewScoped
public class Documentos implements Serializable {
	
	/*VARIABLES*/
	private static final long serialVersionUID = 2241011585117683858L;
	private FacesContext context;	
	private String paramProceso;
	private Integer paramId;
	private Boolean hnEsComprador;
	
	/*CONTROLES PLANTILLAS*/
	private ArrayList<Plantilla> plantillasDropDown;
	private Plantilla plantillaSeleccionada = new Plantilla();
	
	/*CAMPOS*/
		
	/*SERVICIOS*/
		
	public Documentos() {
		obtenerParam();
		llenarPlantillasDropDown();
	}
		
	private void obtenerParam(){
		context = FacesContext.getCurrentInstance();
		try {
			paramProceso = context.getExternalContext().getRequestParameterMap().get("proceso");
			paramId = Integer.parseInt(context.getExternalContext().getRequestParameterMap().get("id"));
			
		}catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}
	}
	
	
	/**
	 * Método que genera un archivo según la plantilla seleccionada
	 * @param 
	 * @return String
	 */
	public String generarPlantilla(){
		context = FacesContext.getCurrentInstance();
		try {
			
			Plantilla plantilla = new Plantilla();
			if(plantillaSeleccionada != null
					&& plantillaSeleccionada.getId() != null
					&& plantillasDropDown != null){
				for (Iterator<Plantilla> itPlantillas = plantillasDropDown.iterator(); itPlantillas.hasNext();) {
					Plantilla plantillaTmp = (Plantilla) itPlantillas.next();
					if(plantillaSeleccionada.getId().equals(plantillaTmp.getId()))
						plantilla = plantillaTmp;
				}
			}else{
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se han seleccionado la plantilla a generar", "")); // MENSAJE
				return "";
			}
			
			byte[] decodedFile = com.sh.pvs.utils.Metodos.decodeFile(plantilla.getAdjunto());
			InputStream fileTemplate = new ByteArrayInputStream(decodedFile);
			
			HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
			response.setContentType(plantilla.getTipoArchivo());
			response.setHeader("Content-disposition", "attachment;filename=" + plantilla.getNombreArchivo());
			
			HWPFDocument word = new HWPFDocument(new POIFSFileSystem(fileTemplate));
			
			if(this.paramProceso.equals(com.sh.pvs.core.utils.Constantes.CODCONSIGNACION)){
				IConsignacionServices servicio = new ConsignacionServices();
				Consignacion consignacion = servicio.findById(this.paramId);
				ConsignacionTagsReplacement ctr = new ConsignacionTagsReplacement(word, consignacion);
				ctr.replaceTags();
			}else if(this.paramProceso.equals(com.sh.pvs.core.utils.Constantes.CODRETOMA)){
				IRetomaServices servicio = new RetomaServices();
				Retoma retoma = servicio.findById(this.paramId);
				RetomaTagsReplacement rtr = new RetomaTagsReplacement(word, retoma);
				rtr.replaceTags();
			}else if(this.paramProceso.equals(com.sh.pvs.core.utils.Constantes.CODVENTA)){
				IVentaServices servicio = new VentaServices();
				Venta venta = servicio.findById(this.paramId);
				VentaTagsReplacement vtr = new VentaTagsReplacement(word, venta);
				vtr.replaceTags();
			}else if(this.paramProceso.equals(com.sh.pvs.core.utils.Constantes.CODHOJANEGOCIO)){
				IHojaNegocioServices servicio = new HojaNegocioServices();
				HojaNegocio hojaNegocio = servicio.findById(this.paramId);
				HojaNegocioTagsReplacement hntr = new HojaNegocioTagsReplacement(word, hojaNegocio, hnEsComprador);
				hntr.replaceTags();
			}
			
            OutputStream output = response.getOutputStream();
			output.flush();
			word.write(output);
			output.close();
			fileTemplate.close();
			context.responseComplete();
			
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}
		return "";
	}
		
	/**
 	 * Método que llena el selector de plantillas
 	 */
 	public String llenarPlantillasDropDown(){
 		context = FacesContext.getCurrentInstance();
 		setPlantillasDropDown(new ArrayList<Plantilla>());
		try {
			setPlantillasDropDown(new ArrayList<Plantilla>());
			IPlantillaServices plantillaSrv = new PlantillaServices();
			plantillasDropDown = (ArrayList<Plantilla>)plantillaSrv.findAllActivosByProceso(Metodos.getUserSession().getCompania().getId(), this.paramProceso);
		} catch (Exception e) {
			setPlantillasDropDown(new ArrayList<Plantilla>());
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));		
		}
		
		return "";
 	}
 	
 	
	/* SETTERS AND GETTERS */
	public Integer getParamId() {
		return paramId;
	}

	public void setParamId(Integer paramId) {
		this.paramId = paramId;
	}
	
	public ArrayList<Plantilla> getPlantillasDropDown() {
		return plantillasDropDown;
	}
	
	public void setPlantillasDropDown(ArrayList<Plantilla> plantillasDropDown) {
		this.plantillasDropDown = plantillasDropDown;
	}

	public Plantilla getPlantillaSeleccionada() {
		return plantillaSeleccionada;
	}

	public void setPlantillaSeleccionada(Plantilla plantillaSeleccionada) {
		this.plantillaSeleccionada = plantillaSeleccionada;
	}
	
	public String getDescripcionPlantilla() {
		String descplantilla = "";
		if(plantillaSeleccionada != null
				&& plantillaSeleccionada.getId() != null
				&& plantillasDropDown != null){
			for (Iterator<Plantilla> itPlantillas = plantillasDropDown.iterator(); itPlantillas.hasNext();) {
				Plantilla plantilla = (Plantilla) itPlantillas.next();
				if(plantillaSeleccionada.getId().equals(plantilla.getId()))
					descplantilla = plantilla.getDescripcion();
			}
		}
		return descplantilla;
	}
	
	public String getNombreArchivoPlantilla() {
		String descplantilla = "";
		if(plantillaSeleccionada != null
				&& plantillaSeleccionada.getId() != null
				&& plantillasDropDown != null){
			for (Iterator<Plantilla> itPlantillas = plantillasDropDown.iterator(); itPlantillas.hasNext();) {
				Plantilla plantilla = (Plantilla) itPlantillas.next();
				if(plantillaSeleccionada.getId().equals(plantilla.getId()))
					descplantilla = plantilla.getNombreArchivo();
			}
		}
		return descplantilla;
	}

	public Boolean getHnEsComprador() {
		return hnEsComprador;
	}

	public void setHnEsComprador(Boolean hnEsComprador) {
		this.hnEsComprador = hnEsComprador;
	}

	public String getParamProceso() {
		return paramProceso;
	}

	public void setParamProceso(String paramProceso) {
		this.paramProceso = paramProceso;
	}
	
}