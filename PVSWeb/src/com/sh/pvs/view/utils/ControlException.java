/**
 * ControlException.java:
 * 
 * 	Esta clase es utilizada para controlar las excepciones que pueda presentar el sistema
 *
 * @author Sergio Henao, 06/03/2014
 * @Modifier Sergio Henao, 06/03/2014
 * @version 1.0
 * 
 */
package com.sh.pvs.view.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.faces.context.FacesContext;

import com.sh.pvs.view.utils.Metodos;

public class ControlException {
	
	/*METHODS*/
	/**
	 * Controla errores generales del sistema
	 * 
	 * @param e
	 * @param context
	 * @return
	 * 
	 */
	public static String controlException(Exception e) {
		FacesContext context = FacesContext.getCurrentInstance();
		String mensaje = "";
		try {
			String[] excArr = e.getMessage().split(":");
			String tipoExcepcion = excArr[0]; //TECNICA, NEGOCIO
			String claseGenExcepcion = excArr[1]; //Indica la clase en la que se generó la excepción
			String entidadGenExcepcion = excArr[2]; // Entidad de negocio en la cuál se genera la excepción
			String metodoExcepcion = excArr[3]; // Método en el que se genera la excepción
			String claseExcepcion = excArr[4]; // Clase o tipo de excepción
			String mensajeExcepcion = excArr[5]; // Mensaje generado por la excepción
						
			String codigo = generarCodigoExcepcion();
			
			if(context != null){
				if(claseExcepcion != null  && claseExcepcion.equals("org.hibernate.NonUniqueObjectException")){
					mensaje = Metodos.getMessageResourceString("exception_duplicate", new Object[]{entidadGenExcepcion});
				}else if(claseExcepcion != null  && claseExcepcion.equals("org.hibernate.exception.ConstraintViolationException")){
					if(mensajeExcepcion != null && (mensajeExcepcion.contains("insert") || metodoExcepcion.contains("save") || mensajeExcepcion.contains("batch update"))){
						mensaje = Metodos.getMessageResourceString("exception_ConstraintViolationExceptionInsert", new Object[]{entidadGenExcepcion});
					}else{
						mensaje = Metodos.getMessageResourceString("exception_ConstraintViolationExceptionDelete", new Object[]{entidadGenExcepcion});
					}					
				}else{					
						mensaje = Metodos.getMessageResourceString("exception_gencode", new Object[]{codigo});
				}
			}
			
			String msg = "Código: " + codigo +
								" | Tipo: " + tipoExcepcion +
								" | Clase: " + claseGenExcepcion +
								" | Entidad: " + entidadGenExcepcion +
								" | Método: " + metodoExcepcion +
								" | Excepción: " + claseExcepcion +
								" | Mensaje: " + mensajeExcepcion;
			com.sh.pvs.utils.Metodos.errorLog(Thread.currentThread(), msg);
			
		} catch (Exception e2) {
			if(context != null)
				mensaje = Metodos.getMessageResourceString("exception_gennocode", null);
			com.sh.pvs.utils.Metodos.errorLog(Thread.currentThread(), e2.getMessage());
		}
		return mensaje;
	}
	
	/**
	 * Método para generar el código que relacionará la excepción para el administrador
	 * @return código
	 */
	private static String generarCodigoExcepcion(){		
		String codigo = new SimpleDateFormat("yyyyMMdd_HHmmSS").format(new Date());
		return codigo;
		
	}
	
}
