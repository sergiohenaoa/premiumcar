/**
	 * 	MovimientoCarteraMBean.java:
	 * 
	 * 	Esta clase corresponde al Managed Bean para controlar el CRUD de 
	 * 	la entidad MovimientoCartera
	 *
	 * @author Sergio Henao - sergiohenaoa@gmail.com - 11/04/2015
	 * @Modifier Sergio Henao - sergiohenaoa@gmail.com - 11/04/2015
	 * @version 1.0
	 * 
	 */
package com.sh.pvs.view.contabilidad;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.sh.pvs.core.interfaces.contabilidad.IReporteCarteraServices;
import com.sh.pvs.core.services.contabilidad.ReporteCarteraServices;
import com.sh.pvs.model.dto.contabilidad.ReporteCarteraRow;
import com.sh.pvs.model.dto.seguridad.Usuario;
import com.sh.pvs.view.utils.Constantes;
import com.sh.pvs.view.utils.ControlException;
import com.sh.pvs.view.utils.Metodos;

@ManagedBean
@ViewScoped
public class ReporteCarteraMBean implements Serializable {

	/*Serializable Attribute*/
	private static final long serialVersionUID = 1L;
	
	/*Variables*/
	private Boolean compraPPago = false;
	private Boolean ventaPCobro = false;
	private Boolean pagosPVendedor = false;
	private Boolean cobrosPVendedor = false;
	private Boolean pagosPComprador = false;
	private Boolean cobrosPComprador = false;
 	private List<ReporteCarteraRow> entityList; 				// Lista de registros del datatable
 	private Usuario loggedUser;									//Usuario logeado (En Sesión)
 	
 	/*Navegación*/
 	private String mode;									// view - edit - list - create ==> Modo para identificar la vista
 	
 	/*Servicios*/
 	private IReporteCarteraServices entityService;			// Servicios para la gestión de la entidad
	
 	/*Variables para navegación*/
 	public static String openDialog = "/contabilidad/reporteCarteraForm.faces";
 	public static String openWebMobile = "reporteCarteraForm.faces";
 	public static String closeRedirectMobile = "reporteCarteraFormList.faces";
	public static String closeRedirectWeb = "/contabilidad/reporteCarteraFormList.faces?faces-redirect=true";
 	
 	/*Constructors*/	
	/**	Default constructor */
	public ReporteCarteraMBean() {

	}
	
	/*METHODS*/
	
	/**
	 * Método para inicializar las variables
	 * 
	 * */
	@PostConstruct
	private void init(){
		
		FacesContext context = FacesContext.getCurrentInstance();
		
		try {
			
			Metodos.loadSessionMesagges();
			
			loggedUser = Metodos.getUserSession();
			
			if(context.getExternalContext().getRequestParameterMap().get("mode") != null)
				mode = context.getExternalContext().getRequestParameterMap().get("mode");
			else
				mode = Constantes.MODELIST;
			
			if(mode.equals(Constantes.MODELIST)){
				fillEntityList();
				return;
			}
						
		} catch (Exception e) {
			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
		}
		
	}
	
	/**
	 * Metodo que retorna las listas
	 * @return
	 */
	public void fillEntityList(){
		FacesContext context = FacesContext.getCurrentInstance(); 
		try {
			entityService = new ReporteCarteraServices(null);
			entityList = null;
			entityList = entityService.findAllMovtos(compraPPago, ventaPCobro, 
					pagosPVendedor, cobrosPVendedor, pagosPComprador, cobrosPComprador, loggedUser.getCompania().getId());
			
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}		
	}
	
	/* SECURITY METHODS*/
	/**
	 * Método que valida si el usuario en sesión puede ejecutar una acción sobre este documento
	 * 
	 * @return
	 */
	public boolean hasPermissionTo(String action){		
		return Metodos.hasPermission(ReporteCarteraMBean.class.getSimpleName(), action);
	}

	/* GETTERS AND SETTERS */
	public Double getValorTotal() {
		Double ingresos = 0.0;
		if(entityList != null && entityList.size()>0){
			for (Iterator<ReporteCarteraRow> itRows = entityList.iterator(); itRows.hasNext();) {
				ReporteCarteraRow row = (ReporteCarteraRow) itRows.next();
				if(row.getValor() != null)
					ingresos += row.getValor();
			}
		}
		return ingresos;
	}

	public List<ReporteCarteraRow> getEntityList() {
		return entityList;
	}

	public void setEntityList(List<ReporteCarteraRow> entityList) {
		this.entityList = entityList;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public Boolean getCompraPPago() {
		return compraPPago;
	}
	
	public void setCompraPPago(Boolean compraPPago) {
		this.compraPPago = compraPPago;
	}
	
	public Boolean getVentaPCobro() {
		return ventaPCobro;
	}

	public void setVentaPCobro(Boolean ventaPCobro) {
		this.ventaPCobro = ventaPCobro;
	}
	
	public Boolean getPagosPVendedor() {
		return pagosPVendedor;
	}

	public void setPagosPVendedor(Boolean pagosPVendedor) {
		this.pagosPVendedor = pagosPVendedor;
	}

	public Boolean getCobrosPComprador() {
		return cobrosPComprador;
	}

	public void setCobrosPComprador(Boolean cobrosPComprador) {
		this.cobrosPComprador = cobrosPComprador;
	}

	
	public Boolean getCobrosPVendedor() {
		return cobrosPVendedor;
	}
	

	public void setCobrosPVendedor(Boolean cobrosPVendedor) {
		this.cobrosPVendedor = cobrosPVendedor;
	}
	

	public Boolean getPagosPComprador() {
		return pagosPComprador;
	}
	

	public void setPagosPComprador(Boolean pagosPComprador) {
		this.pagosPComprador = pagosPComprador;
	}
	
}
