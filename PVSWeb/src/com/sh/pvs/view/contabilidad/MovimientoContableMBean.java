/**
	 * 	MovimientoContableMBean.java:
	 * 
	 * 	Esta clase corresponde al Managed Bean para controlar el CRUD de 
	 * 	la entidad MovimientoContable
	 *
	 * @author Sergio Henao - sergiohenaoa@gmail.com - 11/04/2015
	 * @Modifier Sergio Henao - sergiohenaoa@gmail.com - 11/04/2015
	 * @version 1.0
	 * 
	 */
package com.sh.pvs.view.contabilidad;

import com.sh.pvs.core.interfaces.configuracion.ICuentaServices;
import com.sh.pvs.core.interfaces.contabilidad.IMovimientoContableServices;
import com.sh.pvs.core.interfaces.seguridad.IUsuarioServices;
import com.sh.pvs.core.services.configuracion.CuentaServices;
import com.sh.pvs.core.services.contabilidad.MovimientoContableServices;
import com.sh.pvs.core.services.seguridad.UsuarioServices;
import com.sh.pvs.model.dao.contabilidad.MovimientoContableDAO;
import com.sh.pvs.model.dto.configuracion.Cuenta;
import com.sh.pvs.model.dto.contabilidad.MovimientoContable;
import com.sh.pvs.model.dto.seguridad.Usuario;
import com.sh.pvs.model.utils.Condition;
import com.sh.pvs.model.utils.ControlQuery;
import com.sh.pvs.view.seguridad.LoginMBean;
import com.sh.pvs.view.utils.Constantes;
import com.sh.pvs.view.utils.ControlException;
import com.sh.pvs.view.utils.Metodos;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.List;

@ManagedBean
@ViewScoped
public class MovimientoContableMBean implements Serializable {

	/*Serializable Attribute*/
	private static final long serialVersionUID = 1L;
	
	/*Variables*/
	private MovimientoContable entity; 							// ViewForm - EditForm 	
 	private List<MovimientoContable> entityList; 				// Lista de registros del datatable
 	private List<MovimientoContable> entityFilterList;			// Lista de registros filtrados del datatable
 	private List<Cuenta> cuentasDropDownList;
 	private List<Usuario> usuariosDropDownList;
 	private Usuario loggedUser;								//Usuario logeado (En Sesión)
 	private List<Cuenta> cuentasSaldosDropDownList;
 	
 	/*Navegación*/
 	private String mode;									// view - edit - list - create ==> Modo para identificar la vista
 	
 	/*Servicios*/
 	private IMovimientoContableServices entityService;			// Servicios para la gestión de la entidad
	
 	/*Variables para navegación*/
 	public static String openDialog = "/contabilidad/movimientoContableForm.faces";
 	public static String openWebMobile = "movimientoContableForm.faces";
 	public static String closeRedirectMobile = "movimientoContableList.faces";
	public static String closeRedirectWeb = "/contabilidad/movimientoContableList.faces?faces-redirect=true";
 	
 	/*Constructors*/	
	/**	Default constructor */
	public MovimientoContableMBean() {

	}
	
	/*METHODS*/
	
	/**
	 * Método para inicializar las variables
	 * 
	 * */
	@PostConstruct
	private void init(){
		
		FacesContext context = FacesContext.getCurrentInstance();
		
		try {
			
			fillCuentasSaldosList();
			
			Metodos.loadSessionMesagges();
			
			loggedUser = Metodos.getUserSession();
			
			if(context.getExternalContext().getRequestParameterMap().get("mode") != null)
				mode = context.getExternalContext().getRequestParameterMap().get("mode");
			else
				mode = Constantes.MODELIST;
			
			if(mode.equals(Constantes.MODECREATE)){
				this.entity = new MovimientoContable();
				
				fillCuentasDropDown();
				fillUsuariosDropDown();
				
				return;
			}
			
			if(mode.equals(Constantes.MODELIST)){
				fillEntityList();
				return;
			}
						
			if(mode.equals(Constantes.MODEVIEW) || mode.equals(Constantes.MODEEDIT)){
				
				try {
					
					fillCuentasDropDown();
					fillUsuariosDropDown();
					
					String idParam = context.getExternalContext().getRequestParameterMap().get("id");
					entityService = new MovimientoContableServices();
					this.entity = entityService.findById(Integer.parseInt(idParam));
					
				} catch (Exception e) {
					context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
				}
				
				return;
			}
			
		} catch (Exception e) {
			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
		}
		
	}
	
	/**
	 * Create a new MovimientoContable entity
	 * 
	 */
	public String newEntity() {
		
		entity = new MovimientoContable();
		return Metodos.newNavigation(openDialog, openWebMobile);
		
	}
	
	/**
	 * Select an existing MovimientoContable entity
	 * 
	 */
	public String selectEntity(Integer idKey) {				
		
		FacesContext context = FacesContext.getCurrentInstance();
		
		try {
			
			entityService = new MovimientoContableServices();
			entity = entityService.findById(idKey);
			
		} catch (Exception e) {
			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
		}
		
		return Metodos.selectNavigation(idKey, openDialog, openWebMobile);
	}
	
	/**
	 * Edit an existing MovimientoContable entity
	 * 
	 */
	public String editEntity(Integer idKey, boolean fromList) {
		
		FacesContext context = FacesContext.getCurrentInstance();
		
		try {
			
			entityService = new MovimientoContableServices();
			entity = entityService.findById(idKey);
			
		} catch (Exception e) {
			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
		}
		
		return Metodos.editNavigation(idKey, fromList, openDialog, openWebMobile);
	}
	
	/**
	 * Delete an existing MovimientoContable entity
	 * 
	 */
	public String deleteEntity(Integer idKey, boolean fromList) {
		
		FacesContext context = FacesContext.getCurrentInstance(); 
		
		try {
			entityService = new MovimientoContableServices();
			entity = entityService.findById(idKey);
			entityService.deleteTX(entity);
			
			Metodos.auditLog(Constantes.ACTIONDELETE, MovimientoContableMBean.class.getSimpleName(), entity.getDescripcion(), "");
			
			String msg = Metodos.getMessageResourceString("generics_eliminarOK", new Object[]{entity.getDescripcion()});
			if(!fromList || (!Metodos.isMobile() && Metodos.isDlg())){
				Metodos.setSessionMesagges(Constantes.SESSIONMSGINFO, msg);
			}
				
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
				
			fillCuentasSaldosList();
			fillEntityList();
		} catch (Exception e) {
			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
		}
		return  Metodos.close(closeRedirectWeb, closeRedirectMobile, fromList);
		
	}
	
	/**
	 * Metodo que retorna las listas
	 * @return
	 */
	public void fillEntityList(){
		FacesContext context = FacesContext.getCurrentInstance(); 
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		try {
			entityService = new MovimientoContableServices();
			entityList = null;
			LoginMBean loginMBean = (LoginMBean)Metodos.getManagedBean(request, "loginMBean");
			ControlQuery cq = new ControlQuery();
			cq.add(new Condition(MovimientoContableDAO.IDCOMPANIA, Condition.IGUAL, loginMBean.getUsuario().getCompania().getId()));
			cq.getOrderby().add(new String[]{ MovimientoContableDAO.FECHA, Condition.DESC});
			entityList = entityService.findAll(cq);
			
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}		
	}
	
	/**
	 * Metodo para guardar
	 */
	public String saveEntity(){
		
		FacesContext context = FacesContext.getCurrentInstance(); 
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		
		try{
			if(mode != null && mode.equals(Constantes.MODECREATE)
					&& hasPermissionTo(Constantes.ACTIONCREATE)){
				
				LoginMBean loginMBean = (LoginMBean)Metodos.getManagedBean(request, "loginMBean");
				entity.setCompania(loginMBean.getUsuario().getCompania());
				entityService = new MovimientoContableServices();
				entityService.createTX(entity);
				
				Metodos.auditLog(Constantes.ACTIONCREATE, MovimientoContableMBean.class.getSimpleName(), entity.getDescripcion(), "");
				
				String msg = Metodos.getMessageResourceString("generics_guardarOK", new Object[]{entity.getDescripcion()});
				
				if(!Metodos.isMobile()){
					Metodos.setSessionMesagges(Constantes.SESSIONMSGINFO, msg);
				}
					
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
					
				return  Metodos.close(closeRedirectWeb, closeRedirectMobile, true);
			}
			
			else if(mode != null && mode.equals(Constantes.MODEEDIT)
					&& hasPermissionTo(Constantes.ACTIONUPDATE)){
				
				entityService = new MovimientoContableServices();
				entityService.updateTX(entity);
				
				Metodos.auditLog(Constantes.ACTIONUPDATE, MovimientoContableMBean.class.getSimpleName(), entity.getDescripcion(), "");
				
				String msg = Metodos.getMessageResourceString("generics_actualizarOK", new Object[]{entity.getDescripcion()});
				
				if(!Metodos.isMobile()){
					Metodos.setSessionMesagges(Constantes.SESSIONMSGINFO, msg);
				}
					
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
					
				return  Metodos.close(closeRedirectWeb, closeRedirectMobile, true);
			}
					
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}
		
		return "";
	}

	/**
	 * Metodo que se ejecuta cuando se cierra una modal
	 * @return
	 */
	public void dialogListener(){
		fillEntityList();
		fillCuentasSaldosList();
		Metodos.loadSessionMesagges();
	}
	
	/**
	 * Método que llena la lista de cuentas activas para el selector
	 * @param 
	 */
	private void fillCuentasDropDown(){
		FacesContext context = FacesContext.getCurrentInstance(); 
		cuentasDropDownList = null;
		try {
			ICuentaServices service = new CuentaServices();
			cuentasDropDownList = service.findAllActivos(loggedUser.getCompania().getId());
		} catch (Exception e) {
			cuentasDropDownList = null;
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}		
	}
	
	/**
	 * Metodo que retorna las listas
	 * @return
	 */
	public void fillCuentasSaldosList(){
		FacesContext context = FacesContext.getCurrentInstance(); 
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		try {
			CuentaServices cuentaService = new CuentaServices();
			cuentasSaldosDropDownList = null;
			LoginMBean loginMBean = (LoginMBean)Metodos.getManagedBean(request, "loginMBean");
			cuentasSaldosDropDownList = cuentaService.findAllActivosConSaldo(loginMBean.getUsuario().getCompania().getId());
			
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}		
	}
	
	/**
	 * Método que llena la lista de usuarios activos para el selector
	 * @param 
	 */
	private void fillUsuariosDropDown(){
		FacesContext context = FacesContext.getCurrentInstance(); 
		usuariosDropDownList = null;
		try {
			IUsuarioServices service = new UsuarioServices();
			usuariosDropDownList = service.UsuariosActivosByCompania(loggedUser.getCompania().getId());
		} catch (Exception e) {
			usuariosDropDownList = null;
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}		
	}
	
	/* SECURITY METHODS*/
	/**
	 * Método que valida si el usuario en sesión puede ejecutar una acción sobre este documento
	 * 
	 * @return
	 */
	public boolean hasPermissionTo(String action){		
		return Metodos.hasPermission(MovimientoContableMBean.class.getSimpleName(), action);
	}
	
	/* GETTERS AND SETTERS */

	public MovimientoContable getEntity() {
		return entity;
	}

	public void setEntity(MovimientoContable entity) {
		this.entity = entity;
	}

	public List<MovimientoContable> getEntityList() {
		return entityList;
	}

	public void setEntityList(List<MovimientoContable> entityList) {
		this.entityList = entityList;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public List<MovimientoContable> getEntityFilterList() {
		return entityFilterList;
	}

	public void setEntityFilterList(List<MovimientoContable> entityFilterList) {
		this.entityFilterList = entityFilterList;
	}

	
	public List<Cuenta> getCuentasDropDownList() {
		return cuentasDropDownList;
	}

	
	public void setCuentasDropDownList(List<Cuenta> cuentasDropDownList) {
		this.cuentasDropDownList = cuentasDropDownList;
	}

	
	public List<Usuario> getUsuariosDropDownList() {
		return usuariosDropDownList;
	}

	
	public void setUsuariosDropDownList(List<Usuario> usuariosDropDownList) {
		this.usuariosDropDownList = usuariosDropDownList;
	}

	
	public List<Cuenta> getCuentasSaldosDropDownList() {
		return cuentasSaldosDropDownList;
	}
	

	public void setCuentasSaldosDropDownList(List<Cuenta> cuentasSaldosDropDownList) {
		this.cuentasSaldosDropDownList = cuentasSaldosDropDownList;
	}
	
}
