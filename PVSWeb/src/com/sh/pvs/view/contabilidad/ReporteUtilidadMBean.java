package com.sh.pvs.view.contabilidad;

import com.sh.pvs.core.interfaces.hojanegocio.IHojaNegocioServices;
import com.sh.pvs.core.services.hojanegocio.HojaNegocioServices;
import com.sh.pvs.model.dto.hojanegocio.HojaNegocio;
import com.sh.pvs.model.dto.seguridad.Usuario;
import com.sh.pvs.view.utils.ControlException;
import com.sh.pvs.view.utils.Metodos;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


@ManagedBean
@ViewScoped
public class ReporteUtilidadMBean implements Serializable {

	/*Serializable Attribute*/
	private static final long serialVersionUID = 1L;
	
	/*Variables*/
	private Date fchIni;
	private Date fchFin;
 	private List<HojaNegocio> entityList; 				// Lista de registros del datatable
 	private Usuario loggedUser;									//Usuario logeado (En Sesión)
 	
 	/*Navegación*/
 	private String mode;									// view - edit - list - create ==> Modo para identificar la vista
 	
 	/*Servicios*/
 	private IHojaNegocioServices entityService;			// Servicios para la gestión de la entidad

 	/*Constructors*/	
	/**	Default constructor */
	public ReporteUtilidadMBean() {

	}
	
	/*METHODS*/
	
	/**
	 * Método para inicializar las variables
	 * 
	 * */
	@PostConstruct
	private void init(){
		
		FacesContext context = FacesContext.getCurrentInstance();
		
		try {
			
			Metodos.loadSessionMesagges();			
			loggedUser = Metodos.getUserSession();
									
		} catch (Exception e) {
			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
		}
		
	}
	
	/**
	 * Metodo que retorna las listas
	 * @return
	 */
	public void fillEntityList(){
		FacesContext context = FacesContext.getCurrentInstance(); 
		try {
			entityService = new HojaNegocioServices();
			entityList = null;
			entityList = entityService.findAllHojasNegocioByFiltro(fchIni, fchFin, loggedUser.getCompania().getId());
			
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}		
	}
		
	/* SECURITY METHODS*/
	/**
	 * Método que valida si el usuario en sesión puede ejecutar una acción sobre este documento
	 * 
	 * @return
	 */
	public boolean hasPermissionTo(String action){		
		return Metodos.hasPermission(ReporteUtilidadMBean.class.getSimpleName(), action);
	}

	/* GETTERS AND SETTERS */
	public Date getFchIni() {
		return fchIni;
	}

	public void setFchIni(Date fchIni) {
		this.fchIni = fchIni;
	}

	public Date getFchFin() {
		return fchFin;
	}

	public void setFchFin(Date fchFin) {
		this.fchFin = fchFin;
	}

	public Double getTotalUtilidades() {
		Double utilidades = 0.0;
		if(entityList != null && entityList.size()>0){
			for (Iterator<HojaNegocio> itHojaNegocio = entityList.iterator(); itHojaNegocio.hasNext();) {
				HojaNegocio hojaNegocio = itHojaNegocio.next();
				utilidades += hojaNegocio.getUtilidadTotal("CI");
			}
		}
		return utilidades;
	}
	
	public Double getTotalComisiones() {
		Double comisiones = 0.0;
		if(entityList != null && entityList.size()>0){
			for (Iterator<HojaNegocio> itHojaNegocio = entityList.iterator(); itHojaNegocio.hasNext();) {
				HojaNegocio hojaNegocio = itHojaNegocio.next();
				if(hojaNegocio.getPorcentajeComision() != null){
					comisiones += hojaNegocio.getVlrVehVendedor() * hojaNegocio.getPorcentajeComision() / 100;
				}
			}
		}
		return comisiones;
	}
	
	public Double getTotalOtrosIngresos() {
		Double otrosIngresos = 0.0;
		if(entityList != null && entityList.size()>0){
			for (Iterator<HojaNegocio> itHojaNegocio = entityList.iterator(); itHojaNegocio.hasNext();) {
				HojaNegocio hojaNegocio = itHojaNegocio.next();
				if(hojaNegocio.getVlrVehComprador() != null && hojaNegocio.getVlrVehVendedor() != null){
					otrosIngresos += hojaNegocio.getVlrVehComprador() - hojaNegocio.getVlrVehVendedor();
				}
			}
		}
		return otrosIngresos;
	}
	
	public Double getTotalGastosAdicionales() {
		Double gastosAdicionales = 0.0;
		if(entityList != null && entityList.size()>0){
			for (Iterator<HojaNegocio> itHojaNegocio = entityList.iterator(); itHojaNegocio.hasNext();) {
				HojaNegocio hojaNegocio = itHojaNegocio.next();
				gastosAdicionales += hojaNegocio.getTotalGastosAdicionales("CI");
			}
		}
		return gastosAdicionales;
	}

	public List<HojaNegocio> getEntityList() {
		return entityList;
	}

	public void setEntityList(List<HojaNegocio> entityList) {
		this.entityList = entityList;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}
	
}
