/**
	 * 	InventarioAccesoriosMBean.java:
	 * 
	 * 	Esta clase corresponde al Managed Bean para controlar el CRUD de 
	 * 	la entidad InventarioAccesorios
	 *
	 * @author Sergio Henao - sergiohenaoa@gmail.com - 11/04/2015
	 * @Modifier Sergio Henao - sergiohenaoa@gmail.com - 11/04/2015
	 * @version 1.0
	 * 
	 */
package com.sh.pvs.view.contabilidad;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.sh.pvs.core.interfaces.configuracion.IAccesorioServices;
import com.sh.pvs.core.interfaces.contabilidad.IInventarioAccesoriosServices;
import com.sh.pvs.core.services.configuracion.AccesorioServices;
import com.sh.pvs.core.services.contabilidad.InventarioAccesoriosServices;
import com.sh.pvs.model.dao.contabilidad.InventarioAccesoriosDAO;
import com.sh.pvs.model.dto.configuracion.Accesorio;
import com.sh.pvs.model.dto.contabilidad.InventarioAccesorios;
import com.sh.pvs.model.dto.seguridad.Usuario;
import com.sh.pvs.view.utils.Constantes;
import com.sh.pvs.view.utils.ControlException;
import com.sh.pvs.view.utils.Metodos;

@ManagedBean
@ViewScoped
public class InventarioAccesoriosMBean implements Serializable {

	/*Serializable Attribute*/
	private static final long serialVersionUID = 1L;
	
	/*Variables*/
	private InventarioAccesorios entity; 							// ViewForm - EditForm 	
 	private List<InventarioAccesorios> entityList; 				// Lista de registros del datatable
 	private List<InventarioAccesorios> entityFilterList;			// Lista de registros filtrados del datatable
 	private List<Accesorio> accesoriosDropDownList;
 	private Usuario loggedUser;	
 	
 	/*Navegación*/
 	private String mode;									// view - edit - list - create ==> Modo para identificar la vista
 	
 	/*Servicios*/
 	private IInventarioAccesoriosServices entityService;			// Servicios para la gestión de la entidad
	
 	/*Variables para navegación*/
 	public static String openDialog = "/contabilidad/inventarioAccesoriosForm.faces";
 	public static String openWebMobile = "inventarioAccesoriosForm.faces";
 	public static String closeRedirectMobile = "inventarioAccesoriosList.faces";
	public static String closeRedirectWeb = "/contabilidad/inventarioAccesoriosList.faces?faces-redirect=true";
 	
 	/*Constructors*/	
	/**	Default constructor */
	public InventarioAccesoriosMBean() {

	}
	
	/*METHODS*/
	
	/**
	 * Método para inicializar las variables
	 * 
	 * */
	@PostConstruct
	private void init(){
		
		FacesContext context = FacesContext.getCurrentInstance();
		
		try {
			
			Metodos.loadSessionMesagges();
			
			loggedUser = Metodos.getUserSession();
			
			if(context.getExternalContext().getRequestParameterMap().get("mode") != null)
				mode = context.getExternalContext().getRequestParameterMap().get("mode");
			else
				mode = Constantes.MODELIST;
			
			if(mode.equals(Constantes.MODECREATE)){
				this.entity = new InventarioAccesorios();
				entity.setAccesorio(new Accesorio());
				fillAccesoriosDropDown();
				return;
			}
			
			if(mode.equals(Constantes.MODELIST)){
				fillEntityList();
				return;
			}
						
			if(mode.equals(Constantes.MODEVIEW) || mode.equals(Constantes.MODEEDIT)){
				
				try {
					
					String idParam = context.getExternalContext().getRequestParameterMap().get("id");
					entityService = new InventarioAccesoriosServices();
					this.entity = entityService.findById(Integer.parseInt(idParam));
					
					if(mode.equals(Constantes.MODEEDIT))
						fillAccesoriosDropDown();
					
				} catch (Exception e) {
					context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
				}
				
				return;
			}
			
		} catch (Exception e) {
			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
		}
		
	}
	
	/**
	 * Create a new InventarioAccesorios entity
	 * 
	 */
	public String newEntity() {
		
		entity = new InventarioAccesorios();
		return Metodos.newNavigation(openDialog, openWebMobile);
		
	}
	
	/**
	 * Select an existing InventarioAccesorios entity
	 * 
	 */
	public String selectEntity(Integer idKey) {				
		
		FacesContext context = FacesContext.getCurrentInstance();
		
		try {
			
			entityService = new InventarioAccesoriosServices();
			entity = entityService.findById(idKey);
			
		} catch (Exception e) {
			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
		}
		
		return Metodos.selectNavigation(idKey, openDialog, openWebMobile);
	}
	
	/**
	 * Edit an existing InventarioAccesorios entity
	 * 
	 */
	public String editEntity(Integer idKey, boolean fromList) {
		
		FacesContext context = FacesContext.getCurrentInstance();
		
		try {
			
			entityService = new InventarioAccesoriosServices();
			entity = entityService.findById(idKey);
			
		} catch (Exception e) {
			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
		}
		
		return Metodos.editNavigation(idKey, fromList, openDialog, openWebMobile);
	}
	
	/**
	 * Delete an existing InventarioAccesorios entity
	 * 
	 */
	public String deleteEntity(Integer idKey, boolean fromList) {
		
		FacesContext context = FacesContext.getCurrentInstance(); 
		
		try {
			entityService = new InventarioAccesoriosServices();
			entity = entityService.findById(idKey);
			entityService.deleteTX(entity);
			
			Metodos.auditLog(Constantes.ACTIONDELETE, InventarioAccesoriosMBean.class.getSimpleName(), entity.getAccesorio().getNombre(), "");
			
			String msg = Metodos.getMessageResourceString("generics_eliminarOK", new Object[]{entity.getAccesorio().getNombre()});
			if(!fromList || (!Metodos.isMobile() && Metodos.isDlg())){
				Metodos.setSessionMesagges(Constantes.SESSIONMSGINFO, msg);
			}
				
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
				
			fillEntityList();
		} catch (Exception e) {
			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
		}
		return  Metodos.close(closeRedirectWeb, closeRedirectMobile, fromList);
		
	}
	
	/**
	 * Metodo que retorna las listas
	 * @return
	 */
	public void fillEntityList(){
		FacesContext context = FacesContext.getCurrentInstance(); 
		try {
			entityService = new InventarioAccesoriosServices();
			entityList = null;
			entityList = entityService.findByProperty(InventarioAccesoriosDAO.IDCOMPANIA , loggedUser.getCompania().getId());
			
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}		
	}
	
	/**
	 * Método que llena la lista deaccesorios activos para el selector
	 * @param 
	 */
	private void fillAccesoriosDropDown(){
		FacesContext context = FacesContext.getCurrentInstance(); 
		accesoriosDropDownList = null;
		try {
			IAccesorioServices accesoriosSrv = new AccesorioServices();
			accesoriosDropDownList = accesoriosSrv.findAllActivos(loggedUser.getCompania().getId());
		} catch (Exception e) {
			accesoriosDropDownList = null;
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}		
	}
	
	/**
	 * Metodo para guardar
	 */
	public String saveEntity(){
		
		FacesContext context = FacesContext.getCurrentInstance(); 
		
		try{
			if(mode != null && mode.equals(Constantes.MODECREATE)
					&& hasPermissionTo(Constantes.ACTIONCREATE)){
				
				entity.setCompania(loggedUser.getCompania());
				entityService = new InventarioAccesoriosServices();
				entityService.createTX(entity);
				
				Metodos.auditLog(Constantes.ACTIONCREATE, InventarioAccesoriosMBean.class.getSimpleName(), entity.getAccesorio().getNombre(), "");
				
				String msg = Metodos.getMessageResourceString("generics_guardarOK", new Object[]{entity.getAccesorio().getNombre()});
				
				if(!Metodos.isMobile()){
					Metodos.setSessionMesagges(Constantes.SESSIONMSGINFO, msg);
				}
					
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
					
				return  Metodos.close(closeRedirectWeb, closeRedirectMobile, true);
			}
			
			else if(mode != null && mode.equals(Constantes.MODEEDIT)
					&& hasPermissionTo(Constantes.ACTIONUPDATE)){
				
				entityService = new InventarioAccesoriosServices();
				entityService.updateTX(entity);
				
				Metodos.auditLog(Constantes.ACTIONUPDATE, InventarioAccesoriosMBean.class.getSimpleName(), entity.getAccesorio().getNombre(), "");
				
				String msg = Metodos.getMessageResourceString("generics_actualizarOK", new Object[]{entity.getAccesorio().getNombre()});
				
				if(!Metodos.isMobile()){
					Metodos.setSessionMesagges(Constantes.SESSIONMSGINFO, msg);
				}
					
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
					
				return  Metodos.close(closeRedirectWeb, closeRedirectMobile, true);
			}
					
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}
		
		return "";
	}

	/**
	 * Metodo que se ejecuta cuando se cierra una modal
	 * @return
	 */
	public void dialogListener(){
		fillEntityList();
		Metodos.loadSessionMesagges();
	}
	
	/* SECURITY METHODS*/
	/**
	 * Método que valida si el usuario en sesión puede ejecutar una acción sobre este documento
	 * 
	 * @return
	 */
	public boolean hasPermissionTo(String action){		
		return Metodos.hasPermission(InventarioAccesoriosMBean.class.getSimpleName(), action);
	}
	
	/* GETTERS AND SETTERS */

	public InventarioAccesorios getEntity() {
		return entity;
	}

	public void setEntity(InventarioAccesorios entity) {
		this.entity = entity;
	}

	public List<InventarioAccesorios> getEntityList() {
		return entityList;
	}

	public void setEntityList(List<InventarioAccesorios> entityList) {
		this.entityList = entityList;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public List<InventarioAccesorios> getEntityFilterList() {
		return entityFilterList;
	}

	public void setEntityFilterList(List<InventarioAccesorios> entityFilterList) {
		this.entityFilterList = entityFilterList;
	}

	
	public List<Accesorio> getAccesoriosDropDownList() {
		return accesoriosDropDownList;
	}
	

	public void setAccesoriosDropDownList(List<Accesorio> accesoriosDropDownList) {
		this.accesoriosDropDownList = accesoriosDropDownList;
	}	
	
}
