/**
	 * 	CompraVentaAccesoriosMBean.java:
	 * 
	 * 	Esta clase corresponde al Managed Bean para controlar el CRUD de 
	 * 	la entidad CompraVentaAccesorios
	 *
	 * @author Sergio Henao - sergiohenaoa@gmail.com - 11/04/2015
	 * @Modifier Sergio Henao - sergiohenaoa@gmail.com - 11/04/2015
	 * @version 1.0
	 * 
	 */
package com.sh.pvs.view.contabilidad;

import com.sh.pvs.core.interfaces.configuracion.IAccesorioServices;
import com.sh.pvs.core.interfaces.configuracion.ICuentaServices;
import com.sh.pvs.core.interfaces.contabilidad.IAccesorioxCompraVentaServices;
import com.sh.pvs.core.interfaces.contabilidad.ICompraVentaAccesoriosServices;
import com.sh.pvs.core.interfaces.seguridad.IUsuarioServices;
import com.sh.pvs.core.services.configuracion.AccesorioServices;
import com.sh.pvs.core.services.configuracion.CuentaServices;
import com.sh.pvs.core.services.contabilidad.AccesorioxCompraVentaServices;
import com.sh.pvs.core.services.contabilidad.CompraVentaAccesoriosServices;
import com.sh.pvs.core.services.contabilidad.MovimientoContableServices;
import com.sh.pvs.core.services.seguridad.UsuarioServices;
import com.sh.pvs.model.dto.configuracion.Accesorio;
import com.sh.pvs.model.dto.configuracion.Cuenta;
import com.sh.pvs.model.dto.contabilidad.AccesorioxCompraVenta;
import com.sh.pvs.model.dto.contabilidad.CompraVentaAccesorios;
import com.sh.pvs.model.dto.contabilidad.MovimientoContable;
import com.sh.pvs.model.dto.seguridad.Usuario;
import com.sh.pvs.view.utils.Constantes;
import com.sh.pvs.view.utils.ControlException;
import com.sh.pvs.view.utils.Metodos;
import org.primefaces.context.RequestContext;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.*;


@ManagedBean
@ViewScoped
public class CompraVentaAccesoriosMBean implements Serializable {

	/*Serializable Attribute*/
	private static final long serialVersionUID = 1L;
	
	/*Variables*/
	private CompraVentaAccesorios entity; 								// ViewForm - EditForm 	
 	private List<CompraVentaAccesorios> entityList; 					// Lista de registros del datatable
 	private List<CompraVentaAccesorios> entityFilterList;				// Lista de registros filtrados del datatable
 	private AccesorioxCompraVenta accesorioxCompraVenta;
 	private List<Accesorio> accesoriosDropDownList;
 	private List<Cuenta> cuentasDropDownList;
 	private List<Usuario> usuariosDropDownList;
 	private Usuario loggedUser;											//Usuario logeado (En Sesión)
 	
 	/*Navegación*/
 	private String mode;									// view - edit - list - create ==> Modo para identificar la vista
 	
 	/*Servicios*/
 	private ICompraVentaAccesoriosServices entityService;			// Servicios para la gestión de la entidad
	
 	/*Variables para navegación*/
 	public static String openDialog = "/contabilidad/compraVentaAccesoriosForm.faces";
 	public static String openWebMobile = "compraVentaAccesoriosForm.faces";
 	public static String closeRedirectMobile = "compraVentaAccesoriosList.faces";
	public static String closeRedirectWeb = "/contabilidad/compraVentaAccesoriosList.faces?faces-redirect=true";
	
	public static String openDialogAccesorio = "/contabilidad/accesorioxCompraVentaForm.faces";
 	public static String openWebMobileAccesorio = "accesorioxCompraVentaForm.faces";
 	
 	/*Constructors*/	
	/**	Default constructor */
	public CompraVentaAccesoriosMBean() {

	}
	
	/*METHODS*/
	
	/**
	 * Método para inicializar las variables
	 * 
	 * */
	@PostConstruct
	private void init(){
		
		FacesContext context = FacesContext.getCurrentInstance();
		
		try {
			
			Metodos.loadSessionMesagges();
			
			loggedUser = Metodos.getUserSession();
			
			if(context.getExternalContext().getRequestParameterMap().get("mode") != null)
				mode = context.getExternalContext().getRequestParameterMap().get("mode");
			else
				mode = Constantes.MODELIST;
			
			if(mode.equals(Constantes.MODECREATE)){
				this.entity = new CompraVentaAccesorios();
				this.entity.setCuenta(new Cuenta());
				this.entity.setUsuario(new Usuario());
				fillCuentasDropDown();
				fillUsuariosDropDown();
				return;
			}
			
			if(mode.equals(Constantes.MODELIST)){
				fillEntityList();
				return;
			}
						
			if(mode.equals(Constantes.MODEVIEW) || mode.equals(Constantes.MODEEDIT)){
				
				try {
					
					String idParam = context.getExternalContext().getRequestParameterMap().get("id");
					entityService = new CompraVentaAccesoriosServices();
					entity = entityService.findById(Integer.parseInt(idParam));
					if(entity.getCuenta() == null){
						entity.setCuenta(new Cuenta());
					}
					if(mode.equals(Constantes.MODEEDIT)){
						fillCuentasDropDown();
						fillUsuariosDropDown();
					}
							
				} catch (Exception e) {
					context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
				}
				
				return;
			}
			
			//Si es la consulta de un accesorio
			if(mode.equals("ACCESORIOVIEW")){
				
				IAccesorioxCompraVentaServices accesorioSrv = new AccesorioxCompraVentaServices();
				String idaccesorio = context.getExternalContext().getRequestParameterMap().get("idaccesorio");
				accesorioxCompraVenta = accesorioSrv.findById(Integer.parseInt(idaccesorio));
				
				entityService = new CompraVentaAccesoriosServices();
				entity = entityService.findById(accesorioxCompraVenta.getCompraVentaAccesorios().getId());
				if(entity.getCuenta() == null){
					entity.setCuenta(new Cuenta());
				}
			}
			
			//Si es la creación de un accesorio
			if(mode.equals("ACCESORIONEW")){
				
				entityService = new CompraVentaAccesoriosServices();
				String idCVA = context.getExternalContext().getRequestParameterMap().get("idCVA");
				if(idCVA != null && !idCVA.equals("null")){
					entity = entityService.findById(Integer.parseInt(idCVA));
					if(entity.getCuenta() == null){
						entity.setCuenta(new Cuenta());
					}
				}else{
					entity = new CompraVentaAccesorios();
				}
				
				accesorioxCompraVenta = new AccesorioxCompraVenta();
				accesorioxCompraVenta.setCompraVentaAccesorios(entity);
				accesorioxCompraVenta.setAccesorio(new Accesorio());
				
				fillAccesoriosDropDown();
				
			}
			
		} catch (Exception e) {
			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
		}
		
	}
	
	/**
	 * Create a new Accesorio entity
	 * 
	 */
	public String newEntity() throws Exception{
		
		entity = new CompraVentaAccesorios();
		
		FacesContext context = FacesContext.getCurrentInstance(); 
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		response.sendRedirect(openWebMobile + "?mode=" + Constantes.MODECREATE);
		
		return "";
		
	}
	
	/**
	 * Select an existing CompraVentaAccesorios entity
	 * 
	 */
	public String selectEntity(Integer idKey) throws Exception {			
		
		FacesContext context = FacesContext.getCurrentInstance(); 
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		response.sendRedirect(openWebMobile + "?id=" + idKey + "&mode=" + Constantes.MODEVIEW);
		
		return "";
	}
	
	/**
	 * Edit an existing CompraVentaAccesorios entity
	 * 
	 */
	public String editEntity(Integer idKey, boolean fromList) throws Exception {
		
		FacesContext context = FacesContext.getCurrentInstance(); 
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		response.sendRedirect(openWebMobile + "?id=" + idKey + "&mode=" + Constantes.MODEEDIT);
		
		return "";
		
	}
	
	/**
	 * Delete an existing CompraVentaAccesorios entity
	 * 
	 */
	public String deleteEntity(Integer idKey, boolean fromList) {
		
		FacesContext context = FacesContext.getCurrentInstance(); 
		
		try {
			entityService = new CompraVentaAccesoriosServices();
			entity = entityService.findById(idKey);
			if(entity.getCuenta() == null){
				entity.setCuenta(new Cuenta());
			}
			entityService.deleteTX(entity);
			
			Metodos.auditLog(Constantes.ACTIONDELETE, CompraVentaAccesoriosMBean.class.getSimpleName(), entity.getNroFactura(), "");
			
			String msg = Metodos.getMessageResourceString("generics_eliminarOK", new Object[]{entity.getNroFactura()});
			if(!fromList || (!Metodos.isMobile() && Metodos.isDlg())){
				Metodos.setSessionMesagges(Constantes.SESSIONMSGINFO, msg);
			}
				
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
				
			fillEntityList();
		} catch (Exception e) {
			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
		}
		return  Metodos.close(closeRedirectWeb, closeRedirectMobile, fromList);
		
	}
	
	/**
	 * Metodo que retorna las listas
	 * @return
	 */
	public void fillEntityList(){
		FacesContext context = FacesContext.getCurrentInstance(); 
		try {
			entityService = new CompraVentaAccesoriosServices();
			entityList = entityService.findAll(loggedUser.getCompania().getId());
			
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}		
	}
	
	/**
	 * Metodo para guardar
	 */
	public String saveEntity(){
		
		FacesContext context = FacesContext.getCurrentInstance(); 
		
		try{
			if(entity.getEsPagado()){
				entity.setPlazo(0);
			}
			if(mode != null && mode.equals(Constantes.MODECREATE)
					&& hasPermissionTo(Constantes.ACTIONCREATE)){
				
				entity.setCompania(loggedUser.getCompania());
				entityService = new CompraVentaAccesoriosServices();
				if(entity.getCuenta() != null && 
						(entity.getCuenta().getId() == null || entity.getCuenta().getId().equals(new Integer(0)))){
					entity.setCuenta(null);
				}
				entityService.createTX(entity);
				
				Metodos.auditLog(Constantes.ACTIONCREATE, CompraVentaAccesoriosMBean.class.getSimpleName(), entity.getNroFactura(), "");
				
				String msg = Metodos.getMessageResourceString("generics_guardarOK", new Object[]{entity.getNroFactura()});
				
				if(!Metodos.isMobile()){
					Metodos.setSessionMesagges(Constantes.SESSIONMSGINFO, msg);
				}
					
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
				
				String redirect = "/contabilidad/compraVentaAccesoriosForm.faces?faces-redirect=true&mode="+Constantes.MODEEDIT+"&id="+entity.getId();
				return  Metodos.close(redirect, closeRedirectMobile, true);
			}
			
			else if(mode != null && mode.equals(Constantes.MODEEDIT)
					&& hasPermissionTo(Constantes.ACTIONUPDATE)){
				
				CompraVentaAccesorios entityOLD = entityService.findById(entity.getId());
				SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
				MovimientoContableServices movimientoContableServices = new MovimientoContableServices();
				MovimientoContable movimientoContable = new MovimientoContable();
				movimientoContable.setCodigo(dateFormat.format(new Date()));
				movimientoContable.setCompania(entity.getCompania());
				movimientoContable.setCuenta(entity.getCuenta());
				movimientoContable.setDescripcion("Compra venta accesorios - Factura: " + entity.getNroFactura() + " Observaciones: " + entity.getObservaciones());
				movimientoContable.setEsIngreso(entity.getEsCompra());
				movimientoContable.setFecha(new Date());
				movimientoContable.setUsuario(entity.getUsuario());
				movimientoContable.setValor(entityOLD.getTotalCalculado());
				movimientoContable.setEsPendiente(false);
				movimientoContableServices.createTX(movimientoContable);
								
				entityService = new CompraVentaAccesoriosServices();
				if(entity.getCuenta() != null && 
						(entity.getCuenta().getId() == null || entity.getCuenta().getId().equals(new Integer(0)))){
					entity.setCuenta(null);
				}
				entityService.updateTX(entity);
				updateMovtosContables();
				
				Metodos.auditLog(Constantes.ACTIONUPDATE, CompraVentaAccesoriosMBean.class.getSimpleName(), entity.getNroFactura(), "");
				
				String msg = Metodos.getMessageResourceString("generics_actualizarOK", new Object[]{entity.getNroFactura()});
				
				if(!Metodos.isMobile()){
					Metodos.setSessionMesagges(Constantes.SESSIONMSGINFO, msg);
				}
					
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
													
				String redirect = "/contabilidad/compraVentaAccesoriosForm.faces?faces-redirect=true&mode="+Constantes.MODEEDIT+"&id="+entity.getId();
				return  Metodos.close(redirect, closeRedirectMobile, true);
			}
					
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}
		
		return "";
	}

	/**
	 * Metodo que se ejecuta cuando se cierra una modal
	 * @return
	 */
	public void dialogListener(){
		fillEntityList();
		Metodos.loadSessionMesagges();
	}
	
	/**
	 * Metodo para guardar una entidad de asociación
	 */
	public String saveAccesorio(){
		
		FacesContext context = FacesContext.getCurrentInstance(); 
		
		try{
			if(hasPermissionTo(Constantes.ACTIONUPDATE)){
				String msg = "";
				if(entity.getId() != null){
					entityService = new CompraVentaAccesoriosServices();
					entityService.addAccesorio(accesorioxCompraVenta);
					entity = entityService.findById(entity.getId());
										
					if(entity.getAccesorios() != null && entity.getAccesorios().size() > 1){
						SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
						MovimientoContableServices movimientoContableServices = new MovimientoContableServices();
						MovimientoContable movimientoContable = new MovimientoContable();
						movimientoContable.setCodigo(dateFormat.format(new Date()));
						movimientoContable.setCompania(entity.getCompania());
						movimientoContable.setCuenta(entity.getCuenta());
						movimientoContable.setDescripcion("Compra venta accesorios - Factura: " + entity.getNroFactura() + " Observaciones: " + entity.getObservaciones());
						movimientoContable.setEsIngreso(entity.getEsCompra());
						movimientoContable.setFecha(new Date());
						movimientoContable.setUsuario(entity.getUsuario());
						movimientoContable.setEsPendiente(false);
						movimientoContable.setValor(entity.getTotalCalculado() - (accesorioxCompraVenta.getCantidad() * accesorioxCompraVenta.getPrecio()));
						movimientoContableServices.createTX(movimientoContable);
					}
					
					updateMovtosContables();
					
					if(entity.getCuenta() == null){
						entity.setCuenta(new Cuenta());
					}
					
					Metodos.auditLog(Constantes.ACTIONUPDATE, CompraVentaAccesoriosMBean.class.getSimpleName(), entity.getId().toString(), "Accesorio asociado: " + accesorioxCompraVenta.getAccesorio().getNombre());
					accesorioxCompraVenta = new AccesorioxCompraVenta();
					accesorioxCompraVenta.setAccesorio(new Accesorio());
					
					msg = Metodos.getMessageResourceString("generics_actualizarOK", new Object[]{entity.getId()});
					context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
					
				}else{
					msg = "Por favor complete y guarde los datos de la factura antes de agregar accesorios.";
					context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, msg, "")); // MENS
				}
				
				if(!Metodos.isMobile()){
					Metodos.setSessionMesagges(Constantes.SESSIONMSGINFO, msg);
				}
				
			}
					
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}
		
		return  Metodos.close(closeRedirectWeb, closeRedirectMobile, false);
	}
	
	/**
	 * Metodo para eliminar una entidad de asociación
	 */
	public String deleteAccesorio(AccesorioxCompraVenta accesorioToDelete){
		
		FacesContext context = FacesContext.getCurrentInstance(); 
		
		try{
			if(hasPermissionTo(Constantes.ACTIONUPDATE)){
				
				SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
				MovimientoContableServices movimientoContableServices = new MovimientoContableServices();
				MovimientoContable movimientoContable = new MovimientoContable();
				movimientoContable.setCodigo(dateFormat.format(new Date()));
				movimientoContable.setCompania(entity.getCompania());
				movimientoContable.setCuenta(entity.getCuenta());
				movimientoContable.setDescripcion("Compra venta accesorios - Factura: " + entity.getNroFactura() + " Observaciones: " + entity.getObservaciones());
				movimientoContable.setEsIngreso(entity.getEsCompra());
				movimientoContable.setFecha(new Date());
				movimientoContable.setUsuario(entity.getUsuario());
				movimientoContable.setValor(entity.getTotalCalculado());
				movimientoContable.setEsPendiente(false);
				movimientoContableServices.createTX(movimientoContable);

				entityService = new CompraVentaAccesoriosServices();
				entityService.deleteAccesorio(accesorioToDelete);
				entity = entityService.findById(entity.getId());
				if(entity.getCuenta() == null){
					entity.setCuenta(new Cuenta());
				}
				
				Metodos.auditLog(Constantes.ACTIONUPDATE, CompraVentaAccesoriosMBean.class.getSimpleName(), entity.getId().toString(), "Accesorio eliminado: " + accesorioToDelete.getAccesorio().getNombre());
				
				String msg = Metodos.getMessageResourceString("generics_actualizarOK", new Object[]{entity.getId()});
				
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
				
				if(!Metodos.isMobile() && Metodos.isDlg()){
					Metodos.setSessionMesagges(Constantes.SESSIONMSGINFO, msg);
				}
				updateMovtosContables();
			}
					
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}
		
		if(Metodos.isDlg()){
		    RequestContext.getCurrentInstance().closeDialog(null);
	    }	
		return "";
	}
	
	public void onAccesorioEdit(AccesorioxCompraVenta accesorio) {
		FacesContext context = FacesContext.getCurrentInstance(); 
		
		try{
			if(hasPermissionTo(Constantes.ACTIONUPDATE)){
				
				CompraVentaAccesorios entityOLD = entityService.findById(entity.getId());
				
				SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
				MovimientoContableServices movimientoContableServices = new MovimientoContableServices();
				MovimientoContable movimientoContable = new MovimientoContable();
				movimientoContable.setCodigo(dateFormat.format(new Date()));
				movimientoContable.setCompania(entity.getCompania());
				movimientoContable.setCuenta(entity.getCuenta());
				movimientoContable.setDescripcion("Compra venta accesorios - Factura: " + entity.getNroFactura() + " Observaciones: " + entity.getObservaciones());
				movimientoContable.setEsIngreso(entity.getEsCompra());
				movimientoContable.setFecha(new Date());
				movimientoContable.setUsuario(entity.getUsuario());
				movimientoContable.setValor(entityOLD.getTotalCalculado());
				movimientoContable.setEsPendiente(false);
				movimientoContableServices.createTX(movimientoContable);
				
				entityService = new CompraVentaAccesoriosServices();
				entityService.updateAccesorio(accesorio);
				entity = entityService.findById(entity.getId());
				if(entity.getCuenta() == null){
					entity.setCuenta(new Cuenta());
				}
				
				Metodos.auditLog(Constantes.ACTIONUPDATE, CompraVentaAccesoriosMBean.class.getSimpleName(), entity.getId().toString(), "Accesorio actualizado: " + accesorio.getAccesorio().getNombre());
				
				String msg = Metodos.getMessageResourceString("generics_actualizarOK", new Object[]{entity.getId()});
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
				updateMovtosContables();	
			}
					
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}
		
    }
	
	/**
	 * Método que llena la lista de accesorios activos para el selector
	 * @param 
	 */
	private void fillAccesoriosDropDown(){
		FacesContext context = FacesContext.getCurrentInstance(); 
		accesoriosDropDownList = null;
		try {
			IAccesorioServices srv = new AccesorioServices();
			accesoriosDropDownList = srv.findAllActivos(loggedUser.getCompania().getId());
		} catch (Exception e) {
			accesoriosDropDownList = null;
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}
	}
	
	/**
	 * Método que inicializa las variables y abre el formulario para agregar o consultar un accesorio
	 * 
	 */
	public String openAccesorioForm(String section_id, Integer accesorio_id) throws Exception{
		
		//Definición de los modos de las vistas
		String modeAccesorio = "";
		
		//Si se dio clic en el botón agregar accesorio
		if(section_id.equals("ADDACCESORIO")){
			
			//Si no hay un id del accesorio, se supone que es la creación de uno nuevo
			if(accesorio_id == null || accesorio_id.equals(new Integer(0))){
				modeAccesorio = "ACCESORIONEW"; //Nuevo accesorio
			}else{
				modeAccesorio = "ACCESORIOVIEW"; //Consulta accesorio
			}
		}
		
		FacesContext context = FacesContext.getCurrentInstance(); 
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		
		if(!Metodos.isMobile()){
			
			Map<String,Object> options = new HashMap<String,Object>();
	        options.put("modal", true);
	        options.put("resizable", false);
	        options.put("contentHeight", 450);
	        options.put("contentWidth", 600);
	        
	        Map<String, List<String>> params = new HashMap<String, List<String>>();
		       
	        List<String> values = new ArrayList<String>();
	        values.add(modeAccesorio);
	        params.put("mode", values);
	        
	        List<String> valuesidaccesorio = new ArrayList<String>();	        
	        valuesidaccesorio.add(accesorio_id.toString());
	        params.put("idaccesorio", valuesidaccesorio);
	        
	        List<String> valuesidCVA = new ArrayList<String>();	        
	        valuesidCVA.add(String.valueOf(entity.getId()));
	        params.put("idCVA", valuesidCVA);
			
			RequestContext.getCurrentInstance().openDialog(openDialogAccesorio, options, params);
			
		}else{
			//Si la navegación es en dispoisitivos móviles, se redirecciona
			try {
				response.sendRedirect(openWebMobileAccesorio + "?idaccesorio=" + accesorio_id + "&mode=" + modeAccesorio + "&idCVA=" + entity.getId().toString());
			} catch (Exception e) {
				return "";
			}
			
		}
		
		return "";
		
	}
	
	/**
	 * Metodo que se ejecuta cuando se cierra una modal
	 * @return
	 */
	public void accesorioDialogListener() throws Exception{
		entityService = new CompraVentaAccesoriosServices();
		if(entity.getId() != null){
			entity = entityService.findById(entity.getId());
			if(entity.getCuenta() == null){
				entity.setCuenta(new Cuenta());
			}
		}
		Metodos.loadSessionMesagges();
	}
	
	/**
	 * Método que llena la lista de cuentas activas para el selector
	 * @param 
	 */
	private void fillCuentasDropDown(){
		FacesContext context = FacesContext.getCurrentInstance(); 
		cuentasDropDownList = null;
		try {
			ICuentaServices cuentaSrv = new CuentaServices();
			cuentasDropDownList = cuentaSrv.findAllActivos(loggedUser.getCompania().getId());
		} catch (Exception e) {
			cuentasDropDownList = null;
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}		
	}
	
	/**
	 * Método que llena la lista de usuarios activas para el selector
	 * @param 
	 */
	private void fillUsuariosDropDown(){
		FacesContext context = FacesContext.getCurrentInstance(); 
		usuariosDropDownList = null;
		try {
			IUsuarioServices srv = new UsuarioServices();
			usuariosDropDownList = srv.UsuariosActivosByCompania(loggedUser.getCompania().getId());
		} catch (Exception e) {
			usuariosDropDownList = null;
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}		
	}
	
	private void updateMovtosContables() throws Exception
	{
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		MovimientoContableServices movimientoContableServices = new MovimientoContableServices();
		MovimientoContable movimientoContable = new MovimientoContable();
		
		movimientoContable.setCodigo(dateFormat.format(new Date()));
		movimientoContable.setCompania(entity.getCompania());
		movimientoContable.setCuenta(entity.getCuenta());
		movimientoContable.setDescripcion("Compra venta accesorios - Factura: " + entity.getNroFactura() + " Observaciones: " + entity.getObservaciones());
		movimientoContable.setEsIngreso(!entity.getEsCompra());
		movimientoContable.setFecha(new Date());
		movimientoContable.setUsuario(entity.getUsuario());
		movimientoContable.setValor(entity.getTotalCalculado());
		movimientoContable.setEsPendiente(false);
		movimientoContableServices.createTX(movimientoContable);
		
	}
	
	/**
	 * Open Plantillas Form
	 * 
	 */
	public String openGenerarPlantillaForm() {
		
		return Metodos.openGenerarPlantillaForm(com.sh.pvs.core.utils.Constantes.CODCOMPRAVENTAACCESORIOS, entity.getId().toString());
		
	}
	 
	/* SECURITY METHODS*/
	/**
	 * Método que valida si el usuario en sesión puede ejecutar una acción sobre este documento
	 * 
	 * @return
	 */
	public boolean hasPermissionTo(String action){		
		return Metodos.hasPermission(CompraVentaAccesoriosMBean.class.getSimpleName(), action);
	}
	
	/* GETTERS AND SETTERS */

	public CompraVentaAccesorios getEntity() {
		return entity;
	}

	public void setEntity(CompraVentaAccesorios entity) {
		this.entity = entity;
	}

	public List<CompraVentaAccesorios> getEntityList() {
		return entityList;
	}

	public void setEntityList(List<CompraVentaAccesorios> entityList) {
		this.entityList = entityList;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public List<CompraVentaAccesorios> getEntityFilterList() {
		return entityFilterList;
	}

	public void setEntityFilterList(List<CompraVentaAccesorios> entityFilterList) {
		this.entityFilterList = entityFilterList;
	}

	public AccesorioxCompraVenta getAccesorioxCompraVenta() {
		return accesorioxCompraVenta;
	}
	
	public void setAccesorioxCompraVenta(AccesorioxCompraVenta accesorioxCompraVenta) {
		this.accesorioxCompraVenta = accesorioxCompraVenta;
	}

	public List<Accesorio> getAccesoriosDropDownList() {
		return accesoriosDropDownList;
	}

	public void setAccesoriosDropDownList(List<Accesorio> accesoriosDropDownList) {
		this.accesoriosDropDownList = accesoriosDropDownList;
	}

	public List<Cuenta> getCuentasDropDownList() {
		return cuentasDropDownList;
	}

	public void setCuentasDropDownList(List<Cuenta> cuentasDropDownList) {
		this.cuentasDropDownList = cuentasDropDownList;
	}

	public List<Usuario> getUsuariosDropDownList() {
		return usuariosDropDownList;
	}

	public void setUsuariosDropDownList(List<Usuario> usuariosDropDownList) {
		this.usuariosDropDownList = usuariosDropDownList;
	}
	
}
