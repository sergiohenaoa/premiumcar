/**
	 * 	MovimientoContableMBean.java:
	 * 
	 * 	Esta clase corresponde al Managed Bean para controlar el CRUD de 
	 * 	la entidad MovimientoContable
	 *
	 * @author Sergio Henao - sergiohenaoa@gmail.com - 11/04/2015
	 * @Modifier Sergio Henao - sergiohenaoa@gmail.com - 11/04/2015
	 * @version 1.0
	 * 
	 */
package com.sh.pvs.view.contabilidad;

import java.io.Serializable;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.sh.pvs.core.interfaces.configuracion.ICuentaServices;
import com.sh.pvs.core.interfaces.contabilidad.IReporteContableServices;
import com.sh.pvs.core.services.configuracion.CuentaServices;
import com.sh.pvs.core.services.contabilidad.ReporteContableServices;
import com.sh.pvs.model.dto.configuracion.Cuenta;
import com.sh.pvs.model.dto.contabilidad.ReporteContableRow;
import com.sh.pvs.model.dto.seguridad.Usuario;
import com.sh.pvs.view.utils.Constantes;
import com.sh.pvs.view.utils.ControlException;
import com.sh.pvs.view.utils.Metodos;

@ManagedBean
@ViewScoped
public class ReporteContableMBean implements Serializable {

	/*Serializable Attribute*/
	private static final long serialVersionUID = 1L;
	
	/*Variables*/
	private Date fchIni;
	private Date fchFin;
	private Cuenta cuenta = new Cuenta();
 	private List<ReporteContableRow> entityList; 				// Lista de registros del datatable
 	private List<Cuenta> cuentasDropDownList;
 	private Usuario loggedUser;									//Usuario logeado (En Sesión)
 	
 	/*Navegación*/
 	private String mode;									// view - edit - list - create ==> Modo para identificar la vista
 	
 	/*Servicios*/
 	private IReporteContableServices entityService;			// Servicios para la gestión de la entidad
	
 	/*Variables para navegación*/
 	public static String openDialog = "/contabilidad/reporteContableForm.faces";
 	public static String openWebMobile = "reporteContableForm.faces";
 	public static String closeRedirectMobile = "reporteContableFormList.faces";
	public static String closeRedirectWeb = "/contabilidad/reporteContableFormList.faces?faces-redirect=true";
 	
 	/*Constructors*/	
	/**	Default constructor */
	public ReporteContableMBean() {

	}
	
	/*METHODS*/
	
	/**
	 * Método para inicializar las variables
	 * 
	 * */
	@PostConstruct
	private void init(){
		
		FacesContext context = FacesContext.getCurrentInstance();
		
		try {
			
			Metodos.loadSessionMesagges();
			
			loggedUser = Metodos.getUserSession();
			
			if(context.getExternalContext().getRequestParameterMap().get("mode") != null)
				mode = context.getExternalContext().getRequestParameterMap().get("mode");
			else
				mode = Constantes.MODELIST;
			
			if(mode.equals(Constantes.MODELIST)){
				fillCuentasDropDown();
				fillEntityList();
				return;
			}
						
		} catch (Exception e) {
			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
		}
		
	}
	
	/**
	 * Metodo que retorna las listas
	 * @return
	 */
	public void fillEntityList(){
		FacesContext context = FacesContext.getCurrentInstance(); 
		try {
			entityService = new ReporteContableServices(null);
			entityList = null;
			entityList = entityService.findAllMovtos(fchIni, fchFin, cuenta, loggedUser.getCompania().getId());
			
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}		
	}
	
	/**
	 * Método que llena la lista de cuentas activas para el selector
	 * @param 
	 */
	private void fillCuentasDropDown(){
		FacesContext context = FacesContext.getCurrentInstance(); 
		cuentasDropDownList = null;
		try {
			ICuentaServices service = new CuentaServices();
			cuentasDropDownList = service.findAllActivos(loggedUser.getCompania().getId());
		} catch (Exception e) {
			cuentasDropDownList = null;
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}		
	}
	
	/* SECURITY METHODS*/
	/**
	 * Método que valida si el usuario en sesión puede ejecutar una acción sobre este documento
	 * 
	 * @return
	 */
	public boolean hasPermissionTo(String action){		
		return Metodos.hasPermission(ReporteContableMBean.class.getSimpleName(), action);
	}

	/* GETTERS AND SETTERS */
	public Date getFchIni() {
		return fchIni;
	}

	public void setFchIni(Date fchIni) {
		this.fchIni = fchIni;
	}

	public Date getFchFin() {
		return fchFin;
	}

	public void setFchFin(Date fchFin) {
		this.fchFin = fchFin;
	}

	public Cuenta getCuenta() {
		return cuenta;
	}

	public void setCuenta(Cuenta cuenta) {
		this.cuenta = cuenta;
	}

	public Double getTotalIngresos() {
		Double ingresos = 0.0;
		if(entityList != null && entityList.size()>0){
			for (Iterator<ReporteContableRow> itRows = entityList.iterator(); itRows.hasNext();) {
				ReporteContableRow row = (ReporteContableRow) itRows.next();
				if(row.getIngreso() != null)
					ingresos += row.getIngreso();
			}
		}
		return ingresos;
	}

	public Double getTotalEgresos() {
		Double egresos = 0.0;
		if(entityList != null && entityList.size()>0){
			for (Iterator<ReporteContableRow> itRows = entityList.iterator(); itRows.hasNext();) {
				ReporteContableRow row = (ReporteContableRow) itRows.next();
				if(row.getEgreso() != null)
					egresos += row.getEgreso();
			}
		}
		return egresos;
	}

	public Double getBalance() {
		return this.getTotalIngresos() - this.getTotalEgresos();
	}

	public List<ReporteContableRow> getEntityList() {
		return entityList;
	}

	public void setEntityList(List<ReporteContableRow> entityList) {
		this.entityList = entityList;
	}

	public List<Cuenta> getCuentasDropDownList() {
		return cuentasDropDownList;
	}

	public void setCuentasDropDownList(List<Cuenta> cuentasDropDownList) {
		this.cuentasDropDownList = cuentasDropDownList;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}
	
}
