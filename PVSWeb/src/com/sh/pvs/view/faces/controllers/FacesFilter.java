/**
 * FacesFilter.java:
 * 
 * 	Esta clase corresponde al filtro que se ejecutará inicialmente, ante cualquier petición que
 * 	se realice sobre el sistema, para controlar el acceso.
 *
 * @author Sergio Henao, 12/03/2014
 * @Modifier Sergio Henao, 27/02/2015
 * @version 2.0
 * 
 */

package com.sh.pvs.view.faces.controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sh.pvs.core.services.seguridad.CompaniaServices;
import com.sh.pvs.model.dao.seguridad.CompaniaDAO;
import com.sh.pvs.model.dto.seguridad.Compania;
import com.sh.pvs.view.seguridad.LoginMBean;
import com.sh.pvs.view.utils.Metodos;
import com.sh.pvs.view.utils.ControlException;

public class FacesFilter implements Filter {
	
	/*VARIABLES*/
	private final static String EXTENSION = "faces";
	private final static String[] EXTENSIONSPERMITED = {
					"js.faces", "css.faces", "png.faces", "jpg.faces",
					"eot.faces", "svg.faces", "ttf.faces", "woff.faces", "woff2.faces", "otf.faces",
					"dynamiccontent.properties.faces"};
	
	/*METHODS*/
	/**
	 * The doFilter method of the Filter is called by the container each time a request/response pair 
	 * is passed through the chain due to a client request for a resource at the end of the chain.
	 */
	@Override
	
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
		
		//Se inicializan las variabes
		HttpServletRequest request = (HttpServletRequest)req;
		HttpServletResponse response = (HttpServletResponse)res;
		
		try{
			
			String uri = request.getRequestURI();
			
			//Se obtienen los parámetros de la URL
			String queryString = Metodos.getQueryString(request);
			
			//Se obtiene el bean de sesión que controla el acceso
			LoginMBean loginMBean = (LoginMBean)Metodos.getManagedBean(request, "loginMBean");
			
			//Si el llamado llega desde un xhtml, se redirecciona con la extensión apropiada
			if (uri.endsWith(".xhtml")) {
				
				redirectToFaces(response, uri, queryString);
				return;			
			
			}else{
				
				//Se validan las diferentes extensiones de archivos que no requieren seguridad
				if(hasPermitedExtension(uri)){
					chain.doFilter(request, response);
					return;
				}
					
				//Si el acceso fue directamente al sistema y no a una página en específico
				//se envía al login manteniendo los parámetros
				if(uri.equals("/PVSWeb/")){
					response.sendRedirect(request.getContextPath() + "/seguridad/loginForm.faces" + queryString);
					return;
					
				//Si el acceso fue directamente a la página de definición de compañía, se cierra la sesión actual en caso de existir,
				//y se abre directamente la página
				}else if(uri.contains("definirCiaForm.faces")){
					
					if(loginMBean != null){
						loginMBean.cerrarSesionNoRedirect();
					}
					
					chain.doFilter(request, response);
					return;
					
				//Si la petición llega a la página de login	 o a la página de solicitud de acceso		
				}else if(uri.contains("loginForm.faces") || uri.contains("solicitudAccesoForm.faces")){
					
						//Se valida que el usuario no esté logeado, en caso contrario se enviará directamente al área de trabajo
						if(loginMBean != null && loginMBean.isLogeado()){
							
							response.sendRedirect(request.getContextPath() + "/seguridad/workArea.faces");
							return;
						
						}else{
							
							//Si se redirecciona al login y tiene parámetros se mantiene esa página
							//En caso contrario se redirecciona para que se defina la compañía
							String cia = getCiaParam(request, loginMBean);
							
							//Si se conoce el parámetro de compañía se settea en el bean
							if ((cia != null) && (!cia.equals(""))) {
								
								//Si el parámetro corresponde a una compañía válida se settea
								Compania compania = findCompania(cia);
								if(compania != null){
									
									if(loginMBean == null){
										loginMBean = new LoginMBean();
									}
									
									loginMBean.getUsuario().setCompania(compania);
									
									chain.doFilter(request, response);
									return;
									
								}else{
									//Si el parámetro no corresponde a una compañía válida se redirecciona a definirCia
									response.sendRedirect(request.getContextPath() + "/seguridad/definirCiaForm.faces" + queryString);
									return;
								}
								
							} else {
								//Si no se tiene el parámetro de compañía se redirecciona a definirCia
								response.sendRedirect(request.getContextPath() + "/seguridad/definirCiaForm.faces" + queryString);
								return;
							}
						}
					
					
				//Se validan las páginas que no requieren autenticación para ser visualizadas y que no sean definirCia ni Login
				}else if(uri.endsWith("usuNoAutenticado.faces") || uri.endsWith("errorGeneral.faces")){
					
					if(loginMBean != null && loginMBean.getUsuario() != null
							 && loginMBean.getUsuario().getCompania() != null
							 && loginMBean.getUsuario().getCompania().getCodigo() != null
							 && !loginMBean.getUsuario().getCompania().getCodigo().equals("")){
						chain.doFilter(request, response);
						return;
					} else {
						//Si no se tiene el parámetro de compañía se redirecciona a definirCia
						response.sendRedirect(request.getContextPath() + "/seguridad/definirCiaForm.faces" + queryString);
						return;
					}
					
				//Se valida si es acceso a las páginas de ayuda para hacer la redirección adecuada
				}else if(uri.startsWith("/PVSWeb/ayuda/help_") && uri.endsWith(".faces")){
					
					redirectHelp(request, response, chain);
					return;
				
				//Cualquier otra página se valida en esta sección (Si hay excepciones a nivel de permisos se dejan pasar, en 
				//caso contrario se valida que el usuario tenga permisos de acceso sobre la página específicada
				}else{
					if(loginMBean != null && loginMBean.isLogeado()){
						
						//Se valida si el usuario tiene permiso de acceso a la página en cuestión o si es una página que no 
						//requiere permisos especiales para acceder
						if(!requireSpecificPermission(uri)
								|| Metodos.hasAccess(request, uri)){
							chain.doFilter(request, response);
						}else{
							response.sendRedirect(request.getContextPath() + "/seguridad/controlAcceso.faces");
						}
					}else{
						response.sendRedirect(request.getContextPath() + "/seguridad/usuNoAutenticado.faces");
					}
					return;
				}
			}
		}catch (Exception e) {
			ControlException.controlException(e);
			response.sendRedirect(request.getContextPath() + "/seguridad/errorGeneral.faces");
		}
	}
	
	/**
	 * Called by the web container to indicate to a filter that it is being taken out of service.
	 */
	@Override
	public void destroy() {
	}

	/**
	 * Called by the web container to indicate to a filter that it is being placed into service.
	 */
	@Override
	public void init(FilterConfig arg0) throws ServletException {
				
	}

	/**
	 * Método que cambia la extensión de la página y la redirecciona
	 * @param response
	 * @param uri
	 * @param queryString
	 */
	private void redirectToFaces(HttpServletResponse response, String uri, String queryString) throws Exception{
		
			//si uri termina en loginForm.xhtml redireccionar a donde está el loginForm
			if(uri.endsWith("loginForm.xhtml")){
				uri = "/PVSWeb/seguridad/loginForm.xhtml";
			}
			
			//Se cambia la extensión xhtml por faces
			int length = uri.length();
			String newAddress = uri.substring(0, length-5) + EXTENSION;
			
			//Se redirecciona a la nueva dirección manteniendo los parámetros respectivos
			response.sendRedirect(newAddress + queryString);
	}

	/**
	 * Método que valida si la extensión del archivo es permitida
	 * @param uri
	 * @return
	 */
	private boolean hasPermitedExtension(String uri){
		for (int i = 0; i < EXTENSIONSPERMITED.length; i++) {
			if(uri.endsWith(EXTENSIONSPERMITED[i])){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Método que obtiene el parámetro de compañía de la url
	 * @param request
	 * @param loginMBean
	 * @return
	 */
	private String getCiaParam(HttpServletRequest request, LoginMBean loginMBean){
		String cia = request.getParameter("cia");
		//Si no tiene parámetro se valida si lo contiene el bean
		if(cia == null || cia.equals("")){
			if(loginMBean != null 
					&& loginMBean.getUsuario() != null
					&& loginMBean.getUsuario().getCompania() != null
					&& loginMBean.getUsuario().getCompania().getCodigo() != null
					&& !loginMBean.getUsuario().getCompania().getCodigo().equals("")){
				cia = loginMBean.getUsuario().getCompania().getCodigo();
			}
		}
		return cia;
	}
	
	/**
	 * Método para buscar una compañía
	 * @param cia
	 * @return
	 * @throws Exception
	 */
	private Compania findCompania(String cia) throws Exception{
		
		CompaniaServices ciaSrv = new CompaniaServices();
		Compania compania = null;								
		List<Compania> companias = ciaSrv.findByProperty(CompaniaDAO.CODIGO, cia);
		if(companias.size()>0){
			compania = new Compania();
			compania.setCodigo(companias.get(0).getCodigo());
			compania.setEsActiva(companias.get(0).getEsActiva());
			compania.setId(companias.get(0).getId());
			compania.setNombre(companias.get(0).getNombre());
			compania.setCssFolder(companias.get(0).getCssFolder());
		}
		
		return compania;
	}
	
	/**
	 * Método que identifica la página de ayuda requerida y la redirecciona
	 * @param request
	 * @param response
	 * @param chain
	 * @throws Exception
	 */
	private void redirectHelp(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws Exception{
		//String actualPage = "";				
		if(request.getParameter("actualPage") != null){
			//actualPage = request.getParameter("actualPage").toString();
			//if(!actualPage.contains("help_"))
				//response.sendRedirect(request.getContextPath() + "/ayuda/help_"+actualPage);
			//else{
				response.sendRedirect(request.getContextPath() + "/ayuda/help_workArea.faces");
			//}
		}else{
			chain.doFilter(request, response);
		}
	}
	
	/**
	 * Método que define si una página requiere permisos específicos o no
	 * @param uri
	 * @return
	 */
	private boolean requireSpecificPermission(String uri){
		
		if(uri.endsWith("workArea.faces")
			|| uri.endsWith("controlAcceso.faces")
			|| uri.endsWith("cambiarPassword.faces")
			|| uri.endsWith("generarPlantilla.faces")){
			return false;
		}
		
		return true;
	}
}