/**
 * LoginMBean.java:
 * 
 * 	Esta clase corresponde al Managed Bean para controlar la página de
 * 	inicio de sesión (Login)
 *
 * @author Sergio Henao, 06/03/2014
 * @Modifier Sergio Henao, 27/02/2015
 * @version 2.0
 * 
 */

package com.sh.pvs.view.seguridad;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.primefaces.model.menu.MenuModel;

import com.sh.pvs.core.interfaces.seguridad.ICompaniaServices;
import com.sh.pvs.core.interfaces.seguridad.IUsuarioServices;
import com.sh.pvs.core.services.seguridad.CompaniaServices;
import com.sh.pvs.core.services.seguridad.UsuarioServices;
import com.sh.pvs.model.dao.seguridad.UsuarioDAO;
import com.sh.pvs.model.dto.seguridad.Compania;
import com.sh.pvs.model.dto.seguridad.Usuario;
import com.sh.pvs.model.utils.Condition;
import com.sh.pvs.model.utils.ControlQuery;
import com.sh.pvs.view.utils.Constantes;
import com.sh.pvs.view.utils.ControlException;
import com.sh.pvs.view.utils.Metodos;

@ManagedBean
@SessionScoped
public class LoginMBean implements java.io.Serializable {
	
	/*VARIABLES*/
	private static final long serialVersionUID = 1L;
	private boolean logeado = false;
	private Usuario usuario = new Usuario();
	
	private MenuModel model;
	
	private String pwAnterior;
	private String pwNueva;
	private String pwConfirmar;
	
	private Compania compania = new Compania();
	private List<Compania> companias;
	
	private ICompaniaServices ciaSrv = new CompaniaServices();
	private IUsuarioServices usrSrv;
	
	@PostConstruct
    public void init() {
         
        //Compañías
        try {
			companias = ciaSrv.findAllActivasOrdenados();
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
	
	/*METHODS*/
	/**
     * Redirecciona al formulario de login adecuado.
     * 
     * @throws Exception Excepción para mejorar presentación de excepciones no controladas
     */
	public String registrarCia(){
		
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			
			//this.cerrarSesion(actionEvent);
			
			CompaniaServices ciaSrv = new CompaniaServices();
			compania = ciaSrv.findById(compania.getId());
			
			HttpServletRequest request = (HttpServletRequest)context.getExternalContext().getRequest();
			HttpServletResponse response = (HttpServletResponse)context.getExternalContext().getResponse();
			
			response.sendRedirect(request.getContextPath() + "/seguridad/loginForm.faces?cia="+compania.getCodigo());
			
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}
		
		return "";
	}
	
	/**
     * Valida el usuario que se intenta autenticar.
     * Redirecciona al área de trabajo con el usuario autenticado o presenta los mensajes de control respectivos.
     * 
     * @throws Exception Excepción para mejorar presentación de excepciones no controladas
     */
	public String validarUsuario(ActionEvent actionEvent){
		FacesContext context = FacesContext.getCurrentInstance(); 
		this.setLogeado(false);
		try {
			
			usrSrv = new UsuarioServices();
			Boolean usuarioValido = usrSrv.validarLogin(usuario);
			if(usuarioValido == null){
				
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, Metodos.getMessageResourceString("exception_usu_pw_novalid", null), ""));
				com.sh.pvs.utils.Metodos.warnLog(Thread.currentThread(), Metodos.getMessageResourceString("log_autenticacionFallida", new Object[]{this.usuario.getUsuario()}));
			
			}else if(usuarioValido){
				
				ControlQuery queryUsuarioCia = new ControlQuery();
				Condition condicionUsuario = new Condition(UsuarioDAO.USUARIO, Condition.IGUAL, usuario.getUsuario());
				Condition condicionCia = new Condition(UsuarioDAO.IDCOMPANIA, Condition.IGUAL, usuario.getCompania().getId());
				queryUsuarioCia.add(condicionUsuario);
				queryUsuarioCia.addAND(condicionCia);
				
				usuario = usrSrv.findAll(queryUsuarioCia).get(0);	
				
				Compania cia = new Compania();
				cia.setCodigo(usuario.getCompania().getCodigo());
				cia.setEsActiva(usuario.getCompania().getEsActiva());
				cia.setId(usuario.getCompania().getId());
				cia.setNombre(usuario.getCompania().getNombre());
				cia.setCssFolder(usuario.getCompania().getCssFolder());
				usuario.setCompania(cia);
				
				createMenuModel();
				this.setLogeado(true);				
				context.getExternalContext().redirect("workArea.xhtml");
				com.sh.pvs.utils.Metodos.infoLog(Thread.currentThread(), Metodos.getMessageResourceString("log_usuAutenticado", new Object[]{this.usuario.getUsuario()}));
			
			}else{				
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, Metodos.getMessageResourceString("exception_usu_inactivo", null), ""));				
			}
			
		} catch (Exception e) {
			this.setLogeado(false);	
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}
		
		return "";
	}
	
	/**
     * Valida el nombre de usuario y envía la contraseña al correo electrónico del mismo.
     * 
     * @param actionEvent Evento que genera el llamado al método.
     * @throws Exception Excepción para mejorar presentación de excepciones no controladas
     */
	public String recordarPassword(ActionEvent actionEvent) throws Exception{
		FacesContext context = FacesContext.getCurrentInstance(); 
		try {
			boolean camposOk = true;
			if(usuario.getUsuario() == null 
					|| usuario.getUsuario().trim().equals("")){
				String campo = Metodos.getMessageResourceString("loginPage_user", null);			
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, Metodos.getMessageResourceString("generics_required", new Object[]{campo}), ""));
				camposOk = false;
			}
			if(!camposOk)
				return "";
			
			usrSrv = new UsuarioServices();
			Boolean envioOK = usrSrv.reenviarPw(usuario);
			if(envioOK){
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, Metodos.getMessageResourceString("loginPage_envio_pw_ok", null), ""));
				com.sh.pvs.utils.Metodos.infoLog(Thread.currentThread(), Metodos.getMessageResourceString("log_solicitudPw", new Object[]{this.usuario.getUsuario()}));
			}else{
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, Metodos.getMessageResourceString("exception_usu_novalid", null), ""));
			}
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}
		
		return "";
	}
	
	/**
     * Valida las contraseñas y las cambia si los campos están bien diligenciado
     * 
     * @param actionEvent Evento que genera el llamado al método.
     * @throws Exception Excepción para mejorar presentación de excepciones no controladas
     */
	public String cambiarPassword(ActionEvent actionEvent) throws Exception{
		FacesContext context = FacesContext.getCurrentInstance(); 
		try {
			
			usrSrv = new UsuarioServices();
			Boolean cambioOK = usrSrv.cambiarPw(usuario, this.pwAnterior, this.pwNueva, this.pwConfirmar);
			if(cambioOK){
				
				String msg =  Metodos.getMessageResourceString("cambiarPw_cambiook", null);
				
				if(!Metodos.isMobile()){
					Metodos.setSessionMesagges(Constantes.SESSIONMSGINFO, msg);
				}
					
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
				
				com.sh.pvs.utils.Metodos.infoLog(Thread.currentThread(), Metodos.getMessageResourceString("log_cambioPw", new Object[]{this.usuario.getUsuario()}));
				
				HttpServletRequest request = (HttpServletRequest)context.getExternalContext().getRequest();
				HttpServletResponse response = (HttpServletResponse)context.getExternalContext().getResponse();
				
				response.sendRedirect(request.getContextPath() + "/seguridad/workArea.faces");
				
			}else{
				String msg =  Metodos.getMessageResourceString("cambiarPw_cambioerror", null);
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, "")); // MENSAJE
				
			}
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}
		
		return "";
	}
	
	/**
     * Cierra la sesión actual del usuario
     * 
     * @throws Exception Excepción para mejorar presentación de excepciones no controladas
     */
	public String cerrarSesionNoRedirect() throws Exception{
		FacesContext context = FacesContext.getCurrentInstance();			
		try {
			
			logeado = false;
			usuario = new Usuario();
			model = null;
			
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}
		
		return "";
	}
	
	/**
     * Cierra la sesión actual del usuario y redirecciona a la página de login
     * 
     * @param actionEvent Evento que genera el llamado al método.
     * @throws Exception Excepción para mejorar presentación de excepciones no controladas
     */
	public String cerrarSesion(ActionEvent actionEvent) throws Exception{
		
		FacesContext context = FacesContext.getCurrentInstance();
		
		try {
			
			String codCia = this.getUsuario().getCompania().getCodigo();
			this.cerrarSesionNoRedirect();
			
			HttpServletRequest request = (HttpServletRequest)context.getExternalContext().getRequest();
			HttpServletResponse response = (HttpServletResponse)context.getExternalContext().getResponse();
			
			response.sendRedirect(request.getContextPath() + "/seguridad/loginForm.faces?cia=" + codCia);

		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}
		
		return "";
	}
	
	/**
	 * Método que construye el menú de acuerdo a la información del usuario
	 * @param actionEvent
	 * @return
	 */
	public MenuModel createMenuModel() throws Exception{
		model = Metodos.createMenuModel();
		return model;
	}
	
	/*
	 * GETTERS AND SETTERS
	 */
	/**
	 * 
	 * @return usuario
	 */
	public Usuario getUsuario() {
		return usuario;
	}

	/**
	 * 
	 * @param usuario
	 */
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isLogeado() {
		return logeado;
	}

	/**
	 * 
	 * @param logeado
	 */
	public void setLogeado(boolean logeado) {
		this.logeado = logeado;
	}

	/**
	 * 
	 * @return
	 */
	public String getPwAnterior() {
		return pwAnterior;
	}

	/**
	 * 
	 * @param pwAnterior
	 */
	public void setPwAnterior(String pwAnterior) {
		this.pwAnterior = pwAnterior;
	}

	/**
	 * 
	 * @return
	 */
	public String getPwNueva() {
		return pwNueva;
	}

	/**
	 * 
	 * @param pwNueva
	 */
	public void setPwNueva(String pwNueva) {
		this.pwNueva = pwNueva;
	}

	/**
	 * 
	 * @return
	 */
	public String getPwConfirmar() {
		return pwConfirmar;
	}

	/**
	 * 
	 * @param pwConfirmar
	 */
	public void setPwConfirmar(String pwConfirmar) {
		this.pwConfirmar = pwConfirmar;
	}

	public MenuModel getModel() {
		return model;
	}

	public void setModel(MenuModel model) {
		this.model = model;
	}

	public List<Compania> getCompanias() {
		return companias;
	}

	public void setCompanias(List<Compania> companias) {
		this.companias = companias;
	}

	public Compania getCompania() {
		return compania;
	}

	public void setCompania(Compania compania) {
		this.compania = compania;
	}
	
}
