/**
 * 	PerfilMBean.java:
 * 
 * 	Esta clase corresponde al Managed Bean para controlar el CRUD de 
 * 	la entidad Perfil
 *
 * @author Jhoel Acosta - jhoel.acosta@e-deas.com.co - 01/04/2015
 * @Modifier Jhoel Acosta - jhoel.acosta@e-deas.com.co - 01/04/2015
 * @version 1.0
 * 
 */

package com.sh.pvs.view.seguridad;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.sh.pvs.core.interfaces.seguridad.IAccionxFormularioServices;
import com.sh.pvs.core.interfaces.seguridad.IFormularioServices;
import com.sh.pvs.core.interfaces.seguridad.IPerfilServices;
import com.sh.pvs.core.interfaces.seguridad.IPermisoServices;
import com.sh.pvs.core.services.seguridad.AccionxFormularioServices;
import com.sh.pvs.core.services.seguridad.FormularioServices;
import com.sh.pvs.core.services.seguridad.PerfilServices;
import com.sh.pvs.core.services.seguridad.PermisoServices;
import com.sh.pvs.model.dao.seguridad.PerfilDAO;
import com.sh.pvs.model.dto.seguridad.AccionxFormulario;
import com.sh.pvs.model.dto.seguridad.Formulario;
import com.sh.pvs.model.dto.seguridad.Perfil;
import com.sh.pvs.model.dto.seguridad.Usuario;
import com.sh.pvs.view.utils.Constantes;
import com.sh.pvs.view.utils.ControlException;
import com.sh.pvs.view.utils.Metodos;


@ManagedBean
@ViewScoped
public class PerfilMBean implements Serializable {

	/*Serializable Attribute*/
	private static final long serialVersionUID = 1L;
	
	/*Variables*/
	private Perfil entity; 										// ViewForm - EditForm 	
 	private List<Perfil> entityList; 							// Lista de registros del datatable
 	private List<Perfil> entityFilterList;						// Lista de registros filtrados del datatable
 	private boolean panelForm;									//boolean para mostrar el panel perfil o el panel de permisos; panelPerfil=false; panelpermisos = true.
 	private ArrayList<String> accionesxFormularioAvailable; 	//Acciones disponibles 
 	private String[] accionesxFormulario; 						//Acciones seleccionadas 
 	private Integer idFormulario; 								//Id del formulario seleccionado para los permisos
 	private ArrayList<Formulario> formulariosDropDown;			//Lista de formularios
 	private Usuario loggedUser;									//Usuario logeado (En Sesión)
 		
 	/*Navegación*/
 	private String mode;											// view - edit ==> Modo para identificar la vista
 	
 	/*Servicios*/
 	private IPerfilServices entityService;				// Servicios para la gestión de la entidad
 	
 	/*Variables para navegación*/
 	public static String openDialog = "/seguridad/perfil/perfilForm.faces";
 	public static String openWebMobile = "perfilForm.faces";
 	public static String closeRedirectMobile = "perfilList.faces";
	public static String closeRedirectWeb = "/seguridad/perfil/perfilList.faces?faces-redirect=true";
	
 	/*Constructors*/	
	/**	Default constructor */
	public PerfilMBean() {
	}
	
	/*METHODS*/
	/**
	 * Método para inicializar las variables*/
	@PostConstruct
	private void init(){
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			
			loggedUser = Metodos.getUserSession();
			
			Metodos.loadSessionMesagges();
			
			if(context.getExternalContext().getRequestParameterMap().get("mode") != null)
				mode = context.getExternalContext().getRequestParameterMap().get("mode");
			else
				mode = Constantes.MODELIST;
			
			if(mode.equals(Constantes.MODECREATE)){
				panelForm = false;
				this.entity = new Perfil();
				return;
			}
			
			if(mode.equals(Constantes.MODELIST)){
				fillEntityList();
				return;
			}
						
			if(mode.equals(Constantes.MODEVIEW) || mode.equals(Constantes.MODEEDIT)){
				try {
					panelForm = false;
					String idParam = context.getExternalContext().getRequestParameterMap().get("id");
					entityService = new PerfilServices();
					this.entity = entityService.findById(Integer.parseInt(idParam));
					fillFormulariosDropDown();

				} catch (Exception e) {
					context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
				}
				return;
			}

		} catch (Exception e) {
			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
		}
		
	}
	
	/**
	 * Create a new Constante entity
	 * 
	 */
	public String newEntity() {
		this.entity = new Perfil();
		return Metodos.newNavigation(openDialog, openWebMobile);
		
	}
	
	/**
	 * Select an existing Constante entity
	 * 
	 */
	public String selectEntity(Integer idKey) {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			entityService = new PerfilServices();
			entity = entityService.findById(idKey);
		} catch (Exception e) {
			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
		}
		return Metodos.selectNavigation(idKey, openDialog, openWebMobile);
	}
	
	/**
	 * Edit an existing Constante entity
	 * 
	 */
	public String editEntity(Integer idKey, boolean fromList) {
		
		FacesContext context = FacesContext.getCurrentInstance();
		
		try {
			
			entityService = new PerfilServices();
			entity = entityService.findById(idKey);
			
		} catch (Exception e) {
			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
		}
		
		return Metodos.editNavigation(idKey, fromList, openDialog, openWebMobile);
	}
	
	/**
	 * Delete an existing Constante entity
	 * 
	 */
	public String deleteEntity(Integer idKey, boolean fromList) {
		
		FacesContext context = FacesContext.getCurrentInstance(); 
		
		try {
			entityService = new PerfilServices();
			entity = entityService.findById(idKey);
			entityService.deleteTX(entity);
			
			Metodos.auditLog(Constantes.ACTIONDELETE, PerfilMBean.class.getSimpleName(), entity.getNombre(), "");
			
			String msg = Metodos.getMessageResourceString("generics_eliminarOK", new Object[]{entity.getNombre()});
			if(!fromList || (!Metodos.isMobile() && Metodos.isDlg())){
				Metodos.setSessionMesagges(Constantes.SESSIONMSGINFO, msg);
			}
				
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
				
			fillEntityList();
		} catch (Exception e) {
			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
		}
		return  Metodos.close(closeRedirectWeb, closeRedirectMobile, fromList);
		
	}
	
	/**
	 * Metodo que retorna las listas
	 * @return
	 */
	public void fillEntityList(){
		FacesContext context = FacesContext.getCurrentInstance(); 
		try {
			entityService = new PerfilServices();
			entityList = null;
			entityList = entityService.findByProperty(PerfilDAO.COMPANIA, loggedUser.getCompania().getId());
			} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}		
	}
	
	/**
	 * Método que llena la lista de formularios activos para el selector de padres
	 * @param formulario: contiene el formulario actual que debe ser eliminado del dropdown
	 */
	private void fillFormulariosDropDown(){
		FacesContext context = FacesContext.getCurrentInstance(); 
		formulariosDropDown = new ArrayList<Formulario>();
		ArrayList<Formulario> formulariosDropDownTmp = new ArrayList<Formulario>();
		try {
			IFormularioServices formSrv = new FormularioServices();
			formulariosDropDownTmp = formSrv.findAllActivos(loggedUser.getCompania().getId());
			for (Iterator<Formulario> itForms = formulariosDropDownTmp.iterator(); itForms.hasNext();) {
				Formulario formTmp = itForms.next();
				//El selector solo tendrá los documentos que no son formularios
				if(formTmp.getEsFormulario()){
					formulariosDropDown.add(formTmp);
				}
			}
			
		} catch (Exception e) {
			formulariosDropDown = new ArrayList<Formulario>();
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}		
	}
	
	/**
	 * Metodo para guardar
	 */
	public String saveEntity(){
		FacesContext context = FacesContext.getCurrentInstance(); 
		try {	
			if(mode != null && mode.equals(Constantes.MODECREATE)
					&& hasPermissionTo(Constantes.ACTIONCREATE)){				
				entity.setCompania(loggedUser.getCompania());
				
				entityService = new PerfilServices();
				entityService.createTX(entity);
				
				Metodos.auditLog(Constantes.ACTIONCREATE, PerfilMBean.class.getSimpleName(), entity.getNombre(), "");
				
				String msg = Metodos.getMessageResourceString("generics_guardarOK", new Object[]{entity.getNombre()});
				
				if(!Metodos.isMobile()){
					Metodos.setSessionMesagges(Constantes.SESSIONMSGINFO, msg);
				}
					
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
					
				return  Metodos.close(closeRedirectWeb, closeRedirectMobile, true);
					
			} else if (mode != null && mode.equals(Constantes.MODEEDIT)
					&& hasPermissionTo(Constantes.ACTIONUPDATE)) {
				entityService = new PerfilServices();
				entityService.updateTX(entity);
				
				Metodos.auditLog(Constantes.ACTIONUPDATE, PerfilMBean.class.getSimpleName(), entity.getNombre(), "");
				
				String msg = Metodos.getMessageResourceString("generics_actualizarOK", new Object[]{entity.getNombre()});
				
				if(!Metodos.isMobile()){
					Metodos.setSessionMesagges(Constantes.SESSIONMSGINFO, msg);
				}
					
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
					
				return  Metodos.close(closeRedirectWeb, closeRedirectMobile, true);
					
			}
					
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}
		
		return "";
	}
	
	
	/**
	 * Metodo que se ejecuta cuando se cierra una modal
	 * @return
	 */
	public void dialogListener(){
		fillEntityList();
		Metodos.loadSessionMesagges();
	}
	
	/**
	 * Metodo para eliminar
	 */
	public void eliminarPermiso(String [] permisoSelected){
		FacesContext context = FacesContext.getCurrentInstance(); 
		try {	
			if(hasPermissionTo(Constantes.ACTIONUPDATE)){
				IPermisoServices permisoSrv = new PermisoServices();
				permisoSrv.borrarPermisosByFormTX(entity.getId(), permisoSelected[0], loggedUser.getCompania().getId());		
				
				String msg = Metodos.getMessageResourceString("generics_actualizarOK", new Object[]{entity.getNombre()});
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
				
				IPerfilServices perfilSrv=new PerfilServices();				
				entity = perfilSrv.findById(entity.getId());
				
			}
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}
	}
	
	/**
	 * Metodo para eliminar
	 */
	public void guardarPermiso(){
		
		FacesContext context = FacesContext.getCurrentInstance(); 
		try {	
			if(hasPermissionTo(Constantes.ACTIONUPDATE)){
				IPermisoServices permisoSrv = new PermisoServices();			
				permisoSrv.crearPermisoTX(entity.getId(), idFormulario, accionesxFormulario);
				
				
				String msg = Metodos.getMessageResourceString("generics_actualizarOK", new Object[]{entity.getNombre()});
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
				
				IPerfilServices perfilSrv=new PerfilServices();				
				entity = perfilSrv.findById(entity.getId());
				accionesxFormulario = new String[0];
				//this.registrarAuditoria(Constantes.ACTUALIZAR, perfil.getVNombre(), Constantes.CREAR + " " + OBSAUDITORIAPERMISOS);
			}
		}catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}
	}
	
	public void abrirPermisos(){
		panelForm = true;
	}
	
	public void abrirPerfil(){
		panelForm = false;
	}
	
	/**
	 * Método que llena la lista de formularios activos para el selector de padres
	 * @param formulario: contiene el formulario actual que debe ser eliminado del dropdown
	 */
	public void llenarAccionesxFormularioDropDown(){
		accionesxFormularioAvailable = new ArrayList<String>();
		FacesContext context = FacesContext.getCurrentInstance(); 
		try {
			IAccionxFormularioServices servicios = new AccionxFormularioServices();
			Formulario formSelected = new Formulario();
			formSelected.setId(idFormulario);
			ArrayList<AccionxFormulario> acciones = servicios.findAllByFormulario(formSelected);
			for (Iterator<AccionxFormulario> itAcciones = acciones.iterator(); itAcciones.hasNext();) {
				AccionxFormulario accionxFormularioTmp = itAcciones.next();
				accionesxFormularioAvailable.add(accionxFormularioTmp.getAccion().getNombre());
			}		
			
		} catch (Exception e) {
			accionesxFormularioAvailable = new ArrayList<String>();
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}
	}
	
	/* SECURITY METHODS*/
	/**
	 * Método que valida si el usuario en sesión puede ejecutar una acción sobre este documento
	 * 
	 * @return
	 */
	public boolean hasPermissionTo(String action){		
		return Metodos.hasPermission(PerfilMBean.class.getSimpleName(), action);
	}
	
	
	/* GETTERS AND SETTERS */

	public Perfil getEntity() {
		return entity;
	}

	public void setEntity(Perfil entity) {
		this.entity = entity;
	}

	public List<Perfil> getEntityList() {
		return entityList;
	}

	public void setEntityList(List<Perfil> entityList) {
		this.entityList = entityList;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public List<Perfil> getEntityFilterList() {
		return entityFilterList;
	}

	public void setEntityFilterList(List<Perfil> entityFilterList) {
		this.entityFilterList = entityFilterList;
	}

	public boolean isPanelForm() {
		return panelForm;
	}

	public void setPanelForm(boolean panelForm) {
		this.panelForm = panelForm;
	}

	public ArrayList<String> getAccionesxFormularioAvailable() {
		return accionesxFormularioAvailable;
	}

	public void setAccionesxFormularioAvailable(
			ArrayList<String> accionesxFormularioAvailable) {
		this.accionesxFormularioAvailable = accionesxFormularioAvailable;
	}

	public Integer getIdFormulario() {
		return idFormulario;
	}

	public void setIdFormulario(Integer idFormulario) {
		this.idFormulario = idFormulario;
	}

	public String[] getAccionesxFormulario() {
		return accionesxFormulario;
	}

	public void setAccionesxFormulario(String[] accionesxFormulario) {
		this.accionesxFormulario = accionesxFormulario;
	}

	public ArrayList<Formulario> getFormulariosDropDown() {
		return formulariosDropDown;
	}

	public void setFormulariosDropDown(ArrayList<Formulario> formulariosDropDown) {
		this.formulariosDropDown = formulariosDropDown;
	}
	
}
