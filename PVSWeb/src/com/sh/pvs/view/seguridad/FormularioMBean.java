/**
 * 	FormularioMBean.java:
 * 
 * 	Esta clase corresponde al Managed Bean para controlar el CRUD de 
 * 	la entidad Formulario
 *
 * @author Jhoel Acosta - jhoel.acosta@e-deas.com.co - 01/04/2015
 * @Modifier Jhoel Acosta - jhoel.acosta@e-deas.com.co - 01/04/2015
 * @version 1.0
 * 
 */

package com.sh.pvs.view.seguridad;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.sh.pvs.core.interfaces.seguridad.IFormularioServices;
import com.sh.pvs.core.services.seguridad.FormularioServices;
import com.sh.pvs.model.dto.seguridad.Formulario;
import com.sh.pvs.model.dto.seguridad.Usuario;
import com.sh.pvs.view.utils.Constantes;
import com.sh.pvs.view.utils.ControlException;
import com.sh.pvs.view.utils.Metodos;


@ManagedBean
@ViewScoped
public class FormularioMBean implements Serializable {

	/*Serializable Attribute*/
	private static final long serialVersionUID = 1L;
	
	/*Variables*/
	private Formulario entity; 								// ViewForm - EditForm 	
 	private List<Formulario> entityList; 					// Lista de registros del datatable
 	private List<Formulario> entityFilterList;				// Lista de registros filtrados del datatable
 	private List<Formulario> formulariosDropDownList;
 	private Usuario loggedUser;								//Usuario logeado (En Sesión)
 	
 	/*Navegación*/
 	private String mode;											// view - edit ==> Modo para identificar la vista
 	
 	/*Servicios*/
 	private IFormularioServices entityService;				// Servicios para la gestión de la entidad
 	
 	/*Variables para navegación*/
 	public static String openDialog = "/seguridad/formulario/formularioForm.faces";
 	public static String openWebMobile = "formularioForm.faces";
 	public static String closeRedirectMobile = "formularioList.faces";
	public static String closeRedirectWeb = "/seguridad/formulario/formularioList.faces?faces-redirect=true";
	
 	/*Constructors*/	
	/**	Default constructor */
	public FormularioMBean() {
	}
	
	/*METHODS*/
	/**
	 * Método para inicializar las variables*/
	@PostConstruct
	private void init(){
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			
			Metodos.loadSessionMesagges();
			
			loggedUser = Metodos.getUserSession();
			
			if(context.getExternalContext().getRequestParameterMap().get("mode") != null)
				mode = context.getExternalContext().getRequestParameterMap().get("mode");
			else
				mode = Constantes.MODELIST;
			
			if(mode.equals(Constantes.MODECREATE)){
				this.entity = new Formulario();
				this.entity.setFormularioPadre(new Formulario());
				llenarFormulariosDropDown(this.entity);
				return;
			}
			
			if(mode.equals(Constantes.MODELIST)){
				fillEntityList();
				return;
			}
						
			if(mode.equals(Constantes.MODEVIEW) || mode.equals(Constantes.MODEEDIT)){
				
				try {
						
					String idParam = context.getExternalContext().getRequestParameterMap().get("id");
					entityService = new FormularioServices();
					this.entity = entityService.findById(Integer.parseInt(idParam), loggedUser.getCompania().getId());
					if(entity != null){
						if (this.entity.getFormularioPadre() == null) {
							this.entity.setFormularioPadre(new Formulario());
						}
						llenarFormulariosDropDown(this.entity);
					}
				} catch (Exception e) {
					context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
				}
				return;
			}

		} catch (Exception e) {
			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
		}
		
	}
	
	/**
	 * Create a new Constante entity
	 * 
	 */
	public String newEntity() {
		this.entity = new Formulario();
		this.entity.setFormularioPadre(new Formulario());
		return Metodos.newNavigation(openDialog, openWebMobile);
		
	}
	
	/**
	 * Select an existing Constante entity
	 * 
	 */
	public String selectEntity(Integer idKey) {				
		
		FacesContext context = FacesContext.getCurrentInstance();
		
		try {
			
			entityService = new FormularioServices();
			entity = entityService.findById(idKey, loggedUser.getCompania().getId());
			
		} catch (Exception e) {
			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
		}
		
		return Metodos.selectNavigation(idKey, openDialog, openWebMobile);
	}
	
	/**
	 * Edit an existing Constante entity
	 * 
	 */
	public String editEntity(Integer idKey, boolean fromList) {
		
		FacesContext context = FacesContext.getCurrentInstance();
		
		try {
			
			entityService = new FormularioServices();
			entity = entityService.findById(idKey, loggedUser.getCompania().getId());
			
		} catch (Exception e) {
			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
		}
		
		return Metodos.editNavigation(idKey, fromList, openDialog, openWebMobile);
	}
	
	/**
	 * Delete an existing Constante entity
	 * 
	 */
	public String deleteEntity(Integer idKey, boolean fromList) {
		
		FacesContext context = FacesContext.getCurrentInstance(); 
		
		try {
			entityService = new FormularioServices();
			entity = entityService.findById(idKey, loggedUser.getCompania().getId());
			entityService.deleteTX(entity);
			
			Metodos.auditLog(Constantes.ACTIONDELETE, FormularioMBean.class.getSimpleName(), entity.getNombreDescriptivo(), "");
			
			String msg = Metodos.getMessageResourceString("generics_eliminarOK", new Object[]{entity.getNombreDescriptivo()});
			if(!fromList || (!Metodos.isMobile() && Metodos.isDlg())){
				Metodos.setSessionMesagges(Constantes.SESSIONMSGINFO, msg);
			}
				
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
				
			fillEntityList();
		} catch (Exception e) {
			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
		}
		return  Metodos.close(closeRedirectWeb, closeRedirectMobile, fromList);
		
	}
	
	/**
	 * Metodo que retorna las listas
	 * @return
	 */
	public void fillEntityList(){
		FacesContext context = FacesContext.getCurrentInstance(); 
		try {
			entityService = new FormularioServices();
			entityList = null;
			entityList = entityService.findAll(loggedUser.getCompania().getId());
			
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}		
	}
	
	/**
	 * Método que llena la lista de formularios activos para el selector de padres
	 * @param formulario: contiene el formulario actual que debe ser eliminado del dropdown
	 */
	private void llenarFormulariosDropDown(Formulario formulario){
		FacesContext context = FacesContext.getCurrentInstance(); 
		formulariosDropDownList = null;
		try {
			IFormularioServices formSrv = new FormularioServices();
			formulariosDropDownList = formSrv.findAllActivosAndNoFormularioAndNoIdentidad(formulario.getId(), loggedUser.getCompania().getId());
		} catch (Exception e) {
			formulariosDropDownList = null;
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}		
	}
	
	/**
	 * Metodo para guardar
	 */
	public String saveEntity(){
		FacesContext context = FacesContext.getCurrentInstance(); 
		try {	
			
			if(mode != null && mode.equals(Constantes.MODECREATE)
					&& hasPermissionTo(Constantes.ACTIONCREATE)){				
				
				entityService = new FormularioServices();
				entity.setCompania(loggedUser.getCompania());
				
				entityService.crearFormularioTX(entity);
				
				if(entity.getFormularioPadre() == null)
					entity.setFormularioPadre(new Formulario());
				
				Metodos.auditLog(Constantes.ACTIONCREATE, FormularioMBean.class.getSimpleName(), entity.getNombreDescriptivo(), "");
				
				String msg = Metodos.getMessageResourceString("generics_guardarOK", new Object[]{entity.getNombreDescriptivo()});
				
				if(!Metodos.isMobile()){
					Metodos.setSessionMesagges(Constantes.SESSIONMSGINFO, msg);
				}
					
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
					
				return  Metodos.close(closeRedirectWeb, closeRedirectMobile, true);
			} else if (mode != null && mode.equals(Constantes.MODEEDIT)
					&& hasPermissionTo(Constantes.ACTIONUPDATE)) {
			
				entityService = new FormularioServices();
				entityService.actualizarFormularioTX(entity);
				
				if(entity.getFormularioPadre() == null)
					entity.setFormularioPadre(new Formulario());
				
				Metodos.auditLog(Constantes.ACTIONUPDATE, FormularioMBean.class.getSimpleName(), entity.getNombreDescriptivo(), "");
				
				String msg = Metodos.getMessageResourceString("generics_actualizarOK", new Object[]{entity.getNombreDescriptivo()});
				
				if(!Metodos.isMobile()){
					Metodos.setSessionMesagges(Constantes.SESSIONMSGINFO, msg);
				}
					
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
					
				return  Metodos.close(closeRedirectWeb, closeRedirectMobile, true);
				
			}
					
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}
		
		return "";
	}
	
	/**
	 * Metodo que se ejecuta cuando se cierra una modal
	 * @return
	 */
	public void dialogListener(){
		fillEntityList();
		Metodos.loadSessionMesagges();
	}
	
	
	/* SECURITY METHODS*/
	/**
	 * Método que valida si el usuario en sesión puede ejecutar una acción sobre este documento
	 * 
	 * @return
	 */
	public boolean hasPermissionTo(String action){		
		return Metodos.hasPermission(FormularioMBean.class.getSimpleName(), action);
	}
	
	/* GETTERS AND SETTERS */

	public Formulario getEntity() {
		return entity;
	}

	public void setEntity(Formulario entity) {
		this.entity = entity;
	}

	public List<Formulario> getEntityList() {
		return entityList;
	}

	public void setEntityList(List<Formulario> entityList) {
		this.entityList = entityList;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public List<Formulario> getEntityFilterList() {
		return entityFilterList;
	}

	public void setEntityFilterList(List<Formulario> entityFilterList) {
		this.entityFilterList = entityFilterList;
	}

	public List<Formulario> getFormulariosDropDownList() {
		return formulariosDropDownList;
	}

	public void setFormulariosDropDownList(List<Formulario> formulariosDropDownList) {
		this.formulariosDropDownList = formulariosDropDownList;
	}

}
