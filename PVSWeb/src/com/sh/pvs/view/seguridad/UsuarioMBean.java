/**
	 * 	UsuarioMBean.java:
	 * 
	 * 	Esta clase corresponde al Managed Bean para controlar el CRUD de 
	 * 	la entidad Usuario
	 *
	 * @author Jhoel Acosta - jhoel.acosta@e-deas.com.co - 16/03/2015
	 * @Modifier Jhoel Acosta - jhoel.acosta@e-deas.com.co - 16/03/2015
	 * @version 1.0
	 * 
	 */

package com.sh.pvs.view.seguridad;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.primefaces.context.RequestContext;

import com.sh.pvs.core.interfaces.seguridad.IPerfilServices;
import com.sh.pvs.core.interfaces.seguridad.IPerfilxUsuarioServices;
import com.sh.pvs.core.interfaces.seguridad.IUsuarioServices;
import com.sh.pvs.core.services.seguridad.PerfilServices;
import com.sh.pvs.core.services.seguridad.PerfilxUsuarioServices;
import com.sh.pvs.core.services.seguridad.UsuarioServices;
import com.sh.pvs.model.dao.configuracion.ConstanteDAO;
import com.sh.pvs.model.dto.seguridad.Perfil;
import com.sh.pvs.model.dto.seguridad.PerfilxUsuario;
import com.sh.pvs.model.dto.seguridad.Usuario;
import com.sh.pvs.view.utils.Constantes;
import com.sh.pvs.view.utils.ControlException;
import com.sh.pvs.view.utils.Metodos;


@ManagedBean
@ViewScoped
public class UsuarioMBean implements Serializable {

	/*Serializable Attribute*/
	private static final long serialVersionUID = 1L;
	
	/*Variables*/
	private Usuario entity; 							// ViewForm - EditForm 	
 	private List<Usuario> entityList; 					// Lista de registros del datatable
 	private List<Usuario> entityFilterList;				// Lista de registros filtrados del datatable
	private List<Perfil> perfilesList;
	private Perfil selectedPerfil = new Perfil();
	
 	/*Navegación*/
 	private String mode;								// view - edit ==> Modo para identificar la vista
 	
 	/*Servicios*/
 	private IUsuarioServices entityService;				// Servicios para la gestión de la entidad
 	private IPerfilServices perfilService;
 	private IPerfilxUsuarioServices perfilxUsuarioService;
 	
 	/*Variables para navegación*/
 	public static String openDialog = "/seguridad/usuario/usuarioForm.faces";
 	public static String openWebMobile = "usuarioForm.faces";
 	public static String closeRedirectMobile = "usuarioList.faces";
	public static String closeRedirectWeb = "/seguridad/usuario/usuarioList.faces?faces-redirect=true";
	
 	/*Constructors*/	
	/**	Default constructor */
	public UsuarioMBean() {
	}
	
	/*METHODS*/
	/**
	 * Método para inicializar las variables*/
	@PostConstruct
	private void init(){
		FacesContext context = FacesContext.getCurrentInstance(); 
		try {
			
			Metodos.loadSessionMesagges();
			
			if(context.getExternalContext().getRequestParameterMap().get("mode") != null)
				mode = context.getExternalContext().getRequestParameterMap().get("mode");
			else
				mode = Constantes.MODELIST;
			
			if(mode.equals(Constantes.MODECREATE)){
				this.entity = new Usuario();
				fillPerfilList();
			}
			
			if(mode.equals(Constantes.MODELIST)){
				fillEntityList();
				return;
			}
						
			if(mode.equals(Constantes.MODEVIEW) || mode.equals(Constantes.MODEEDIT)){
				
				try {
					String idParam = context.getExternalContext().getRequestParameterMap().get("id");
					entityService = new UsuarioServices();
					this.entity = entityService.findById(Integer.parseInt(idParam));
					fillPerfilList();
				} catch (Exception e) {
					context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
				}
			return;
			}
		} catch (Exception e) {
			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
		}
		
	}
	
	/**
	 * Create a new Constante entity
	 * 
	 */
	public String newEntity() {
		this.entity = new Usuario();
		return Metodos.newNavigation(openDialog, openWebMobile);
		
	}
	
	public  String newEntityCliente (){
		FacesContext context = FacesContext.getCurrentInstance(); 
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		if(!Metodos.isMobile()){
			
			//Si la navegación es en web se abren modales
			Map<String,Object> options = new HashMap<String, Object>();
	        options.put("modal", true);
	        options.put("resizable", false);
	        options.put("contentHeight", 450);
	        options.put("contentWidth", 600);
	        
	        Map<String, List<String>> params = new HashMap<String, List<String>>();
		       
	        List<String> values = new ArrayList<String>();
	        params.put("mode", values);
	        
			RequestContext.getCurrentInstance().openDialog(openDialog, options, params);
			
		}else{
			//Si la navegación es en dispositivos móviles, se redirecciona
			try {
				response.sendRedirect(openWebMobile);
			} catch (Exception e) {
				return "";
			}
			
		}
		
		return "";
	}
	
	/**
	 * Select an existing Constante entity
	 * 
	 */
	public String selectEntity(Integer idKey) {				
		
		FacesContext context = FacesContext.getCurrentInstance();
		
		try {
			
			entityService = new UsuarioServices();
			entity = entityService.findById(idKey);
			
		} catch (Exception e) {
			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
		}
		
		return Metodos.selectNavigation(idKey, openDialog, openWebMobile);
	}
	
	/**
	 * Edit an existing Constante entity
	 * 
	 */
	public String editEntity(Integer idKey, boolean fromList) {
		
		FacesContext context = FacesContext.getCurrentInstance();
		
		try {
			
			entityService = new UsuarioServices();
			entity = entityService.findById(idKey);
			
		} catch (Exception e) {
			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
		}
		
		return Metodos.editNavigation(idKey, fromList, openDialog, openWebMobile);
	}
	
	/**
	 * Delete an existing Constante entity
	 * 
	 */
	public String deleteEntity(Integer idKey, boolean fromList) {
		
		FacesContext context = FacesContext.getCurrentInstance(); 
		
		try {
			entityService = new UsuarioServices();
			entity = entityService.findById(idKey);
			entityService.deleteTX(entity);
			
			Metodos.auditLog(Constantes.ACTIONDELETE, UsuarioMBean.class.getSimpleName(), entity.getNombre(), "");
			
			String msg = Metodos.getMessageResourceString("generics_eliminarOK", new Object[]{entity.getNombre()});
			if(!fromList || (!Metodos.isMobile() && Metodos.isDlg())){
				Metodos.setSessionMesagges(Constantes.SESSIONMSGINFO, msg);
			}
				
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
				
			fillEntityList();
		} catch (Exception e) {
			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
		}
		return  Metodos.close(closeRedirectWeb, closeRedirectMobile, fromList);
		
	}
	
	/**
	 * Metodo que retorna las listas
	 * @return
	 */
	public void fillEntityList(){
		FacesContext context = FacesContext.getCurrentInstance(); 
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		try {
			entityService = new UsuarioServices();
			entityList = null;
			LoginMBean loginMBean = (LoginMBean)Metodos.getManagedBean(request, "loginMBean");
			entityList = entityService.findByProperty(ConstanteDAO.IDCOMPANIA, loginMBean.getUsuario().getCompania().getId());
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}		
	}
	
	/**
	 * Metodo que retorna la lista de perfiles activos
	 * @return
	 */
	private void fillPerfilList(){
		FacesContext context = FacesContext.getCurrentInstance(); 
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest(); 
		perfilService = new PerfilServices();
		perfilesList = null;
		try {
			LoginMBean loginMBean = (LoginMBean)Metodos.getManagedBean(request, "loginMBean");
			perfilesList = perfilService.PerfilesActivosByCompania(loginMBean.getUsuario().getCompania().getId());
		} catch (Exception e) {
			try {
				LoginMBean loginMBean = (LoginMBean)Metodos.getManagedBean(request,"loginMBean");
				perfilesList = perfilService.PerfilesActivosByCompania(loginMBean.getUsuario().getCompania().getId());
			} catch (Exception ex) {
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(ex), ""));
			}
		}		
	}
	
	/**
	 * Metodo para guardar
	 */
	public String saveEntity(){
		FacesContext context = FacesContext.getCurrentInstance(); 
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		try {	
			if(mode != null && (mode.equals(Constantes.MODECREATE))
				&& hasPermissionTo(Constantes.ACTIONCREATE)){				
				
				LoginMBean loginMBean = (LoginMBean)Metodos.getManagedBean(request,"loginMBean");
				entity.setCompania(loginMBean.getUsuario().getCompania());
				
				entityService = new UsuarioServices();
				entityService.crearUsuarioTX(entity);
				
				Metodos.auditLog(Constantes.ACTIONCREATE, UsuarioMBean.class.getSimpleName(), entity.getNombre(), "");
				
				String msg = Metodos.getMessageResourceString("generics_guardarOK", new Object[]{entity.getNombre()});
				
				if(!Metodos.isMobile()){
					Metodos.setSessionMesagges(Constantes.SESSIONMSGINFO, msg);
				}
					
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
					
				return  Metodos.close(closeRedirectWeb, closeRedirectMobile, true);
			}		
			else if(mode != null && mode.equals(Constantes.MODEEDIT)
				&& hasPermissionTo(Constantes.ACTIONUPDATE)){
				
				entityService = new UsuarioServices();
				entityService.updateTX(entity);
				
				Metodos.auditLog(Constantes.ACTIONUPDATE, UsuarioMBean.class.getSimpleName(), entity.getNombre(), "");
				
				String msg = Metodos.getMessageResourceString("generics_actualizarOK", new Object[]{entity.getNombre()});
				
				if(!Metodos.isMobile()){
					Metodos.setSessionMesagges(Constantes.SESSIONMSGINFO, msg);
				}
					
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
					
				return  Metodos.close(closeRedirectWeb, closeRedirectMobile, true);
			}
					
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}
		
		return "";
	}
	
	/**
	 * Metodo para guardar
	 */
	public String savePerfil(){
		FacesContext context = FacesContext.getCurrentInstance(); 
		try {	
			if(hasPermissionTo(Constantes.ACTIONCREATE) || hasPermissionTo(Constantes.ACTIONUPDATE)){				
				
				PerfilxUsuario perfilxusuario = new PerfilxUsuario();
				perfilxusuario.setPerfil(selectedPerfil);
				perfilxusuario.setUsuario(entity);
				
				perfilxUsuarioService = new PerfilxUsuarioServices();
				perfilxUsuarioService.createTX(perfilxusuario);
				
				Metodos.auditLog(Constantes.ACTIONUPDATE, UsuarioMBean.class.getSimpleName(), entity.getNombre(), "Perfil asociado");
				
				String msg = Metodos.getMessageResourceString("generics_actualizarOK", new Object[]{entity.getNombre()});
				
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
				
				entityService = new UsuarioServices();
				entity = entityService.findById(entity.getId());
				
				return "";
			}		
					
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}
		
		return "";
	}
	
	/**
	 * Metodo para guardar
	 */
	public String deletePerfil(Integer perfilxusuario_id){
		FacesContext context = FacesContext.getCurrentInstance(); 
		try {	
			if(hasPermissionTo(Constantes.ACTIONCREATE) || hasPermissionTo(Constantes.ACTIONUPDATE)){				
				
				perfilxUsuarioService = new PerfilxUsuarioServices();
				PerfilxUsuario perfilxusuario = perfilxUsuarioService.findById(perfilxusuario_id);
				perfilxUsuarioService.deleteTX(perfilxusuario);
				
				Metodos.auditLog(Constantes.ACTIONUPDATE, UsuarioMBean.class.getSimpleName(), entity.getNombre(), "Perfil eliminado");
				
				String msg = Metodos.getMessageResourceString("generics_eliminarOK", new Object[]{entity.getNombre()});
				
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
				
				entityService = new UsuarioServices();
				entity = entityService.findById(entity.getId());
				
				return "";
			}		
					
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}
		
		return "";
	}
	
	/**
	 * Metodo que se ejecuta cuando se cierra una modal
	 * @return
	 */
	public void dialogListener(){
		fillEntityList();
		Metodos.loadSessionMesagges();
	}
	
	/**
     * Recalcula y reenvía la contreaseña del usuario
     * 
     * @throws Exception Excepción para mejorar presentación de excepciones no controladas
     */
	public String restituirPw(){
		FacesContext context = FacesContext.getCurrentInstance(); 
		try {			
			if(hasPermissionTo(Constantes.ACTIONRESTITUIRPASSWORD)){		
				entityService.restituriPw(entity);
				
				Metodos.auditLog(Constantes.ACTIONRESTITUIRPASSWORD, UsuarioMBean.class.getSimpleName(), entity.getNombre(), "");
				
				String msg = Metodos.getMessageResourceString("usuario_clave_restituida", new Object[]{entity.getNombre()});
				Metodos.setSessionMesagges(Constantes.SESSIONMSGINFO, msg);
					
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, "")); // MENSAJE
				
			}
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, ControlException.controlException(e), ""));
		}
		return Metodos.close(closeRedirectWeb, closeRedirectMobile, false);
	}
	
	/* SECURITY METHODS*/
	/**
	 * Método que valida si el usuario en sesión puede ejecutar una acción sobre este documento
	 * 
	 * @return
	 */
	public boolean hasPermissionTo(String action){		
		return Metodos.hasPermission(UsuarioMBean.class.getSimpleName(), action);
	}
	
	/* GETTERS AND SETTERS */

	public Usuario getEntity() {
		return entity;
	}

	public void setEntity(Usuario entity) {
		this.entity = entity;
	}

	public List<Usuario> getEntityList() {
		return entityList;
	}

	public void setEntityList(List<Usuario> entityList) {
		this.entityList = entityList;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public List<Usuario> getEntityFilterList() {
		return entityFilterList;
	}

	public void setEntityFilterList(List<Usuario> entityFilterList) {
		this.entityFilterList = entityFilterList;
	}

	public List<Perfil> getPerfilesList() {
		return perfilesList;
	}

	public void setPerfilesList(List<Perfil> perfilesList) {
		this.perfilesList = perfilesList;
	}

	
	public Perfil getSelectedPerfil() {
		return selectedPerfil;
	}

	public void setSelectedPerfil(Perfil selectedPerfil) {
		this.selectedPerfil = selectedPerfil;
	}
	
}