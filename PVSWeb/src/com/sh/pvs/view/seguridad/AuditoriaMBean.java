/**
 * 	AuditoriaMBean.java:
 * 
 * 	Esta clase corresponde al Managed Bean para controlar la consulta
 * 	de auditoria
 *
 * @author Jhoel Acosta - jhoel.acosta@e-deas.com.co - 17/03/2015
 * @Modifier Jhoel Acosta - jhoel.acosta@e-deas.com.co - 17/03/2015
 * @version 1.0
 * 
 */

package com.sh.pvs.view.seguridad;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.primefaces.model.LazyDataModel;

import com.sh.pvs.core.interfaces.seguridad.IAuditoriaServices;
import com.sh.pvs.core.services.seguridad.AuditoriaServices;
import com.sh.pvs.model.dto.seguridad.Auditoria;
import com.sh.pvs.view.utils.Constantes;
import com.sh.pvs.view.utils.ControlException;
import com.sh.pvs.view.utils.Metodos;


@ManagedBean
@ViewScoped
public class AuditoriaMBean implements Serializable {

	/*Serializable Attribute*/
	private static final long serialVersionUID = 1L;
	
	/*Variables*/
	private Auditoria entity; 								// ViewForm - EditForm 	
 	private LazyDataModel<Auditoria> entityList; 			// Lista de registros del datatable
 	private List<Auditoria> entityFilterList;				// Lista de registros filtrados del datatable
	private Date fechaIni;
	private Date fechaFin;
	private List<Auditoria> lista;							//Lista de registros del datatable provicional al LAZY
 	
 	/*Navegación*/
 	private String mode;									// view - edit ==> Modo para identificar la vista
 	
 	/*Servicios*/
 	private IAuditoriaServices entityService;				// Servicios para la gestión de la entidad
 	
 	/*Variables para navegación*/
 	public static String openDialog = "/seguridad/auditoria/auditoriaForm.faces";
 	public static String openWebMobile = "auditoriaForm.faces";
 	public static String closeRedirectMobile = "auditoriaList.faces";
	public static String closeRedirectWeb = "/seguridad/auditoria/auditoriaList.faces?faces-redirect=true";
	
	
/*
 *   RequestContext.getCurrentInstance().openDialog("/seguridad/auditoria/auditoriaForm.faces", options, params);
		        
			}else{
				//Si la navegación es en dispoisitivos móviles, se redirecciona
				response.sendRedirect("auditoriaForm.faces?mode="+mode+"&dlg=false"+idParam);
 */
 	
 	/*Constructors*/	
	/**	Default constructor */
	public AuditoriaMBean() {
	}
	
	/*METHODS*/
	/**
	 * Método para inicializar las variables*/
	@PostConstruct
	private void init(){
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			Metodos.loadSessionMesagges();
			
			if(context.getExternalContext().getRequestParameterMap().get("mode") != null)
				mode = context.getExternalContext().getRequestParameterMap().get("mode");
			else
				mode = Constantes.MODELIST;
			
			
			if(mode.equals(Constantes.MODELIST)){
				entity = new Auditoria();
				fechaFin = new Date();
				return;
			}
						
			if(mode.equals(Constantes.MODEVIEW)){
				try{
					String idParam = context.getExternalContext().getRequestParameterMap().get("id");
					entityService = new AuditoriaServices();
					this.entity = entityService.findById(Integer.parseInt(idParam));
				} catch (Exception e) {
					context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
				}
				return;
			}
		} catch (Exception e) {
			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
		}
		
	}
	
	public void filtrar(){
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			if (hasPermissionTo(Constantes.ACTIONREAD)) {
				HttpServletRequest request = (HttpServletRequest)context.getExternalContext().getRequest();
				LoginMBean loginMBean = (LoginMBean)Metodos.getManagedBean(request, "loginMBean");
				entityService = new AuditoriaServices();
				entityList = entityService.findAllLazyAuditoriaByFilters(fechaIni, fechaFin, entity, loginMBean.getUsuario().getCompania().getId());
				lista = entityService.findAllAuditoriaByFilters(fechaIni, fechaFin,	entity, loginMBean.getUsuario().getCompania().getId());
			}
		} catch (Exception e) {
			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL,ControlException.controlException(e), ""));
		}
	}
	
	/**
	 * Select an existing MensajeNotificacion entity
	 * 
	 */
	public String selectEntity(Integer idKey) {				
		return Metodos.selectNavigation(idKey, openDialog, openWebMobile);
	}
	
	/* SECURITY METHODS*/
	/**
	 * Método que valida si el usuario en sesión puede ejecutar una acción sobre este documento
	 * 
	 * @return
	 */
	public boolean hasPermissionTo(String action){		
		return Metodos.hasPermission(AuditoriaMBean.class.getSimpleName(), action);
	}
	
	
	/* GETTERS AND SETTERS */

	public Auditoria getEntity() {
		return entity;
	}

	public void setEntity(Auditoria entity) {
		this.entity = entity;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public List<Auditoria> getEntityFilterList() {
		return entityFilterList;
	}
	
	public void setEntityFilterList(List<Auditoria> entityFilterList) {
		this.entityFilterList = entityFilterList;
	}

	public LazyDataModel<Auditoria> getEntityList() {
		return entityList;
	}

	public void setEntityList(LazyDataModel<Auditoria> entityList) {
		this.entityList = entityList;
	}

	public List<Auditoria> getLista() {
		return lista;
	}

	public void setLista(List<Auditoria> lista) {
		this.lista = lista;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public Date getFechaIni() {
		return fechaIni;
	}

	public void setFechaIni(Date fechaIni) {
		this.fechaIni = fechaIni;
	}

}
	

